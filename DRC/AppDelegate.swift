//
//  AppDelegate.swift
//  DRC
//
//  Created by Edward Reilly on 16/09/2019.
//  Copyright © 2019 Norcott. All rights reserved.
//

import UIKit
import Combine
import UserNotifications
import WindowsAzureMessaging


@UIApplicationMain





class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate, UNUserNotificationCenterDelegate,
                   MSNotificationHubDelegate, MSInstallationLifecycleDelegate {
    
    // MARK: - Declarations
    
    static let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .full
        dateFormatter.timeStyle = .full
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = Locale(identifier: "en-GB")
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        return dateFormatter
    }()
    
    static let dateFormatterNoTime: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .full
        dateFormatter.timeStyle = .none
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = Locale(identifier: "en-GB")
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        
        return dateFormatter
    }()
    
    
    public enum ScanMode: Int {
        case NoScan = 0
        case Process = 1
        case Inspection = 2
        case Batch = 3
        case Confirmation = 4
        case Sample = 5
        case Panel = 6
    }

    var window: UIWindow?
    
    public static var secondWindow: UIWindow?
    public static var extViewController: ExternalViewController?
    
   


    static var NorcottBlue = UIColor(red:87/255.0, green:143/255.0, blue:207/255.0, alpha:1.0)
    static var NorcottLightBlue =  UIColor(red:127/255.0, green:183/255.0, blue:247/255.0, alpha:1.0)
    static var NorcottPaleBlue = UIColor(red:225/255.0, green:225/255.0, blue:225/255.0, alpha:1.0)
    static var NorcottOrange = UIColor(red:255/255.0, green:139/255.0, blue:20/255.0, alpha:1.0)
    static var NorcottLightOrange = UIColor(red:255/255.0, green:204/255.0, blue:102/255.0, alpha:1.0)
    static var NorcottLightYellow = UIColor(red:243/255.0, green:245/255.0, blue:219/255.0, alpha:1.0)
    
    
    
    struct UserData  {
        @UserDefault(key: "serverIP", defaultValue: "192.168.33.9")
        static var serverIP: String
        
        @UserDefault(key: "portNumber", defaultValue: "6002")
        static var portNumber: String
        
        @UserDefault(key: "userInitials", defaultValue: "")
        static var userInitials: String
        
        @UserDefault(key: "userID", defaultValue: 0)
        static var userID: NSInteger
        
        @UserDefault(key: "userName", defaultValue: "")
        static var userName: String

        @UserDefault(key: "userRights", defaultValue: 0)
        static var userRights: NSInteger
        
        @UserDefault(key: "password", defaultValue: "")
        static var password: String
        
        @UserDefault(key: "processName", defaultValue: "SMT")
        static var processName: String
        
        @UserDefault(key: "stationID", defaultValue: "iPAD-Not-Set")
        static var stationID: String
        
        @UserDefault(key: "deviceIndex", defaultValue: 0)
        static var deviceIndex: NSInteger
        
        @UserDefault(key: "deviceName", defaultValue: "")
        static var deviceName: String
        
        @UserDefault(key: "deviceUUID", defaultValue: "")
        static var deviceUUID: String
        
        @UserDefault(key: "deviceToken", defaultValue: "")
        static var deviceToken: String
        
        @UserDefault(key: "lastLogin", defaultValue: "")
        static var lastLogin: String
        
        @UserDefault(key: "elevatedMode", defaultValue: false)
        static var elevatedMode: Bool
        
        @UserDefault(key: "batchQAMode", defaultValue: false)
        static var batchQAMode: Bool
        
        @UserDefault(key: "apnsServerIP", defaultValue: "https://norcottnotificationapi.azurewebsites.net/")
        static var apnsServerIP: String

    
    }
    
    struct AppData {
        // Stores current WOCModel
        @AppState(key: "currentWOC",
                  defaultValue:
                    WOCModel(
                        PinNumber: "", GPinIndex: 0, JobNumber: 0, JobIndex: 0, BatchNumber: 0, BatchIndex: 0, RouteCardHdrIndex: 0, ClientCode: "", ClientPart: "", FirstSerial: "", BuildDate: Date(), QMPCreated: Date(), QMPBy: "", Engineer: "", Quantity: 0, BatchComment: "", JobDescription: "", GDescription: "", ClientOrder: "", PMR: "", Alert: false, AlertComment: "", AOI: false, GNCS: 0, ActiveTLP: 0, ActiveSLP: 0, AlertSLP: false, Class: 0, Step: 0, ERCInstructions: "", StockAllocated: true, Finish: "", Visible: false, QAAlerts: 0)
                )
        static var currentWOC: WOCModel
        
        static var legacyMappings = [LegacyModel]()
        
        // Stores current TLP
        @AppState(key: "TLPStepID", defaultValue: -1)
        static var TLPStepID: Int
        
        // Stores current SLP
        @AppState(key: "SLPStepID", defaultValue: -1)
        static var SLPStepID: Int
        
        // Stores current TLP at Instruction Level
        @AppState(key: "TLPIndexPath", defaultValue: [])
        static var TLPIndexPath: IndexPath
        
        // Stores current SLP at Instruction Level
        @AppState(key: "SLPIndexPath", defaultValue: [])
        static var SLPIndexPath: IndexPath
        
        // Stores File Path to PDF Drawings
        // @AppState(key: "drawingPath", defaultValue: "\\\\norserver04\\Production\\")
        // static var drawingPath: String
        
        
    }

    // MARK: - Notification Handling
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if (response.notification.request.identifier == "ercIdentifier") {
           print("Handled Notification in AppDelegate \(response.notification.request.identifier)")
        }
        if let notification = response.notification.request.content.userInfo as? [String: AnyObject] {
            let message = parseRemoteNotification(notification: notification)
            print(message as Any)
        }
        
        completionHandler()
    }
    

    private func parseRemoteNotification(notification:[String: AnyObject]) -> String? {
        if let aps = notification["aps"] as? [String: AnyObject] {
            let alert = aps["alert"] as? String
            return alert
        }
        return nil
    }
        
    // The token has to be registered with API after creating new certs !!
    // Current = 9d1286455bbb865ce2ebe1bbd2dd819a66fa69a1bdc37739a8c1840bd69fa322
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        UserData.deviceToken = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("Token: \(UserData.deviceToken)")
    }

     func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Error: \(error.localizedDescription)")
    }
    
    
    // MARK: - MS Azure Notification Hub Registration
    
    func notificationHub(_ notificationHub: MSNotificationHub, didRequestAuthorization granted: Bool, error: Error?) {
        //
        print("Hub Authorised: \(granted)" .description)
    }
    

    
    func notificationHub(_ notificationHub: MSNotificationHub, didReceivePushNotification message: MSNotificationHubMessage) {
        
        let userInfo = ["message": message]
        NotificationCenter.default.post(name: NSNotification.Name("MessageReceived"), object: nil, userInfo: userInfo)
        
        // NotificationCenter.default.post(name: Notification.Name("refreshSubProcess"), object: self.processItem)
        
        if (UIApplication.shared.applicationState == .background) {
            NSLog("Notification received in the background")
        }

    }
    
    
    // MARK: - Life Cycle
    
    func printCurrentSwiftVersion() {
    #if swift(>=5.9)
    print("Current Swift Version: Swift 5.9")
    
    #endif
    }

    func notificationHub(_ notificationHub: MSNotificationHub!, didSave installation: MSInstallation!) {
        let installationID = installation.installationId
        print("Installation ID: \(String(describing: installationID))")
    }

    func notificationHub(_ notificationHub: MSNotificationHub!, didFailToSave installation: MSInstallation!, withError error: Error!) {
        print("Failed to save installation")
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        application.isIdleTimerDisabled = true
        
        UNUserNotificationCenter.current().delegate = self
        
        print(UIDevice.current.model)
        print(UIDevice.current.name)
        print(UIDevice.current.identifierForVendor?.uuidString as Any)
        
   
        UserData.deviceName = UIDevice.current.name
        print("Device Name=  \(UIDevice.current.name .description)")
        UserData.deviceUUID = UIDevice.current.identifierForVendor!.uuidString
        
        
        MSNotificationHub.setDelegate(self)
        MSNotificationHub.setLifecycleDelegate(self)
        
        let hubOptions = MSNotificationHubOptions(withOptions: [.alert, .badge, .sound])

        
        MSNotificationHub.start(connectionString: "Endpoint=sb://norcotthubns.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=TZdGTzwmntuXtB6Pjg4h0SIhdQ11YSrG/qQgg5b9zy8=", hubName: "norcottHub", options: hubOptions!)
        
        MSNotificationHub.addTag(AppDelegate.UserData.userName)
        
        MSNotificationHub.setUserId("\(AppDelegate.UserData.userID)")
        
        print("Tag: \(AppDelegate.UserData.userName)")
        print("UserID: \(AppDelegate.UserData.userID)")
        
        
        #if compiler(<5)
        if(UIApplication.instancesRespond(to: #selector(UIApplication.registerUserNotificationSettings(_:))))
        {
            application.registerUserNotificationSettings(UIUserNotificationSettings(types: UIUserNotificationType(rawValue: UIUserNotificationType.sound.rawValue | UIUserNotificationType.alert.rawValue | UIUserNotificationType.badge.rawValue), categories: nil))
        }
        #else
        // Swift 5+
        UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
             if granted {
                DispatchQueue.main.async(execute:  {
                   UIApplication.shared.registerForRemoteNotifications()
                })
             }
         }
        #endif
        
        self.printCurrentSwiftVersion()
        
        let notificationOption = launchOptions?[.remoteNotification]
        if let notification = notificationOption as? [String: AnyObject],
           let aps = notification["aps"] as? [String: AnyObject] {
            print(aps)
        }

        
        Thread.sleep(forTimeInterval: 2);
        AppDelegate.AppData.TLPIndexPath = []
        AppDelegate.AppData.SLPIndexPath = []
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let lb = storyboard.instantiateViewController(withIdentifier: "loginBackground") as! LoginBackgroundView
        self.window?.rootViewController = lb;
    
        return true
    }
    
 
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // MARK: - Split view

    func splitViewController(_ splitViewController: UISplitViewController,
        collapseSecondary secondaryViewController:UIViewController, onto primaryViewController:UIViewController) -> Bool {
        guard let secondaryAsNavController = secondaryViewController as? UINavigationController else { return false }
        guard let topAsDetailController = secondaryAsNavController.topViewController as? SetupViewController else { return false }
        if topAsDetailController.detailItem == nil {
            // Return true to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
            return true
        }
        return false
    }

}

