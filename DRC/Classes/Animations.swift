//
//  Animations.swift
//  DRC
//
//  Created by Edward Reilly on 12/12/2019.
//  Copyright © 2019 Norcott. All rights reserved.
//

import UIKit
import Combine
import Foundation

class Animations : NSObject, CAAnimationDelegate {
    
    private var pulseArray = [CAShapeLayer]()

    func animationDidStart(_ anim: CAAnimation) {

        print("Animation started")
    }
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        print("Animation removed")
    }
    
    // Adds pulsation effect to any layer
    func addPulsation (object: CALayer) {
        
        let scaleAnimation:CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.duration = 1.0
        scaleAnimation.repeatCount = .greatestFiniteMagnitude
        scaleAnimation.autoreverses = true
        scaleAnimation.fromValue = 1.1;
        scaleAnimation.toValue = 0.90;
        scaleAnimation.isRemovedOnCompletion = false
        object.add(scaleAnimation, forKey: "scale")
    }
    
    // Adds pulsation effect to any layer
    func addPulsations (object: CALayer, count: Float) {
        let scaleAnimation:CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.duration = 1.0
        scaleAnimation.repeatCount = count
        scaleAnimation.autoreverses = false
        scaleAnimation.fromValue = 0.80;
        scaleAnimation.toValue = 1.0;
        scaleAnimation.isRemovedOnCompletion = false
        object.add(scaleAnimation, forKey: "scale")
    }
    
    private func animatePulsatingLayerAt(index:Int, color: UIColor) {
        
        //Giving color to the layer
        pulseArray[index].strokeColor = color.cgColor // UIColor.orange.cgColor
        
        //Creating scale animation for the layer, from and to value should be in range of 0.0 to 1.0
        // 0.0 = minimum
        //1.0 = maximum
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.fromValue = 0.0
        scaleAnimation.toValue = 1.6
        scaleAnimation.isRemovedOnCompletion = false
        
        //Creating opacity animation for the layer, from and to value should be in range of 0.0 to 1.0
        // 0.0 = minimum
        //1.0 = maximum
        let opacityAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        opacityAnimation.fromValue = 0.9
        opacityAnimation.toValue = 0.0
        opacityAnimation.isRemovedOnCompletion = false
       
        // Grouping both animations and giving animation duration, animation repat count
        let groupAnimation = CAAnimationGroup()
        groupAnimation.delegate = self
        groupAnimation.animations = [scaleAnimation, opacityAnimation]
        groupAnimation.duration = 2.3
        groupAnimation.repeatCount = .greatestFiniteMagnitude
        groupAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        groupAnimation.isRemovedOnCompletion = false
        //adding groupanimation to the layer
        pulseArray[index].add(groupAnimation, forKey: "groupanimation")
        
    }
    
    func createPulse(button: UIButton, color: UIColor) {
        
        for _ in 0...2 {
            let circularPath = UIBezierPath(arcCenter: .zero, radius: ((button.frame.size.width ) )/2, startAngle: 0, endAngle: 2 * .pi , clockwise: true)
            let pulsatingLayer = CAShapeLayer()
            pulsatingLayer.path = circularPath.cgPath
            pulsatingLayer.lineWidth = 2.5
            pulsatingLayer.fillColor = UIColor.clear.cgColor
            pulsatingLayer.lineCap = CAShapeLayerLineCap.round
            pulsatingLayer.setValue("shape", forKey: "circle")
            pulsatingLayer.position = CGPoint(x: button.frame.size.width / 2.0, y: button.frame.size.width / 2.0)
            button.layer.addSublayer(pulsatingLayer)
            
            pulseArray.append(pulsatingLayer)
        }
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.animatePulsatingLayerAt(index: 0, color: color)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                self.animatePulsatingLayerAt(index: 1, color: color)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.6, execute: {
                   self.animatePulsatingLayerAt(index: 2, color: color)
                })
            })
        })
        
    }
    
    
    
    func fadeInAnimation(object: CALayer) {
          // Creating opacity animation for the layer, from and to value should be in range of 0.0 to 1.0
          let opacityAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
          opacityAnimation.fromValue = 0.0
          opacityAnimation.toValue = 0.9
          opacityAnimation.duration = 2.0
          opacityAnimation.repeatCount = 1
          opacityAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
          opacityAnimation.isRemovedOnCompletion = false
          object.add(opacityAnimation, forKey: "opacity")
    }

    
}
