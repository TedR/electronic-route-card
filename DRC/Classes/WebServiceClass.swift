//
//  WebServiceClass.swift
//  DRC
//
//  Created by Edward Reilly on 31/10/2019.
//  Copyright © 2019 Norcott. All rights reserved.
//

import Foundation
import UIKit



// Generic HTTP GET web service call
// Pass service name and params
// Returns data as Data
/*
public func httpGETWebService(serviceName: String, params: String,
                              completionHandler: @escaping (Result<Data, Error>) -> Void) {
    guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/" + serviceName + "/" + params) else {
                print("Invalid URL!")
            return
        }
    let request = URLRequest(url: url)
    
    // Asynchronously Api is hit here
    let task = URLSession.shared.dataTask(with: request) { data, _, error in
         guard let data = data else {
             completionHandler(.failure(error!))
             return
         }
         completionHandler(.success(data))
     }
     task.resume()
}
*/
/*
httpGETWebService(serviceName:"GetPanelLayout", params: "1870", completionHandler: { result in
    switch result {
        case .failure(let error):
            print(error)
        case .success(let data):
            print(data)
    }
})
 */



// Generic HTTP GET web service call using delegates
// Pass service name and params
// Returns data as JSON decoded Model

protocol WebRequestDelegate: AnyObject {
    func onError(error: String)
}

public struct WebRequest<Model: Codable> {
    
    public typealias SuccessCompletionHandler = (_ response: [Model]) -> Void
    public typealias FailureCompletionHandler = (_ response: Error?) -> Void
    
    static func get(_ delegate: WebRequestDelegate?,
                    service: String,
                    params: [String],
                    success successCallback: @escaping SuccessCompletionHandler
                    
        ) {
        
        
        let urlStr = String(stringLiteral: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/")
        var parameters = String()
        for param in params {
            parameters.append(param + "/")
        }
        
        // if not valid call the delegate to manage the error
        guard let urlComponent = URLComponents(string: urlStr  + service + "/" + parameters), let usableUrl = urlComponent.url else {
            delegate?.onError(error: "URL Error")
            return
        }
        
        var request = URLRequest(url: usableUrl)
        request.timeoutInterval = 30
        request.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        request.httpMethod = "GET"
        
        var dataTask: URLSessionDataTask?
        let defaultSession = URLSession(configuration: .default)
        
        dataTask =
            defaultSession.dataTask(with: request) { data, response, error in
                defer {
                    dataTask = nil
                    print("WebRequest Completed")
                }
                if error != nil {
                   delegate?.onError(error: "Web RequestError")
                } else if
                    let data = data,
                    let response = response as? HTTPURLResponse,
                    response.statusCode == 200 {
                    guard let model = self.parsedModel(with: data) else {
                        delegate?.onError(error: "Model Decode Error")
                        return
                    }
                    successCallback(model)
                }
        }
        dataTask?.resume()
        
    }
    
    static func parsedModel(with data: Data) -> [Model]? {
           let decoder = JSONDecoder()
           decoder.dateDecodingStrategy = .formatted(AppDelegate.dateFormatter)
           if let model = try? decoder.decode([Model].self, from: data) {
                return model
           } else {
                return nil
            }
        
    }



}
