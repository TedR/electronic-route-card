//
//  BomPinViewCell.swift
//  DRC
//
//  Created by TechFusion on 03/11/2023.
//  Copyright © 2023 Norcott. All rights reserved.
//

import UIKit

class BomPinViewCell: UITableViewCell {

    @IBOutlet var gpinLbl: UILabel!
    @IBOutlet var alertLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
