//
//  BomViewCell.swift
//  DRC
//
//  Created by TechFusion on 07/11/2023.
//  Copyright © 2023 Norcott. All rights reserved.
//

import UIKit

class BomViewCell: UITableViewCell {

    @IBOutlet var refDesTextView: UITextView!
    @IBOutlet var fittedPinLbl: UILabel!
    @IBOutlet var originalPinLbl: UILabel!
    @IBOutlet var clientPinLbl: UILabel!
    @IBOutlet var measuredQtyLbl: UILabel!
    @IBOutlet var splitQtyLbl: UILabel!
    @IBOutlet var descriptionLbl: UILabel!
    @IBOutlet var packageTypeLbl: UILabel!
    @IBOutlet var manufacturerLbl: UILabel!
    @IBOutlet var manuPartLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
