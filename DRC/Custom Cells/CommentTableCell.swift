//
//  CommentTableCell.swift
//  DRC
//
//  Created by Edward Reilly on 13/02/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

class CommentTableCell: UITableViewCell {
    
    @IBOutlet var indexLbl: UILabel!
    @IBOutlet var processLbl: UILabel!
    @IBOutlet var initialsLbl: UILabel!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var dateCreatedLbl: UILabel!
    @IBOutlet var commentsTextView: UITextView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
