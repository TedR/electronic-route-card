//
//  ConformalTableCell.swift
//  DRC
//
//  Created by Edward Reilly on 09/06/2021.
//  Copyright © 2021 Norcott. All rights reserved.
//

import UIKit

class ConformalTableCell: UITableViewCell {
    
    @IBOutlet weak var opName: UILabel!
    @IBOutlet weak var serialNumber: UILabel!
    @IBOutlet weak var dateAdded: UILabel!
    @IBOutlet weak var thickness: UILabel!
    @IBOutlet weak var authBy: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
