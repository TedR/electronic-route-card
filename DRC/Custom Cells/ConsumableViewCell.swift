//
//  ConsumableViewCell.swift
//  DRC
//
//  Created by Edward Reilly on 21/04/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

class ConsumableViewCell: UITableViewCell {
    
    @IBOutlet weak var pinNumberLbl: UILabel!
    @IBOutlet weak var traceCodeLbl: UILabel!
    @IBOutlet weak var expiryDateLbl: UILabel!
    @IBOutlet weak var commentsLbl: UILabel!
    @IBOutlet weak var departmentLbl: UILabel!
    @IBOutlet weak var eventDateLbl: UILabel!
    @IBOutlet weak var signatureLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
