//
//  DrawingViewCell.swift
//  DRC
//
//  Created by Edward Reilly on 09/04/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

class DrawingViewCell: UITableViewCell {
    
    @IBOutlet var docTypeLbl: UILabel!
    @IBOutlet var originLbl: UILabel!
    @IBOutlet var drgNumberLbl: UILabel!
    @IBOutlet var drgDateLbl: UILabel!
    @IBOutlet var qtyLbl: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
