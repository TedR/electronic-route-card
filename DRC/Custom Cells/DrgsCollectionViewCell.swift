//
//  DrgsCollectionViewCell.swift
//  DRC
//
//  Created by Edward Reilly on 06/05/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit
import PDFKit

class DrgsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet weak var castBtn: UIButton!
    @IBOutlet weak var detailsBtn: UIButton!
    
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.minimumScaleFactor = 0.5

    }
    
     override var isSelected: Bool {
        didSet{
            if self.isSelected {
                self.backgroundColor = .systemPink
                self.layer.borderWidth = 2
            }
            else {
                self.backgroundColor  = .systemIndigo
                self.layer.borderWidth = 0
            }
        }
    }
    

}
