//
//  GNCViewCell.swift
//  DRC
//
//  Created by Edward Reilly on 05/03/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

class GNCViewCell: UITableViewCell {
    
    @IBOutlet var pinLbl: UILabel!
    @IBOutlet var descTextView: UITextView!
    @IBOutlet var gncNumberLbl: UILabel!
    @IBOutlet var dateRaisedLbl: UILabel!
    @IBOutlet var statusImg: UIImageView!
    @IBOutlet var acceptedImg: UIImageView!
    
    @IBOutlet var capaTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
