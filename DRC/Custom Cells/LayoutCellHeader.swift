//
//  LayoutTableCell.swift
//  DRC
//
//  Created by Edward Reilly on 01/05/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

class LayoutCellHeader: UITableViewCell {

    @IBOutlet weak var SectionTitle: UILabel!
    @IBOutlet weak var DataField1: UILabel!
    @IBOutlet weak var ValueField1: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
