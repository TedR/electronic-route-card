//
//  RequestTableCell.swift
//  DRC
//
//  Created by TechFusion on 28/08/2023.
//  Copyright © 2023 Norcott. All rights reserved.
//

import UIKit

class QAAlertProcessCell: UITableViewCell {
    
    @IBOutlet var processStepLbl: UILabel!
    @IBOutlet var processStepValue: UITextView!
    @IBOutlet var raisedByLbl: UILabel!
    @IBOutlet var raisedByValue: UILabel!
    @IBOutlet var dateRaisedLbl: UILabel!
    @IBOutlet var dateRaisedValue: UILabel!
    @IBOutlet var alertDetailValue: UITextView!
    @IBOutlet var confirmButton: PassableUIButton!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 2
        self.layer.borderWidth = 1
        self.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
