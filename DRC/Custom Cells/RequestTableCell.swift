//
//  RequestTableCell.swift
//  DRC
//
//  Created by Edward Reilly on 07/05/2021.
//  Copyright © 2021 Norcott. All rights reserved.
//

import UIKit

class RequestTableCell: UITableViewCell {
    
    @IBOutlet var indexLbl: UILabel!
    @IBOutlet var batchLbl: UILabel!
    @IBOutlet var pinLbl: UILabel!
    @IBOutlet var operatorLbl: UILabel!
    @IBOutlet var processLbl: UILabel!
    @IBOutlet var serialLbl: UILabel!
    @IBOutlet var instructionLbl: UITextView!
    @IBOutlet var authorisedByLbl: UILabel!
    @IBOutlet var dateAuthorisedLbl: UILabel!
    @IBOutlet var commentsTextView: UITextView!
    @IBOutlet var statusImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 0
        self.layer.borderWidth = 1
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
