//
//  SLPTableCell.swift
//  DRC
//
//  Created by Edward Reilly on 09/01/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

class SLPTableCell: UITableViewCell {

    @IBOutlet var ProcessNameLbl: UILabel!
    @IBOutlet var ProcessStepLbl: UILabel!
    @IBOutlet var TTS1Lbl: UILabel!
    @IBOutlet var TTS2Lbl: UILabel!
    @IBOutlet var TT1SIG: UILabel!
    @IBOutlet var TT2SIG: UILabel!
    @IBOutlet var ShowDrawings: UIButton!
    @IBOutlet var WorkInstsView: UITextView!
    @IBOutlet var ProcModeImg: UIButton!
    @IBOutlet var ProcModeLbl: UILabel!
    @IBOutlet var viewCommentsImg: UIImageView!
    @IBOutlet var reminderImg: UIImageView!
    @IBOutlet var messageImg: UIImageView!
    @IBOutlet var drgImg: UIImageView!
    @IBOutlet var procStateImg: UIImageView!
    @IBOutlet var progressBar: UIProgressView!
    @IBOutlet var tts1StatusImg: UIImageView!
    @IBOutlet var tts2StatusImg: UIImageView!
    @IBOutlet var auth1Img: UIImageView!
    @IBOutlet var auth2Img: UIImageView!
    @IBOutlet var buttonsView: UIStackView!
    
    @IBOutlet var expectedLbl: PaddingLabel!
    @IBOutlet var actualLbl: PaddingLabel!
    
    @IBOutlet weak var worksInstLbl: PaddingLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state

    }

    
}
