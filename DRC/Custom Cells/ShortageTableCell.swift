//
//  ShortageTableCell.swift
//  DRC
//
//  Created by Edward Reilly on 17/03/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

class ShortageTableCell: UITableViewCell {
    @IBOutlet var pinNumberLbl: UILabel!
    @IBOutlet var descriptionLbl: UILabel!
    @IBOutlet var qtyLbl: UILabel!
    @IBOutlet var designatorLbl: PaddingLabel!
    @IBOutlet var reportedByLbl: UILabel!
    @IBOutlet var fittedByLbl: UILabel!
    @IBOutlet var dateFittedLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.sizeToFit()
        self.descriptionLbl.numberOfLines = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /*
    override func layoutSubviews() {
        super.layoutSubviews()
        let padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        contentView.frame = contentView.frame.inset(by: padding)
    }
   */
    
}
