//
//  TLPTableCell.swift
//  DRC
//
//  Created by Edward Reilly on 19/12/2019.
//  Copyright © 2019 Norcott. All rights reserved.
//

import UIKit

class TLPTableCell: UITableViewCell {

    @IBOutlet var TLPImage: UIImageView!
    @IBOutlet var TLPName: UILabel!
    @IBOutlet var InstructionLbl: UILabel!
    @IBOutlet var slpImage: UIImageView!
    @IBOutlet var tlpImage: UIImageView!
  
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        /*
        if (selected) {
            Animations().fadeInAnimation(object: self.layer)
            //Animations().addPulsations(object: self.TLPImage.layer, count: 2)
            Animations().addPulsations(object: self.layer, count: 1)
        }
         */
        
    }
    

}
