//
//  TLPUserTableCell.swift
//  DRC
//
//  Created by Edward Reilly on 13/05/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

class TLPUserTableCell: UITableViewCell {
    
    @IBOutlet weak var TLPLbl: UILabel!
    @IBOutlet weak var batchLbl: UILabel!
    @IBOutlet weak var pinLbl: UILabel!
    @IBOutlet weak var buildLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    
    @IBOutlet weak var stateImg: UIImageView!
    @IBOutlet weak var processesImg: UIImageView!


    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 0
        self.layer.borderWidth = 1
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
