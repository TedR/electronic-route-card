//
//  TorqueTableCell.swift
//  DRC
//
//  Created by Edward Reilly on 10/06/2021.
//  Copyright © 2021 Norcott. All rights reserved.
//

import UIKit

class TorqueTableCell: UITableViewCell {
    @IBOutlet weak var driverNumber: UILabel!
    @IBOutlet weak var torqueSetting: UILabel!
    @IBOutlet weak var torqueMeasurement: UILabel!
    @IBOutlet weak var dateAdded: UILabel!
    @IBOutlet weak var opName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
