//
//  WOCTableCell.swift
//  DRC
//
//  Created by Edward Reilly on 12/12/2019.
//  Copyright © 2019 Norcott. All rights reserved.
//

import UIKit

class WOCTableCell: UITableViewCell {
    @IBOutlet var TLPIndicator: UIImageView!
    @IBOutlet var SLPIndicator: UIImageView!
    @IBOutlet var AlertIndicator: UIImageView!
    @IBOutlet var GNCAlert: UIImageView!
    @IBOutlet var AOIImage: UIImageView!

    @IBOutlet var FirstSerial: UILabel!
    @IBOutlet var ClientPart: UILabel!
    @IBOutlet var batchLabel: UILabel!
    @IBOutlet var qtyLabel: UILabel!
    @IBOutlet var pinLabel: UILabel!
    @IBOutlet var jobLabel: UILabel!
    @IBOutlet var buildLabel: UILabel!
    @IBOutlet var pmrLabel: UILabel!
    @IBOutlet var ERCInstructions: UILabel!
    @IBOutlet var ProcessFinish: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    
}
