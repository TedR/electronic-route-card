//
//  BatchScanViewController.swift
//  DRC
//
//  Created by Edward Reilly on 19/03/2021.
//  Copyright © 2021 Norcott. All rights reserved.
//

import UIKit

class BatchScanViewController: UIViewController, UITextFieldDelegate {
    
    // MARK:- Outlets
    @IBOutlet var waitSpinner: UIActivityIndicatorView!
    @IBOutlet var serialTxt: UITextField!
    @IBOutlet var statusImg: UIImageView!
    @IBOutlet var processModeImg: UIImageView!
    
    @IBOutlet var serialLbl: UILabel!
    @IBOutlet var batchLbl: UILabel!
    @IBOutlet var jobLbl: UILabel!
    @IBOutlet var assyLbl: UILabel!
    
    @IBOutlet var closeBtn: UIButton!
    
    @IBOutlet var statusSwitch: UISwitch!
    
    @IBOutlet var statusLabel: UILabel!
    
    @IBOutlet var titleLabel: UILabel!
    
    
    // MARK:- Life Cycle
    var processItem: ProcessModel?
    var indexPath: Int?
    var processResult: Int?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        statusSwitch.addTarget(self, action: #selector(switchDidChangeState), for: .valueChanged)
        // Do any additional setup after loading the view.
        // Give modal translucent background
        view.backgroundColor = UIColor.systemBackground.withAlphaComponent(0.9)
        // Setup dummy receiver so text box can become first responder without showing keyboard
        let dummyView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        // Activate delegate methods
        self.serialTxt.delegate = self
        self.serialTxt.inputView = dummyView
        // Become responsive
        self.serialTxt.becomeFirstResponder()
        self.serialTxt.autocapitalizationType = .allCharacters
        self.waitSpinner.stopAnimating()
        self.batchLbl.text = String(processItem!.BatchNumber)
        self.jobLbl.text = String(processItem!.JobNumber)
        self.assyLbl.text = processItem?.PinNumber

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Set icon based on production or inspection scan
        // Process = 1
        if (processItem?.ScanTypeIndex == 1) {
            self.processModeImg.image = UIImage(systemName: "barcode.viewfinder")
            self.statusLabel.isHidden = true
            self.statusSwitch.isHidden = true
            self.statusImg.isHidden = true
        }
        // Inspection = 2
        if (processItem?.ScanTypeIndex == 2) {
            self.processModeImg.image = UIImage(systemName: "checkmark.square")
            self.statusLabel.isHidden = false
            self.statusSwitch.isHidden = false
            self.statusImg.isHidden = false
        }
        // Added Sample = 5
        if (processItem?.ScanTypeIndex == 5) {
            self.processModeImg.image = UIImage(systemName: "qrcode.viewfinder")
            self.statusLabel.isHidden = true
            self.statusSwitch.isHidden = true
            self.statusImg.isHidden = true
            self.titleLabel.text = "Batch Scan - Sample Mode"
        }
        // Added Panel = 6
        if (processItem?.ScanTypeIndex == 6) {
            self.processModeImg.image = UIImage(systemName: "square.3.layers.3d.down.right")
            self.statusLabel.isHidden = true
            self.statusSwitch.isHidden = true
            self.statusImg.isHidden = true
            self.titleLabel.text = "Batch Scan - Panel Mode"
        }
        self.statusSwitch.thumbTintColor = (self.statusSwitch.isOn) ? .systemGreen : .systemRed
        self.statusImg.image = (self.statusSwitch.isOn) ? UIImage(named: "Circle_Green") :
        UIImage(named: "Circle_Red")
        self.statusLabel.text = (self.statusSwitch.isOn) ? "Pass" : "Fail"
        processResult = (self.statusSwitch.isHidden) ? 0 : 1
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Send message to have Sub-processes refeshed to counters in sync .....
        
        // If in elevated mode then use self.indexPath as refreshing specific process else use entire object
        // NotificationCenter.default.post(name: Notification.Name("refreshSubProcess"), object: self.indexPath)
        if (AppDelegate.UserData.elevatedMode != true) {
        NotificationCenter.default.post(name: Notification.Name("refreshSubProcess"), object: self.processItem)
        } else {
            NotificationCenter.default.post(name: Notification.Name("refreshSubProcess"), object: self.indexPath)
        }
        
    }
    
    
    @IBAction func closeView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK:- Textfield Events
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Responds to Return key
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          textField.text = (textField.text! as NSString).replacingCharacters(in: range, with: string.uppercased())
        return false
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        // Coerce to NSString
        let serial = textField.text! as NSString
        if (serial.length > 0) {
            // clear out text field
            // textField.text = ""
            var psn_ix: Int = 0
            var pmm = PanelMultiplerModel(PSN_IX: 0, PSN_QOB: 0, PSN_PIN_IX: 0, PIN_Panel_Number: 0, PIN_Validated: false)
            var status =  PreviousScanInfo(StepText: "", Scanned: nil, Serial: "")
            // Convert to PSN INDEX Value
            do {
                // See if we have Client serial number
                var translatedSerial = Utils().CallTranslateSerial(serial: serial)
                // See if a bonded assy
                translatedSerial = Utils().CallTopLevelSerial(serial: translatedSerial == "" ? serial as NSString : translatedSerial as NSString)
                // See if a valid Serial Number, throw exception if not
                psn_ix = try Utils().SerialToIndex(serial: translatedSerial == "" ? serial as NSString : translatedSerial as NSString)
                pmm = Utils().CallPanelMultiplier(serial: Int32(psn_ix))
                // If panel scan then check it has been validated
                if (processItem!.ScanTypeIndex == 6) && (pmm.PIN_Validated == false) {
                    self.showAlert("Error", "Panel Scan Not Validated: \(String(status.StepText))")
                    return
                }
                
                // Check previous scan status
                // a. Empty - OK - first process step so no data
                // b. 0 - NOT OK - Not Seen at previous step
                // c. 1 - OK - seen at previous step
                status = Utils().CallCheckPreviousScan(prcIndex: Int32(processItem!.UniqueStepID), snIndex: Int32(psn_ix), scanType: Int32(processItem!.ScanTypeIndex), panelIndex: Int32(pmm.PSN_PIN_IX))
                if (status.Scanned == false) {
                    if (processItem!.ScanTypeIndex == 6) {
                        self.showAlert("Error", "Serial: \(String(status.Serial)) Not Scanned at: \(String(status.StepText))")
                    }
                    else {
                      self.showAlert("Error", "Serial Not Scanned at: \(String(status.StepText))")
                    }
                    return
                }
                
            } catch {
                psn_ix = 0
                self.showAlert("Error", "Serial Format Error: \(String(serial))")
            }
            if (psn_ix > 0) && (status.Scanned == nil || status.Scanned == true) {
                // This will get the mappng for combination of parent step index and sub-process index
                /*
                let mapping = AppDelegate.AppData.legacyMappings.filter {
                    return ($0.LM_PRS_Parent_Step_IX == processItem!.ParentIndex && $0.LM_PRS_Step_IX == processItem?.ProcessID)
                }.first
                if ((mapping) != nil) {
                    
                }
                 */
                
                // Create payload
                let dc = ScanHistoryModel(PSH_IX: 0, PSH_PSN_IX: psn_ix, PSH_PRC_IX: processItem!.UniqueStepID, PSH_PRS_IX: processItem!.ProcessID, PSH_RCI_IX: processItem!.InstIndex, PSH_BH_IX: processItem!.BatchIndex, PSH_ST_IX: processItem!.ScanTypeIndex, PSH_Result: self.processResult ?? 0, PSH_UN_IX: AppDelegate.UserData.userID, PSH_TTS_IX: processItem!.TTSIndex_1, PSH_By: AppDelegate.UserData.userInitials, PSH_Quantity: pmm.PSN_QOB, PSH_DateTime: nil, Device_Name: AppDelegate.UserData.deviceName, PSN_PIN_IX: pmm.PSN_PIN_IX)
                self.waitSpinner.startAnimating()
                self.PostDataCapture(dc)
                self.waitSpinner.stopAnimating()
                // Send message to have Sub-processes refeshed to counters in sync .....
                print(AppDelegate.UserData.elevatedMode .description)
                if (AppDelegate.UserData.elevatedMode == true) {
                    NotificationCenter.default.post(name: Notification.Name("refreshSubProcess"), object: self.indexPath)
                } else {
                    NotificationCenter.default.post(name: Notification.Name("refreshSubProcess"), object: processItem)
                }
                
            } else {
                self.showAlert("Error", "Serial Format Error: \(String(serial))")
            }
        }
    }
    
    // MARK: - Status Switch
    @objc func switchDidChangeState(sender: UISwitch!) {
        Animations().addPulsations(object: sender.layer, count: 1)
        self.processResult = (self.statusSwitch.isOn) ? 1 : 0
        self.waitSpinner.color = (self.statusSwitch.isOn) ? .systemGreen : .systemRed
        self.statusSwitch.thumbTintColor = (self.statusSwitch.isOn) ? .systemGreen : .systemRed
        self.statusImg.image = (self.statusSwitch.isOn) ? UIImage(named: "Circle_Green") : UIImage(named: "Circle_Red")
        self.statusLabel.text = (self.statusSwitch.isOn) ? "Pass" : "Fail"
        print(self.processResult!)
    }
    
    
    // MARK: - Alert Dialog
    func showAlert(_ title: String, _ body: String) {
        let alertController = UIAlertController(title: title, message: body, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: {
            self.serialTxt.text = ""
            self.serialTxt.becomeFirstResponder() })
    }
    
    // MARK: - Web API Calls
    // POSTS Scan History Entry Based on Data Capture Type ...
    func PostDataCapture(_ scanHist: ScanHistoryModel) {
        // http://192.168.33.9:6002/api/routecardservice/DataCapture
        let semaphore = DispatchSemaphore(value: 1)
        let params = ["PSH_PSN_IX": scanHist.PSH_PSN_IX,
                      "PSH_PRC_IX": scanHist.PSH_PRC_IX,
                      "PSH_PRS_IX": scanHist.PSH_PRS_IX,
                      "PSH_RCI_IX": scanHist.PSH_RCI_IX,
                      "PSH_BH_IX": scanHist.PSH_BH_IX,
                      "PSH_ST_IX": scanHist.PSH_ST_IX,
                      "PSH_Result": scanHist.PSH_Result,
                      "PSH_UN_IX": scanHist.PSH_UN_IX,
                      "PSH_TTS_IX": scanHist.PSH_TTS_IX,
                      "PSH_Quantity": scanHist.PSH_Quantity,
                      "PSH_By": scanHist.PSH_By ?? "",
                      "Device_Name": scanHist.Device_Name ?? "",
                      "PSN_PIN_IX": scanHist.PSN_PIN_IX
                     ] as [String : Any]
                  //create the url with URL
                  guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/DataCapture") else {
                        print("Invalid URL!")
                        return
                  }
                  //now create the URLRequest object using the url object
                  var request = URLRequest(url: url)
                 
                  request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                  request.addValue("application/json", forHTTPHeaderField: "Accept")
                  request.httpMethod = "POST"
                  request.timeoutInterval = 20
                  
                  do {
                   request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
                  } catch let error {
                      self.showAlert("Error Scanning", "\(error.localizedDescription)")
                  }
                   URLSession.shared.dataTask(with: request) { data, response, error in
                           if let error = error {
                               self.waitSpinner.stopAnimating()
                               self.showAlert("Error Scanning", "\(error.localizedDescription)")
                           }
                    guard let data = data else {
                        self.waitSpinner.stopAnimating()
                        self.showAlert("Error Scanning", "\(String(describing: error))")
                        return
                    }
                    semaphore.signal()
                       if let decodedResponse = try? JSONDecoder().decode(POSTStatus.self, from: data) {
                           DispatchQueue.main.async {
                               if (decodedResponse.StatusCode == 1) {
                                   print("Success: \(decodedResponse.StatusMessage)")
                                   // All done so close screen if in single mode!!
                                   // self.dismiss(animated: true, completion: nil)
                                   // Update Labels
                                   self.serialLbl.text = self.serialTxt.text
                                   // Make input box active again ready to receve next scan ....
                                   self.serialTxt.becomeFirstResponder()
                                   // Clear out current serial number
                                   self.serialTxt.text = ""
                               } else
                               {
                                 self.waitSpinner.stopAnimating()
                                 self.showAlert("Error Scanning", "\(decodedResponse.StatusMessage)")
                               }
                          }
                       }
                   }.resume()
            semaphore.wait()
    }
    

}
