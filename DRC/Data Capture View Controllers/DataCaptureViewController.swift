//
//  DataCaptureTableViewController.swift
//  DRC
//
//  Created by Edward Reilly on 27/03/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

class DataCaptureViewController: UITableViewController {
    @IBOutlet var cancelBtn: UIBarButtonItem!
    @IBOutlet var addBtn: UIBarButtonItem!
    
    // Flat data array
    var dataCaptured = [ScanHistoryModel]()
    // Grouped dictionary of data
    var dataSource = [String : [ScanHistoryModel]]()
    // Keys of grouped data
    var uniqueKeys = NSOrderedSet()
    

    var indexPath: Int?
    var processResult: Int?

    // MARK: - Life Cyles
    
    @IBAction func captureButton(_ sender: UIButton) {
        (sender as UIButton).isEnabled = false
       //  self.PostReportShortageAPI(shortage)
       //  self.showAlertView()
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "UserEntryScreen") as! UserDataEntryController
        vc.processItem = processItem
        vc.indexPath = self.indexPath
        let nav : UINavigationController = UINavigationController(rootViewController: vc)
        nav.definesPresentationContext = true
        nav.modalPresentationStyle = .formSheet
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func dismissAlertView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    var processItem: ProcessModel? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    func configureView() {
        // Set up font
        let font = UIFont.systemFont(ofSize: 24)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font : font]
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.darkGray]
        UINavigationBar.appearance().titleTextAttributes = textAttributes
        // Update the user interface for the detail item.
        // This is a process step level comment
        if let detail = processItem {
            self.GetDataCaptureAPI(stepIX: detail.UniqueStepID, scanType: detail.ScanTypeIndex, batchHdrIX: detail.BatchIndex)
            self.tableView.reloadData()
            let total = dataCaptured.lazy.compactMap({ $0.PSH_Quantity }).reduce(0, +)
            self.title = "Add Quantity for Step: \(detail.StepText!) [Remaining: \(detail.ExpectedScans -  total)]"
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
        
        // Add the observer
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("refreshUserEntries"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("refreshUserEntries"), object: nil)
        
        // self.configureView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setToolbarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.post(name: Notification.Name("refreshSubProcess"), object: self.indexPath)
    }
    
     
    // This is receiver object for message sent from FitShortageController
    @objc func methodOfReceivedNotification(notification: Notification) {
        print("Message Received : \(notification.name)")
        self.addBtn.isEnabled = true
        self.configureView()
     }
    
    // MARK: - Web Services
    
    // Get data captured for this process step
    func GetDataCaptureAPI(stepIX: Int, scanType: Int, batchHdrIX: Int) {
           // http://192.168.33.9:6002/api/routecardservice/getScanHistory/3/50241/72
           // getDataCaptured/{scanType}/{batchHdrIDX}/{stepIX}
           let params = "/\(stepIX)/\(scanType)/\(batchHdrIX)"
           guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/GetScanHistory" + params) else {
                       print("Invalid URL!")
                   return
               }
               let request = URLRequest(url: url)
               URLSession.shared.dataTask(with: request) { data, response, error in
                   if let data = data {
                       let decoder = JSONDecoder()
                       decoder.dateDecodingStrategy = .formatted(AppDelegate.dateFormatter)
                       if let decodedResponse = try? decoder.decode([ScanHistoryModel].self, from: data) {
                           DispatchQueue.main.async {
                            // Flat list of data captures ...
                            self.dataCaptured = decodedResponse.self
                            // Create keys on user intials
                            self.uniqueKeys = NSMutableOrderedSet(array: (self.dataCaptured.map { $0.PSH_By! as String }))
                            // For each key in ordered key set, create a section list
                            for key in self.uniqueKeys.array {
                                let group: [ScanHistoryModel] = self.self.dataCaptured.filter {
                                    return ($0.PSH_By!) == key as! String
                                }
                                print(group.count)
                                // Add section to datasource
                                self.dataSource.updateValue(group, forKey: key as! String)
                            }
                            // Render tableview ...
                            
                            self.tableView.reloadData()
                            let total = self.dataCaptured.lazy.compactMap({ $0.PSH_Quantity }).reduce(0, +)
                            self.title = "Add Quantity for Step: \(self.processItem?.StepText! ?? "Error") [Remaining: \(self.processItem!.ExpectedScans -  total)]"
                            
                            self.addBtn.isEnabled = self.processItem!.ExpectedScans -  total > 0

                            }
                       } else {
                        print("Error: JSON Decoding Error")
                    }
                    
                   }
               }.resume()
     }

    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // This is count of unique sections specified by user initials
        return self.dataSource.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if (self.dataSource.count > 0) {
            // Get the key for this section from ordered set
            let sect = self.uniqueKeys.object(at: section)
            // Get matching rows for section
            let group: [ScanHistoryModel] = self.dataCaptured.filter {
                return ($0.PSH_By!) == sect as! String
            }
            return group.count
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        // Configure the cell...
        let sect = self.uniqueKeys.object(at: indexPath.section)
        let group: [ScanHistoryModel] = self.dataCaptured.filter {
            return ($0.PSH_By!) == sect as! String
        }
        cell.textLabel?.text = String(group[indexPath.row].PSH_Quantity)
        cell.detailTextLabel?.text =  ("\(AppDelegate.dateFormatter.string(from: group[indexPath.row].PSH_DateTime!))")  

        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.uniqueKeys.object(at: section) as? String
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:40))
        headerView.backgroundColor = UIColor.systemGray6
        // Add label for Section
        let font: UIFont = UIFont.systemFont(ofSize: 18.0, weight: .ultraLight)
        let lbl = UILabel(frame: CGRect(x: 20, y:0, width: 200, height: 40))
        lbl.text = self.uniqueKeys.object(at: section) as? String
        lbl.font = font
        headerView.addSubview(lbl)
        return headerView
    }
    
    /*
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        let sect = self.uniqueKeys.object(at: section)
        let group: [ScanHistoryModel] = self.dataCaptured.filter {
            return ($0.PSH_By!) == sect as! String
        }
        let total = group.lazy.compactMap({ $0.PSH_Quantity }).reduce(0, +)
        return String(total)
    }
    */
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:40))
        footerView.backgroundColor = UIColor.init(red: 52/255, green: 199/255, blue: 89/255, alpha: 0.4)
        
        let sect = self.uniqueKeys.object(at: section)
        let group: [ScanHistoryModel] = self.dataCaptured.filter {
            return ($0.PSH_By!) == sect as! String
        }
        let total = group.lazy.compactMap({ $0.PSH_Quantity }).reduce(0, +)
        // Add label for total
        let font: UIFont = UIFont.boldSystemFont(ofSize: 14)
        let lbl = UILabel(frame: CGRect(x: 20, y:0, width:100, height:40))
        lbl.text = (sect as! String == AppDelegate.UserData.userInitials ? String("My Total: \(String(total))") : String(total))
        lbl.font = font
        lbl.shadowColor = .lightGray
        
        footerView.addSubview(lbl)
        
        return footerView
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    @IBAction func doRefresh(_ sender: UIRefreshControl) {
        let font = UIFont.systemFont(ofSize: 16)
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: UIColor.red,
        ]
        let title = NSAttributedString(string: "Refreshing Data Capture ..", attributes: attributes)
        sender.attributedTitle = title
        self.configureView()
        sender.endRefreshing()
        
    }
    
    @objc func showAlertView() {
      let alertController = UIAlertController(title: "Add Data Capture", message: nil, preferredStyle: .alert)
      let confirmAction = UIAlertAction(title: "Add", style: .default) { (_) in
          if let txtField = alertController.textFields?.first, let text = txtField.text {
            // operations
            print("Data Capture Added: " + text)
          }
      }
      let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
      alertController.addTextField { (textField) in
          textField.placeholder = "Add Quantity"
      }
        
        let total = dataCaptured.lazy.compactMap({ $0.PSH_Quantity }).reduce(0, +)
        let remaining = self.processItem!.ExpectedScans -  total

    
        let qtyStepper = UIStepper(frame: CGRect(x: 160, y: 60 , width: 0, height: 0))
        qtyStepper.wraps = true
        qtyStepper.backgroundColor = .systemGreen
        qtyStepper.isOpaque = false
        qtyStepper.maximumValue = Double(remaining)
        qtyStepper.minimumValue = 1
        alertController.view.addSubview(qtyStepper)  
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
        

        
      }
    

    
    // MARK: - Alert Dialog
    
    func showAlert(_ title: String, _ body: String) {
        let alertController = UIAlertController(title: title, message: body, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: {
        })
    }
    

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */




}
