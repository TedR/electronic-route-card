//
//  MeasurementTabController.swift
//  DRC
//
//  Created by Edward Reilly on 07/06/2021.
//  Copyright © 2021 Norcott. All rights reserved.
//

import UIKit

class MeasurementTabController: UITabBarController, UITabBarControllerDelegate, UITextFieldDelegate {
    
    
    var woc: WOCModel?
    var process: ProcessModel?
    


    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tabBarController?.delegate = self
        
        let exit = UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(dismissView))
        self.navigationItem.rightBarButtonItems = [exit]
        
        self.selectedIndex = 0
        
        self.navigationItem.title = self.tabBar.selectedItem!.title! + " - Measurement"



    }
    
    // Use NSCoder to support dependency injection of objects in to this view
    required init?(coder: NSCoder, wocItem: WOCModel, processItem: ProcessModel) {
        super.init(coder: coder)
        self.woc = wocItem
        print(woc?.BatchNumber .description as Any)
        self.process = processItem
        print(process?.UniqueStepID .description as Any)
        
    
   
        // Grab all controllers
        let viewControllers = self.viewControllers
        // As Tabcontroller embedded in nav controller, access each index
        // Pass the data objects to each TAB .....
        
        for controller in 0...2 {
        
            let nc =  viewControllers![controller] as! UINavigationController
            let mc = nc.topViewController as! MeasurementViewController
            mc.woc = wocItem
            mc.process = processItem
            mc.type = controller
        
        }
     }
    
    
    // Use NSCoder to support dependency injection of objects in to this view
    // This is error handler
    required init(coder: NSCoder) {
      fatalError("You must provide a WOC and/or ProcessModel object to this view controller")
    }
    
    @objc func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    

    // UITabBarControllerDelegate
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        self.navigationItem.title = item.title! + " - Measurement"

    }
    

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
