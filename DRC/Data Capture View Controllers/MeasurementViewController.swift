//
//  MeasurementViewController.swift
//  DRC
//
//  Created by Edward Reilly on 07/06/2021.
//  Copyright © 2021 Norcott. All rights reserved.
//

import UIKit

class MeasurementViewController: UITableViewController, UITextFieldDelegate  {
    
    var measurements = [Measurement]()
    
    var process: ProcessModel? {
        didSet {
            
            // Update the view.
            configureView()
        }
    }
    
    var woc: WOCModel? {
        didSet {
            // Update the view.
        }
    }
    
    var type: Int? {
        didSet {
            // Update the view.
        }
    }
    
    
    // MARK: - Life Cycle
    
    @objc func configureView() {
        if let detail = process {
            DispatchQueue.main.async {
                self.navigationItem.title = "\(detail.ProcessName!) - \(detail.StepText!)"
            }
            self.GetMeasurementsAPI(process!.UniqueStepID)
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        let add = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addMeasurement))
        add.tintColor = .systemGreen
        self.navigationItem.leftBarButtonItems = [add]
        
        let exit = UIBarButtonItem(title: "Exit", style: .done, target: self, action: #selector(dismissController))
        exit.tintColor = .red
        self.navigationItem.rightBarButtonItems = [exit]
        
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = UITableView.automaticDimension
        
        // Register my custom cells
        switch self.type {
        case 0:
            self.tableView.register(UINib(nibName: "ConformalTableCell", bundle: nil), forCellReuseIdentifier: "Cell")
            let headerNib = UINib.init(nibName: "ConformalCellHeader", bundle: Bundle.main)
            self.tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "HeaderCell")
        case 1:
            self.tableView.register(UINib(nibName: "TorqueTableCell", bundle: nil), forCellReuseIdentifier: "Cell")
            let headerNib = UINib.init(nibName: "TorqueCellHeader", bundle: Bundle.main)
            self.tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "HeaderCell")
        case 2:
            self.tableView.register(UINib(nibName: "CrimpTableCell", bundle: nil), forCellReuseIdentifier: "Cell")
            let headerNib = UINib.init(nibName: "CrimpCellHeader", bundle: Bundle.main)
            self.tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "HeaderCell")
        default:
            return
        }
        
        
        self.configureView()
        
   
    }
    
    @objc func dismissController() {
        self.dismiss(animated: false, completion: nil)
    }
    
    // MARK: - Text Delegate Methods
    
    
    // Invoke delegate method to force some validation of field lengths
    @objc func textChanged(_ sender: Any) {
        let tf = sender as! UITextField
        // Force to uppercase
        tf.text = tf.text?.uppercased()
        var resp : UIResponder! = tf
        while !(resp is UIAlertController) { resp = resp.next }
        let alert = resp as! UIAlertController
        // Has to be >= 4 and a valid serial number
        let translatedSerial = Utils().CallTranslateSerial(serial: tf.text! as NSString)
        if (translatedSerial == "") {
            alert.actions[0].isEnabled = ((tf.text!.count >= 4) && (try! Utils().SerialToIndex(serial: tf.text! as NSString) > 0))
        } else {
            alert.actions[0].isEnabled = ((tf.text!.count >= 4) && (try! Utils().SerialToIndex(serial: translatedSerial as NSString) > 0))
        }
        
    }
    
    // MARK: - WEB API Service Calls
    
    // Add CC Measurement
    @objc func addThicknessMeasurement() {
        let alertController = UIAlertController(title: "Measurement Capture", message: "Record Data Measurement", preferredStyle: .alert)
        
        alertController.addTextField { tf in
            tf.placeholder = "Serial"
            tf.isSecureTextEntry = false
            tf.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        }
  
        alertController.addTextField { tf in
            tf.placeholder = "Operator"
            tf.isSecureTextEntry = false
            tf.isUserInteractionEnabled = false
            tf.text = AppDelegate.UserData.userName
        }
        
        alertController.addTextField { tf in
            tf.placeholder = "Thickness (um)"
            tf.isSecureTextEntry = false
        }
        
        alertController.addTextField { tf in
            tf.placeholder = "Authorised By"
            tf.isSecureTextEntry = false
        }
        
        let continueAction = UIAlertAction(title: "Continue",
                                           style: .default) { [weak alertController] _ in
                                            guard let textFields = alertController?.textFields else { return }
                                            
                                            if var serialText = textFields[0].text,
                                               let operatorText = textFields[1].text,
                                               let thicknessText = textFields[2].text,
                                               let authBy = textFields[3].text
                                            {

                                                let translatedSerial = Utils().CallTranslateSerial(serial: textFields[0].text! as NSString)
                                                serialText = translatedSerial == "" ? serialText : translatedSerial
                                                
                                                // See if a bonded assy
                                                serialText = Utils().CallTopLevelSerial(serial: serialText as NSString)
                                                
                                                // Check format of serial
                                                let psn_ix = try! Utils().SerialToIndex(serial: serialText as NSString)
                                                
                                                let dateNow = Date()
                                                let pmrDataObject = ConformalMeasurement(PMR_OP_Name: operatorText, PMR_OP_ID: AppDelegate.UserData.userID, PMR_Serial: serialText, PMR_Serial_Index: psn_ix, PMR_Measurement:  (thicknessText as NSString).floatValue, PMR_Auth_By: authBy, PMR_Added: dateNow)
                                        
                                                let enc = JSONEncoder()
                                                let data = try? enc.encode(pmrDataObject)
                                                // Already JSON so encode as utf8
                                                let pmrData = String(data: data!, encoding: .utf8)!
                                                
                                                // Create actual measurement object
                                                let measure = Measurement(PMR_IX: -1, PMR_PRC_IX: self.process?.RouteCardHdrIndex, PMR_PRS_IX: self.process?.UniqueStepID, PMR_RCI_IX: self.process?.InstIndex, PMR_BH_IX: self.process?.BatchIndex, PMR_Type: self.type, PMR_Data: pmrData)
                                                 
                                                // Invoke API
                                                self.InsertMeasurementAPI(measure)
                                                                                           
                                            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .destructive, handler: { _ in
                  //Cancel Action
                  print("Cancel")
               })

        let height:NSLayoutConstraint = NSLayoutConstraint(item: alertController.view!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: self.view.frame.height * 0.40)
        alertController.view.addConstraint(height)
        
        alertController.addAction(continueAction)
        
        alertController.addAction(cancelAction)
        
        (alertController.actions[0] as UIAlertAction).isEnabled = false

        self.present(alertController, animated: true, completion: {
            
        })
    }
    
    // Add Torque Measurement
    @objc func addTorqueMeasurement() {
        let alertController = UIAlertController(title: "Measurement Capture", message: "Record Data Measurement", preferredStyle: .alert)
        
        alertController.addTextField { tf in
            tf.placeholder = "Driver Number"
            tf.isSecureTextEntry = false
        }
  
        alertController.addTextField { tf in
            tf.placeholder = "Torque Setting"
            tf.isSecureTextEntry = false
        }
        
        alertController.addTextField { tf in
            tf.placeholder = "Torque Recorded (Nm)"
            tf.isSecureTextEntry = false
        }
        
        alertController.addTextField { tf in
            tf.placeholder = "Signature"
            tf.isSecureTextEntry = false
            tf.text = AppDelegate.UserData.userName
            tf.isUserInteractionEnabled = false
        }
        
        let continueAction = UIAlertAction(title: "Continue",
                                           style: .default) { [weak alertController] _ in
                                            guard let textFields = alertController?.textFields else { return }
                                            
                                            if let driverNumber = textFields[0].text,
                                               let torqueSetting = textFields[1].text,
                                               let torqueRecorded = textFields[2].text,
                                               let signature = textFields[3].text
                                            {

                                                let dateNow = Date()
                                                let pmrDataObject = TorqueMeasurement(PMR_Driver_Number: driverNumber, PMR_Torque_Setting: torqueSetting, PMR_Measurement: (torqueRecorded as NSString).floatValue, PMR_Added: dateNow, PMR_OP_Name: signature, PMR_OP_ID: AppDelegate.UserData.userID)
                                        
                                                let enc = JSONEncoder()
                                                let data = try? enc.encode(pmrDataObject)
                                                // Already JSON so encode as utf8
                                                let pmrData = String(data: data!, encoding: .utf8)!
                                                
                                                // Create actual measurement object
                                                let measure = Measurement(PMR_IX: -1, PMR_PRC_IX: self.process?.RouteCardHdrIndex, PMR_PRS_IX: self.process?.UniqueStepID, PMR_RCI_IX: self.process?.InstIndex, PMR_BH_IX: self.process?.BatchIndex, PMR_Type: self.type, PMR_Data: pmrData)
                                                 
                                                // Invoke API
                                                self.InsertMeasurementAPI(measure)
                                                                                           
                                            }
        }

        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .destructive, handler: { _ in
                  //Cancel Action
                  print("Cancel")
               })
        
        let height:NSLayoutConstraint = NSLayoutConstraint(item: alertController.view!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: self.view.frame.height * 0.40)
        alertController.view.addConstraint(height)
        
        alertController.addAction(continueAction)
        alertController.addAction(cancelAction)
        
       // (alertController.actions[0] as UIAlertAction).isEnabled = false

        self.present(alertController, animated: true, completion: {
            
        })
    }
    
    // Add Pulloff Measurement
    @objc func addPullOffMeasurement() {
        let alertController = UIAlertController(title: "Measurement Capture", message: "Record Data Measurement", preferredStyle: .alert)
        
        alertController.addTextField { tf in
            tf.placeholder = "Crimp Type"
            tf.isSecureTextEntry = false
        }
  
        alertController.addTextField { tf in
            tf.placeholder = "Wire Type"
            tf.isSecureTextEntry = false
        }
        
        alertController.addTextField { tf in
            tf.placeholder = "Crimp Tool"
            tf.isSecureTextEntry = false
        }
        
        alertController.addTextField { tf in
            tf.placeholder = "Pull Test Result"
            tf.isSecureTextEntry = false
        }
        
        alertController.addTextField { tf in
            tf.placeholder = "Signature"
            tf.isSecureTextEntry = false
            tf.text = AppDelegate.UserData.userName
            tf.isUserInteractionEnabled = false
        }
        
        
        let continueAction = UIAlertAction(title: "Continue",
                                           style: .default) { [weak alertController] _ in
                                            guard let textFields = alertController?.textFields else { return }
                                            
                                            if let crimpType = textFields[0].text,
                                               let wireType = textFields[1].text,
                                               let crimpTool = textFields[2].text,
                                               let measurement = textFields[3].text,
                                               let signature = textFields[4].text
                                            {

                                                let dateNow = Date()
                                                let pmrDataObject = CrimpMeasurement(PMR_Crimp_Type: crimpType, PMR_Wire_Type: wireType, PMR_Crimp_Tool: crimpTool, PMR_Measurement: (measurement as NSString).floatValue, PMR_Added: dateNow, PMR_OP_Name: signature, PMR_OP_ID: AppDelegate.UserData.userID)
                                        
                                                let enc = JSONEncoder()
                                                let data = try? enc.encode(pmrDataObject)
                                                // Already JSON so encode as utf8
                                                let pmrData = String(data: data!, encoding: .utf8)!
                                                
                                                // Create actual measurement object
                                                let measure = Measurement(PMR_IX: -1, PMR_PRC_IX: self.process?.RouteCardHdrIndex, PMR_PRS_IX: self.process?.UniqueStepID, PMR_RCI_IX: self.process?.InstIndex, PMR_BH_IX: self.process?.BatchIndex, PMR_Type: self.type, PMR_Data: pmrData)
                                                 
                                                // Invoke API
                                                self.InsertMeasurementAPI(measure)
                                                                                           
                                            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .destructive, handler: { _ in
                  //Cancel Action
                  print("Cancel")
               })
        

        let height:NSLayoutConstraint = NSLayoutConstraint(item: alertController.view!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: self.view.frame.height * 0.40)
        alertController.view.addConstraint(height)
        
        alertController.addAction(continueAction)
        alertController.addAction(cancelAction)
        
       // (alertController.actions[0] as UIAlertAction).isEnabled = false

        self.present(alertController, animated: true, completion: {
            
        })
    }
    
    
    // Measurement type selector
    @objc func addMeasurement() {
        switch self.type {
        case 0:
            self.addThicknessMeasurement()
        case 1:
            self.addTorqueMeasurement()
        case 2:
            self.addPullOffMeasurement()
        default:
            break
        }
    }
    
    
    func GetMeasurementsAPI(_ prc: Int) {
       // http://192.168.33.9:6002/api/routecardservice/GetMeasurements/77257
       // GetConsumables/{rch}/{prc}/{rci}
        WebRequest<Measurement>.get(self, service: "GetMeasurements", params:[String(prc)], success: { result in
                // Flat list of data captures ...
            
            self.measurements = result
            self.measurements = self.measurements.filter {
                return ( $0.PMR_Type! == self.type! )
            }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            )
     }
    
    func InsertMeasurementAPI(_ measurement: Measurement) {
        // http://192.168.33.9:6002/api/routecardservice/InsertMeasurement
        let params = ["PMR_PRC_IX": measurement.PMR_PRC_IX!,
                      "PMR_PRS_IX": measurement.PMR_PRS_IX!,
                      "PMR_RCI_IX": measurement.PMR_RCI_IX!,
                      "PMR_BH_IX": measurement.PMR_BH_IX!,
                      "PMR_Type": measurement.PMR_Type!,
                      "PMR_Data": measurement.PMR_Data!] as [String : Any]
           //create the url with URL
           guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/InsertMeasurement") else {
                 print("Invalid URL!")
                 return
           }
           //now create the URLRequest object using the url object
           var request = URLRequest(url: url)
          
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           request.addValue("application/json", forHTTPHeaderField: "Accept")
           request.httpMethod = "POST"
           request.timeoutInterval = 20
           
           do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
           } catch let error {
               print(error)

           }

            URLSession.shared.dataTask(with: request) { data, response, error in
                    if let error = error {
                        print("Something went wrong: \(error)")
                    }
                    if let data = data {
                        if let decodedResponse = try? JSONDecoder().decode([Measurement].self, from: data) {
                            DispatchQueue.main.async {
                                if (decodedResponse.count >= 0) {
                                    self.doRefresh(self.refreshControl!)
                                }
                            }
                        }
                    }
                    
               }.resume()
       }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.measurements.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var returnCell = UITableViewCell()
        var measurement: Measurement
        measurement = self.measurements[indexPath.row]
        let data = Data(measurement.PMR_Data!.utf8)

        // Give me a clue what's wrong?!!!!
        /*
        do {
            _ =  try JSONDecoder().decode(ConformalMeasurement.self, from: data)
        } catch {
            print("Error Decoding:  \(error)")
        }
        */
        
        
        switch measurement.PMR_Type {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ConformalTableCell
            if let decodedResponse = try? JSONDecoder().decode(ConformalMeasurement.self, from: data) {
               
                cell.opName.text = decodedResponse.PMR_OP_Name
                cell.serialNumber.text = decodedResponse.PMR_Serial
                let nf = NumberFormatter()
                nf.numberStyle = .decimal
                cell.thickness.text = nf.string(from: NSNumber(value: decodedResponse.PMR_Measurement))
                cell.authBy.text = decodedResponse.PMR_Auth_By
                cell.dateAdded.text = AppDelegate.dateFormatterNoTime.string(from: decodedResponse.PMR_Added)
                returnCell = cell
            }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TorqueTableCell
            if let decodedResponse = try? JSONDecoder().decode(TorqueMeasurement.self, from: data) {
                
                cell.driverNumber.text = decodedResponse.PMR_Driver_Number
                cell.torqueSetting.text = decodedResponse.PMR_Torque_Setting
                cell.opName.text = decodedResponse.PMR_OP_Name
                let nf = NumberFormatter()
                nf.numberStyle = .decimal
                cell.torqueMeasurement.text = nf.string(from: NSNumber(value: decodedResponse.PMR_Measurement))
                cell.dateAdded.text = AppDelegate.dateFormatterNoTime.string(from: decodedResponse.PMR_Added)
                returnCell = cell
            }
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CrimpTableCell
            if let decodedResponse = try? JSONDecoder().decode(CrimpMeasurement.self, from: data) {
          
                cell.opName.text = decodedResponse.PMR_OP_Name

                let nf = NumberFormatter()
                nf.numberStyle = .decimal
                cell.pullMeasurement.text = nf.string(from: NSNumber(value: decodedResponse.PMR_Measurement))
                cell.wireType.text = decodedResponse.PMR_Wire_Type
                cell.crimpType.text = decodedResponse.PMR_Crimp_Type
                cell.crimpTool.text = decodedResponse.PMR_Crimp_Tool
                cell.dateAdded.text = AppDelegate.dateFormatterNoTime.string(from: decodedResponse.PMR_Added)
                returnCell = cell
            }
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ConformalTableCell
            if let decodedResponse = try? JSONDecoder().decode(ConformalMeasurement.self, from: data) {

                cell.opName.text = decodedResponse.PMR_OP_Name
                cell.serialNumber.text = decodedResponse.PMR_Serial
                let nf = NumberFormatter()
                nf.numberStyle = .decimal
                cell.thickness.text = nf.string(from: NSNumber(value: decodedResponse.PMR_Measurement))
                cell.authBy.text = decodedResponse.PMR_Auth_By
                cell.dateAdded.text = AppDelegate.dateFormatterNoTime.string(from: decodedResponse.PMR_Added)
                returnCell = cell
          }
        }
        return returnCell
    }
        
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {


        // This will hold array of available actions once determined
        var actions = [UIContextualAction]()
            
            
        let actionAdd = UIContextualAction(style: .normal, title: String("Add"),
                  handler: { (actionAdd, view, completionHandler) in
                  // Update data source when user taps action
                  self.addMeasurement()
                  completionHandler(true)
                })
                actionAdd.image = UIImage(systemName: "plus.circle")
                actionAdd.backgroundColor = .systemGreen
            
            actions.append(actionAdd)
           

            
        let configuration = UISwipeActionsConfiguration(actions: actions)
        
           
           
        return configuration
    }
    
    @IBAction func doRefresh(_ sender: UIRefreshControl) {
        let font = UIFont.systemFont(ofSize: 16)
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: UIColor.red,
        ]
        let title = NSAttributedString(string: "Refreshing Measurements ..", attributes: attributes)
        sender.attributedTitle = title
        self.configureView()
        sender.endRefreshing()
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch self.type {
        case 0:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderCell")
            let backgroundView = UIView(frame: CGRect.zero)
            backgroundView.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
            headerView?.backgroundView = backgroundView
            return headerView
        case 1:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderCell")
            let backgroundView = UIView(frame: CGRect.zero)
            backgroundView.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
            headerView?.backgroundView = backgroundView
            return headerView
        case 2:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderCell")
            let backgroundView = UIView(frame: CGRect.zero)
            backgroundView.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
            headerView?.backgroundView = backgroundView
            return headerView
        default:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderCell")
            let backgroundView = UIView(frame: CGRect.zero)
            backgroundView.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
            headerView?.backgroundView = backgroundView
            return headerView
        }
        

    }
    



}
