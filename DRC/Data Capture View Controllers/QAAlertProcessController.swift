//
//  QAAlertController.swift
//  DRC
//
//  Created by TechFusion on 28/08/2023.
//  Copyright © 2023 Norcott. All rights reserved.
//

import UIKit

class QAAlertProcessController: UITableViewController {
    
    var slp: ProcessModel?
    var qaAlerts = [QAAlert]()
    // Keys of grouped data
    var uniqueKeys = NSOrderedSet()
    // Grouped dictionary of data
    var dataSource = [String : [QAAlert]]()

    
    // MARK: - LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.clearsSelectionOnViewWillAppear = false
        
        // Register my custom cell
        self.tableView.register(UINib(nibName: "QAAlertProcessCell", bundle: nil), forCellReuseIdentifier: "QAAlertProcessCell")
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = UITableView.automaticDimension
        
        let exit = UIBarButtonItem(title: "Exit", style: .done, target: self, action: #selector(dismissAlertView))

        exit.tintColor = .red
        self.navigationItem.rightBarButtonItems = [exit]
    
        
        self.configureView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setToolbarHidden(false, animated: animated)

    }
    
    // Use NSCoder to support dependency injection of objects in to this view
    required init?(coder: NSCoder, slp: ProcessModel) {
        super.init(coder: coder)
        self.slp = slp
     }
    
    
    // Use NSCoder to support dependency injection of objects in to this view
    // This is error handler
    required init(coder: NSCoder) {
      fatalError("You must provide a WOC and/or ProcessModel object to this view controller")
    }
    
    func configureView() {
        // Set up font
        let font = UIFont.systemFont(ofSize: 24)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font : font]
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.darkGray]
        UINavigationBar.appearance().titleTextAttributes = textAttributes

        // Grab the QA Alerts!
        self.GetProcessQAAlertsAPI(slp!.RouteCardHdrIndex, slp!.UniqueStepID, AppDelegate.UserData.userID)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.post(name: Notification.Name("refreshSubProcess"), object: self.slp)
    }
    
    @objc func dismissAlertView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Web Services
    
    // Get QAAlerts
    func GetProcessQAAlertsAPI(_ rchIX: Int, _ prcIX: Int, _ userIndex: Int) {
        // http://192.168.33.9:6002/api/routecardservice/getProcessLevelQAAlert/4020/108795/104
        qaAlerts = [QAAlert]()
        // Grouped dictionary of data
        dataSource = [String : [QAAlert]]()
        // Keys of grouped data
        uniqueKeys = NSOrderedSet()
           let params = "/\(rchIX)/\(prcIX)/\(userIndex)"
           guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/getProcessLevelQAAlert" + params) else {
                       print("Invalid URL!")
                   return
               }
               let request = URLRequest(url: url)
               URLSession.shared.dataTask(with: request) { data, response, error in
                   if let data = data {
                       let decoder = JSONDecoder()
                       decoder.dateDecodingStrategy = .formatted(AppDelegate.dateFormatter)
                       if let decodedResponse = try? decoder.decode([QAAlert].self, from: data) {
                           DispatchQueue.main.async {
                            // Flat list of data captures ...
                            self.qaAlerts = decodedResponse.self
                            // Create keys on user intials
                               self.uniqueKeys = NSMutableOrderedSet(array: (self.qaAlerts.map { String($0.TLP!) as String }))
                            // For each key in ordered key set, create a section list
                            for key in self.uniqueKeys.array {
                                let group: [QAAlert] = self.qaAlerts.filter {
                                    return (String($0.TLP!)) == key as! String
                                }
                                print(group.count)
                                // Add section to datasource
                                self.dataSource.updateValue(group, forKey: key as! String)
                            }
                            // Render tableview ...
                            self.tableView.reloadData()
                            self.title = "User QA Alerts: \(self.qaAlerts.count)"
                            UIApplication.shared.applicationIconBadgeNumber = self.qaAlerts.count
                            if (self.qaAlerts.count <= 0) {
                                self.dismissAlertView()
                              }
                            }
                      
                       } else {
                        print("Error: JSON Decoding Error")
                    }
                    
                   }
               }.resume()
     }
    
    func InsertAlertSignoffAPI(alertAck: QAAlertAck) {
         
        // http://192.168.33.9:6002/api/routecardservice/InsertAlertAck

        let params = ["QAA_QA_IX": alertAck.QAA_QA_IX,
                      "QAA_QPL_IX": alertAck.QAA_QPL_IX,
                      "QAA_RCH_IX": alertAck.QAA_RCH_IX,
                      "QAA_PRC_IX": alertAck.QAA_PRC_IX,
                      "QAA_QAT_IX": alertAck.QAA_QAT_IX,
                      "QAA_UN_IX": alertAck.QAA_UN_IX,
                      "QAA_By": alertAck.QAA_By ?? ""
                     ] as [String : Any]
           //create the url with URL
           guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/InsertAlertAck") else {
                 print("Invalid URL!")
                 return
           }
           //now create the URLRequest object using the url object
           var request = URLRequest(url: url)
          
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           request.addValue("application/json", forHTTPHeaderField: "Accept")
           request.httpMethod = "POST"
           request.timeoutInterval = 20
           
           do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes)
            // pass dictionary to nsdata object and set it as request body
           } catch let error {
               print(error.localizedDescription)

           }

            URLSession.shared.dataTask(with: request) { data, response, error in
                    if let error = error {
                        print("Something went wrong: \(error)")
                    }
                    if let data = data {
                        if let decodedResponse = try? JSONDecoder().decode(POSTStatus.self, from: data) {
                            DispatchQueue.main.async {
                                if (decodedResponse.StatusCode == 1) {
                                    print("Success: \(decodedResponse.StatusMessage)")
                                    self.doRefresh(self.refreshControl!)
                                }
                            }
                        }
                    }
                    
               }.resume()
       }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // This is count of unique process steps specified by process
        if self.dataSource.count > 0 { return self.dataSource.count }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 210
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.dataSource.count > 0) {
            // Get the key for this section from ordered set
            let sect = self.uniqueKeys.object(at: section)
            // Get matching rows for section
            let group: [QAAlert] = self.qaAlerts.filter {
                return (String($0.TLP!)) == sect as! String
            }
            return group.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.uniqueKeys.count > 0 {
            let prefix = "Process: "
            return "\(prefix)" + "\(self.uniqueKeys.object(at: section))"
        }
        return ""
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QAAlertProcessCell", for: indexPath) as! QAAlertProcessCell

        // Configure the cell...
        let sect = self.uniqueKeys.object(at: indexPath.section)
        let group: [QAAlert] = self.qaAlerts.filter {
            return String($0.TLP!) == sect as! String
        }
        cell.processStepValue.text = "\(group[indexPath.row].PRS_Step_Text!)"
        cell.raisedByValue.text = "\(group[indexPath.row].QA_Raised_By ?? "")"
        
        cell.dateRaisedValue.text =  group[indexPath.row].QA_Raised_Date == nil ? "" : ("\(AppDelegate.dateFormatterNoTime.string(from: group[indexPath.row].QA_Raised_Date!))")
        
        cell.alertDetailValue.text = "\(group[indexPath.row].QA_Alert_Detail!)"

        
        cell.confirmButton.params["currentRow"] = group[indexPath.row]
        cell.confirmButton.addTarget(self, action: #selector(confirmAlert(_: )), for: .touchUpInside)
        
        if (group[indexPath.row].EngAck == true)
        {
            if (group[indexPath.row].QAA_By ?? "" != AppDelegate.UserData.userInitials) {
                cell.confirmButton.isEnabled = true
                cell.confirmButton.backgroundColor = UIColor.systemGray6
                cell.confirmButton.setTitle("User Confirm", for: UIControl.State.normal)
            }
            else {
                cell.confirmButton.isEnabled = false
                cell.confirmButton.backgroundColor = UIColor.systemGreen
                cell.confirmButton.setTitle("Confirmed", for: UIControl.State.normal)
            }
        }
         else
         {
            cell.confirmButton.isEnabled = false
            cell.confirmButton.backgroundColor = UIColor.systemOrange
            cell.confirmButton.setTitle("Awaiting Eng", for: UIControl.State.disabled)
         }

        return cell
    }
    
    @IBAction func doRefresh(_ sender: UIRefreshControl) {
        let font = UIFont.systemFont(ofSize: 16)
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: UIColor.red,
        ]
        let title = NSAttributedString(string: "Refreshing QA Alerts ..", attributes: attributes)
        sender.attributedTitle = title
        self.configureView()
        sender.endRefreshing()
    }
    
    @objc func confirmAlert(_ sender: PassableUIButton) {
        sender.isEnabled = false
        let alert: QAAlert = sender.params["currentRow"] as! QAAlert
        let ack = QAAlertAck(QAA_IX: -1, QAA_QA_IX: alert.QA_IX, QAA_QPL_IX: alert.QPL_IX, QAA_RCH_IX: alert.QPL_RCH_IX, QAA_PRC_IX: slp!.UniqueStepID, QAA_QAT_IX: 1, QAA_UN_IX: AppDelegate.UserData.userID, QAA_By: AppDelegate.UserData.userInitials, QAA_Date: Date())
        self.InsertAlertSignoffAPI(alertAck: ack)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.init(red: 0.5, green: 0.4, blue: 0.6, alpha: 0.4)
        cell!.selectedBackgroundView = backgroundView
    
        
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
