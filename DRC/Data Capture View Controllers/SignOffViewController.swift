//
//  SignOffViewController.swift
//  DRC
//
//  Created by Edward Reilly on 04/05/2021.
//  Copyright © 2021 Norcott. All rights reserved.
//

import UIKit

class SignOffViewController: UITableViewController, UITextFieldDelegate {
    
    @IBOutlet var closeBtn: UIBarButtonItem!
    @IBOutlet var switchLabel: UILabel!

    
    var woc: WOCModel?
    var userRoles = [UserRole]()
    var showCompleted = false

    
    // Flat data array
    var signOffsCaptured = [SignOff]()
    // Grouped dictionary of data
    var dataSource = [String : [SignOff]]()
    // Keys of grouped data
    var uniqueKeys = NSOrderedSet()

    //MARK: - Life Cycle
    
    // Use NSCoder to support dependency injection of objects in to this view
    required init?(coder: NSCoder, wocItem: WOCModel, notification: UNNotification?) {
        super.init(coder: coder)
        self.woc = wocItem
        if let aps = notification?.request.content.userInfo["aps"] as? NSDictionary {
            let alert = aps["alert"] as? NSDictionary
            print(alert as Any)
        }
     }
    
    
    // Use NSCoder to support dependency injection of objects in to this view
    // This is error handler
    required init(coder: NSCoder) {
      fatalError("You must provide a WOC and/or ProcessModel object to this view controller")
    }
    
    @IBAction func dismissAlertView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
        // Register my custom cell
        self.tableView.register(UINib(nibName: "RequestTableCell", bundle: nil), forCellReuseIdentifier: "RequestCell")
        
        // let headerNib = UINib.init(nibName: "CommentTableHeader", bundle: Bundle.main)
        // self.tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "CommentCellHeader")

        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.GetUserRolesAPI(false)
        
        let showCompletedSwitch:UISwitch = UISwitch()
        showCompletedSwitch.isOn = showCompleted
        showCompletedSwitch.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
        let completedToolButton:UIBarButtonItem = UIBarButtonItem(customView: showCompletedSwitch);
        self.navigationItem.leftBarButtonItem = completedToolButton
        
        // Add label for switch
        let font: UIFont = UIFont.boldSystemFont(ofSize: 10)
        self.switchLabel = UILabel(frame: CGRect(x: 150, y:150, width:200, height:0))
        self.switchLabel.text = "Active"
        self.switchLabel.font = font
        self.switchLabel.shadowColor = .lightGray
        
        // Need to add as custom view if adding to UIToolBar ...
        let labelButton: UIBarButtonItem = UIBarButtonItem(customView: self.switchLabel)
        self.navigationItem.leftBarButtonItems?.append(labelButton)
        
        self.configureView()


    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setToolbarHidden(false, animated: animated)

    }
    
    func configureView() {
        // Set up font
        let font = UIFont.systemFont(ofSize: 24)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font : font]
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.darkGray]
        UINavigationBar.appearance().titleTextAttributes = textAttributes

        // Grab the notifications for this user
        if self.woc != nil {
            
            if (showCompleted) {
                  self.switchLabel.text = "All"
               } else
               {
                  self.switchLabel.text = "Active"
            }
            
            
            self.GetUserRequestsAPI(user: AppDelegate.UserData.userID, batch: AppDelegate.AppData.currentWOC.BatchNumber, showCompleted: showCompleted)
            
          
            DispatchQueue.main.async {
                self.tableView.reloadData()
                
            }
        }
    }
    
    @objc func switchChanged(completedSwitch: UISwitch) {
        showCompleted = completedSwitch.isOn
        // Grab the notifications for this user & completed if selected
        self.configureView()
    }
    
    // Invoke delegate method to force some validation of field lengths
    @objc func textChanged(_ sender: Any) {
        let tf = sender as! UITextField
        // Force to uppercase
        tf.text = tf.text?.uppercased()
        var resp : UIResponder! = tf
        while !(resp is UIAlertController) { resp = resp.next }
        let alert = resp as! UIAlertController
        // Has to be >= 4 and a valid serial number
        let translatedSerial = Utils().CallTranslateSerial(serial: tf.text! as NSString)
        if (translatedSerial == "") {
            alert.actions[0].isEnabled = ((tf.text!.count >= 4) && (try! Utils().SerialToIndex(serial: tf.text! as NSString) > 0))
        } else {
            alert.actions[0].isEnabled = ((tf.text!.count >= 4) && (try! Utils().SerialToIndex(serial: translatedSerial as NSString) > 0))
        }
    }
    
    // Show dialog to record serial number & comments
    func presentAlertController(status: String, request: SignOff, woc: WOCModel) {
        let alertController = UIAlertController(title: "First Off - \(status)",
                                                message: "Scan the assembly serial and add comment if required",
                                                preferredStyle: .alert)

        alertController.addTextField { tf in
            tf.placeholder = "Serial"
            tf.isSecureTextEntry = false
            tf.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        }
        
        if (request.RCI_Step == "") {
            
        }
        
        alertController.addTextField { tf in
            tf.placeholder = "Comments"
            tf.isSecureTextEntry = false
        }
        
   
        let continueAction = UIAlertAction(title: "Continue",
                                           style: .default) { [weak alertController] _ in
                                            guard let textFields = alertController?.textFields else { return }
                                            
                                            if let serialText = textFields[0].text,
                                               let commentsText = textFields[1].text
                                            {
                                                print("Serial: \(serialText)")
                                                print("Comments: \(commentsText)")
                                                
                                                self.UpdateUserRequest(serial: serialText, comment: commentsText, status: status, request: request, woc: woc)
                                                /*
                                                let translatedSerial = Utils().CallTranslateSerial(serial: textFields[0].text! as NSString)
                                                serialText = translatedSerial == "" ? serialText : translatedSerial
                                                
                                                // See if a bonded assy
                                                serialText = Utils().CallTopLevelSerial(serial: serialText as NSString)
                                                
                                                // Check format of serial
                                                let psn_ix = try! Utils().SerialToIndex(serial: serialText as NSString)
                                                
                                                let note = RequestNotification(RN_IX: -1, RN_RCH_IX: request.PSO_PCH_IX , RN_PRC_IX: request.PSO_PRC_IX, RN_RCI_IX: request.PSO_RCI_IX, RN_RCI_TTS_IX: -1, RN_UN_IX: AppDelegate.UserData.userID, RN_PDR_IX: -1, RN_Tracking_ID: request.PSO_RN_Tracking_ID!, RN_Date_Added: nil, RN_Status: "Actioned", RN_User_Name: AppDelegate.UserData.userName)
                                                
                                                self.UpdateUserRequestAPI(notification: note)
                                                // request.PSO_PRC_IX
                                                let sign = SignOff(BH_Number: request.BH_Number, BH_PIN: request.BH_PIN, PSO_IX: request.PSO_IX, PSO_RN_Tracking_ID: request.PSO_RN_Tracking_ID, PSO_PRC_IX: request.PSO_PRC_IX, PSO_PCH_IX: request.PSO_PCH_IX, PSO_RCI_IX: request.PSO_RCI_IX, PSO_PRS_Step_IX: request.PSO_PRS_Step_IX, PSO_User: request.PSO_User, UN_Full_Name: request.UN_Full_Name, PSO_Approved_By: AppDelegate.UserData.userID, PSO_PSN_IX: psn_ix, PSO_PSN_Serial: serialText, PSO_Quantity: 1, PSO_Completed: nil, PSO_Comments: commentsText, RN_UN_IX: request.RN_UN_IX, RN_Status: request.RN_Status, RCI_Step: request.RCI_Step, RCI_Instruction: request.RCI_Instruction, PSO_Status: status)
                                                
                                                self.UpdateUserSignoffAPI(signoff: sign)
                                                
                                                // Get the role for recipient
                                    
                                                let role = self.userRoles.filter {
                                                    return ($0.DeviceToken?.isEmpty != nil && $0.UserID == request.PSO_User)
                                                }.first
                                                if let role = role {
                                                  Utils().PushACKMessageAPI(woc: self.woc!, role: role, request: "Request Received", category: "Signoff_ACK")
                                                }
                                                 */
                                                
                                            }
        }


        alertController.addAction(continueAction)
        
        let height:NSLayoutConstraint = NSLayoutConstraint(item: alertController.view!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: self.view.frame.height * 0.30)
         alertController.view.addConstraint(height)
        
        (alertController.actions[0] as UIAlertAction).isEnabled = false
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func UpdateUserRequest(serial: String, comment: String, status: String, request: SignOff, woc: WOCModel)  {
        
        let translatedSerial = Utils().CallTranslateSerial(serial: serial as NSString)
        var serialText = translatedSerial == "" ? serial as String : translatedSerial
        
        // See if a bonded assy
        serialText = Utils().CallTopLevelSerial(serial: serialText as NSString)
        
        // Check format of serial
        let psn_ix = try! Utils().SerialToIndex(serial: serialText as NSString)
        
        let note = RequestNotification(RN_IX: -1, RN_RCH_IX: request.PSO_PCH_IX , RN_PRC_IX: request.PSO_PRC_IX, RN_RCI_IX: request.PSO_RCI_IX, RN_RCI_TTS_IX: -1, RN_UN_IX: AppDelegate.UserData.userID, RN_PDR_IX: -1, RN_Tracking_ID: request.PSO_RN_Tracking_ID!, RN_Date_Added: nil, RN_Status: "Actioned", RN_User_Name: AppDelegate.UserData.userName)
        
        self.UpdateUserRequestAPI(notification: note)
        // request.PSO_PRC_IX
        let sign = SignOff(BH_Number: request.BH_Number, BH_PIN: request.BH_PIN, PSO_IX: request.PSO_IX, PSO_RN_Tracking_ID: request.PSO_RN_Tracking_ID, PSO_PRC_IX: request.PSO_PRC_IX, PSO_PCH_IX: request.PSO_PCH_IX, PSO_RCI_IX: request.PSO_RCI_IX, PSO_PRS_Step_IX: request.PSO_PRS_Step_IX, PSO_User: request.PSO_User, UN_Full_Name: request.UN_Full_Name, PSO_Approved_By: AppDelegate.UserData.userID, PSO_PSN_IX: psn_ix, PSO_PSN_Serial: serialText, PSO_Quantity: 1, PSO_Completed: nil, PSO_Comments: comment, RN_UN_IX: request.RN_UN_IX, UN_Initials: "", RN_Status: request.RN_Status, RCI_Step: request.RCI_Step, RCI_Instruction: request.RCI_Instruction, PSO_Status: status)
        
        self.UpdateUserSignoffAPI(signoff: sign)
        
        // Get the role for recipient

        let role = self.userRoles.filter {
            return ($0.DeviceToken?.isEmpty != nil && $0.UserID == request.PSO_User)
        }.first
        if let role = role {
          Utils().PushACKMessageAPI(woc: self.woc!, role: role, request: "Request Received", category: "Signoff_ACK")
        }
     }
    
    
    // MARK: - Web Services
    
    // Get User Roles
    func GetUserRolesAPI(_ elevated: Bool) {
       // http://192.168.33.9:6002/api/routecardservice/GetUserRoles/true
        
        WebRequest<UserRole>.get(self, service: "GetUserRoles", params:[String(elevated)], success: { result in
            
            self.userRoles = result
            
            for role in self.userRoles {
                print(role.DeviceToken? .description ?? "Not Set")
            }

          })
        
      }

    func GetUserRequestsAPI(user: Int, batch: Int, showCompleted: Bool) {
           // http://192.168.33.9:6002/api/routecardservice/GetSignOffs/104/12345
        signOffsCaptured = [SignOff]()
        // Grouped dictionary of data
        dataSource = [String : [SignOff]]()
        // Keys of grouped data
        uniqueKeys = NSOrderedSet()
           let params = "/\(user)/\(batch)/\(showCompleted)"
           guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/GetSignOffs" + params) else {
                       print("Invalid URL!")
                   return
               }
               let request = URLRequest(url: url)
               URLSession.shared.dataTask(with: request) { data, response, error in
                   if let data = data {
                       let decoder = JSONDecoder()
                       decoder.dateDecodingStrategy = .formatted(AppDelegate.dateFormatter)
                       if let decodedResponse = try? decoder.decode([SignOff].self, from: data) {
                           DispatchQueue.main.async {
                            // Flat list of data captures ...
                            self.signOffsCaptured = decodedResponse.self
                            // Create keys on user intials
                            self.uniqueKeys = NSMutableOrderedSet(array: (self.signOffsCaptured.map { $0.UN_Full_Name! as String }))
                            // For each key in ordered key set, create a section list
                            for key in self.uniqueKeys.array {
                                let group: [SignOff] = self.signOffsCaptured.filter {
                                    return ($0.UN_Full_Name!) == key as! String
                                }
                                print(group.count)
                                // Add section to datasource
                                self.dataSource.updateValue(group, forKey: key as! String)
                            }
                            // Render tableview ...
                            self.tableView.reloadData()
                            self.title = "Requested 1st-offs : \(self.signOffsCaptured.count)"
                            UIApplication.shared.applicationIconBadgeNumber = self.signOffsCaptured.count
                            }
                      
                       } else {
                        print("Error: JSON Decoding Error")
                    }
                    
                   }
               }.resume()
     }
    
    func UpdateUserRequestAPI(notification: RequestNotification) {
         
        // http://192.168.33.9:6002/api/routecardservice/UpdateRequestNotification

        let params = ["RN_IX": notification.RN_IX,
                      "RN_RCH_IX": notification.RN_RCH_IX,
                      "RN_Status": notification.RN_Status,
                      "RN_Tracking_ID": notification.RN_Tracking_ID,
                      "RN_UN_IX": AppDelegate.UserData.userID
                     ] as [String : Any]
           //create the url with URL
           guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/UpdateRequestNotification") else {
                 print("Invalid URL!")
                 return
           }
           //now create the URLRequest object using the url object
           var request = URLRequest(url: url)
          
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           request.addValue("application/json", forHTTPHeaderField: "Accept")
           request.httpMethod = "POST"
           request.timeoutInterval = 20
           
           do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes)
            // pass dictionary to nsdata object and set it as request body
           } catch let error {
               print(error.localizedDescription)

           }

            URLSession.shared.dataTask(with: request) { data, response, error in
                    if let error = error {
                        print("Something went wrong: \(error)")
                    }
                    if let data = data {
                        if let decodedResponse = try? JSONDecoder().decode(POSTStatus.self, from: data) {
                            DispatchQueue.main.async {
                                if (decodedResponse.StatusCode == 1) {
                                    print("Success: \(decodedResponse.StatusMessage)")
                                    self.doRefresh(self.refreshControl!)
                                }
                            }
                        } else {
                            print("Something went wrong!")
                        }
                        
                    }
                    
               }.resume()
       }
    
    func UpdateUserSignoffAPI(signoff: SignOff) {
         
        // http://192.168.33.9:6002/api/routecardservice/UpdateUserSignoff

        let params = ["PSO_IX": signoff.PSO_IX,
                      "PSO_Approved_By": signoff.PSO_Approved_By!,
                      "PSO_PSN_IX": signoff.PSO_PSN_IX ?? -1,
                      "PSO_PSN_Serial": signoff.PSO_PSN_Serial ?? "",
                      "PSO_Quantity": signoff.PSO_Quantity ?? -1,
                      "PSO_Comments": signoff.PSO_Comments ?? "",
                      "PSO_Status": signoff.PSO_Status ?? ""
                     ] as [String : Any]
           //create the url with URL
           guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/UpdateUserSignoff") else {
                 print("Invalid URL!")
                 return
           }
           //now create the URLRequest object using the url object
           var request = URLRequest(url: url)
          
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           request.addValue("application/json", forHTTPHeaderField: "Accept")
           request.httpMethod = "POST"
           request.timeoutInterval = 20
           
           do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes)
            // pass dictionary to nsdata object and set it as request body
           } catch let error {
               print(error.localizedDescription)

           }

            URLSession.shared.dataTask(with: request) { data, response, error in
                    if let error = error {
                        print("Something went wrong: \(error)")
                    }
                    if let data = data {
                        if let decodedResponse = try? JSONDecoder().decode(POSTStatus.self, from: data) {
                            DispatchQueue.main.async {
                                if (decodedResponse.StatusCode == 1) {
                                    print("Success: \(decodedResponse.StatusMessage)")
                                    self.doRefresh(self.refreshControl!)
                                }
                            }
                        }
                    }
                    
               }.resume()
       }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // This is count of unique sections specified by process
        if self.dataSource.count > 0 { return self.dataSource.count }
        return 0
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 238
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.dataSource.count > 0) {
            // Get the key for this section from ordered set
            let sect = self.uniqueKeys.object(at: section)
            // Get matching rows for section
            let group: [SignOff] = self.signOffsCaptured.filter {
                return ($0.UN_Full_Name!) == sect as! String
            }
            return group.count
        }
        return 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestCell", for: indexPath) as! RequestTableCell

        // Configure the cell...
        let sect = self.uniqueKeys.object(at: indexPath.section)
        let group: [SignOff] = self.signOffsCaptured.filter {
            return ($0.UN_Full_Name!) == sect as! String
        }
        cell.batchLbl?.text = "\(group[indexPath.row].BH_Number)"
        cell.pinLbl?.text = "\(group[indexPath.row].BH_PIN)"
        cell.operatorLbl?.text = "\(group[indexPath.row].PSO_User)"
        cell.indexLbl?.text = "\(group[indexPath.row].PSO_IX)"
        cell.processLbl?.text  = group[indexPath.row].RCI_Step
        cell.instructionLbl?.text  = group[indexPath.row].RCI_Instruction
        cell.commentsTextView?.text = group[indexPath.row].PSO_Comments
        cell.serialLbl?.text = group[indexPath.row].PSO_PSN_Serial
        cell.authorisedByLbl?.text = "User: \(group[indexPath.row].UN_Initials ?? "")"
        cell.dateAuthorisedLbl?.text = group[indexPath.row].PSO_Completed != nil ? ("\(AppDelegate.dateFormatterNoTime.string(from: group[indexPath.row].PSO_Completed!))") : "Active"
        
        switch group[indexPath.row].PSO_Status {
        case "Approved":
            cell.statusImage.image = UIImage(named: "Circle_Green")
        case "Failed":
            cell.statusImage.image = UIImage(named: "Circle_Red")
        case "Pending":
            cell.statusImage.image = UIImage(named: "Circle_Orange")
        default:
            cell.statusImage.image = UIImage(named: "Circle_Grey")
        }


        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.uniqueKeys.count > 0 {
            return self.uniqueKeys.object(at: section) as? String
        }
        return ""
    }
        
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {

        // Get current state from data source
        let sect = self.uniqueKeys.object(at: indexPath.section)
        let group: [SignOff] = self.signOffsCaptured.filter {
            return ($0.UN_Full_Name!) == sect as! String
        }
        let request: SignOff = group[indexPath.row]
        print(request.PSO_IX .description)

        // This will hold array of available actions once determined
        var actions = [UIContextualAction]()
            
            
        let actionApprove = UIContextualAction(style: .normal, title: String("Approve"),
                  handler: { (actionApprove, view, completionHandler) in
                  // Update data source when user taps action
                    self.presentAlertController(status: "Approved", request: request, woc: self.woc!)
                  completionHandler(true)
                })
                actionApprove.image = UIImage(systemName: "hand.thumbsup")
                actionApprove.backgroundColor = .systemGreen
            
            actions.append(actionApprove)
           
        let actionFail = UIContextualAction(style: .normal, title: String("Fail"),
                  handler: { (actionFail, view, completionHandler) in
                  // Update data source when user taps action
                    self.presentAlertController(status: "Failed", request: request, woc: self.woc!)
                  completionHandler(true)
                })
                actionFail.image = UIImage(systemName: "hand.thumbsdown")
                actionFail.backgroundColor = .systemRed
            
            actions.append(actionFail)
        
        let actionDismiss = UIContextualAction(style: .normal, title: String("Dismiss"),
                  handler: { (actionFail, view, completionHandler) in
                  // Update data source when user taps action
                    // self.presentAlertController(status: "Dismissed", request: request, woc: self.woc!)
                    self.UpdateUserRequest(serial: "", comment: "Dismissed by User", status: "Dismissed", request: request, woc: self.woc!)
                  completionHandler(true)
                })
                actionDismiss.image = UIImage(systemName: "trash")
                actionDismiss.backgroundColor = .systemBlue
            
            actions.append(actionDismiss)

            
        let configuration = UISwipeActionsConfiguration(actions: actions)
        
           
           
        return configuration
    }

    
    @IBAction func doRefresh(_ sender: UIRefreshControl) {
        let font = UIFont.systemFont(ofSize: 16)
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: UIColor.red,
        ]
        let title = NSAttributedString(string: "Refreshing User Requests ..", attributes: attributes)
        sender.attributedTitle = title
        self.configureView()
        sender.endRefreshing()
        
    }

    
    // Override to support editing the table view.
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}
