//
//  UserConfirmViewController.swift
//  DRC
//
//  Created by Edward Reilly on 06/04/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

class UserConfirmViewController: UIViewController {
    
    @IBOutlet var TTSign1Lbl: UILabel!
    @IBOutlet var TTSign2Lbl: UILabel!
    @IBOutlet var ttsImg1: UIButton!
    @IBOutlet var ttsImg2: UIButton!
    
    @IBOutlet var tts1Processed: UIImageView!
    @IBOutlet var tts2Processed: UIImageView!
    @IBOutlet var auth1Img: UIImageView!
    @IBOutlet var auth2Img: UIImageView!
    
    
    // MARK: - Life Cycle Events

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        self.ttsImg1.tag = processItem?.TTSIndex_1 ?? -1 as Int
        self.ttsImg2.tag = processItem?.TTSIndex_2 ?? -1 as Int
        
        self.ttsImg1.roundCorners(corners: [.allCorners], radius: 6)
        self.ttsImg2.roundCorners(corners: [.allCorners], radius: 6)
     
        
        self.ttsImg1.isEnabled = (processItem!.Authorised_1 == 1) && (processItem!.TS1 < 1)
        self.ttsImg2.isEnabled = (processItem!.Authorised_2 == 1) && (processItem!.TS2 < 1)
        
        self.TTSign1Lbl.text = String(processItem?.TTS_TEAM_1 ?? "N/A")
        self.TTSign2Lbl.text = String(processItem?.TTS_TEAM_2 ?? "N/A")
        
        self.tts1Processed.image = (processItem!.TS1 > 1) ? UIImage(systemName: "checkmark.circle.fill")?.withTintColor(UIColor.systemGreen, renderingMode: .alwaysOriginal) : UIImage(systemName: "checkmark.circle.fill")!.withTintColor(UIColor.red, renderingMode: .alwaysOriginal)
        
        self.tts2Processed.image = (processItem!.TS2 > 1) ? UIImage(systemName: "checkmark.circle.fill")?.withTintColor(UIColor.systemGreen, renderingMode: .alwaysOriginal) : UIImage(systemName: "checkmark.circle.fill")!.withTintColor(UIColor.red, renderingMode: .alwaysOriginal)
        
        self.auth1Img.image = (processItem!.Authorised_1 == 1) ? UIImage(systemName: "person.circle")?.withTintColor(UIColor.systemGreen, renderingMode: .alwaysOriginal) : UIImage(systemName: "person.crop.circle.badge.xmark")!.withTintColor(UIColor.red, renderingMode: .alwaysOriginal)
        
        self.auth2Img.image = (processItem!.Authorised_2 == 1) ? UIImage(systemName: "person.circle")?.withTintColor(UIColor.systemGreen, renderingMode: .alwaysOriginal) : UIImage(systemName: "person.crop.circle.badge.xmark")!.withTintColor(UIColor.red, renderingMode: .alwaysOriginal)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // If in elevated mode then use self.indexPath as refreshing specific process else use entire object
        // NotificationCenter.default.post(name: Notification.Name("refreshSubProcess"), object: self.indexPath)
        if (AppDelegate.UserData.elevatedMode != true) {
        NotificationCenter.default.post(name: Notification.Name("refreshSubProcess"), object: self.processItem)
        } else {
            NotificationCenter.default.post(name: Notification.Name("refreshSubProcess"), object: self.indexPath)
        }
    }
    
    
    // MARK: - Data Object Passed
    
    var processItem: ProcessModel? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    var indexPath: Int?
    
    func configureView() {
        // Set up font
        let font = UIFont.systemFont(ofSize: 24)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font : font]
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.darkGray]
        UINavigationBar.appearance().titleTextAttributes = textAttributes
        // Update the user interface for the detail item.
        // This is a process step level comment
        if let detail = processItem {
            self.title = "Add Confirmatory Signature for Step: \(detail.StepText!)"
        }
        
    }
    
    // MARK: - Actions
    
    @IBAction func TTSAction(_ sender: UIButton) {
        // Disable button to prevent duplicate taps
        sender.isEnabled = false
        // Get current TTS frombutton tag [1 or 2]
        let member: Int = sender.tag
        let data = ScanHistoryModel(PSH_IX: 0, PSH_PSN_IX: processItem!.BatchNumber, PSH_PRC_IX: processItem!.UniqueStepID, PSH_PRS_IX: processItem!.ProcessID, PSH_RCI_IX: processItem!.InstIndex, PSH_BH_IX: processItem!.BatchIndex, PSH_ST_IX: processItem!.ScanTypeIndex,  PSH_Result: 1, PSH_UN_IX: AppDelegate.UserData.userID, PSH_TTS_IX: member, PSH_By: AppDelegate.UserData.userInitials, PSH_Quantity: 1, PSH_DateTime: nil, Device_Name: AppDelegate.UserData.deviceName, PSN_PIN_IX: -1)
        self.PostDataCapture(data)
    }
    

    // MARK: - Web API
    
    // POSTS Scan History Entry Based on Data Capture Type ...
    func PostDataCapture(_ scanHist: ScanHistoryModel) {
        // http://192.168.33.9:6002/api/routecardservice/UserConfirmationCapture
        let semaphore = DispatchSemaphore(value: 1)
        let params = ["PSH_PSN_IX": scanHist.PSH_PSN_IX,
                        "PSH_PRC_IX": scanHist.PSH_PRC_IX,
                        "PSH_PRS_IX": scanHist.PSH_PRS_IX,
                        "PSH_RCI_IX": scanHist.PSH_RCI_IX,
                        "PSH_BH_IX": scanHist.PSH_BH_IX,
                        "PSH_ST_IX": scanHist.PSH_ST_IX,
                        "PSH_Result": scanHist.PSH_Result,
                        "PSH_UN_IX": scanHist.PSH_UN_IX,
                        "PSH_TTS_IX": scanHist.PSH_TTS_IX,
                        "PSH_Quantity": scanHist.PSH_Quantity,
                        "PSH_By": scanHist.PSH_By ?? "",
                        "Device_Name": scanHist.Device_Name ?? "Unknown"
                    ] as [String : Any]
                    //create the url with URL
                    guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/UserConfirmationCapture") else {
                        print("Invalid URL!")
                        return
                    }
                    //now create the URLRequest object using the url object
                    var request = URLRequest(url: url)
                
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.addValue("application/json", forHTTPHeaderField: "Accept")
                    request.httpMethod = "POST"
                    request.timeoutInterval = 20
                    
                    do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
                    } catch let error {
                        self.showAlert("Error Processing", "\(error.localizedDescription)")
                    }
                    URLSession.shared.dataTask(with: request) { data, response, error in
                            if let error = error {
                                self.showAlert("Error Processing", "\(error.localizedDescription)")
                            }
                    guard let data = data else {
                        self.showAlert("Error Processing", "\(String(describing: error))")
                        return
                    }
                    semaphore.signal()
                        if let decodedResponse = try? JSONDecoder().decode(POSTStatus.self, from: data) {
                            DispatchQueue.main.async {
                                if (decodedResponse.StatusCode == 1) {
                                    print("Success: \(decodedResponse.StatusMessage)")
                                    // All done so close screen
                                    self.dismiss(animated: true, completion: nil)
                                } else
                                {
                                self.showAlert("Error Processing", "\(decodedResponse.StatusMessage)")
                                }
                            }
                        }
                    }.resume()
        semaphore.wait()
    }
    
    // MARK: - Alert Dialog
    
    func showAlert(_ title: String, _ body: String) {
        let alertController = UIAlertController(title: title, message: body, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: {
        })
    }
    
    
    

}
