//
//  UserDataEntryController.swift
//  DRC
//
//  Created by Edward Reilly on 31/03/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

class UserDataEntryController: UIViewController  {
    
    // MARK: - Outlets
    
    @IBOutlet var qtyTxt: UITextField!
    @IBOutlet var qtyStepper: UIStepper!
    @IBOutlet var cancelBtn: UIBarButtonItem!
    @IBOutlet var addBtn: UIBarButtonItem!
    @IBOutlet var waitSpinner: UIActivityIndicatorView!
    
    var status =  PreviousBatchInfo(StepText: "", Quantity: 0)
    var enteredQty = 0
    
    
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Ensures we can actually see the toolbar!!!
        self.navigationController?.isToolbarHidden = false
        
        // Get previous scan status
        status = Utils().CallCheckPreviousBatchScan(prcIndex: Int32(processItem!.UniqueStepID), bhIndex: Int32(processItem!.BatchIndex))
        
        self.qtyStepper.maximumValue = Double(self.processItem!.ExpectedScans - self.processItem!.EnteredQuantity)
        
        self.enteredQty = GetDataCaptureAPI(stepIX: processItem!.UniqueStepID, scanType: processItem!.ScanTypeIndex, batchHdrIX: processItem!.BatchIndex)
        
    }
    
    // MARK: - Actions
    
    @IBAction func dismissAlertView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        //NotificationCenter.default.post(name: Notification.Name("refreshUserEntries"), object: false)
        
        // If in elevated mode then use self.indexPath as refreshing specific process else use entire object
        // NotificationCenter.default.post(name: Notification.Name("refreshSubProcess"), object: self.indexPath)
        if (AppDelegate.UserData.elevatedMode != true) {
            // This will refresh main screen
            NotificationCenter.default.post(name: Notification.Name("refreshSubProcess"), object: self.processItem)
            // This will force refresh of user entry screen
            NotificationCenter.default.post(name: Notification.Name("refreshUserEntries"), object: self.processItem)
        } else {
            NotificationCenter.default.post(name: Notification.Name("refreshSubProcess"), object: self.indexPath)
            // This will force refresh of user entry screen
            NotificationCenter.default.post(name: Notification.Name("refreshUserEntries"), object: self.indexPath)
        }
        
    }
    
    @IBAction func addData() {
        // Validate qty to ensure current + amount to add does not exceed previous step count
        if (self.enteredQty + Int(self.qtyStepper.value) <= status.Quantity) || (status.StepText == "No Data")
        {
            let tts: Int = (processItem!.TTSIndex_1 > 1 && processItem!.TS1 > 1) ? processItem!.TTSIndex_2 : processItem!.TTSIndex_1
            let data = ScanHistoryModel(PSH_IX: 0, PSH_PSN_IX: processItem!.BatchNumber, PSH_PRC_IX: processItem!.UniqueStepID, PSH_PRS_IX: processItem!.ProcessID, PSH_RCI_IX: processItem!.InstIndex, PSH_BH_IX: processItem!.BatchIndex, PSH_ST_IX: processItem!.ScanTypeIndex,  PSH_Result: 0, PSH_UN_IX: AppDelegate.UserData.userID, PSH_TTS_IX: tts, PSH_By: AppDelegate.UserData.userInitials, PSH_Quantity: Int(self.qtyStepper.value), PSH_DateTime: nil, Device_Name: AppDelegate.UserData.deviceName, PSN_PIN_IX: -1)
            self.PostDataCapture(data)

        } else {
            self.showAlert("Error", "Only \(String(status.Quantity)) Item(s) seen at: \(String(status.StepText))")
            return
        }
    }
    
    @IBAction func textFieldChanged(_ sender: UITextField) {
        print(sender.text!)
        self.qtyStepper.value = Double(sender.text ?? "1") ?? 1
    }

    
    @IBAction func stepperValueChanged(_ sender: UIStepper) {
        self.qtyTxt.text = String(Int(sender.value))
    }

    
    
    // MARK: - Data Object Passed
    
    var indexPath: Int?
    
    var processItem: ProcessModel? {
        didSet {
            // Update the view.
            print(processItem?.PinNumber ?? "")
        }
    }
    
    
    // MARK: - Alert Dialog
    
    func showAlert(_ title: String, _ body: String) {
        let alertController = UIAlertController(title: title, message: body, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: {
        })
    }
    
    // MARK: - Web API
    
    // POSTS Scan History Entry Based on Data Capture Type ...
    func PostDataCapture(_ scanHist: ScanHistoryModel) {
        // http://192.168.33.9:6002/api/routecardservice/UserDataCapture
        let semaphore = DispatchSemaphore(value: 1)
        let params = ["PSH_PSN_IX": scanHist.PSH_PSN_IX,
                        "PSH_PRC_IX": scanHist.PSH_PRC_IX,
                        "PSH_PRS_IX": scanHist.PSH_PRS_IX,
                        "PSH_RCI_IX": scanHist.PSH_RCI_IX,
                        "PSH_BH_IX": scanHist.PSH_BH_IX,
                        "PSH_ST_IX": scanHist.PSH_ST_IX,
                        "PSH_Result": scanHist.PSH_Result,
                        "PSH_UN_IX": scanHist.PSH_UN_IX,
                        "PSH_TTS_IX": scanHist.PSH_TTS_IX,
                        "PSH_Quantity": scanHist.PSH_Quantity,
                        "PSH_By": scanHist.PSH_By ?? "",
                        "Device_Name": scanHist.Device_Name ?? ""
                    ] as [String : Any]
                    //create the url with URL
                    guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/UserDataCapture") else {
                        print("Invalid URL!")
                        return
                    }
                    //now create the URLRequest object using the url object
                    var request = URLRequest(url: url)
                
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.addValue("application/json", forHTTPHeaderField: "Accept")
                    request.httpMethod = "POST"
                    request.timeoutInterval = 20
                    
                    do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
                    } catch let error {
                        self.showAlert("Error Processing", "\(error.localizedDescription)")
                    }
                    URLSession.shared.dataTask(with: request) { data, response, error in
                            if let error = error {
                                self.showAlert("Error Processing", "\(error.localizedDescription)")
                            }
                    guard let data = data else {
                        self.showAlert("Error Processing", "\(String(describing: error))")
                        return
                    }
                    semaphore.signal()
                        if let decodedResponse = try? JSONDecoder().decode(POSTStatus.self, from: data) {
                            DispatchQueue.main.async {
                                if (decodedResponse.StatusCode == 1) {
                                    print("Success: \(decodedResponse.StatusMessage)")
                                    // All done so close screen
                                    self.dismiss(animated: true, completion: nil)
                                } else
                                {
                                self.showAlert("Error Processing", "\(decodedResponse.StatusMessage)")
                                }
                            }
                        }
                    }.resume()
        semaphore.wait()
    }
    
    // Get data captured for this process step
    func GetDataCaptureAPI(stepIX: Int, scanType: Int, batchHdrIX: Int) -> Int {
           // http://192.168.33.9:6002/api/routecardservice/getScanHistory/3/50241/72
           // getDataCaptured/{scanType}/{batchHdrIDX}/{stepIX}
           // Flat data array
           var dataCaptured = [ScanHistoryModel]()
           var total  = 0
           let params = "/\(stepIX)/\(scanType)/\(batchHdrIX)"
           let semaphore = DispatchSemaphore(value: 0)
           guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/GetScanHistory" + params) else {
                       print("Invalid URL!")
                       return -1
               }
               let request = URLRequest(url: url)
               let task = URLSession.shared.dataTask(with: request) { data, response, error in
                   if let data = data {
                       let decoder = JSONDecoder()
                       decoder.dateDecodingStrategy = .formatted(AppDelegate.dateFormatter)
                       if let decodedResponse = try? decoder.decode([ScanHistoryModel].self, from: data) {
                            // Flat list of data captures ...
                            dataCaptured = decodedResponse.self
                            total = dataCaptured.lazy.compactMap({ $0.PSH_Quantity }).reduce(0, +)
                            semaphore.signal()
                       } else {
                        print("Error: JSON Decoding Error")
                    }
                    
                   }
               }
        task.resume()
        semaphore.wait()
        return total
     }


}
