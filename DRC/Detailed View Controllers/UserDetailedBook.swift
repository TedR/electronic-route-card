//
//  UserDetailedBook.swift
//  DRC
//
//  Created by Edward Reilly on 15/05/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit


class UserDetailedBook: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var pageControl = UIPageControl()
    var orderedViewControllers: [UIViewController] = []
    var processes = [ProcessModel]()
    var userRoles = [UserRole]()
    var woc: WOCModel?
    var process: ProcessModel?

    
    // MARK: - Page Control Delegate Methods
    
    private func configurePageControl()
    {
        if #available(iOS 14.0, *) {
            // self.pageControl.backgroundStyle = .automatic
        } else {
            // Fallback on earlier versions
        }
        self.pageControl.numberOfPages = orderedViewControllers.count
        self.pageControl.currentPage = 0
        self.pageControl.tintColor = .white
        self.pageControl.pageIndicatorTintColor = .systemGray3
        self.pageControl.currentPageIndicatorTintColor = .systemGreen
        self.view.addSubview(self.pageControl)

        self.pageControl.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = self.pageControl.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        // This places page controller (dots) above lower toolbar
        let verticalConstraint = self.pageControl.centerYAnchor.constraint(equalTo: view.bottomAnchor, constant: -64.0)
        let widthConstraint = self.pageControl.widthAnchor.constraint(equalToConstant: 200)
        let heightConstraint = self.pageControl.heightAnchor.constraint(equalToConstant: 20)
        self.view.addConstraints([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
               guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
                return nil
            }
            
            let previousIndex = viewControllerIndex - 1
            
            guard previousIndex >= 0 else {
                return nil
            }
            
            guard orderedViewControllers.count > previousIndex else {
                return nil
            }
            
            return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
                    return nil
                }
                
                let nextIndex = viewControllerIndex + 1
                let orderedViewControllersCount = orderedViewControllers.count

                guard orderedViewControllersCount != nextIndex else {
                    return nil
                }
                
                guard orderedViewControllersCount > nextIndex else {
                    return nil
                }
                
                return orderedViewControllers[nextIndex]
    }
    
    /*
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        if (processes.count > 0) {
        for proc in processes {
            self.addChild(self.newDetailViewController(id: proc.ProcessName ?? "Detail1"))
        }
            return self.orderedViewControllers }
        else {
        return [self.newDetailViewController(id: "Detail1"),
                self.newDetailViewController(id: "Detail2")
            ]
        }
    } ()
 */

    private func newDetailViewController(identifier: String, wocItem: WOCModel, processItem: ProcessModel, roles: [UserRole]) -> UIViewController {
       let vc  = self.storyboard!.instantiateViewController(identifier: identifier, creator: { coder in
        UserView1Controller(coder: coder, wocItem: wocItem, processItem: processItem, roles: self.userRoles)
        })
        return vc
    }
        
    
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = orderedViewControllers.firstIndex(of: pageContentViewController)!

    }
    



    // MARK: - Life Cycle
    
    required init?(coder: NSCoder, wocItem: WOCModel, processItem: ProcessModel) {
        super.init(coder: coder)
        self.woc = wocItem
        self.process = processItem
     }
    
    required init(coder: NSCoder) {
      fatalError("You must provide a WOC and/or ProcessModel and userRoles object to this view controller")
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        dataSource = self
        delegate = self


    
        let exit = UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(dismissView))
        self.navigationItem.rightBarButtonItems = [exit]
        

        // self.GetProcessesAPI(self.woc!.BatchNumber, 0, AppDelegate.UserData.userID, true)
        
        // self.GetFilteredProcessesAPI(self.woc!.BatchNumber, AppDelegate.UserData.userID, true)
        
        // TODO: - If we want to just see authorised processes for current user then set to true else false
        
        let showNonAuth: Bool = false
        
        self.GetSubProcessesAPI(self.woc!.BatchNumber, 0, self.process!.ParentIndex, self.process!.ParentPRCIndex,  AppDelegate.UserData.userID, showNonAuth, self.process!.ProcStatus)
        
        self.GetUserRolesAPI(false)
        
        
    }
    
    
    @objc func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    // MARK: - Web Services
    
    
    // Get User Roles
    func GetUserRolesAPI(_ elevated: Bool) {
       // http://192.168.33.9:6002/api/routecardservice/GetUserRoles/true
        
        WebRequest<UserRole>.get(self, service: "GetUserRoles", params:[String(elevated)], success: { result in
            
            self.userRoles = result
            
            for role in self.userRoles {
                print(role.DeviceToken? .description ?? "Not Set")
            }

          })
        
      }
    
    // Get Process Steps
    func GetSubProcessesAPI(_ batch: Int, _ tlp: Int, _ prsIX: Int, _ prcIX: Int, _ un_ix: Int, _ myProc: Bool, _ procState:Int) {
       // http://192.168.33.9:6002/api/routecardservice/getProcesses/10192/1/104/true
       // GetProcesses/{batch}/{tlp}/{un_ix}/{myProc}
        
        WebRequest<ProcessModel>.get(self, service: "GetProcesses", params:[String(batch), String(tlp), String(un_ix), String(myProc)], success: { result in
                    self.processes = result
                    DispatchQueue.main.async {
                         self.processes = self.processes.filter {
                            if (AppDelegate.UserData.elevatedMode == true) {
                                return ( $0.ParentIndex == prsIX && $0.ParentPRCIndex == prcIX) }
                                    else {
                                        // TODO: - adding commented filter will restrict view to matching process states
                                return ( $0.ParentIndex == prsIX && $0.ParentPRCIndex == prcIX ) // && $0.ProcStatus == procState)
                                }
                         }
                        if (self.processes.count > 0) {
                            for proc in self.processes {
                                self.orderedViewControllers.append(self.newDetailViewController(identifier: "Detail1", wocItem: self.woc!, processItem: proc, roles: [UserRole]()))
                            }
                            
                            if let firstViewController = self.orderedViewControllers.first {
                                self.setViewControllers([firstViewController],
                                                      direction: .forward,
                                                      animated: true,
                                                      completion: nil)
                            }
                            self.configurePageControl()
                        }
                    }
                }
          )
        
     }
    
    func GetFilteredProcessesAPI(_ batch: Int, _ un_ix: Int, _ myProc: Bool) {

        // http://192.168.33.9:6002/api/routecardservice/getFilteredProcesses/10192/104/true
        // GetFilteredProcesses/{batch}/{un_ix}/{myProc}
        
        WebRequest<ProcessModel>.get(self, service: "GetFilteredProcesses", params:[String(batch), String(un_ix), String(myProc)], success: { result in
                    self.processes = result
                    DispatchQueue.main.async {
                        if (self.processes.count > 0) {
                            for proc in self.processes {
                                self.orderedViewControllers.append(self.newDetailViewController(identifier: "Detail1", wocItem: self.woc!, processItem: proc, roles: [UserRole]()))
                            }
                            if let firstViewController = self.orderedViewControllers.first {
                                self.setViewControllers([firstViewController],
                                                      direction: .forward,
                                                      animated: true,
                                                      completion: nil)
                               }
                            self.configurePageControl()
                        }
                    }
                }
         )
        
     }
    
    func GetProcessesAPI(_ batch: Int, _ tlp: Int, _ un_ix: Int, _ myProc: Bool) {

        // http://192.168.33.9:6002/api/routecardservice/getProcesses/10192/1/104/true
        // GetProcesses/{batch}/{tlp}/{un_ix}/{myProc}
        
        WebRequest<ProcessModel>.get(self, service: "GetProcesses", params:[String(batch), String(tlp), String(un_ix), String(myProc)], success: { result in
                    self.processes = result
                    DispatchQueue.main.async {
                        if (self.processes.count > 0) {
                            for proc in self.processes {
                                self.orderedViewControllers.append(self.newDetailViewController(identifier: "Detail1", wocItem: self.woc!, processItem: proc, roles: [UserRole]()))
                            }
                            if let firstViewController = self.orderedViewControllers.first {
                                self.setViewControllers([firstViewController],
                                                      direction: .forward,
                                                      animated: true,
                                                      completion: nil)
                               }
                            self.configurePageControl()
                        }
                    }
                }
         )
        
     }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
