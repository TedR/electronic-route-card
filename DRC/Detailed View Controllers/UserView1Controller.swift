//
//  Detail1ViewController.swift
//  DRC
//
//  Created by Edward Reilly on 15/05/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import MessageUI

class UserView1Controller: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet var pinNumberLbl: UILabel!
    @IBOutlet var descriptionLbl: UILabel!
    @IBOutlet var panelStep: UILabel!
    @IBOutlet var waitSpinner: UIActivityIndicatorView!
    @IBOutlet var bannerView: UIView!
    @IBOutlet var buildTypeLbl: UILabel!
    @IBOutlet var ipcStandardLbl: UILabel!
    @IBOutlet var ipcClassLbl: UILabel!
    @IBOutlet var instTextView: UITextView!
    @IBOutlet var expectedQtyLbl: UILabel!
    @IBOutlet var actualQtyLbl: UILabel!
    @IBOutlet var scanButton: UIButton!
    @IBOutlet var timeStatusButton: UIButton!
    @IBOutlet var timeStopButton: UIButton!
    @IBOutlet var TTS1Lbl: UILabel!
    @IBOutlet var TTS2Lbl: UILabel!
    @IBOutlet var TT1SIG: UILabel!
    @IBOutlet var TT2SIG: UILabel!
    @IBOutlet var stepTextLbl: UILabel!
    @IBOutlet var tts1StatusImg: UIImageView!
    @IBOutlet var tts2StatusImg: UIImageView!
    @IBOutlet var auth1Img: UIImageView!
    @IBOutlet var auth2Img: UIImageView!
    @IBOutlet var scanProgressBar: UIProgressView!
    @IBOutlet var lowerToolBar: UIToolbar!
    
    @IBOutlet var drawingsImg: UIBarButtonItem!
    @IBOutlet var commentsImg: UIBarButtonItem!
    @IBOutlet var reminderImg: UIBarButtonItem!
    @IBOutlet var messageImg: UIBarButtonItem!
    @IBOutlet var shortageImg: UIBarButtonItem!
    @IBOutlet var consumableImg: UIBarButtonItem!
    @IBOutlet var assistButton: UIBarButtonItem!
    @IBOutlet var measureButton: UIBarButtonItem!

    
    var woc: WOCModel?
    var process: ProcessModel?
    var timeStates = [TimeStateModel]()
    var timeReasons = [TimeReasonModel]()
    var currentState: TimeStateModel?
    var userRoles = [UserRole]()
    
    //MARK: - Life Cycle
    
    
    // Use NSCoder to support dependency inection of objects in to this view
    required init?(coder: NSCoder, wocItem: WOCModel,  processItem: ProcessModel, roles: [UserRole]) {
        super.init(coder: coder)
        woc = wocItem
        process = processItem
        userRoles = roles
     }
    
    
    // Use NSCoder to support dependency inection of objects in to this view
    // This is error handler
    required init(coder: NSCoder) {
      fatalError("You must provide a WOC and/or ProcessModel object to this view controller")
    }
    
    
    // More detailed setup of views
    func configureViews(processItem: ProcessModel?) {
        if let processItem = processItem {
            self.pinNumberLbl?.text = processItem.ProcessName
            self.ipcStandardLbl?.text = "IPC-A-610"
            self.instTextView?.text = processItem.Instruction ?? "Not Set"
        
            self.navigationItem.title = String("User Detailed Views: Process - \(processItem.ProcessName)")
            self.stepTextLbl.text = "Works Instruction: " + processItem.StepText!
            
            switch processItem.ScanTypeIndex {
            case 0: self.actualQtyLbl?.text = "-"
                // Added Sample SCAN = 5
            case 1, 2, 5:
                self.actualQtyLbl?.text = String(processItem.ScanCount)
            case 3:
                self.actualQtyLbl?.text = String(processItem.EnteredQuantity)
            case 4:
                self.actualQtyLbl?.text = String(processItem.Signatures)
            default:
                self.actualQtyLbl?.text = "Error"
            }
            
            self.TTS1Lbl?.text = processItem.TTS_TEAM_1
            self.TTS2Lbl?.text = processItem.TTS_TEAM_2
            self.TT1SIG?.text = processItem.TT1SIG
            self.TT2SIG?.text = processItem.TT2SIG
            
            UIView.animate(withDuration: 2.0) {
                self.scanProgressBar.setProgress(Float(truncating: NSNumber(value: Double(truncating: NSDecimalNumber(decimal: processItem.ProgressPercent / 100.00)))), animated: true)
            }
            
            self.actualQtyLbl.textColor = (processItem.ProgressPercent == 100.00 ? .systemGreen : .systemRed)
            // cell.expectedLbl.text = String(slp.ExpectedScans)
            
            // Can we complete the process timer??
            self.completeProcessIfNeeded(processItem)
            
            if let wocItem = woc {
                self.ipcClassLbl?.text =  "\(wocItem.Class)"
                self.descriptionLbl?.text = wocItem.GDescription
                self.panelStep?.text = "\(wocItem.Step)"
                self.expectedQtyLbl?.text = "\(processItem.ExpectedScans)"
               
                switch String((self.woc?.Finish)!) {
                    case "Lead Free":
                        self.bannerView.backgroundColor = .systemGreen
                        self.buildTypeLbl.text = wocItem.Finish
                    case "Tin Lead":
                        self.bannerView.backgroundColor = .systemPink
                        self.buildTypeLbl.text = wocItem.Finish
                    case "Lead Free Aqueous":
                        self.bannerView.backgroundColor = .systemYellow
                        self.buildTypeLbl.text = wocItem.Finish
                    case "Tin Lead Aqueous":
                        self.bannerView.backgroundColor = .systemOrange
                        self.buildTypeLbl.text = wocItem.Finish
                    case "Customer Specific":
                        self.bannerView.backgroundColor = AppDelegate.NorcottLightOrange
                        self.buildTypeLbl.text = wocItem.Finish
                    default:
                        self.bannerView.backgroundColor = .secondarySystemBackground
                        self.buildTypeLbl.text = "Finish Not Set?"
                }
                
            }
            
            switch processItem.ScanTypeIndex {
                case AppDelegate.ScanMode.Process.rawValue :
                    scanButton?.setBackgroundImage(UIImage(systemName: "barcode.viewfinder"), for: .normal)
                    scanButton?.isEnabled = (processItem.Authorised_1 == 1 || processItem.Authorised_2 == 1) ? true : false
                    scanButton?.tintColor = (processItem.Authorised_1 == 1 || processItem.Authorised_2 == 1) ? .systemBlue : .systemRed
                    
                    if (!scanButton.isEnabled) {
                       //Animations().createPulse(button: scanButton, color: .red)
                       //Animations().addPulsation(object: scanButton.layer)
                    }
                // Added Sample = 5
                case AppDelegate.ScanMode.Sample.rawValue :
                    scanButton?.setBackgroundImage(UIImage(systemName: "qrcode.viewfinder"), for: .normal)
                    scanButton?.isEnabled = (processItem.Authorised_1 == 1 || processItem.Authorised_2 == 1) ? true : false
                    scanButton?.tintColor = (processItem.Authorised_1 == 1 || processItem.Authorised_2 == 1) ? .systemBlue : .systemRed
                    
                    if (!scanButton.isEnabled) {
                       //Animations().createPulse(button: scanButton, color: .red)
                       //Animations().addPulsation(object: scanButton.layer)
                }
                case AppDelegate.ScanMode.Inspection.rawValue :
                    scanButton?.setBackgroundImage(UIImage(systemName: "checkmark.square"), for: .normal)
                    scanButton?.isEnabled = (processItem.Authorised_1 == 1 || processItem.Authorised_2 == 1) ? true : false
                    scanButton?.tintColor = (processItem.Authorised_1 == 1 || processItem.Authorised_2 == 1) ? .systemBlue : .systemRed
                    
                    if (!scanButton.isEnabled) {
                       //Animations().createPulse(button: scanButton, color: .red)
                       //Animations().addPulsation(object: scanButton.layer)
                    }
                case AppDelegate.ScanMode.Confirmation.rawValue :
                    scanButton?.setBackgroundImage(UIImage(systemName: "signature"), for: .normal)
                    scanButton?.isEnabled = (processItem.Authorised_1 == 1 || processItem.Authorised_2 == 1) ? true : false
                    scanButton?.tintColor = (processItem.Authorised_1 == 1 || processItem.Authorised_2 == 1) ? .systemBlue : .systemRed
                    
                    if (!scanButton.isEnabled) {
                       //Animations().createPulse(button: scanButton, color: .red)
                       //Animations().addPulsation(object: scanButton.layer)
                    }
                    
                case AppDelegate.ScanMode.Batch.rawValue :
                    scanButton?.setBackgroundImage(UIImage(systemName: "list.number"), for: .normal)
                    scanButton?.isEnabled = (processItem.Authorised_1 == 1 || processItem.Authorised_2 == 1) ? true : false
                    scanButton?.tintColor = (processItem.Authorised_1 == 1 || processItem.Authorised_2 == 1) ? .systemBlue : .systemRed
                    
                    if (!scanButton.isEnabled) {
                       //Animations().createPulse(button: scanButton, color: .red)
                       //Animations().addPulsation(object: scanButton.layer)
                    
                    }
                    
                default:
                    scanButton?.setBackgroundImage(UIImage(systemName: "questionmark.square"), for: .normal)
            }
            
            assistButton?.isEnabled = (processItem.Authorised_1 == 1) ? false : true
            assistButton?.tintColor = (processItem.Authorised_1 == 1) ? .systemGreen : .systemRed
            
            self.tts1StatusImg.image = (processItem.TS1 > 1) ? UIImage(systemName: "checkmark.circle.fill")?.withTintColor(UIColor.systemGreen, renderingMode: .alwaysOriginal) : UIImage(systemName: "xmark.circle.fill")!.withTintColor(UIColor.red, renderingMode: .alwaysOriginal)
            
            self.tts2StatusImg.image = (processItem.TS2 > 1) ? UIImage(systemName: "checkmark.circle.fill")?.withTintColor(UIColor.systemGreen, renderingMode: .alwaysOriginal) : UIImage(systemName: "xmark.circle.fill")!.withTintColor(UIColor.red, renderingMode: .alwaysOriginal)
            
            self.auth1Img.image = (processItem.Authorised_1 == 1) ? UIImage(systemName: "person.circle")?.withTintColor(UIColor.systemGreen, renderingMode: .alwaysOriginal) : UIImage(systemName: "person.crop.circle.badge.xmark")!.withTintColor(UIColor.red, renderingMode: .alwaysOriginal)
            
            self.auth2Img.image = (processItem.Authorised_2 == 1) ? UIImage(systemName: "person.circle")?.withTintColor(UIColor.systemGreen, renderingMode: .alwaysOriginal) : UIImage(systemName: "person.crop.circle.badge.xmark")!.withTintColor(UIColor.red, renderingMode: .alwaysOriginal)
            
            
            // Add event handler for message
            messageImg.action = #selector(ButtonTapped(sender:))
            // Add event handler for comments
            commentsImg.action = #selector(ButtonTapped(sender:))
            // Add event handler for reminder
            reminderImg.action = #selector(ButtonTapped(sender:))
            // Add event handler for shortage
            shortageImg.action = #selector(ButtonTapped(sender:))
            // Add event handler for consumable
            consumableImg.action = #selector(ButtonTapped(sender:))
            if (self.process!.Drawings > 0) {
                // Add event handler for drawings
                drawingsImg.isEnabled = true
                drawingsImg.action = #selector(ButtonTapped(sender:))
            } else {
                drawingsImg.isEnabled = false
            }
            // Add event for Request Assist
            assistButton.action = #selector(ButtonTapped(sender:))
            // Add event for measurement button
            measureButton.action = #selector(ButtonTapped(sender:))
        }
    }
    
    
    // Actions on view load
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("refreshSubProcess"), object: nil)
        
        self.GetTimeMonitorStatesAPI(true)
        self.GetTimeMonitorReasonsAPI(true)
        
        self.configureViews(processItem: process)

    }


    override func viewDidAppear(_ animated: Bool) {
       // self.configureViews(processItem: process)
    }
    
    // This is receiver object for message sent from ScanCaptureViewController or BatchScanViewController
    @objc func methodOfReceivedNotification(notification: Notification) {
        if (notification.object != nil) {
        print("Message Received : \(notification.name)")
        
        if (notification.name.rawValue == "refreshSubProcess") {
           let tempModel  = notification.object as! ProcessModel
            print(tempModel.BatchNumber .description)
            
                // User processes if not in elevated mode
                if (AppDelegate.UserData.elevatedMode != true) {
                    

                    // Refresh data for this process step to keep everything nicely in sync ..
                    self.GetProcessesAPI(tempModel.BatchNumber, 0, AppDelegate.UserData.userID, false)
                    
                    
                }
            
            }
        }
        
     }
    
    // MARK: - Web Services
    
    func PostCommentAPI(_ hdrIX: Int, _ stepIX: Int, _ comment: String) {
          // If requesting all comments against a batch then set stepIX = - 1
          // http://192.168.33.9:6002/api/routecardservice/AddProcessComment

        let params = ["PUC_PCH_IX": hdrIX,
                      "PUC_PRC_IX": stepIX,
                      "PUC_UN_IX": AppDelegate.UserData.userID,
                      "PUC_Comments": comment] as [String : Any]
           //create the url with URL
           guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/AddProcessComment") else {
                 print("Invalid URL!")
                 return
           }
           //now create the URLRequest object using the url object
           var request = URLRequest(url: url)
          
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           request.addValue("application/json", forHTTPHeaderField: "Accept")
           request.httpMethod = "POST"
           request.timeoutInterval = 20
           
           do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
           } catch let error {
               print(error.localizedDescription)

           }

            URLSession.shared.dataTask(with: request) { data, response, error in
                    if let error = error {
                        print("Something went wrong: \(error)")
                    }
                    if let data = data {
                        if let decodedResponse = try? JSONDecoder().decode(POSTStatus.self, from: data) {
                            DispatchQueue.main.async {
                                if (decodedResponse.StatusCode == 1) {
                                    print("Success: \(decodedResponse.StatusMessage)")
                                }
                            }
                        }
                    }
                    
               }.resume()
       }
    
    func GetProcessesAPI(_ batch: Int, _ tlp: Int, _ un_ix: Int, _ myProc: Bool) {

        // http://192.168.33.9:6002/api/routecardservice/getFilteredProcesses/10192/104/true
        // GetFilteredProcesses/{batch}/{un_ix}/{myProc}
        // http://norserver02:6002/api/RouteCardService/GetProcesses/9656/0/104/true
        WebRequest<ProcessModel>.get(self, service: "GetProcesses", params:[String(batch), String(tlp), String(un_ix), String(myProc)], success: { result in
            let processes = result
            // Filter by ParentPRCIndex, UniqueStepID and InstIndex to ensure same item is refreshed
            self.process = processes.filter {
                return ( $0.ParentPRCIndex == self.process?.ParentPRCIndex && $0.UniqueStepID == self.process?.UniqueStepID && $0.InstIndex == self.process?.InstIndex)
            }.first
            print(self.process?.ProcessName ?? "Error!")
            // Now update GUI
            DispatchQueue.main.async {
                // Need to refresh other process related GUi items
                if let processItem = self.process {
                    print(processItem.ProcessName ?? "Error!")
                    self.configureViews(processItem: processItem)
                    
                }
                // Refresh process timer states
                if (processes.count > 0) {
                   self.GetTimeMonitorStatesAPI(true)
                }
            }
        }
         )
        
     }
    
    
    

    // Get Time Monitor States
    func GetTimeMonitorStatesAPI(_ showAll: Bool) {
       // http://192.168.33.9:6002/api/routecardservice/getTimeState/true
       // GetTimeState/{showAll}
        
        WebRequest<TimeStateModel>.get(self, service: "GetTimeState", params:[String(showAll)], success: { result in
                    DispatchQueue.main.async {
                        // Flat list of data captures ...
                        self.timeStates = result
                        
                        if let processItem = self.process {
                            self.currentState = self.timeStates.filter {
                                return ( $0.PTS_State == processItem.ProcStatus )
                            }.first
                                
                                
                            switch self.currentState?.PTS_Description {
                                case "Not Started": 

                                     self.timeStatusButton.setBackgroundImage(UIImage(systemName: "timelapse"), for: .normal)
                                     self.timeStatusButton.tintColor = .systemGray
                                     self.timeStatusButton.tag = self.currentState?.PTS_State ?? 0
                                     // The job may have been completed by another user so disable if progress < 100%
                                     self.timeStatusButton.isEnabled = processItem.ProgressPercent < 100.00
                                     self.timeStatusButton.isUserInteractionEnabled = processItem.ProgressPercent < 100.00
                                    
                                     self.timeStopButton.isEnabled = false
                                     self.timeStopButton.isUserInteractionEnabled = false
                                    
                                case "Active": // Started so option to PAUSE or COMPLETE

                                     self.timeStatusButton.setBackgroundImage(UIImage(systemName: "play.circle.fill"), for: .normal)
                                     self.timeStatusButton.tintColor = .systemGreen
                                     self.timeStatusButton.tag = self.currentState?.PTS_State ?? 0
                                     self.timeStopButton.isEnabled = true
                                     self.timeStopButton.isUserInteractionEnabled = true
                                    
                                case "Paused": // Paused so Option to Resume or Complete

                                    self.timeStatusButton.setBackgroundImage(UIImage(systemName: "pause.circle.fill"), for: .normal)
                                    self.timeStatusButton.tintColor = .systemOrange
                                    self.timeStatusButton.tag = self.currentState?.PTS_State ?? 0
                                    self.timeStopButton.isEnabled = true
                                    self.timeStopButton.isUserInteractionEnabled = true
                                    
                                case "Complete":
                                    self.timeStatusButton.setBackgroundImage(UIImage(systemName: "stop.circle.fill"), for: .normal)
                                    self.timeStatusButton.tintColor = .systemRed
                                    self.timeStatusButton.tag = self.currentState?.PTS_State ?? 0
                                    self.timeStopButton.isEnabled = false
                                    self.timeStopButton.isUserInteractionEnabled = false
                                    self.timeStopButton.isHidden = true
                                default: // Error!
                                    print("Error - Option not Configured")
                                }
                        } // if let process item
                    }
                }
            )
        
      }
    
    // Once a Tracking ID is received back, save to SQL
    func SaveSignOffAPI(_ signoff: SignOff) {
        let params = ["PSO_RN_Tracking_ID": signoff.PSO_RN_Tracking_ID!,
                      "PSO_PRC_IX": signoff.PSO_PRC_IX,
                      "PSO_PCH_IX": signoff.PSO_PCH_IX,
                      "PSO_RCI_IX": signoff.PSO_RCI_IX,
                      "PSO_PRS_Step_IX": signoff.PSO_PRS_Step_IX,
                      "PSO_User": signoff.PSO_User,
                      "PSO_PSN_Serial": "",
                      "PSO_Comments": ""
                     ] as [String : Any]
        
        // create the url with URL
        guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/AddUserSignoff") else {
              print("Invalid URL!")
              return
        }
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        
        do {
         request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
              DispatchQueue.main.async {
                self.showAlert("Error", "Error Encoding message")
             }
        }
        //now create the URLRequest object using the url object
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        request.timeoutInterval = 20
        
        do {
         request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
              DispatchQueue.main.async {
              self.showAlert("Error", "Error Encoding Request")
             }
        }

      
        URLSession.shared.dataTask(with: request) { data, response, error in
        if let error = error {
                  print("Something went wrong: \(error)")
                  DispatchQueue.main.async {
                     self.showAlert("Error", "Error saving Request")
                  }
        }
        guard let data = data else {
              print(String(describing: error))
              DispatchQueue.main.async {
                 self.showAlert("Error", "Error saving Request")
              }
              return
          }
          

         if let decodedResponse = try? JSONDecoder().decode(ServiceReturn.self, from: data) {
             DispatchQueue.main.async {
                if (decodedResponse.statusCode == 1) {
                      print("Request Saved Completion")
                      print("Success: \(decodedResponse.statusMessage)")
                } else {
                  // Display Error
                    print("Request Save Error!")
                    print("Error: \(decodedResponse.statusMessage)")
                }
             }
         }
      }
      .resume()

        
    }
    
    // Once a Tracking ID is received back, save to SQL
    func SaveMessageAPI(_ request: RequestNotification) {
        let params = ["RN_RCH_IX": request.RN_RCH_IX,
                      "RN_PRC_IX": request.RN_PRC_IX,
                      "RN_RCI_IX": request.RN_RCI_IX,
                      "RN_RCI_TTS_IX": request.RN_RCI_TTS_IX,
                      "RN_UN_IX": request.RN_UN_IX,
                      "RN_PDR_IX": request.RN_PDR_IX,
                      "RN_Tracking_ID": request.RN_Tracking_ID,
                      "RN_Status": request.RN_Status,
                      "RN_User_Name": request.RN_User_Name
                     ] as [String : Any]
        
        // create the url with URL
        guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/AddRequestNotification") else {
              print("Invalid URL!")
              return
        }
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        
        do {
         request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
              DispatchQueue.main.async {
                self.showAlert("Error", "Error Encoding message")
             }
        }
        //now create the URLRequest object using the url object
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        request.timeoutInterval = 20
        
        do {
         request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
              DispatchQueue.main.async {
              self.showAlert("Error", "Error Encoding message")
             }
        }

      
        URLSession.shared.dataTask(with: request) { data, response, error in
        if let error = error {
                  print("Something went wrong: \(error)")
                  DispatchQueue.main.async {
                     self.showAlert("Error", "Error saving message")
                  }
        }
        guard let data = data else {
              print(String(describing: error))
              DispatchQueue.main.async {
                 self.showAlert("Error", "Error daving message")
              }
              return
          }
          

             if let decodedResponse = try? JSONDecoder().decode(ServiceReturn.self, from: data) {
                 DispatchQueue.main.async {
                    if (decodedResponse.statusCode == 1) {
                          print("Message Saved Completion")
                          print("Success: \(decodedResponse.statusMessage)")
                    } else {
                      // Display Error
                        print("Message Save Error!")
                        print("Error: \(decodedResponse.statusMessage)")
                    }
                 }
             }
          }
          .resume()

        
    }
    
    // Updates status of request notification
    func UpdateMessageAPI(_ request: RequestNotification) {
        let params = ["RN_IX": request.RN_IX,
                      "RN_RCH_IX": request.RN_RCH_IX,
                      "RN_Status": request.RN_Status,
                      "RN_Tracking_ID": request.RN_Tracking_ID,
                      "RN_UN_IX": request.RN_UN_IX
                     ] as [String : Any]
        
        // create the url with URL
        guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/UpdateRequestNotification") else {
              print("Invalid URL!")
              return
        }
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        
        do {
         request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
              DispatchQueue.main.async {
                self.showAlert("Error", "Error Encoding message")
             }
        }
        //now create the URLRequest object using the url object
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        request.timeoutInterval = 20
        
        do {
         request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
              DispatchQueue.main.async {
              self.showAlert("Error", "Error Encoding message")
             }
        }

      
         URLSession.shared.dataTask(with: request) { data, response, error in
                 if let error = error {
                  print("Something went wrong: \(error)")
                  DispatchQueue.main.async {
                     self.showAlert("Error", "Error saving message")
                  }
                 }
          guard let data = data else {
              print(String(describing: error))
              DispatchQueue.main.async {
                 self.showAlert("Error", "Error daving message")
              }
              return
          }
          

             if let decodedResponse = try? JSONDecoder().decode(ServiceReturn.self, from: data) {
                 DispatchQueue.main.async {
                    if (decodedResponse.statusCode == 1) {
                          print("Message Saved Completion")
                          print("Success: \(decodedResponse.statusMessage)")
                    } else {
                      // Display Error
                        print("Message Save Error!")
                        print("Error: \(decodedResponse.statusMessage)")
                    }
                 }
             }
          }
          .resume()

        
    }
    
    // Send Request for Sign of Using Push Service
    // This needs to be passed the correct device from userRoles table lookup
    func PushMessageAPI(_ process: ProcessModel, _ request: String, _ category: String, _ userRole: UserRole) {
    // http://192.168.33.9:6002/api/routecardservice/PushMessage
        self.waitSpinner.isHidden = false
        self.waitSpinner.startAnimating()
        let msgReq = ["SubTitle": request,
                      "Title": "iERC Notification",
                      "Message": "Please respond to this request",
                      "Category": category,
                      "UserID":  userRole.UserID,
                      "UserName": userRole.UserName,
                      "DeviceToken": userRole.DeviceToken!,
                      "DeviceName": userRole.DeviceName!,
                      "DeviceUUID": userRole.DeviceUUID!
                     ] as [String : Any]
        
        let params = ["BatchNumber": process.BatchNumber,
                      "JobNumber": process.JobNumber,
                      "ParentIndex": process.ParentIndex,
                      "ParentPRCIndex": process.ParentPRCIndex,
                      "UniqueStepID": process.UniqueStepID,
                      "ProcessID": process.ProcessID,
                      "ProcessName": process.ProcessName!,
                      "RouteCarHdrIndex": process.RouteCardHdrIndex,
                      "TTSIndex_1": process.TTSIndex_1,
                      "TTSIndex_2": process.TTSIndex_2,
                      "Message": msgReq
                     ] as [String : Any]
          // create the URL
          // https://notificationapi20210427110435.azurewebsites.net/api/APNSService
          guard let url = URL(string: AppDelegate.UserData.apnsServerIP + "/api/APNSService/PushERCAzureNotification") else {
                print("Invalid URL!")
                return
          }
          //now create the URLRequest object using the url object
          var request = URLRequest(url: url)
         
          request.addValue("application/json", forHTTPHeaderField: "Content-Type")
          request.addValue("application/json", forHTTPHeaderField: "Accept")
          request.httpMethod = "POST"
          request.timeoutInterval = 20
          
          do {
           request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
          } catch let error {
              print(error.localizedDescription)
                DispatchQueue.main.async {
                self.showAlert("Error", "Error sending message")
               }
          }

        
           URLSession.shared.dataTask(with: request) { data, response, error in
                   if let error = error {
                    print("Something went wrong: \(error)")
                    DispatchQueue.main.async {
                       self.showAlert("Error", "Error sending message")
                    }
                   }
            guard let data = data else {
                print(String(describing: error))
                DispatchQueue.main.async {
                    self.showAlert("Error", "Error sending message")
                }
                return
            }
            
 
               if let decodedResponse = try? JSONDecoder().decode(ServiceReturn.self, from: data) {
                   DispatchQueue.main.async {
                      if (decodedResponse.statusCode == 200) {
                        let alertController = UIAlertController(title: "Success",
                                                                message: "Message sent successfully\nTracking ID: " + decodedResponse.statusMessage, preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: "OK", style: .default)
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion: { [self] in
                            print("Message Sent Completion")
                            print("Success: \(decodedResponse.statusMessage)")
                            // Now write the TrackingID to SQL
                            let request = RequestNotification(RN_IX: -1, RN_RCH_IX: process.RouteCardHdrIndex, RN_PRC_IX: process.UniqueStepID, RN_RCI_IX: process.InstIndex, RN_RCI_TTS_IX: process.TTSIndex_1, RN_UN_IX: userRole.UserID, RN_PDR_IX: userRole.DeviceIndex, RN_Tracking_ID: decodedResponse.statusMessage, RN_Date_Added: Date(), RN_Status: "Open", RN_User_Name: userRole.UserName)
                            
                            // PSO_PRS_IX: process.UniqueStepID,
                            let signoff = SignOff(BH_Number: process.BatchNumber, BH_PIN: woc?.PinNumber ?? "", PSO_IX: -1, PSO_RN_Tracking_ID: decodedResponse.statusMessage, PSO_PRC_IX: process.ParentPRCIndex, PSO_PCH_IX: process.RouteCardHdrIndex, PSO_RCI_IX: process.InstIndex, PSO_PRS_Step_IX: process.ProcessID, PSO_User: AppDelegate.UserData.userID, UN_Full_Name: AppDelegate.UserData.userName, PSO_Approved_By: -1, PSO_PSN_IX: -1, PSO_PSN_Serial: "", PSO_Quantity: -1, PSO_Completed: nil, PSO_Comments: "", RN_UN_IX: -1, UN_Initials: "", RN_Status: "Open", RCI_Step: process.StepText, RCI_Instruction: process.Instruction, PSO_Status: "Pending")
                            
                            
                            // print(request)
                            
                            self.SaveMessageAPI(request)
                            
                            // print(signoff)
                            
                            self.SaveSignOffAPI(signoff)
                            
                            
                            self.waitSpinner.stopAnimating()
                            
                        })
                           
                      } else {
                        // Display Error

                        self.showAlert("Error Sending Message\n", decodedResponse.statusMessage)
                      }
                   }
               }
            }
            .resume()
      }
    
    
    // Displays a list of User Request Notifications
    func displayRequestList(_ slp: ProcessModel) {
        // let requestReason: [String] =  [ "Inspection Sign Off", "Query (Frontend)", "Problem (Process)" ]
        var image: String = ""
       
        let alert = UIAlertController(title: "User Assistance", message: "\nSelect Personnel for Request", preferredStyle: UIAlertController.Style.alert)
        alert.view.tintColor = .systemGreen
        // Get roles where device is registerd
        let roles = self.userRoles.filter {
            // return (  ($0.DeviceToken?.isEmpty) != nil && ($0.UserRoleIndex == 4 || $0.UserRoleIndex == 13))
            return (  ($0.DeviceToken?.isEmpty) != nil && ($0.TTSIndex == slp.TTSIndex_1) && ($0.TTSIndex == 4 || $0.TTSIndex == 13) )
        }
        // $0.TTSIndex == slp.TTSIndex_1 &&
        // Loop thru each role
        for role in roles {
            // Add a button for each reason
            let btn = UIAlertAction(title: "\(role.UserInitials) : \(role.UserTitle)", style: UIAlertAction.Style.default) { (action) -> Void in
                      self.PushMessageAPI(slp, "\(role.UserInitials) : \(role.UserTitle)",  "User_Signoff_Request", role)
            }
            switch role.TTSIndex {
                case 4, 13:
                    // Send to QA
                    image = "checkmark.circle"
                default:
                image = "asterisk.circle"

            }
            btn.setValue(UIImage(systemName: image)?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), forKey: "image")
            alert.addAction(btn)
        }
        
        // Default action with nil reason code set
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) { (cancelSelected) -> Void in

        }
        cancelButton.setValue(UIColor.systemRed, forKey: "titleTextColor")
        alert.addAction(cancelButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    // Get Time Monitor States
    func GetTimeMonitorReasonsAPI(_ showAll: Bool) {
        // http://192.168.33.9:6002/api/routecardservice/getTimeReasons/true
        // GetTimeReasons/{showAll}
        
        WebRequest<TimeReasonModel>.get(self, service: "GetTimeReasons", params:[String(showAll)], success: { result in
                    DispatchQueue.main.async {
                         self.timeReasons = result
                    }
                }
            )
        
        
      }
    
    
    // Updates time monitor records
    func PostTimeMonitorAPI(_ timeMonitor: TimeMonitorModel) {
    // http://192.168.33.9:6002/api/routecardservice/AddTimeMonitor

        let params = ["PTM_RCH_IX": timeMonitor.PTM_RCH_IX,
                      "PTM_PRC_IX": timeMonitor.PTM_PRC_IX,
                      "PTM_RCI_IX": timeMonitor.PTM_RCI_IX,
                      "PTM_UN_IX": timeMonitor.PTM_UN_IX,
                      "PTM_PTS_IX": timeMonitor.PTM_PTS_IX ?? 0,
                      "PTM_PTR_IX": timeMonitor.PTM_PTR_IX ?? 0
                 ] as [String : Any]
          //create the url with URL
          guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/AddTimeMonitor") else {
                print("Invalid URL!")
                return
          }
          //now create the URLRequest object using the url object
          var request = URLRequest(url: url)
         
          request.addValue("application/json", forHTTPHeaderField: "Content-Type")
          request.addValue("application/json", forHTTPHeaderField: "Accept")
          request.httpMethod = "POST"
          request.timeoutInterval = 20
          
          do {
           request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
          } catch let error {
              print(error.localizedDescription)
          }

           URLSession.shared.dataTask(with: request) { data, response, error in
                   if let error = error {
                       print("Something went wrong: \(error)")

                   }
            guard let data = data else {
                print(String(describing: error))
                return
            }

               if let decodedResponse = try? JSONDecoder().decode(POSTStatus.self, from: data) {
                   DispatchQueue.main.async {
                      if (decodedResponse.StatusCode == 1) {
                        print("Success: \(decodedResponse.StatusMessage)")
                           
                        // If not in Elevated Mode Force a reload of list on the Master Page ..
                        if (AppDelegate.UserData.elevatedMode == false) {
                            NotificationCenter.default.post(name: Notification.Name("refreshUserCentricProcess"), object: nil)
                            
                            // User processes if not in elevated mode
                            if (AppDelegate.UserData.elevatedMode != true) {

                                // Refresh data for this process step to keep everything nicely in sync ..
                                
                                NotificationCenter.default.post(name: Notification.Name("refreshSubProcess"), object: self.process)
                                
                                
                            }
                        }
                      }
                   }
               }
            }
            .resume()
      }
    
    private func newMeasurementViewController(identifier: String, wocItem: WOCModel, processItem: ProcessModel) -> UIViewController {
       let vc  = self.storyboard!.instantiateViewController(identifier: identifier, creator: { coder in
        MeasurementTabController(coder: coder, wocItem: wocItem, processItem: processItem)
        })
        return vc
    }
    
    // Handles actions required for bottom bar buttona
    @objc func ButtonTapped(sender: UIBarButtonItem)
    {
        let btn: UIBarButtonItem = sender 
        // Full list of comments for this process
        switch btn.title {
        
        case "RequestAssist":
        
        print("Assistance Requested" .description )
            
            
            if let slp: ProcessModel = self.process {
            
                self.displayRequestList(slp)

            }
        
        case "Measurement":
            
            print("Measurement Required" .description )
            
            let vc = self.newMeasurementViewController(identifier: "MeasurementController", wocItem: self.woc!, processItem: self.process!)
            
            // Attach to NAV for presentation
            let nav : UINavigationController = UINavigationController(rootViewController: vc)
            nav.definesPresentationContext = true
            nav.modalPresentationStyle = .formSheet
            self.navigationController?.present(nav, animated: true, completion: nil)

            
        case "Comments":
            let vc = storyboard!.instantiateViewController(withIdentifier: "CommentsViewController") as! CommentsViewController
            if let slp: ProcessModel = self.process  {
                vc.processItem = slp
                let nav : UINavigationController = UINavigationController(rootViewController: vc)
                    self.navigationController?.present(nav, animated: true, completion: nil)
            }
            
        case "Reminder":
             setReminder()
            
        case "Message":
            if let slp: ProcessModel = self.process {
                let alertController = UIAlertController(title: "Send Message to Frontend", message: nil, preferredStyle: .alert)
                let confirmAction = UIAlertAction(title: "Add", style: .default) { (_) in
                    if let txtField = alertController.textFields?.first, let text = txtField.text {
                     // operations
                     print("Message Added: " + text)
                     self.sendEmail(slp, "ERC Message: ", "<p>" + text + "</p>")
                    }
                }
                
                let confirmSaveAction = UIAlertAction(title: "Send & Save", style: .default) { (_) in
                    if let txtField = alertController.textFields?.first, let text = txtField.text {
                      // operations
                      print("Message Added & Saved: " + text)
                     self.sendSaveEmail(slp, "ERC Message: ", text)
                    }
                }
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
                alertController.addTextField { (textField) in
                    textField.placeholder = "Message Body"
                }

                alertController.addAction(confirmAction)
                alertController.addAction(confirmSaveAction)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
                
            }
            
        case "Shortage":
    
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "Shortages") as! ShortagesViewController
            // load for individual process
            vc.processItem = process
            vc.canEdit = true
            let nav : UINavigationController = UINavigationController(rootViewController: vc)
            self.navigationController?.present(nav, animated: true, completion: nil)
            
        case "Consumable":
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "Consumables") as! ConsumablesViewController
            vc.processItem = process
            let nav : UINavigationController = UINavigationController(rootViewController: vc)
            nav.definesPresentationContext = true
            nav.modalPresentationStyle = .fullScreen
            self.navigationController?.present(nav, animated: false, completion: nil)
            
        case "Drawings":
            
            self.performSegue(withIdentifier: "DrawingsCollection", sender: self.process)

                
        default: break
            
        }
        
    }

    
    // Sets a personal reminder
    @objc func setReminder() {
          let alertController = UIAlertController(title: "Add New Reminder", message: "Reminder will be set for 8:30am tomorrow", preferredStyle: .alert)
          let confirmAction = UIAlertAction(title: "Add", style: .default) { (_) in
              if let txtField = alertController.textFields?.first, let text = txtField.text {
                // operations
                print("Reminder Added: " + text)
                self.scheduleLocal("Reminder", text, (self.process?.StepText)!)
              }
          }
          let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
          alertController.addTextField { (textField) in
              textField.placeholder = "Reminder"
          }
          confirmAction.setValue(UIImage(systemName:  "calendar.badge.plus")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
          cancelAction.setValue(UIImage(systemName: "escape")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
          alertController.addAction(confirmAction)
          alertController.addAction(cancelAction)
          self.present(alertController, animated: true, completion: nil)
    }
    
    
    // Schedules local reminders
    @objc func scheduleLocal(_ title: String, _ body: String, _ category: String) {
        let center = UNUserNotificationCenter.current()

        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.categoryIdentifier = category
        content.userInfo = ["customData": AppDelegate.UserData.userID]
        content.sound = UNNotificationSound.default
        
        var dateComponents = DateComponents()
        dateComponents.hour = 8
        dateComponents.minute = 30
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        // let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)

        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        center.add(request)
        
       // center.removeAllPendingNotificationRequests()
    }
    
    
    // Sends email from phyical device
    func sendEmail(_ proc: ProcessModel, _ subject: String, _ body: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            // Live = front.end@norcott.local
            mail.setToRecipients(["front.end@norcott.local"])
            mail.setSubject(subject + "BN: " + String(proc.BatchNumber) + "/" + "JN: " + String(proc.JobNumber) + " - " + " Pin: " + String(proc.PinNumber!) + " - " + " Process: " + String(proc.StepText!))
            mail.setMessageBody(body + "<p><b>" + proc.ProcessName! + " - " + proc.StepText! + "</b></p>", isHTML: true)
            present(mail, animated: true)
        } else {
            self.showAlert("Send Message Error", "Check device is configured to send email")
        }
    }
    
    func sendSaveEmail(_ proc: ProcessModel, _ subject: String, _ body: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            // Live = front.end@norcott.local
            mail.setToRecipients(["front.end@norcott.local"])
            mail.setSubject(subject + "BN: " + String(proc.BatchNumber) + "/" + "JN: " + String(proc.JobNumber) + " - " + " Pin: " + String(proc.PinNumber!) + " - " + " Process: " + String(proc.StepText!))
            mail.setMessageBody("<p>" + body + "</p>" + "<p><b>" + proc.ProcessName! + " - " + proc.StepText! + "</b></p>", isHTML: true)
            present(mail, animated: true)
            self.PostCommentAPI(proc.RouteCardHdrIndex, proc.UniqueStepID, body)
            
        } else {
            self.showAlert("Send Message Error", "Check device is configured to send email")
        }
    }
    
    
    // Handles actions related to tapping of time status button (RHS)
    @IBAction func TimeStateTapped(sender: UIButton) {
        // Cast button
        let btn: UIButton = sender
        // Get current state using tag to lookup index
        let currentState = self.timeStates.filter {
                return ( $0.PTS_State == btn.tag)
        }.first
        // Switch based on state description
        switch currentState?.PTS_Description {
        
            case "Not Started":
            
                print("State: ", currentState?.PTS_Description as Any)
                
                    
                // Get new status required to make Active
                let newState = self.timeStates.filter {
                    return ( $0.PTS_Description == "Active" )
                }.first
                print(String("New State: \(newState?.PTS_Description)"))
                
                // Perform additional validation??
            
                let timeMonitor = TimeMonitorModel(PTM_IX: 0, PTM_RCH_IX: self.process!.RouteCardHdrIndex, PTM_PRC_IX: self.process!.UniqueStepID, PTM_RCI_IX:  self.process!.InstIndex, PTM_UN_IX: AppDelegate.UserData.userID, PTM_Event_Time: nil, PTM_PTS_IX: newState?.PTS_State, PTM_PTR_IX: nil)
                
                self.PostTimeMonitorAPI(timeMonitor)
                
                self.timeStopButton.isEnabled = true
                self.timeStopButton.isUserInteractionEnabled = true

                
 
            case "Active":
            
                print("State: ", currentState?.PTS_Description as Any)
                
                self.displayActionSheet(self.process!)
            
            case "Paused":
                
                print("State: ", currentState?.PTS_Description as Any)
                
                // Get new status required to make Active
                let newState = self.timeStates.filter {
                    return ( $0.PTS_Description == "Active" )
                }.first
                print(String("New State: \(newState?.PTS_Description)"))
                
            
                let timeMonitor = TimeMonitorModel(PTM_IX: 0, PTM_RCH_IX: self.process!.RouteCardHdrIndex, PTM_PRC_IX: self.process!.UniqueStepID, PTM_RCI_IX:  self.process!.InstIndex, PTM_UN_IX: AppDelegate.UserData.userID, PTM_Event_Time: nil, PTM_PTS_IX: newState?.PTS_State, PTM_PTR_IX: nil)
                
                self.PostTimeMonitorAPI(timeMonitor)

        
                
            default: break
            
        }
        
    }
    
    
    // Handles action required for tapping of STOP button
    @IBAction func TimeStopTapped(sender: UIButton) {

        // Get new status required to make Active
        let newState = self.timeStates.filter {
            return ( $0.PTS_Description == "Complete" )
        }.first
        print(String("New State: \(newState?.PTS_Description)"))
        
        // Perform additional validation??
    
        let timeMonitor = TimeMonitorModel(PTM_IX: 0, PTM_RCH_IX: self.process!.RouteCardHdrIndex, PTM_PRC_IX: self.process!.UniqueStepID, PTM_RCI_IX:  self.process!.InstIndex, PTM_UN_IX: AppDelegate.UserData.userID, PTM_Event_Time: nil, PTM_PTS_IX: newState?.PTS_State, PTM_PTR_IX: nil)
        
        self.PostTimeMonitorAPI(timeMonitor)
        
        self.timeStopButton.isHidden = true

    }
    
    
    // Displays a list of stoppage reason codes
    func displayActionSheet(_ slp: ProcessModel) {
        
        
        // Get new status required to make Paused
        let newState = self.timeStates.filter {
            return ( $0.PTS_Description == "Paused" )
        }.first
        
        let alert = UIAlertController(title: "Process Monitor", message: "\nSelect Reason if Extended Delay", preferredStyle: UIAlertController.Style.alert)
    
        alert.view.tintColor = .systemRed
        
        for reason in self.timeReasons {
            // Add a button for each reason
            let btn = UIAlertAction(title: reason.PTR_Reason, style: UIAlertAction.Style.default) { (action) -> Void in
                let timeMonitor = TimeMonitorModel(PTM_IX: 0, PTM_RCH_IX: slp.RouteCardHdrIndex, PTM_PRC_IX: slp.UniqueStepID, PTM_RCI_IX:  slp.InstIndex, PTM_UN_IX: AppDelegate.UserData.userID, PTM_Event_Time: nil, PTM_PTS_IX: newState?.PTS_State, PTM_PTR_IX: reason.PTR_IX)
                self.PostTimeMonitorAPI(timeMonitor)
                
          }
          btn.setValue(UIImage(systemName: "wrench.fill")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), forKey: "image")
          alert.addAction(btn)
        
        }
        // Default action with nil reason code set
        let cancelButton = UIAlertAction(title: "Just Pause", style: UIAlertAction.Style.default) { (cancelSelected) -> Void in
            let timeMonitor = TimeMonitorModel(PTM_IX: 0, PTM_RCH_IX: slp.RouteCardHdrIndex, PTM_PRC_IX: slp.UniqueStepID, PTM_RCI_IX:  slp.InstIndex, PTM_UN_IX: AppDelegate.UserData.userID, PTM_Event_Time: nil, PTM_PTS_IX: newState?.PTS_State, PTM_PTR_IX: nil)
         
            self.PostTimeMonitorAPI(timeMonitor)
        }
        cancelButton.setValue(UIColor.systemOrange, forKey: "titleTextColor")
        alert.addAction(cancelButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    
   // This gets the state and progress for current process and starts the timer if required
    func startProcessIfNeeded(_ slp: ProcessModel) {
        // Shall we start timer?
        // Check current process status and if not started AND count == ZERO then START
        if (self.currentState?.PTS_Description == "Not Started") && (slp.ProgressPercent <= 0.0) {
            // Get new status required to make Active
            let newState = self.timeStates.filter {
                return ( $0.PTS_Description == "Active" )
            }.first
            print(String("New State: \(newState?.PTS_Description)"))
            // Create POST Object
            let timeMonitor = TimeMonitorModel(PTM_IX: 0, PTM_RCH_IX: self.process!.RouteCardHdrIndex, PTM_PRC_IX: self.process!.UniqueStepID, PTM_RCI_IX:  self.process!.InstIndex, PTM_UN_IX: AppDelegate.UserData.userID, PTM_Event_Time: nil, PTM_PTS_IX: newState?.PTS_State, PTM_PTR_IX: nil)
            // Invoke POST to API
            self.PostTimeMonitorAPI(timeMonitor)
            // Enable STOP button to support interactive state changes
            self.timeStopButton.isEnabled = true
            self.timeStopButton.isUserInteractionEnabled = true
        }
    }
    
    // This gets the state and progress for current process and completes the timer if required
     func completeProcessIfNeeded(_ slp: ProcessModel) {
         // Shall we stop timer and complete the process?
         // Check current process status and if not Complete AND ProgressPercent == 100 then STOP
         if (self.currentState?.PTS_Description != "Complete") && (slp.ProgressPercent >= 100.0) {
             // Get new status required to make Complete
             let newState = self.timeStates.filter {
                 return ( $0.PTS_Description == "Complete" )
             }.first
             print(String("New State: \(newState?.PTS_Description)"))
             // Create POST Object
             let timeMonitor = TimeMonitorModel(PTM_IX: 0, PTM_RCH_IX: self.process!.RouteCardHdrIndex, PTM_PRC_IX: self.process!.UniqueStepID, PTM_RCI_IX:  self.process!.InstIndex, PTM_UN_IX: AppDelegate.UserData.userID, PTM_Event_Time: nil, PTM_PTS_IX: newState?.PTS_State, PTM_PTR_IX: nil)
             // Invoke POST to API
             self.PostTimeMonitorAPI(timeMonitor)
             // Disable STOP button
             self.timeStopButton.isEnabled = false
             self.timeStopButton.isUserInteractionEnabled = false
         }
     }
    
    
    // Handles PCBA scanning actions
    @IBAction func ScanButtonTapped(sender: UIButton) {
        print(AppDelegate.UserData.batchQAMode .description)

                
        // Add actions based on scan type
        switch self.process?.ScanTypeIndex {
            // Handles NO SCAN
            case AppDelegate.ScanMode.NoScan.rawValue:
                print("Type: \(self.process?.ScanTypeText ?? "N/A")")
                
                // Start the process if required
                self.startProcessIfNeeded(self.process!)

                
                
            // Handle a production SCAN
            case AppDelegate.ScanMode.Process.rawValue:
                print("Type: \(self.process?.ScanTypeText ?? "N/A")")
                
                // Start the process if required
                self.startProcessIfNeeded(self.process!)
                
                // Now uses modified version of screen to suppport batch scanning
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "BatchScanCapture") as! BatchScanViewController
                vc.processItem = self.process
                // vc.indexPath = Int(sender.tag)
                self.navigationController?.present(vc, animated: true, completion: nil)
            
            // Handle an inspection SCAN
            // Added Sample SCAN
            case AppDelegate.ScanMode.Inspection.rawValue, AppDelegate.ScanMode.Sample.rawValue:
                print("Type: \(self.process?.ScanTypeText ?? "N/A")")
                
                // Start the process if required
                self.startProcessIfNeeded(self.process!)
                
                if (AppDelegate.UserData.batchQAMode == true) {
                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "BatchScanCapture") as! BatchScanViewController
                    vc.processItem = self.process
                    // vc.indexPath = Int(sender.tag)
                    self.navigationController?.present(vc, animated: true, completion: nil)
                } else {
                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "ScanCapture") as! ScanCaptureViewController
                    vc.processItem = self.process
                    // vc.indexPath = Int(sender.tag)
                    self.navigationController?.present(vc, animated: true, completion: nil)
                }
            
            // Handle an interactive User Input Amount
            case AppDelegate.ScanMode.Batch.rawValue:
                print("Type: \(self.process?.ScanTypeText ?? "N/A")")
                
                // Start the process if required
                self.startProcessIfNeeded(self.process!)
                
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "UserDataCapture") as! DataCaptureViewController
                vc.processItem = self.process
                // vc.indexPath = Int(sender.tag)
                
                // Attach to NAV for presentation
                let nav : UINavigationController = UINavigationController(rootViewController: vc)
                nav.definesPresentationContext = true
                nav.modalPresentationStyle = .formSheet
                self.navigationController?.present(nav, animated: true, completion: nil)
            
            // Handle a Confirmatory Signature
            case AppDelegate.ScanMode.Confirmation.rawValue:
                print("Type: \(self.process?.ScanTypeText ?? "N/A")")
                
                // Start the process if required
                self.startProcessIfNeeded(self.process!)
                
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "UserConfirmation") as! UserConfirmViewController
                vc.processItem = self.process
                vc.indexPath = Int(sender.tag)
                // vself.navigationController?.present(vc, animated: true, completion: nil)
                // Attach to NAV for presentation
                let nav : UINavigationController = UINavigationController(rootViewController: vc)
                nav.definesPresentationContext = true
                nav.modalPresentationStyle = .formSheet
                self.navigationController?.present(nav, animated: true, completion: nil)
            
            // It's not been thought of yet!
            default:
                print("Not Valid Case!")
        }
    }
    

    // MARK: - Alert Dialog
    
    // Displays general warning Alert Dialog
    func showAlert(_ title: String, _ body: String) {
        let alertController = UIAlertController(title: title, message: body, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: {
         
        })
    }
    

        
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        let vc = segue.destination as! DrgsCollectionViewController
        vc.title = "Drawings Collection"
        vc.processItem = sender as? ProcessModel

    }

}



