//
//  Utils.swift
//  DRC
//
//  Created by Edward Reilly on 25/03/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import Foundation
import UIKit


class Utils {

 // This hashes the 4 alphanumeric serial to index value
 public func SerialToIndex(serial: NSString) throws -> Int {
    let valid: NSString = "0123456789ABCDEFGHJKLMNPQRSTUVWXYZ?"
    if (serial.length == 4) {
        let c1: Int = valid.range(of: serial.substring(with: NSRange(location: 0, length: 1))).location
        let c2: Int = valid.range(of: serial.substring(with: NSRange(location: 1, length: 1))).location
        let c3: Int = valid.range(of: serial.substring(with: NSRange(location: 2, length: 1))).location
        let c4: Int = valid.range(of: serial.substring(with: NSRange(location: 3, length: 1))).location
        
        
        let (h1Res, overflow1): (Int,Bool) = (c1).multipliedReportingOverflow(by: 34)
        if (overflow1 == true) {
            return 0
        }
        
        let (c2h1, overflowt1): (Int, Bool) = c2.addingReportingOverflow(h1Res)
        if (overflowt1 == true) {
            return 0
        }

        let (h2Res, overflow2): (Int,Bool) = c2h1.multipliedReportingOverflow(by: 34)
        if (overflow2 == true) {
            return 0
        }
        
        let (c3h2, overflowt2): (Int, Bool) = c3.addingReportingOverflow(h2Res)
        if (overflowt2 == true) {
            return 0
        }
        
        let (h3Res, overflow3): (Int,Bool) = c3h2.multipliedReportingOverflow(by: 34)
        if (overflow3 == true) {
            return 0
        }
        
        let (h4Res, overflow4): (Int,Bool) = c4.addingReportingOverflow(h3Res)
        if (overflow4 == true) {
            return 0
        }


        /*
        let h1 = (c1 * 34)
        let h2 = (c2 + h1) * 34
        let h3 = (c3 + h2) * 34
        let h4 = (c4 + h3)
        */
    
        return h4Res
    }
    else
        {
            return 0
        }
    }
    
    
    // Send ACK Request back to user for Sign of Using Push Service
    public func PushACKMessageAPI(woc: WOCModel, role: UserRole, request: String, category: String) {
    // http://192.168.33.9:6002/api/routecardservice/PushMessage
        let msgReq = ["SubTitle": request,
                      "Title": "iERC Notification",
                      "Message": "Notification Acknowledgement From: \(AppDelegate.UserData.userName)",
                      "Category": category,
                      "UserID":  role.UserID,
                      "UserName": role.UserName,
                      "DeviceToken": role.DeviceToken!,
                      "DeviceName": role.DeviceName!,
                      "DeviceUUID": role.DeviceUUID!
                     ] as [String : Any]
        
        let params = ["BatchNumber": woc.BatchNumber,
                      "JobNumber": woc.JobNumber,
                      "Message": msgReq
                     ] as [String : Any]
          // create the URL
          // https://norcottnotificationapi.azurewebsites.net/api/APNSService
          guard let url = URL(string: AppDelegate.UserData.apnsServerIP + "/api/APNSService/PushERCAzureNotification") else {
                print("Invalid URL!")
                return
          }
          //now create the URLRequest object using the url object
          var request = URLRequest(url: url)
         
          request.addValue("application/json", forHTTPHeaderField: "Content-Type")
          request.addValue("application/json", forHTTPHeaderField: "Accept")
          request.httpMethod = "POST"
          request.timeoutInterval = 20
          
          do {
           request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
          } catch let error {
              print(error.localizedDescription)
          }

        
           URLSession.shared.dataTask(with: request) { data, response, error in
                   if let error = error {
                    print("Something went wrong: \(error)")
                   }
            guard let data = data else {
                print(String(describing: error))
                return
            }
            
 
               if let decodedResponse = try? JSONDecoder().decode(ServiceReturn.self, from: data) {
                   DispatchQueue.main.async {
                      if (decodedResponse.statusCode == 200) {
                        print(decodedResponse.statusMessage)
                      } else {
                        // Display Error
                        print(decodedResponse.statusMessage)
                      }
                   }
               }
            }
            .resume()
      }
    
    // Send Request to get back client serial
    func CallTranslateSerial(serial: NSString) -> String {
        //create the url with URL
        guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/TranslateClientSerial") else {
              print("Invalid URL!")
              return String("Invalid URL!")
        }
        let semaphore = DispatchSemaphore(value: 0)
        
        var result: String = ""
        
        let params = ["PSN_Serial": serial
                     ] as [String : Any]
        
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
       
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        request.timeoutInterval = 20
        
        do {
         request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
            print("Invalid Request!")
        }
        
        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
            
            if let decodedResponse = try? JSONDecoder().decode(String.self, from: data!) {
                result = decodedResponse
            }
            semaphore.signal()
        }
        
        task.resume()
        semaphore.wait()
        return result
    }
    
    // Send Request to get back serial
    // Returns base serial or top level if bonded
    func CallTopLevelSerial(serial: NSString) -> String {
        var result: String = ""
        //create the url with URL
        guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/TopLevelSerial") else {
              print("Invalid URL!")
              return result
        }
        let semaphore = DispatchSemaphore(value: 0)
        
        let params = ["PSN_Serial": serial
                     ] as [String : Any]
        
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
       
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        request.timeoutInterval = 20
        
        do {
         request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
            print("Invalid Request!")
        }
        
        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
            
            if let decodedResponse = try? JSONDecoder().decode(String.self, from: data!) {
                result = decodedResponse
            }
            semaphore.signal()
        }
        
        task.resume()
        semaphore.wait()
        return result
    }
    
    /*
    func CallSerialQOB(serial: Int32) -> Int32 {
        let params = String(serial)
        var result: Int32 = 0
        let semaphore = DispatchSemaphore(value: 0)
        guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/SerialQOB/" + params) else {
                print("Invalid URL!")
                return 0
            }

        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 30)
            request.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                if let data = data {
                    if let decodedResponse = try? JSONDecoder().decode(Int32.self, from: data) {
                        result = decodedResponse
                    }
                    semaphore.signal()
            }
         }
        task.resume()
        semaphore.wait()
        return result
    }
     */
    
    func CallPanelMultiplier(serial: Int32) -> PanelMultiplerModel {
        let params = String(serial)
        var result = PanelMultiplerModel(PSN_IX: 0, PSN_QOB: 0, PSN_PIN_IX: 0, PIN_Panel_Number: 0, PIN_Validated: false)
        let semaphore = DispatchSemaphore(value: 0)
        guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/SerialPanelScanMultiplier/" + params) else {
                print("Invalid URL!")
                return result
            }

        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 30)
            request.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                if let data = data {
                    if let decodedResponse = try? JSONDecoder().decode(PanelMultiplerModel.self, from: data) {
                        result = decodedResponse
                    }
                    semaphore.signal()
            }
         }
        task.resume()
        semaphore.wait()
        return result
    }
    
    func CallCheckPreviousScan(prcIndex: Int32, snIndex: Int32, scanType: Int32, panelIndex: Int32) -> PreviousScanInfo {
        let p1 = String(prcIndex)
        let p2 = String(snIndex)
        let p3 = String(scanType)
        let p4 = String(panelIndex)
        var result =  PreviousScanInfo(StepText: "", Scanned: false, Serial: "")
        let semaphore = DispatchSemaphore(value: 0)
        guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/CheckPreviousProcessScan/" + p1 + "/" + p2 + "/" + p3 + "/" + p4) else {
                print("Invalid URL!")
                return result
            }

        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 30)
            request.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                if let data = data {
                    if let decodedResponse = try? JSONDecoder().decode(PreviousScanInfo.self, from: data) {
                        result = decodedResponse
                    }
                    semaphore.signal()
            }
         }
        task.resume()
        semaphore.wait()
        return result
    }
    
    func CallCheckPreviousBatchScan(prcIndex: Int32, bhIndex: Int32) -> PreviousBatchInfo {
        let p1 = String(prcIndex)
        let p2 = String(bhIndex)
        var result =  PreviousBatchInfo(StepText: "", Quantity: 0)
        let semaphore = DispatchSemaphore(value: 0)
        guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/CheckPreviousBatchScan/" + p1 + "/" + p2) else {
                print("Invalid URL!")
                return result
            }

        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 30)
            request.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                if let data = data {
                    if let decodedResponse = try? JSONDecoder().decode(PreviousBatchInfo.self, from: data) {
                        result = decodedResponse
                    }
                    semaphore.signal()
            }
         }
        task.resume()
        semaphore.wait()
        return result
    }
    

    
    

}
