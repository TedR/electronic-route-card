//
//  CommentModel.swift
//  DRC
//
//  Created by Edward Reilly on 28/01/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import Foundation

struct BOMModel: Codable {
    
    let RefDes: String?
    let RP_PIN: String?
    let RP_Original_PIN: String?
    let ClientPart: String?
    let cur_qty: Int
    let SplitQty: Int
    let GPIN_Description: String?
    let PID_Package: String?
    let Manufacturer: String?
    let ManufacturersPartNumber: String?
    let Placement: String?

}

