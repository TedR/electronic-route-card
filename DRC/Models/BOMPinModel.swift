//
//  CommentModel.swift
//  DRC
//
//  Created by Edward Reilly on 28/01/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import Foundation

struct BOMPinModel: Codable {
    
    let `Type`: String?
    let GPIN: String?
    let Alert: String?

}

