//
//  CommentModel.swift
//  DRC
//
//  Created by Edward Reilly on 28/01/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import Foundation

struct CommentModel: Codable {
    
    let PUC_IX: Int
    let PUC_PCH_IX: Int
    let PUC_PRC_IX: Int
    let PUC_UN_IX: Int
    let PUC_Edited: Date
    let PUC_Comments: String?
    let UN_Initials: String?
    let UN_Full_Name: String?
    let TLP: String?
    let SLP: String?

}

