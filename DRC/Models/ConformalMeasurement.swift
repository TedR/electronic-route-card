//
//  ConformalMeasurement.swift
//  DRC
//
//  Created by Edward Reilly on 09/06/2021.
//  Copyright © 2021 Norcott. All rights reserved.
//

import Foundation

struct ConformalMeasurement: Codable {
    
    let PMR_OP_Name: String
    let PMR_OP_ID: Int
    let PMR_Serial: String
    let PMR_Serial_Index: Int
    let PMR_Measurement: Float
    let PMR_Auth_By: String
    let PMR_Added: Date
    
}
