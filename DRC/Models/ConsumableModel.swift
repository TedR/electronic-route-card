//
//  ConsumableModel.swift
//  DRC
//
//  Created by Edward Reilly on 20/04/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import Foundation

struct ConsumableModel: Codable {
    
    let PCI_IX: Int
    let PCI_RCH_IX: Int
    let PCI_PRC_IX: Int
    let PCI_RCI_IX: Int
    let PCI_Pin: String?
    let PCI_Trace: String?
    let PCI_Expiry: Date?
    let PCI_Comments: String?
    let PCI_Department: String?
    let PCI_UN_IX: Int
    let PCI_Date: Date
    let PCI_UN_Initials: String?

}


