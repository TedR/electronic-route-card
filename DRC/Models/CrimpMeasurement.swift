//
//  CrimpMeasurement.swift
//  DRC
//
//  Created by Edward Reilly on 10/06/2021.
//  Copyright © 2021 Norcott. All rights reserved.
//

import Foundation

struct CrimpMeasurement: Codable {
    
    let PMR_Crimp_Type: String
    let PMR_Wire_Type: String
    let PMR_Crimp_Tool: String
    let PMR_Measurement: Float
    let PMR_Added: Date
    let PMR_OP_Name: String
    let PMR_OP_ID: Int
    
}
