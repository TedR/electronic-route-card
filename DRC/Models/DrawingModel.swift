//
//  DrawingModel.swift
//  DRC
//
//  Created by Edward Reilly on 09/04/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import Foundation

struct DrawingModel: Codable {

    let DA_IX: Int
    let DA_RCH_IX: Int
    let DA_Doc_Type: String
    let DA_File_Path: String
    let DA_Origin: String
    let DA_Drawing_No: String?
    let DA_Date: Date?
    let DA_Qty_Issued: Int

}
