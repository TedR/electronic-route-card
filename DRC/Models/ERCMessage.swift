//
//  ERCMessage.swift
//  DRC
//
//  Created by Edward Reilly on 10/05/2021.
//  Copyright © 2021 Norcott. All rights reserved.
//

import Foundation

struct ERCMessage: Codable {
    
    let SubTitle: String
    let Title: String
    let Message: String
    let Category: String
    let UserID: Int
    let UserName: String
    let DeviceToken: String
    let DeviceName: String
    let DeviceUUID: String
    
    
}
