//
//  FeedbackModel.swift
//  DRC
//
//  Created by Edward Reilly on 28/02/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import Foundation

struct FeedbackModel: Codable {
    
    let RCH_IX: Int
    let RCH_Last_Edit_By: String
    let RCH_Last_Edit_Date: Date
    let RCH_Feedback: String
    


}
