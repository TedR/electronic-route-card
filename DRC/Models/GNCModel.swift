//
//  GNCModel.swift
//  DRC
//
//  Created by Edward Reilly on 12/12/2019.
//  Copyright © 2019 Norcott. All rights reserved.
//

import Foundation

struct GNCModel: Codable {
    
    let GNCIndex: Int
    let PinNumber: String?
    let GNCNumber: Int
    let Raised: Date
    let Description: String?
    let PreventiveAction: String?
    let Status: String?
    let Accepted: Bool
    

}
