//
//  ImagesModel.swift
//  DRC
//
//  Created by Edward Reilly on 19/02/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import Foundation

struct ImagesModel: Codable {
    

    let IMG_Pin: String
    let IMG_PMR: String
    let IMG_Side: String
    let IMG_Path: String
    let IMG_Location: String

}

