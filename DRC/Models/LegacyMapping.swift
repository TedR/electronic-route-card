//
//  LegacyMapping.swift
//  DRC
//
//  Created by TechFusion on 31/08/2021.
//  Copyright © 2021 Norcott. All rights reserved.
//

import Foundation


struct LegacyModel: Codable {
    
    let LM_IX: Int
    let LM_Legacy_Process: String?
    let LM_Legacy_Bitfield: Int
    let LM_Legacy_Bitfield_2: Int
    let LM_PRS_Parent_Step_IX: Int
    let LM_PRS_Step_IX: Int
    let LM_Parent_Step_Text: String?
    let LM_PRS_Step_Text: String?

}

