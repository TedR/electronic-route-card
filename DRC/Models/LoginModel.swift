//
//  loginModel.swift
//  DRC
//
//  Created by Edward Reilly on 31/10/2019.
//  Copyright © 2019 Norcott. All rights reserved.
//

import Foundation



struct LoginCredentialsModel: Codable {
    
    let UserInitials: String
    let UserID: Int
    let UserRights: Int
    let DeviceIndex: Int
    let DeviceToken: String
    let DeviceName: String
    let DeviceUUID: String
    let LastLogin: String
    
}
