//
//  Measurement.swift
//  DRC
//
//  Created by Edward Reilly on 08/06/2021.
//  Copyright © 2021 Norcott. All rights reserved.
//

import Foundation

struct Measurement: Codable {
    
    let PMR_IX: Int
    let PMR_PRC_IX: Int?
    let PMR_PRS_IX: Int?
    let PMR_RCI_IX: Int?
    let PMR_BH_IX: Int?
    let PMR_Type: Int?
    let PMR_Data: String?
    
}
