//
//  POSStatus.swift
//  DRC
//
//  Created by Edward Reilly on 20/02/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import Foundation

struct POSTStatus: Codable {
    
    let StatusCode: Int
    let StatusMessage: String

    
}
