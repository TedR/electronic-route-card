//
//  PanelModel.swift
//  DRC
//
//  Created by Edward Reilly on 01/05/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import Foundation

struct LayoutModel: Codable {
    
    let PCB_IX: Int
    let PCB_RCH_IX: Int
    let PCB_Category: String
    let PIT_Item: String
    let PCB_Group: String
    let PCB_Item_Description: String
    let PCB_Item_Number: Int
    let PCB_X_Value: Double
    let PCB_Y_Value: Double
    let PCB_Thickness: Double
    let PCB_Diameter: Double
    let PCB_Style: Int

}
