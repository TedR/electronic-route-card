//
//  PanelMultiplierModel.swift
//  DRC
//
//  Created by TechFusion on 12/06/2023.
//  Copyright © 2023 Norcott. All rights reserved.
//

import Foundation

struct PanelMultiplerModel: Codable {
    
    var PSN_IX: Int
    var PSN_QOB: Int
    var PSN_PIN_IX: Int
    var PIN_Panel_Number: Int;
    var PIN_Validated: Bool;

}
