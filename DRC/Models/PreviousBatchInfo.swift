//
//  PreviousBatchInfo.swift
//  DRC
//
//  Created by TechFusion on 17/05/2023.
//  Copyright © 2023 Norcott. All rights reserved.
//

import Foundation

struct PreviousBatchInfo: Codable {
    
    var StepText: String
    var Quantity: Int

    
}
