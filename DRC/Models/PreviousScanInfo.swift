//
//  PreviousScanInfo.swift
//  DRC
//
//  Created by TechFusion on 16/05/2023.
//  Copyright © 2023 Norcott. All rights reserved.
//

import Foundation

struct PreviousScanInfo: Codable {
    
    var StepText: String
    var Scanned: Bool?
    var Serial: String

    
}
