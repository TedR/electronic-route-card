//
//  ProcessModel.swift
//  DRC
//
//  Created by Edward Reilly on 19/12/2019.
//  Copyright © 2019 Norcott. All rights reserved.
//

import Foundation

struct ProcessModel: Codable {
    
    let PinNumber: String?
    let JobNumber: Int
    let JobIndex: Int
    let BatchNumber: Int
    let BatchIndex: Int
    let RouteCardHdrIndex: Int
    let ParentIndex: Int
    let ParentPRCIndex: Int
    let UniqueStepID: Int
    let ProcessID: Int
    let ProcessName: String?
    let StepText: String?
    let TTS_TEAM_1: String?
    let TTSIndex_1: Int
    let TS1: Int
    let TTS_TEAM_2: String?
    let TTSIndex_2: Int
    let TS2: Int
    let Authorised_1: Int
    let Authorised_2: Int
    let Ordering: Int
    let Position: Int
    let InstIndex: Int
    let Instruction: String?
    let ScanTypeIndex: Int
    let ScanTypeText: String?
    let ExpectedScans: Int
    let ScanCount: Int
    let ProgressPercent: Decimal
    let ActiveTLP: Int
    let ActiveSLP: Int
    let EnteredQuantity: Int
    let ProcStatus: Int
    let Signatures: Int
    let TT1SIG: String?
    let TT2SIG: String?
    let Drawings: Int
    let QAAlerts: Int

}
