//
//  QAAlertModel.swift
//  DRC
//
//  Created by TechFusion on 28/08/2023.
//  Copyright © 2023 Norcott. All rights reserved.
//

import Foundation

struct QAAlertAck: Codable {
    let QAA_IX: Int
    let QAA_QA_IX: Int
    let QAA_QPL_IX: Int
    let QAA_RCH_IX: Int
    let QAA_PRC_IX: Int
    let QAA_QAT_IX: Int
    let QAA_UN_IX: Int
    let QAA_By: String?
    let QAA_Date: Date?
    
}
