//
//  QAAlertModel.swift
//  DRC
//
//  Created by TechFusion on 28/08/2023.
//  Copyright © 2023 Norcott. All rights reserved.
//

import Foundation

struct QAAlert: Codable {
    let QA_IX: Int
    let QA_QAS_IX: Int
    let QA_Alert_Detail: String?
    let QA_Raised_By: String?
    let QA_Raised_Date: Date?
    let QAA_By: String?
    let QAA_Date: Date?
    let QPL_RCH_IX: Int
    let PRS_Step_Text: String?
    let QPL_IX: Int
    let BH_Number: Int
    let TLP: String?
    let EngAck: Bool
}
