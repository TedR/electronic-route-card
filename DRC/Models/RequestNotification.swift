//
//  RequestNotification.swift
//  DRC
//
//  Created by Edward Reilly on 30/04/2021.
//  Copyright © 2021 Norcott. All rights reserved.
//

import Foundation


struct RequestNotification: Codable {
    
    let RN_IX: Int
    let RN_RCH_IX: Int
    let RN_PRC_IX: Int
    let RN_RCI_IX: Int
    let RN_RCI_TTS_IX: Int
    let RN_UN_IX: Int
    let RN_PDR_IX: Int
    let RN_Tracking_ID: String
    let RN_Date_Added: Date?
    let RN_Status: String
    let RN_User_Name: String
    
}

