//
//  ScanHistory.swift
//  DRC
//
//  Created by Edward Reilly on 24/03/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import Foundation

struct ScanHistoryModel: Codable {
    
    let PSH_IX: Int
    let PSH_PSN_IX: Int
    let PSH_PRC_IX: Int
    let PSH_PRS_IX: Int
    let PSH_RCI_IX: Int
    let PSH_BH_IX: Int
    let PSH_ST_IX: Int
    let PSH_Result: Int
    let PSH_UN_IX: Int
    let PSH_TTS_IX: Int
    let PSH_By: String?
    let PSH_Quantity: Int
    let PSH_DateTime: Date?
    let Device_Name: String?
    let PSN_PIN_IX: Int

}
