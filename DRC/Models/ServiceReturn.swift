//
//  ServiceReturn.swift
//  DRC
//
//  Created by Edward Reilly on 29/04/2021.
//  Copyright © 2021 Norcott. All rights reserved.
//

import Foundation

struct ServiceReturn: Codable {
    
    let statusCode: Int
    let statusMessage: String

    
}
