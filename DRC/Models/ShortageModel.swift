//
//  ShortagesModel.swift
//  DRC
//
//  Created by Edward Reilly on 17/03/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import Foundation

struct ShortageModel: Codable {
    

    let CSD_IX: Int
    let CSD_RCH_IX: Int
    let CSD_Pin: String?
    let CSD_Detail: String?
    let CSD_Qty: Int
    let CSD_Location: String?
    let CSD_Confirmed_Fitted: String?
    // Additional Fields
    let CSD_PRS_IX: Int
    let CSD_Parent_PRS_IX: Int
    let CSD_PRC_IX: Int
    let CSD_UN_IX_Reported: Int
    let CSD_UN_Initials_Reported: String?
    let CSD_UN_IX_Fitted_By: Int
    let CSD_Fitted: Date?
    let CSD_Process: String?

}
