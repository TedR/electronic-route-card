//
//  SignOff.swift
//  DRC
//
//  Created by Edward Reilly on 04/05/2021.
//  Copyright © 2021 Norcott. All rights reserved.
//

import Foundation

struct SignOff: Codable {
    
    let BH_Number: Int
    let BH_PIN: String
    let PSO_IX: Int
    let PSO_RN_Tracking_ID: String?
    let PSO_PRC_IX: Int
    let PSO_PCH_IX: Int
    let PSO_RCI_IX: Int
    // let PSO_PRS_IX: Int
    let PSO_PRS_Step_IX: Int
    let PSO_User: Int
    let UN_Full_Name: String?
    let PSO_Approved_By: Int?
    let PSO_PSN_IX: Int?
    let PSO_PSN_Serial: String?
    let PSO_Quantity: Int?
    let PSO_Completed: Date?
    let PSO_Comments: String?
    let RN_UN_IX: Int
    let UN_Initials: String?
    let RN_Status: String?
    let RCI_Step: String?
    let RCI_Instruction: String?
    let PSO_Status: String?
    
    }
    

