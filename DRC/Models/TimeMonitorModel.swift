//
//  TimeMonitorModel.swift
//  DRC
//
//  Created by Edward Reilly on 16/04/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import Foundation

struct TimeMonitorModel: Codable {

    let PTM_IX: Int
    let PTM_RCH_IX: Int
    let PTM_PRC_IX: Int
    let PTM_RCI_IX: Int
    let PTM_UN_IX: Int
    let PTM_Event_Time: Date?
    let PTM_PTS_IX: Int?
    let PTM_PTR_IX: Int?

}
