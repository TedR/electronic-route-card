//
//  TimeReasonModel.swift
//  DRC
//
//  Created by Edward Reilly on 16/04/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import Foundation

struct TimeReasonModel: Codable {

    let PTR_IX: Int
    let PTR_Reason: String
    let PTR_Display: Bool
    let PTR_Order: Int

}
