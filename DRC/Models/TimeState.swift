//
//  TimeState.swift
//  DRC
//
//  Created by Edward Reilly on 17/04/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import Foundation

struct TimeStateModel: Codable {
    
    let PTS_IX: Int
    let PTS_State: Int
    let PTS_Description: String
    let PTS_Display: Bool
    let PTS_Order: Int

}
