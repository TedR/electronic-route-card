//
//  TorqueMeasurement.swift
//  DRC
//
//  Created by Edward Reilly on 10/06/2021.
//  Copyright © 2021 Norcott. All rights reserved.
//

import Foundation

struct TorqueMeasurement: Codable {
    
    let PMR_Driver_Number: String
    let PMR_Torque_Setting: String
    let PMR_Measurement: Float
    let PMR_Added: Date
    let PMR_OP_Name: String
    let PMR_OP_ID: Int
    
}
