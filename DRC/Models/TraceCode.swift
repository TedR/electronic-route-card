//
//  TraceCode.swift
//  DRC
//
//  Created by Edward Reilly on 03/06/2021.
//  Copyright © 2021 Norcott. All rights reserved.
//

import Foundation

struct TraceCode: Codable {
    
    let TC_IX: Int
    let TC_Trace_Code: String
    let TC_PIN: String
    let TC_Date_Code: Date?
    let TC_Shelf_Expiry_Date: Date?
    let TCX_Description: String
    let GPIN_Description: String?
    
}



