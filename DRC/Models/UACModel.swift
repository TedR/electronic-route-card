//
//  UACModel.swift
//  DRC
//
//  Created by Edward Reilly on 10/03/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import Foundation

struct UACModel: Codable {
    let PAC_IX: Int
    let PAC_RCH_IX: Int
    let PAC_PRC_IX: Int
    let PAC_UN_IX: Int
    let PAC_Time: Date
    let PAC_Comment: String
    let PAC_Ref_Number: Int
    
}
