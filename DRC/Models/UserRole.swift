//
//  UserRoles.swift
//  DRC
//
//  Created by Edward Reilly on 28/04/2021.
//  Copyright © 2021 Norcott. All rights reserved.
//

import Foundation

struct UserRole: Codable{
    
    let UserRoleIndex: Int
    let UserInitials: String
    let UserName: String
    let UserTitle: String
    let UserID: Int
    let UserRights: Int
    let DeviceIndex: Int
    let DeviceToken: String?
    let DeviceName: String?
    let DeviceUUID: String?
    let TTSIndex: Int
    let TTSTeam: String?
    
}
