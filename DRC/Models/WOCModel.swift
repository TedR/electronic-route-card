//
//  WOCModel.swift
//  DRC
//
//  Created by Edward Reilly on 12/12/2019.
//  Copyright © 2019 Norcott. All rights reserved.
//

import Foundation


struct WOCModel: Codable {
    
    let PinNumber: String?
    let GPinIndex: Int
    let JobNumber: Int
    let JobIndex: Int
    let BatchNumber: Int
    let BatchIndex: Int
    var RouteCardHdrIndex: Int
    let ClientCode: String?
    let ClientPart: String?
    let FirstSerial: String?
    let BuildDate: Date
    let QMPCreated: Date
    let QMPBy: String?
    let Engineer: String?
    let Quantity: Int
    let BatchComment: String?
    let JobDescription: String?
    let GDescription: String?
    let ClientOrder: String?
    // let Complete: Bool
    let PMR: String?
    let Alert: Bool
    let AlertComment: String?
    let AOI: Bool
    let GNCS: Int
    let ActiveTLP: Int
    let ActiveSLP: Int
    let AlertSLP: Bool
    let Class: Int
    let Step: Int
    let ERCInstructions: String?
    let StockAllocated: Bool
    let Finish: String?
    let Visible: Bool
    let QAAlerts: Int
    
}
