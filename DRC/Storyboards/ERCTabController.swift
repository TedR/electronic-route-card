//
//  ERCTabController.swift
//  DRC
//
//  Created by Edward Reilly on 17/12/2019.
//  Copyright © 2019 Norcott. All rights reserved.
//

import UIKit

class ERCTabController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let btnScreen = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(toggleScreen))
        btnScreen.setBackgroundImage(UIImage(systemName: "uiwindow.split.2x1"), for: .normal, barMetrics: .default)
        self.navigationItem.rightBarButtonItem = btnScreen
        self.splitViewController?.navigationItem.rightBarButtonItem = splitViewController?.displayModeButtonItem
        
        self.tabBarController?.delegate = self
        
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            self.splitViewController?.preferredDisplayMode = .automatic
          
        } else {
            print("Portrait")
             self.splitViewController?.preferredDisplayMode = .primaryOverlay
        }

        
    }
    
    @objc func toggleScreen() {
        
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            self.splitViewController?.preferredDisplayMode = (self.splitViewController?.displayMode != .allVisible ? .allVisible :  .primaryHidden)
          
        } else {
            print("Portrait")
            self.splitViewController?.preferredDisplayMode = (self.splitViewController?.displayMode != .primaryOverlay ? .primaryOverlay :  .primaryHidden)
        }
 
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
          
        } else {
            print("Portrait")
            
        }
    }
    
    // UITabBarControllerDelegate
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if (item.title == "Processes") {
           self.splitViewController?.preferredDisplayMode = .primaryOverlay
            NotificationCenter.default.post(name: Notification.Name("updateProcesses"), object: true)
        } else if (item.title == "WOCS") {
            self.splitViewController?.preferredDisplayMode = .automatic
            NotificationCenter.default.post(name: Notification.Name("updateWOCS"), object: false)
            self.navigationItem.title = String(stringLiteral: "Route Card Processes - \(AppDelegate.AppData.currentWOC.BatchNumber)")
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}
