//
//  SetupViewController.swift
//  DRC
//
//  Created by Edward Reilly on 17/12/2019.
//  Copyright © 2019 Norcott. All rights reserved.
//

import UIKit

class SetupViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet var imageScrollView: UIScrollView!
    @IBOutlet var pageContoller: UIPageControl!
    @IBOutlet var activitySpinner: UIActivityIndicatorView!
    
    @IBOutlet var ipcSTDLbl: UILabel!
    @IBOutlet var ipcClassLbl: UILabel!
    @IBOutlet var stepLbl: UILabel!
    @IBOutlet var buildTypeLbl: UILabel!
    @IBOutlet var descLbl: UITextView!
    @IBOutlet var hdrView: UIView!
    
    
    var images = [ImagesModel]()
    var frame = CGRect.zero
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageContoller.numberOfPages = images.count
        
        // configureView()

        // Do any additional setup after loading the view.
        
        self.imageScrollView.delegate = self
  
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        let subViews = self.imageScrollView.subviews
        for subview in subViews {
            subview.removeFromSuperview()
        }
        
        if let detail = detailItem {
            self.hdrView.isHidden = false
            // Animations().addPulsations(object: self.hdrView.layer, count: 1)
            self.getImagePaths(detail.PinNumber!, detail.PMR!)
            self.descLbl.text = detail.GDescription
            self.ipcClassLbl.text = String(detail.Class)
            self.stepLbl.text = String(detail.Step)
            
            switch String((self.detailItem?.Finish)!) {
                case "Lead Free":
                    self.hdrView.backgroundColor = .systemGreen
                    self.buildTypeLbl.text = detailItem?.Finish
                case "Tin Lead":
                    self.hdrView.backgroundColor = .systemPink
                    self.buildTypeLbl.text = detailItem?.Finish
                case "Lead Free Aqueous":
                    self.hdrView.backgroundColor = .systemYellow
                    self.buildTypeLbl.text = detailItem?.Finish
                case "Tin Lead Aqueous":
                    self.hdrView.backgroundColor = .systemOrange
                    self.buildTypeLbl.text = detailItem?.Finish
                case "Customer Specific":
                    self.hdrView.backgroundColor = AppDelegate.NorcottLightOrange
                    self.buildTypeLbl.text = detailItem?.Finish
                default:
                    self.hdrView.backgroundColor = .secondarySystemBackground
                    self.buildTypeLbl.text = "Finish Not Set?"
            }
        } 
    }

    var detailItem: WOCModel? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    
    // http://localhost:6002/api/routecardservice/getAOIByPMR/MB-MIC-00611_001/PMR015
    func getImagePaths(_ pin: String, _ pmr: String) {
        self.activitySpinner.startAnimating()
        let validPath = pin.replacingOccurrences(of: "/", with: "_")
        let validPMR = "PMR" + String(format: "%03d", Int(pmr)!)
        let params = "/\(validPath)/\(validPMR)"
        guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/getAOIByPMR" + params) else {
                    print("Invalid URL!")
                return
            }

        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 30)
            request.httpMethod = "GET"
            URLSession.shared.dataTask(with: request) { data, response, error in
                if let data = data {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .formatted(AppDelegate.dateFormatter)
                    if let decodedResponse = try? decoder.decode([ImagesModel].self, from: data) {
                        
                        DispatchQueue.main.async {
                         self.images = decodedResponse.self
                         self.activitySpinner.stopAnimating()
                            if (self.images.count > 0) {
                                self.loadImages()
                            }
                         }
                    } else {
                     print("Error: JSON Decoding Error")
                        DispatchQueue.main.async {
                            self.activitySpinner.stopAnimating()
                        }
                 }
                 
                }
            }.resume()
    }

    
    
    private func loadImages() {
        DispatchQueue.main.async {
            self.pageContoller.currentPage = 0
            self.pageContoller.numberOfPages = self.images.count
        }
        for ImagesModel in self.images {
            // print("Pin: \(ImagesModel.IMG_Pin) \(ImagesModel.IMG_Path) \(ImagesModel.IMG_Location)")
            self.getImagePost(ImagesModel.IMG_Path)
        }
    }
    
    
    func getImagePost(_ path: String) {
          self.activitySpinner.startAnimating()
           // This passes credentials of user
           let parameters = ["Param1": path]
           //create the url with URL
           guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/GetImage") else {
                 print("Invalid URL!")
                 return
           }
           //now create the URLRequest object using the url object
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 30)
          
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           request.addValue("application/json", forHTTPHeaderField: "Accept")
           request.httpMethod = "POST"
           
           do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
           } catch let error {
               print(error.localizedDescription)
                DispatchQueue.main.async {
                    self.activitySpinner.stopAnimating()
                }
           }

            URLSession.shared.dataTask(with: request) { data, response, error in
                    
                    if let error = error {
                        print("Something went wrong: \(error)")
                         DispatchQueue.main.async {
                           self.activitySpinner.stopAnimating()
                        }
                    }
                    
                    if let data = data {
                        DispatchQueue.main.async {
                  
                            self.frame.origin.x = self.imageScrollView.frame.size.width * CGFloat(self.pageContoller.currentPage)
                            self.frame.size = self.imageScrollView.frame.size
                            let imgView = UIImageView(frame: self.frame)
                            imgView.image = UIImage(data: data)
                            imgView.contentMode = .scaleAspectFit
                            imgView.translatesAutoresizingMaskIntoConstraints = true
                            self.imageScrollView.addSubview(imgView)
                            
                            self.imageScrollView.contentSize = CGSize(width: (self.imageScrollView.frame.size.width * CGFloat(self.images.count)), height: self.imageScrollView.frame.size.height)
                            let txtView = UITextView(frame: CGRect(x: self.imageScrollView.frame.size.width * CGFloat(self.pageContoller.currentPage), y: 0, width: self.imageScrollView.frame.size.width, height: 24.0))
                            if (self.images.count > 0) {
                            txtView.text = self.images[Int(self.pageContoller.currentPage)].IMG_Pin + " : " + self.images[Int(self.pageContoller.currentPage)].IMG_Side + " : PMR " +
                                self.images[Int(self.pageContoller.currentPage)].IMG_PMR + " : Location " +
                                self.images[Int(self.pageContoller.currentPage)].IMG_Location
                            }
                            
                            /*
                            let centerXConst = NSLayoutConstraint(item: imgView, attribute: .centerX, relatedBy: .equal, toItem: self.imageScrollView, attribute: .centerX, multiplier: 1.0, constant: 0.0)
                            let centerYConst = NSLayoutConstraint(item: imgView, attribute: .centerY, relatedBy: .equal, toItem: self.imageScrollView, attribute: .centerY, multiplier: 1.0, constant: 0.0)
                            
                            let heightConst = NSLayoutConstraint(item: imgView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: self.imageScrollView.frame.size.height)
                            let widthConst = NSLayoutConstraint(item: imgView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: self.imageScrollView.frame.size.width)
                            
                            let heightConst2 = NSLayoutConstraint(item: imgView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: self.view.frame.size.height)
                            let widthConst2 = NSLayoutConstraint(item: imgView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: self.view.frame.size.width)
                            self.imageScrollView.addConstraints([heightConst2, widthConst2])
                            imgView.addConstraints([heightConst, widthConst])
                            NSLayoutConstraint.activate([centerXConst, centerYConst])
                            */
                            
                            self.imageScrollView.addSubview(txtView)
                            
                            self.pageContoller.currentPage += 1

                            self.activitySpinner.stopAnimating()
                            }
                            
                        }
                    
               }.resume()
       }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = self.imageScrollView.contentOffset.x / self.imageScrollView.frame.size.width
        self.pageContoller.currentPage = Int(pageNumber)

    }


    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
