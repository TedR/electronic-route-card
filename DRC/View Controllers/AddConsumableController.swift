//
//  AddConsumableController.swift
//  DRC
//
//  Created by Edward Reilly on 22/04/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit
import AVFoundation
import Combine
import Swift
import UserNotifications



// TODO: - Show Percentage % Value on process views

// TODO: - Need to refresh USER JOB VIEW when changing status of job



class AddConsumableController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var pinNumberTxt: UITextField!
    @IBOutlet weak var traceCodeTxt: UITextField!
    @IBOutlet weak var dateCodeTxt: UITextField!
    @IBOutlet weak var expiryDateTxt: UITextField!
    @IBOutlet weak var dateAddedTxt: UITextField!
    @IBOutlet weak var commentsTxtView: UITextView!
    
    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    // MARK: - Life Cycle
    
    var processItem: ProcessModel? {
        didSet {
            // Update the view.
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          textField.text = (textField.text! as NSString).replacingCharacters(in: range, with: string.uppercased())
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Responds to Return key
        textField.resignFirstResponder()
        self.LookupTraceCode(sender: textField)
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.dateAddedTxt.text = AppDelegate.dateFormatter.string(from: Date())
        self.expiryDateTxt.setInputViewDatePicker(target: self, selector: #selector(tapDone))
        self.traceCodeTxt.becomeFirstResponder()
        self.traceCodeTxt.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.post(name: Notification.Name("refreshConsumables"), object: false)
    }
    


    @IBAction func LookupTraceCode(sender: UITextField) {
        // Call service to get details from trace code
        self.GetTraceCodeAPI(sender.text!)
    }
    
    @objc func tapDone() {
        if let datePicker = self.expiryDateTxt.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            self.expiryDateTxt.text = dateFormatter.string(from: datePicker.date)
        }
        self.expiryDateTxt.resignFirstResponder()
    }
    
    @IBAction func addItem(_ sender: UIButton) {
        (sender as UIButton).isEnabled = false
        // Add validation for expiry date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"

        let expiry = (self.expiryDateTxt.text != "") ? dateFormatter.date(from: self.expiryDateTxt.text!) : nil
        let consumable = ConsumableModel(PCI_IX: 0, PCI_RCH_IX: processItem!.RouteCardHdrIndex, PCI_PRC_IX: processItem!.UniqueStepID, PCI_RCI_IX: processItem!.InstIndex, PCI_Pin: self.pinNumberTxt.text, PCI_Trace: self.traceCodeTxt.text, PCI_Expiry: expiry, PCI_Comments: self.commentsTxtView.text, PCI_Department: processItem?.ProcessName, PCI_UN_IX: AppDelegate.UserData.userID, PCI_Date: Date(), PCI_UN_Initials: nil)

        self.PostConsumableAPI(consumable)
        
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Web API Calls
       
    // Add Consumable Item
    func PostConsumableAPI(_ consumable: ConsumableModel) {
    // http://192.168.33.9:6002/api/routecardservice/AddConsumable
    let semaphore = DispatchSemaphore(value: 1)
    let params = ["PCI_RCH_IX": consumable.PCI_RCH_IX,
                    "PCI_PRC_IX": consumable.PCI_PRC_IX,
                    "PCI_RCI_IX": consumable.PCI_RCI_IX,
                    "PCI_Pin": consumable.PCI_Pin ?? "Not Specified",
                    "PCI_Trace": consumable.PCI_Trace ?? "N/A",
                    "PCI_Expiry": consumable.PCI_Expiry == nil ? "" : AppDelegate.dateFormatter.string(from: consumable.PCI_Expiry!),
                    "PCI_Comments": consumable.PCI_Comments ?? "",
                    "PCI_Department": consumable.PCI_Department ?? "",
                    "PCI_UN_IX": consumable.PCI_UN_IX,
                    "PCI_Date": AppDelegate.dateFormatter.string(from: consumable.PCI_Date)
                    ] as [String : Any]
                //create the url with URL
                guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/AddConsumable") else {
                    print("Invalid URL!")
                    self.addButton.isEnabled = true
                    return
                }
                //now create the URLRequest object using the url object
                var request = URLRequest(url: url)
            
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.httpMethod = "POST"
                request.timeoutInterval = 20
                
                do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
                } catch let error {
                    print(error.localizedDescription)
                    self.addButton.isEnabled = true
                }

                URLSession.shared.dataTask(with: request) { data, response, error in
                        if let error = error {
                            print("Something went wrong: \(error)")
                            self.addButton.isEnabled = true
                        }
                guard let data = data else {
                    print(String(describing: error))
                    return
                }
                semaphore.signal()
                    if let decodedResponse = try? JSONDecoder().decode(POSTStatus.self, from: data) {
                        DispatchQueue.main.async {
                            if (decodedResponse.StatusCode == 1) {
                                print("Success: \(decodedResponse.StatusMessage)")
                                self.addButton.isEnabled = true
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    }
                }
                .resume()
                semaphore.wait()
            }
    
    
    // Get Details from scanned Trace Code to pre-populate Dialog Screen fields ...
    func GetTraceCodeAPI(_ barCode: String) {
         // http://192.168.33.9:6002/api/routecardservice/GetTraceCode
         // Trace code is 4 characters embedded in barcode:
         // Char 7 = T and precedes trace code
        let startIndex = barCode.index(barCode.startIndex, offsetBy: 7)
        let endIndex = barCode.index(startIndex, offsetBy: 4)
        let traceCode = barCode[startIndex..<endIndex]
        let semaphore = DispatchSemaphore(value: 1)
        let params = ["TC_Trace_Code": traceCode,
                     ] as [String : Any]
              //create the url with URL
              guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/GetTraceCode") else {
                    print("Invalid URL!")
                    self.addButton.isEnabled = true
                    return
              }
              //now create the URLRequest object using the url object
              var request = URLRequest(url: url)
             
              request.addValue("application/json", forHTTPHeaderField: "Content-Type")
              request.addValue("application/json", forHTTPHeaderField: "Accept")
              request.httpMethod = "POST"
              request.timeoutInterval = 20
              
              do {
               request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes)
               // pass dictionary to nsdata object and set it as request body
              } catch let error {
                  print(error.localizedDescription)
                  self.addButton.isEnabled = true
              }

               URLSession.shared.dataTask(with: request) { data, response, error in
                    if let error = error {
                           print("Something went wrong: \(error)")
                           self.addButton.isEnabled = true
                    }
                guard let data = data else {
                    print(String(describing: error))
                    return
                }
                semaphore.signal()

                
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .formatted(AppDelegate.dateFormatter)
                
                do {
                    let decodedResponse = try decoder.decode([TraceCode].self, from: data)
                    print(decodedResponse)
                } catch let error {
                    print(error)
                    self.addButton.isEnabled = true
                }
                
                   if let decodedResponse = try? decoder.decode([TraceCode].self, from: data) {
                       DispatchQueue.main.async {
                        if (decodedResponse.count > 0) {
                            print("Success: \(decodedResponse)")
                            self.addButton.isEnabled = true
                            self.pinNumberTxt.text = decodedResponse.first?.TC_PIN
                            self.commentsTxtView.text = decodedResponse.first?.GPIN_Description
                            self.traceCodeTxt.text = String(traceCode)
                            if (decodedResponse.first?.TC_Date_Code != nil) {
                                self.dateCodeTxt.text = ("\(AppDelegate.dateFormatterNoTime.string(from: (decodedResponse.first?.TC_Date_Code)!))")
                            } else {
                                self.dateCodeTxt.text = ""
                            }
                            if (decodedResponse.first?.TC_Shelf_Expiry_Date != nil) {
                                self.expiryDateTxt.text = ("\(AppDelegate.dateFormatterNoTime.string(from: (decodedResponse.first?.TC_Shelf_Expiry_Date)!))")
                            } else {
                                self.expiryDateTxt.text = ""
                            }
                        } else {
                            // Not found alert!
                            self.pinNumberTxt.text = ""
                            self.commentsTxtView.text = ""
                            self.dateCodeTxt.text = ""
                            self.expiryDateTxt.text = ""
                            self.traceCodeTxt.becomeFirstResponder()
                            self.showAlert("Trace Code Warning", "Trace Code: \(traceCode) Not Found")
                        }
                    }
                  }
                }
                .resume()
                semaphore.wait()
    }
    
    
    
    // MARK: - Alert Dialog
    
    func showAlert(_ title: String, _ body: String) {
        let alertController = UIAlertController(title: title, message: body, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: {
        })
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
