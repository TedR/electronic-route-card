//
//  BOMViewController.swift
//  DRC
//
//  Created by TechFusion on 30/10/2023.
//  Copyright © 2023 Norcott. All rights reserved.
//

import UIKit
import simd

class BOMViewController: UITableViewController {
    
    var toolBar: UIToolbar!
    
    var activeTab: UIBarButtonItem!
    
    var bom = [BOMModel]()
    var bomPin = [BOMPinModel]()
    var placements = [PlacementModel]()
    
    // Unique keyed set of Bom
    var bomUniqueKeys = NSOrderedSet()

    // Grouped dictionary of data
    var bomDataSource = [String : [BOMModel]]()
    
    // Grouped dictionary of data
    var bomPinDataSource = [String : [BOMPinModel]]()
    
    var wocItem: WOCModel? {
        didSet {
            // Update the view.
            configureView()
        }
    }

    var processItem: ProcessModel? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    
    // MARK: - Functions
    
    @objc func buttonTapped(_ sender: UIBarButtonItem) {
        for key in self.navigationController?.topViewController?.toolbarItems ?? [] {
            if (key.title == sender.title) {
                key.tintColor = UIColor.systemRed
                self.navigationItem.title = String(stringLiteral: key.title ?? "")
                activeTab = sender
                self.doRefresh(self.refreshControl!)
            } else {
                key.tintColor = UIColor.systemBlue
            }
        }
    }
    
    // MARK: - Life Cycle Events
    
    func configureView() {
        // Set up font
        let font = UIFont.systemFont(ofSize: 24)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font : font]
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.darkGray]
        UINavigationBar.appearance().titleTextAttributes = textAttributes
        activeTab = UIBarButtonItem()
        activeTab.title = "Pin Data"
        if let detail = wocItem {
            LoadData(woc: detail)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.clearsSelectionOnViewWillAppear = false
        
        // Register my custom cell
        self.tableView.register(UINib(nibName: "BomPinViewCell", bundle: nil), forCellReuseIdentifier: "BomPinCell")
        
        self.tableView.register(UINib(nibName: "BomViewCell", bundle: nil), forCellReuseIdentifier: "BomViewCell")
        
        let headerNib = UINib.init(nibName: "BomTableHeader", bundle: Bundle.main)
        self.tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "BomTableHeader")
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.tableView.contentSize.height = 1.0
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        var buttons = [UIBarButtonItem]()
        var newItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        buttons.append(newItem)
        
        let title = "Pin Data"
        newItem = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(buttonTapped))
        newItem.tintColor = UIColor.red
        buttons.append(newItem)
        activeTab = newItem
        self.navigationItem.title = title
        
        newItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        buttons.append(newItem)
        
        for key in self.placements {
            print(key.Position as Any)
            newItem = UIBarButtonItem(title: key.Position, style: .plain, target: self, action: #selector(buttonTapped))
            buttons.append(newItem)
            newItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            buttons.append(newItem)
        }
        self.navigationController?.topViewController?.setToolbarItems(buttons, animated: true)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.topViewController?.toolbarItems?.removeAll()
        activeTab.title = "Pin Data"
            
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
     

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if (activeTab == nil) { return 0 }
        if (activeTab.title == "Pin Data") && (self.bomPinDataSource.count > 0) {
                return self.bomPinDataSource.count
        }
        return 1
    }
 
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (activeTab.title == "Pin Data") && (self.bomPinDataSource.count > 0) {
                // Get the key for this section from ordered set
            if self.bomUniqueKeys.count < 0 { return 0 }
                let sect = self.bomUniqueKeys.object(at: section)
                // Get matching rows for section
                let group: [BOMPinModel] = self.bomPin.filter {
                    return (String($0.Type!)) == sect as! String
                }
                return group.count
    
        } else if (activeTab.title != "Pin Data") && (self.bomDataSource.count > 0)
        {
            if self.bomUniqueKeys.count < 0 { return 0 }
                // Get the key for this section from ordered set
                let sect = self.bomUniqueKeys.object(at: section)
                // Get matching rows for section
                let group: [BOMModel] = self.bom.filter {
                    return (String($0.Placement!)) == sect as! String
                }
                return group.count
        }
        return 0
    }
    
     

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        if (activeTab.title == "Pin Data") && (self.bomPinDataSource.count > 0) {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "BomPinCell", for: indexPath) as? BomPinViewCell else { return cell }
                // Configure the cell...
            
                if self.bomUniqueKeys.count < 0 { return cell }
                if self.bomUniqueKeys.count < indexPath.section { return cell }
                if self.bomUniqueKeys.array.indices.first == nil { return cell }
             
            
                let sect = self.bomUniqueKeys.object(at: indexPath.section)
                let group: [BOMPinModel] = self.bomPin.filter {
                    return String($0.Type!) == sect as! String
                }
            
                guard group.indices.contains(indexPath.row)  else { return cell }
                
                cell.gpinLbl?.text = "\(group[indexPath.row].GPIN!)"
                cell.alertLbl?.text = "\(group[indexPath.row].Alert!)"
                
                return cell
            } else {
                if (activeTab.title != "Pin Data") && (self.bomDataSource.count > 0) {
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "BomViewCell", for: indexPath) as? BomViewCell else { return cell }
                    // Configure the cell...
                    
                    if self.bomUniqueKeys.count < 0 { return cell }
                    if self.bomUniqueKeys.count < indexPath.section { return cell }
                    if self.bomUniqueKeys.array.indices.first == nil { return cell }
                     
                    
                    let sect = self.bomUniqueKeys.object(at: indexPath.section)
                    let group: [BOMModel] = self.bom.filter {
                        return String($0.Placement!) == sect as! String
                    }
                     
                    guard group.indices.contains(indexPath.row)  else { return cell }
                    
                    var tempFont = UIFont()
                    
                    if !group[indexPath.row].RP_Original_PIN!.isEmpty {
                        tempFont = UIFont.systemFontItalic(size: 8.5, fontWeight: .semibold)
                        cell.backgroundColor = UIColor.systemGray6
                        cell.refDesTextView.font = tempFont
                        cell.fittedPinLbl.font = tempFont
                        cell.originalPinLbl.font = tempFont
                        cell.clientPinLbl.font = tempFont
                        cell.measuredQtyLbl.font = tempFont
                        cell.splitQtyLbl.font = tempFont
                        cell.descriptionLbl.font = tempFont
                        cell.packageTypeLbl.font = tempFont
                        cell.manufacturerLbl.font = tempFont
                        cell.manuPartLbl.font = tempFont
                    } else
                    {
                        tempFont = UIFont.systemFont(ofSize: 8.0, weight: .light)
                        cell.backgroundColor = UIColor.systemBackground
                        cell.refDesTextView.font = tempFont
                        cell.fittedPinLbl.font = tempFont
                        cell.originalPinLbl.font = tempFont
                        cell.clientPinLbl.font = tempFont
                        cell.measuredQtyLbl.font = tempFont
                        cell.splitQtyLbl.font = tempFont
                        cell.descriptionLbl.font = tempFont
                        cell.packageTypeLbl.font = tempFont
                        cell.manufacturerLbl.font = tempFont
                        cell.manuPartLbl.font = tempFont
                    }
                    
                    cell.refDesTextView.text = "\(group[indexPath.row].RefDes ?? "")"
                    cell.fittedPinLbl.text =  "\(group[indexPath.row].RP_PIN ?? "")"
                    cell.originalPinLbl.text =  "\(group[indexPath.row].RP_Original_PIN ?? "")"
                    cell.clientPinLbl.text =  "\(group[indexPath.row].ClientPart ?? "")"
                    cell.measuredQtyLbl.text =  "\(group[indexPath.row].cur_qty)"
                    cell.splitQtyLbl.text =  "\(group[indexPath.row].SplitQty)"
                    cell.descriptionLbl.text =  "\(group[indexPath.row].GPIN_Description ?? "")"
                    cell.packageTypeLbl.text =  "\(group[indexPath.row].PID_Package ?? "")"
                    cell.manufacturerLbl.text =  "\(group[indexPath.row].Manufacturer ?? "")"
                    cell.manuPartLbl.text =  "\(group[indexPath.row].ManufacturersPartNumber ?? "")"
                    
                    return cell
                    
                }
            }
        
        return cell
    
                       
    }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if (activeTab.title == "Pin Data") && (self.bomPinDataSource.count > 0)  {
            let headerView = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:40))
            headerView.backgroundColor = UIColor.systemGray6
            // Add label for Section
            let font: UIFont = UIFont.systemFont(ofSize: 18.0, weight: .light)
            let lbl = UILabel(frame: CGRect(x: 20, y:0, width: 200, height: 40))
            if self.bomUniqueKeys.count < 0 { return headerView }
            lbl.text = self.bomUniqueKeys.object(at: section) as? String
            lbl.font = font
            headerView.addSubview(lbl)
            return headerView
            
        } else {
            
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "BomTableHeader")
            let backgroundView = UIView(frame: CGRect.zero)
            backgroundView.backgroundColor = UIColor.systemGray6
            headerView?.backgroundView = backgroundView
            return headerView
        }

    }
    
    
    
    
    @IBAction func doRefresh(_ sender: UIRefreshControl) {
        if (activeTab.isEnabled) {
            sender.beginRefreshing()
            activeTab.isEnabled = false
            let font = UIFont.systemFont(ofSize: 16)
            let attributes: [NSAttributedString.Key: Any] = [
                .font: font,
                .foregroundColor: UIColor.red,
            ]
            let title = NSAttributedString(string: "Refreshing BOM Data ..", attributes: attributes)
            sender.attributedTitle = title
            if wocItem != nil {
                if (activeTab.title == "Pin Data") {
                    GetBOMPinAPI(self.wocItem!.BatchNumber)
                } else {
                    GetBOMAPI(self.wocItem!.BatchNumber, activeTab.title!)
                }
            }
            activeTab.isEnabled = true
            sender.endRefreshing()
        }
    }
    
    
    // MARK: - Web Services

    
    func LoadData(woc: WOCModel) {
        if (activeTab != nil) && (activeTab.isEnabled) {
                GetPlacementsAPI(self.wocItem!.BatchNumber)
                GetBOMPinAPI(self.wocItem!.BatchNumber)
            }
    }
    
    
    func GetPlacementsAPI(_ batch: Int) {
      // http://192.168.33.9:6002/api/routecardservice/GetPlacements/11898
      WebRequest<PlacementModel>.get(self, service: "GetPlacements", params:[String(batch)],
                                     success: { result in
          // Flat list of data captures ...
          self.placements.removeAll()
          self.placements = result
      })
    }
    
    func GetBOMAPI(_ batch: Int, _ placement: String) {
      // http://192.168.33.9:6002/api/routecardservice/GetBOM/11898/SMTTop
      WebRequest<BOMModel>.get(self, service: "GetBOM", params:[String(batch), String(placement)],
                               success: { result in
        // Flat list of data captures ...
        self.bomPin.removeAll()
        self.bomPinDataSource.removeAll()
        self.bom.removeAll()
        self.bomDataSource.removeAll()
        self.bomUniqueKeys = NSOrderedSet()
        self.bom = result.filter({ $0.Placement == self.activeTab.title })
        self.bomUniqueKeys = NSMutableOrderedSet(array: (self.bom.map { String($0.Placement ?? "Not Set") as String }))
        // For each key in ordered key set, create a section list
        for key in self.bomUniqueKeys.array {
            let group: [BOMModel] = self.bom.filter {
                return String($0.Placement ?? "Not Set") == key as! String
            }
            // Add section to datasource
            self.bomDataSource.updateValue(group, forKey: key as! String)
        }
          DispatchQueue.main.async {
              self.tableView.restore()
              if !self.bomDataSource.isEmpty {
                  self.tableView.reloadData()
              }
          }
        })
    }
        
    func GetBOMPinAPI(_ batch: Int) {
      // http://192.168.33.9:6002/api/routecardservice/GetBOMPinData/11898
      WebRequest<BOMPinModel>.get(self, service: "GetBOMPinData", params:[String(batch)],
                               success: { result in
        // Flat list of data captures ...
        self.bom.removeAll()
        self.bomDataSource.removeAll()
        self.bomPin.removeAll()
        self.bomPinDataSource.removeAll()
        self.bomUniqueKeys = NSOrderedSet()
        self.bomPin = result
        // Create keys on Type
        self.bomUniqueKeys = NSMutableOrderedSet(array: (self.bomPin.map { String($0.Type!) as String }))
        // For each key in ordered key set, create a section list
        for key in self.bomUniqueKeys.array {
            let group: [BOMPinModel] = self.bomPin.filter {
                    return (String($0.Type!)) == key as! String
        }
            // Add section to datasource
            self.bomPinDataSource.updateValue(group, forKey: key as! String)
        }

            DispatchQueue.main.async {
                self.tableView.restore()
                if !self.bomPinDataSource.isEmpty {
                    self.tableView.reloadData()
                }
            }

          })
      }
    

}
