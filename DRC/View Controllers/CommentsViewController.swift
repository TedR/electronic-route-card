//
//  CommentsViewController.swift
//  DRC
//
//  Created by Edward Reilly on 28/01/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

class CommentsViewController: UITableViewController {
    
    // Flat data array
    var comments = [CommentModel]()
    // Grouped dictionary of data
    var dataSource = [String : [CommentModel]]()
    // Keys of grouped data
    var uniqueKeys = NSOrderedSet()
    
    
    var wocItem: WOCModel? {
        didSet {
            // Update the view.
            configureView()
        }
    }

    var processItem: ProcessModel? {
        didSet {
            // Update the view.
            configureView()
        }
    }

    
    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
        let exit = UIBarButtonItem(title: "Exit", style: .done, target: self, action: #selector(dismissAlertView))

        exit.tintColor = .red
        self.navigationItem.rightBarButtonItems = [exit]

        
        // self.tableView.backgroundColor = UIColor.clear
        
        // Register my custom cell
        self.tableView.register(UINib(nibName: "CommentTableCell", bundle: nil), forCellReuseIdentifier: "CommentCell")
        
        let headerNib = UINib.init(nibName: "CommentTableHeader", bundle: Bundle.main)
        self.tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "CommentCellHeader")

        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = UITableView.automaticDimension
        
        /*
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        self.tableView.backgroundView = blurEffectView
        self.tableView.separatorEffect = UIVibrancyEffect(blurEffect: blurEffect)
        */
        
    }
    
    @objc func dismissAlertView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func configureView() {
        // Set up font
        let font = UIFont.systemFont(ofSize: 24)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font : font]
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.darkGray]
        UINavigationBar.appearance().titleTextAttributes = textAttributes
        // Update the user interface for the detail item.
        // This is a process step level comment
        if let detail = processItem {
            self.title = "Comments for Step: \(detail.StepText!)"
            let add = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(showAlertView))
            add.tintColor = .systemGreen
            self.navigationItem.leftBarButtonItems = [add]
            self.GetCommentsAPI(detail.RouteCardHdrIndex, detail.UniqueStepID)
        }
        // This is a WOC Batch level comment - i.e. all associated with the hdrIX
        if let detail = wocItem {
            self.title = "Viewing all Comments for Batch: \(detail.BatchNumber)"
            self.GetCommentsAPI(detail.RouteCardHdrIndex, -1)
        }
        
    }
    

    // MARK: - Table view data source
    

    override func numberOfSections(in tableView: UITableView) -> Int {
        // This is count of unique sections specified by process
        return self.dataSource.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (self.dataSource.count > 0) {
            // Get the key for this section from ordered set
            let sect = self.uniqueKeys.object(at: section)
            // Get matching rows for section
            let group: [CommentModel] = self.comments.filter {
                return ($0.TLP!) == sect as! String
            }
            return group.count
        }
        return 0

    }
    
    @IBAction func doRefresh(_ sender: UIRefreshControl) {
        let font = UIFont.systemFont(ofSize: 16)
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: UIColor.red,
        ]
        let title = NSAttributedString(string: "Refreshing Comments ..", attributes: attributes)
        sender.attributedTitle = title
        self.configureView()

        self.tableView.reloadData()
        sender.endRefreshing()
        
    }
    

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentTableCell
        var comment: CommentModel
        
        // Configure the cell...
        let sect = self.uniqueKeys.object(at: indexPath.section)
        let group: [CommentModel] = self.comments.filter {
            return ($0.TLP!) == sect as! String
        }
        comment = group[indexPath.row]
        
        // cell.indexLbl?.text = String("\(comment.PUC_PRC_IX)")
        cell.nameLbl?.text = comment.UN_Full_Name
        cell.initialsLbl?.text = comment.UN_Initials
        cell.dateCreatedLbl?.text = ("\(AppDelegate.dateFormatterNoTime.string(from: comment.PUC_Edited))")
        cell.processLbl?.text = String("\(comment.SLP!)")
        cell.commentsTextView?.text = comment.PUC_Comments
        cell.commentsTextView.isScrollEnabled = false
        cell.commentsTextView.sizeToFit()
      

        return cell
    }
    
    
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:40))
        footerView.backgroundColor = UIColor.init(red: 52/255, green: 199/255, blue: 89/255, alpha: 0.4)
        let sect = self.uniqueKeys.object(at: section)
        let group: [CommentModel] = self.comments.filter {
            return ($0.TLP!) == sect as! String
        }
        let total = group.count
        // Add label for total
        let font: UIFont = UIFont.boldSystemFont(ofSize: 14)
        let lbl = UILabel(frame: CGRect(x: 20, y:0, width: 200, height: 40))
        lbl.text = String("This Process: \(String(total))")
        lbl.font = font
        lbl.shadowColor = .lightGray
        footerView.addSubview(lbl)
        
        return footerView
    }
 
    
    /*
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:40))
        headerView.backgroundColor = UIColor.systemGray6
        // Add label for Section
        let font: UIFont = UIFont.systemFont(ofSize: 18.0, weight: .ultraLight)
        let lbl = UILabel(frame: CGRect(x: 20, y:0, width: 200, height: 40))
        lbl.text = self.uniqueKeys.object(at: section) as? String
        lbl.font = font
        headerView.addSubview(lbl)
        return headerView
    }
 */
 
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont.systemFont(ofSize: 18.0, weight: .ultraLight)
        header.textLabel?.sizeToFit()
        header.textLabel?.numberOfLines = 0
    }
    
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CommentCellHeader")

        return headerView
    }
 
 
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.uniqueKeys.object(at: section) as? String
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    

    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath, animated: true)
           let cell = tableView.cellForRow(at: indexPath)
           let backgroundView = UIView()
           backgroundView.backgroundColor = UIColor.init(red: 0.5, green: 0.6, blue: 0.4, alpha: 0.2)
           cell?.selectedBackgroundView = backgroundView
    }
    
    
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
    
    // MARK: - Web Services

    
    @objc func showAlertView() {
        /*
          let alertController = UIAlertController(title: "Add New Comment", message: nil, preferredStyle: .alert)
          alertController.view.autoresizesSubviews = true
          let confirmAction = UIAlertAction(title: "Add", style: .default) { (_) in
              if let txtField = alertController.textFields?.first, let text = txtField.text {
                // operations
                print("Comment Added: " + text)
                self.PostCommentAPI(self.processItem!.RouteCardHdrIndex, self.processItem!.UniqueStepID, text)
              }
          }
          let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
          alertController.addTextField { (textField) in
              textField.placeholder = "Comment"
              textField.addConstraint(textField.heightAnchor.constraint(equalToConstant: 100))
          }
          alertController.addAction(confirmAction)
          alertController.addAction(cancelAction)
          self.present(alertController, animated: true, completion: nil)
         */
        
        let alert = UIAlertController(title: "Add New Comment", message: "\n\n\n\n\n\n\n\n", preferredStyle: .alert)
        alert.view.autoresizesSubviews = true

        let textView = UITextView(frame: CGRect.zero)
        textView.translatesAutoresizingMaskIntoConstraints = false
        

        let leadConstraint = NSLayoutConstraint(item: alert.view!, attribute: .leading, relatedBy: .equal, toItem: textView, attribute: .leading, multiplier: 1.0, constant: -4)
        
        let trailConstraint = NSLayoutConstraint(item: alert.view!, attribute: .trailing, relatedBy: .equal, toItem: textView, attribute: .trailing, multiplier: 1.0, constant: 4.0)

        let topConstraint = NSLayoutConstraint(item: alert.view!, attribute: .top, relatedBy: .equal, toItem: textView, attribute: .top, multiplier: 1.0, constant: -64.0)
        
        let bottomConstraint = NSLayoutConstraint(item: alert.view!, attribute: .bottom, relatedBy: .equal, toItem: textView, attribute: .bottom, multiplier: 1.0, constant: 64.0)
         
        alert.view.addSubview(textView)
        
        NSLayoutConstraint.activate([leadConstraint, trailConstraint, topConstraint, bottomConstraint])
        alert.addAction(UIAlertAction(title: "Add", style: .default, handler: { action in
             self.PostCommentAPI(self.processItem!.RouteCardHdrIndex, self.processItem!.UniqueStepID, textView.text)
         }))
         alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("\(String(describing: textView.text))")
        }))
        present(alert, animated: true)
      }
    

    
    func GetCommentsAPI(_ hdrIX: Int, _ stepIX: Int) {
        // If requesting all comments against a batch then set stepIX = - 1
        WebRequest<CommentModel>.get(self, service: "GetProcessComments", params:[String(hdrIX), String(stepIX)], success: { result in
                // Flat list of data captures ...
                self.comments = result
                // Create keys on user intials
                self.uniqueKeys = NSMutableOrderedSet(array: (self.comments.map { $0.TLP! }))
                // For each key in ordered key set, create a section list
                for key in self.uniqueKeys.array {
                    let group: [CommentModel] = self.comments.filter {
                        return ($0.TLP!) == key as! String
                    }
                    // Add section to datasource
                    self.dataSource.updateValue(group, forKey: key as! String)
                }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
        )
     }
    
    func PostCommentAPI(_ hdrIX: Int, _ stepIX: Int, _ comment: String) {
          // If requesting all comments against a batch then set stepIX = - 1
          // http://192.168.33.9:6002/api/routecardservice/AddProcessComment

        let params = ["PUC_PCH_IX": hdrIX,
                      "PUC_PRC_IX": stepIX,
                      "PUC_UN_IX": AppDelegate.UserData.userID,
                      "PUC_Comments": comment] as [String : Any]
           //create the url with URL
           guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/AddProcessComment") else {
                 print("Invalid URL!")
                 return
           }
           //now create the URLRequest object using the url object
           var request = URLRequest(url: url)
          
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           request.addValue("application/json", forHTTPHeaderField: "Accept")
           request.httpMethod = "POST"
           request.timeoutInterval = 20
           
           do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
           } catch let error {
               print(error.localizedDescription)

           }

            URLSession.shared.dataTask(with: request) { data, response, error in
                    if let error = error {
                        print("Something went wrong: \(error)")
                    }
                    if let data = data {
                        if let decodedResponse = try? JSONDecoder().decode(POSTStatus.self, from: data) {
                            DispatchQueue.main.async {
                                if (decodedResponse.StatusCode == 1) {
                                    print("Success: \(decodedResponse.StatusMessage)")
                                    self.doRefresh(self.refreshControl!)
                                }
                            }
                        }
                    }
                    
               }.resume()
       }
    
    
}
