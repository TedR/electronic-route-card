//
//  ConsumablesViewController.swift
//  DRC
//
//  Created by Edward Reilly on 20/04/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

class ConsumablesViewController: UITableViewController  {

    
    
    var consumables = [ConsumableModel]()
    
    // MARK: - Life Cycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("refreshConsumables"), object: nil)

        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
        let exit = UIBarButtonItem(title: "Exit", style: .done, target: self, action: #selector(dismissAlertView))

        exit.tintColor = .red
        self.navigationItem.rightBarButtonItems = [exit]
        
        let add = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(reportConsumable))
        add.tintColor = .systemGreen
        self.navigationItem.leftBarButtonItems = [add]
        
   
        
        
        // Register my custom cells
        self.tableView.register(UINib(nibName: "ConsumableViewCell", bundle: nil), forCellReuseIdentifier: "MainCell")
        
        
        let headerNib = UINib.init(nibName: "ConsumableHeader", bundle: Bundle.main)
        self.tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "HeaderCell")
        
        
         self.tableView.rowHeight = UITableView.automaticDimension
         self.tableView.estimatedRowHeight = UITableView.automaticDimension
        
 
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        // Remove the observer
        NotificationCenter.default.removeObserver(self)
        print("Notification Removed")
    }
    

    
    @objc func dismissAlertView() {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    // This is receiver object for message sent from AddConsumableController
    @objc func methodOfReceivedNotification(notification: Notification) {
       print("Message Received : \(notification.name)")
       self.configureView()
     }
     
    
    @objc func configureView() {
        if let detail = processItem {
            DispatchQueue.main.async {
                self.navigationItem.title = "Consumables for process: \(detail.ProcessName!) - \(detail.StepText!)"
            }
            self.GetConsumablesAPI(detail.RouteCardHdrIndex, detail.UniqueStepID, detail.InstIndex)
        }
        
    }
    
    var processItem: ProcessModel? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    
    
    
    // MARK: - Table WEB API Calls
    
    
    @objc func reportConsumable() {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "AddConsumable") as! AddConsumableController
        vc.processItem = self.processItem
        vc.definesPresentationContext = true
        vc.modalPresentationStyle = .formSheet
        // Attach to NAV for presentation
        let nav : UINavigationController = UINavigationController(rootViewController: vc)
        nav.definesPresentationContext = true
        nav.modalPresentationStyle = .formSheet
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    func GetConsumablesAPI(_ rch: Int, _ prc: Int, _ rci: Int) {
       // http://192.168.33.9:6002/api/routecardservice/getConsumables/1870/50222/56254
       // GetConsumables/{rch}/{prc}/{rci}
        WebRequest<ConsumableModel>.get(self, service: "GetConsumables", params:[String(rch), String(prc), String(rci)], success: { result in
                // Flat list of data captures ...
                self.consumables = result
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            )
     }

    // MARK: - Table view data source
    
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {


        // This will hold array of available actions once determined
        var actions = [UIContextualAction]()
            
            
        let actionAdd = UIContextualAction(style: .normal, title: String("Report"),
                  handler: { (actionAdd, view, completionHandler) in
                  // Update data source when user taps action
                  self.reportConsumable()
                  completionHandler(true)
                })
                actionAdd.image = UIImage(systemName: "leaf.arrow.circlepath")
                actionAdd.backgroundColor = .systemGreen
            
            actions.append(actionAdd)
           

            
        let configuration = UISwipeActionsConfiguration(actions: actions)
        
           
           
        return configuration
    }
    
    
    @IBAction func doRefresh(_ sender: UIRefreshControl) {
        let font = UIFont.systemFont(ofSize: 16)
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: UIColor.red,
        ]
        let title = NSAttributedString(string: "Refreshing Consumables ..", attributes: attributes)
        sender.attributedTitle = title
        self.configureView()
        sender.endRefreshing()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return consumables.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainCell", for: indexPath) as! ConsumableViewCell
        // Configure the cell...
        var consumable: ConsumableModel
        consumable = consumables[indexPath.row]

        cell.pinNumberLbl?.text = consumable.PCI_Pin
        cell.traceCodeLbl?.text = consumable.PCI_Trace
        cell.expiryDateLbl?.text = consumable.PCI_Expiry == nil ? "Not Set" : ("\(AppDelegate.dateFormatterNoTime.string(from: consumable.PCI_Expiry!))")
        cell.commentsLbl?.text = consumable.PCI_Comments
        cell.commentsLbl.sizeToFit()
        cell.departmentLbl?.text = consumable.PCI_Department
        cell.eventDateLbl?.text = ("\(AppDelegate.dateFormatterNoTime.string(from: consumable.PCI_Date))")
        cell.signatureLbl?.text = String(consumable.PCI_UN_Initials ?? "N/A")
        
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderCell")
         
        return headerView

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
