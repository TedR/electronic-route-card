//
//  DrawingsViewController.swift
//  DRC
//
//  Created by Edward Reilly on 09/04/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

class DrawingsViewController: UITableViewController {
    
    @IBOutlet weak var tableHeaderLbl: UILabel!
    

    
    var drawings = [DrawingModel]()
    
    var wocItem: WOCModel? {
        didSet {
            // Update the view.
            configureView()
        }
    }

    var processItem: ProcessModel? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    
    // MARK: - Life Cycle Events

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    
        
        // Register my custom cell
        self.tableView.register(UINib(nibName: "DrawingViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
    }
    
    @objc func dismissAlertView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func configureView() {
        // Set up font
        let font = UIFont.systemFont(ofSize: 24)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font : font]
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.darkGray]
        UINavigationBar.appearance().titleTextAttributes = textAttributes
        // Update the user interface for the detail item.
        // This is a process step level comment
        if let detail = processItem {
            self.tableHeaderLbl?.text = "View Drawings for Batch: \(detail.BatchNumber)"
            /*
            let add = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(showAlertView))
            add.tintColor = .systemGreen
            self.navigationItem.leftBarButtonItems = [add]
 
             */
            self.GetDrawingsAPI(detail.RouteCardHdrIndex)
        }
        // This is a WOC Batch level comment - i.e. all associated with the hdrIX
        if let detail = wocItem {
            DispatchQueue.main.async {
                self.tableHeaderLbl?.text = "View Drawings for Batch: \(detail.BatchNumber)"
            }
            self.GetDrawingsAPI(detail.RouteCardHdrIndex)
        }
        
    }
    
    

    
    // MARK: - Web Services
    
    func GetDrawingsAPI(_ hdrIX: Int) {
      // http://192.168.33.9:6002/api/routecardservice/GetDrawings/1870
      // GetDrawings/{hdrIDX}
      WebRequest<DrawingModel>.get(self, service: "GetDrawings", params:[String(hdrIX)], success: { result in
                // Flat list of data captures ...
                self.drawings = result
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            )
        
      }

    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if drawings.count == 0 {
            tableView.setEmptyView(title: "No Drawings.",
                                   message: "Drawings will be shown here.")
        }
        else {
            tableView.restore()
            return self.drawings.count
        }
        
        return 0
    }
    


    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DrawingViewCell

        // Configure the cell...
        var drawing: DrawingModel
        drawing = drawings[indexPath.row]
        
        cell.docTypeLbl.text  = drawing.DA_Doc_Type
        cell.drgDateLbl.text =  drawing.DA_Date == nil ? "N/A" : ("\(AppDelegate.dateFormatterNoTime.string(from: drawing.DA_Date!))")
        cell.drgNumberLbl.text = drawing.DA_Drawing_No ?? "N/A"
        cell.originLbl.text = drawing.DA_Origin
        cell.qtyLbl.text = String(drawing.DA_Qty_Issued)
        

        return cell
    }

    
    @IBAction func doTableRefresh(_ sender: UIRefreshControl) {
        let font = UIFont.systemFont(ofSize: 16)
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: UIColor.red,
        ]
        let title = NSAttributedString(string: "Refreshing Drawings ..", attributes: attributes)
        sender.attributedTitle = title
        
        self.configureView()

        sender.endRefreshing()
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Configure the cell...
        var drawing: DrawingModel
        drawing = drawings[indexPath.row]
        self.performSegue(withIdentifier: "showDrawing", sender: drawing)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       let vc = segue.destination as! DrgDetailViewController
        vc.drawing = sender as? DrawingModel
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}
