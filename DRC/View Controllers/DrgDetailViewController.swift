//
//  DrgDetailViewController.swift
//  DRC
//
//  Created by Edward Reilly on 15/04/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit
import PDFKit
import AVFoundation
import AVKit


class DrgDetailViewController: UIViewController {
    
    // MARK: - Definitions
    
    let pdfView = PDFView()
    var pdfData = Data()

    
    
    // MARK: - Life Cycle Events
    
    var drawing: DrawingModel? {
        didSet {
            // Update the view.
            self.getPDFPost(drawing!.DA_File_Path)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)
        navigationController?.setToolbarHidden(true, animated: false)
    }

    override func viewDidLoad() {
       super.viewDidLoad()

       // Do any additional setup after loading the view
       pdfView.translatesAutoresizingMaskIntoConstraints = false
       view.addSubview(pdfView)
       pdfView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
       pdfView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
       pdfView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
       pdfView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
       pdfView.maxScaleFactor = 8.0
       pdfView.minScaleFactor = pdfView.scaleFactorForSizeToFit
       pdfView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        pdfView.displayMode = .singlePageContinuous
        
        // Do any additional setup after loading the view.
        let castDrawing = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(castScreen))
        castDrawing.setBackgroundImage(UIImage(systemName: "airplayaudio"), for: .normal, barMetrics: .default)
        
        let rotateDrawingLeft = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(rotateScreenLeft))
        rotateDrawingLeft.setBackgroundImage(UIImage(systemName: "rotate.left"), for: .normal, barMetrics: .default)
        
        let rotateDrawingRight = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(rotateScreenRight))
        rotateDrawingRight.setBackgroundImage(UIImage(systemName: "rotate.right"), for: .normal, barMetrics: .default)
        
        
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let spacerFixed = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: self, action: nil)
        toolbarItems = [rotateDrawingLeft, spacerFixed, rotateDrawingRight, spacer, castDrawing]
        navigationController?.setToolbarHidden(false, animated: false)

    }
    
    override func viewDidLayoutSubviews() {
        self.pdfView.scaleFactor = self.pdfView.scaleFactorForSizeToFit
        self.pdfView.minScaleFactor = self.pdfView.scaleFactor
        self.pdfView.autoScales = true
    }
    
    
    // MARK: - Cast Drawing to External Display
    @objc func castScreen() {
        self.setupScreen()
        self.registerForScreenNotifications()
        AppDelegate.extViewController?.pdfData = self.pdfData
    }
    
    // Rotates View Left
    @objc func rotateScreenLeft() {
        self.pdfView.currentPage?.rotation = self.pdfView.currentPage!.rotation - 90
        
    }
    
    // Rotates View Right
    @objc func rotateScreenRight() {
        self.pdfView.currentPage?.rotation = self.pdfView.currentPage!.rotation + 90

        
    }
    
    // MARK:- Airplay Setup
    
    func setupAirPlayButton() {
        var buttonView: UIView? = nil
        let buttonFrame = CGRect(x: 0, y: 64, width: 44, height: 44)
        let airplayButton = AVRoutePickerView(frame: buttonFrame)
        airplayButton.activeTintColor = UIColor.blue
        airplayButton.tintColor = UIColor.gray
        buttonView = airplayButton
        self.view.addSubview(buttonView!)
    }
    
    // MARK: - Setup External Screen
    
      @objc func setupScreen(){
          if UIScreen.screens.count > 1 {
              
              
              // find the second screen
              //let secondScreen = UIScreen.screens[1]
              
              //set up a window for the screen using the screens pixel dimensions
            
            AppDelegate.secondWindow = UIWindow(frame: UIScreen.main.bounds)
              
              // windows require a root view controller
            AppDelegate.extViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "External")
              
              
            AppDelegate.secondWindow?.rootViewController = AppDelegate.extViewController
               
              // tell the window which screen to use
            AppDelegate.secondWindow?.screen = UIScreen.screens[1]
            
              
           // AppDelegate.secondWindow?.screen.overscanCompensation =  UIScreen.OverscanCompensation(rawValue: UIScreen.OverscanCompensation.scale.rawValue.advanced(by: 2))!
              
              
              //unhide the window
            AppDelegate.secondWindow?.isHidden = false

          }
      }
      
      @objc func screenDisconnected(){
    
        AppDelegate.secondWindow = nil
          //secondScreenView = nil
      }
      
      func registerForScreenNotifications(){
          
        NotificationCenter.default.addObserver(self, selector: #selector(self.setupScreen), name: UIScreen.didConnectNotification, object: nil)
          
        NotificationCenter.default.addObserver(self, selector: #selector(self.screenDisconnected), name: UIScreen.didDisconnectNotification, object: nil)
          
      }
    
    // MARK: - WEB API Calls
    
    
    func getPDFPost(_ path: String) {
           // This passes path for PDF
           let fullPath = path
           let parameters = ["Param1": fullPath]
           //create the url with URL
           guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/GetPDF") else {
                 print("Invalid URL!")
                 return
           }
           //now create the URLRequest object using the url object
           var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 30)
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           request.addValue("application/json", forHTTPHeaderField: "Accept")
           request.httpMethod = "POST"
           do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
           } catch let error {
               print(error.localizedDescription)
           }
            URLSession.shared.dataTask(with: request) { data, response, error in
                    if let error = error {
                        print("Something went wrong: \(error)")
                    }
                    if let data = data {
                        DispatchQueue.main.async {
                            self.pdfData = data
                            self.pdfView.document = PDFDocument(data: self.pdfData)
                            self.pdfView.autoScales = true
                            }
                            
                        }
               }.resume()
       }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
