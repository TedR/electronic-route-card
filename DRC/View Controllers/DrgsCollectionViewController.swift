//
//  DrgsCollectionViewController.swift
//  DRC
//
//  Created by Edward Reilly on 06/05/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit
import PDFKit

private let reuseIdentifier = "DrawingCell"

class DrgsCollectionViewController: UICollectionViewController {
    
    
    
    var drawings = [DrawingModel]()
    
    // MARK: - Load Detailed View
    
    @objc func detailScreen(data: NorcottTapGestureRecognizer) {
        let drawing: DrawingModel = data.myCustomValue as! DrawingModel
        self.collectionView.selectItem(at: data.indexPath, animated: true, scrollPosition: .centeredHorizontally)
        self.performSegue(withIdentifier: "ShowDrawingDetail", sender: drawing)
    }

    
    // MARK: - Cast Drawing to External Display
    
    @objc func castScreen(data: NorcottTapGestureRecognizer) {
        self.setupScreen()
        self.registerForScreenNotifications()
        self.collectionView.selectItem(at: data.indexPath, animated: true, scrollPosition: .centeredHorizontally)
        AppDelegate.extViewController?.pdfData = data.myCustomValue as? Data
    }
    

    
    // MARK: - Setup External Screen
    
      @objc func setupScreen(){
          if UIScreen.screens.count > 1 {
              
              
              // find the second screen
              //let secondScreen = UIScreen.screens[1]
              
              //set up a window for the screen using the screens pixel dimensions
            
            AppDelegate.secondWindow = UIWindow(frame: UIScreen.main.bounds)
              
              // windows require a root view controller
            AppDelegate.extViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "External")
              
              
            AppDelegate.secondWindow?.rootViewController = AppDelegate.extViewController
               
              // tell the window which screen to use
            AppDelegate.secondWindow?.screen = UIScreen.screens[1]
            
              
           // AppDelegate.secondWindow?.screen.overscanCompensation =  UIScreen.OverscanCompensation(rawValue: UIScreen.OverscanCompensation.scale.rawValue.advanced(by: 2))!
              
              
              //unhide the window
            AppDelegate.secondWindow?.isHidden = false

          }
      }
      
      @objc func screenDisconnected(){
    
        AppDelegate.secondWindow = nil
          //secondScreenView = nil
      }
      
      func registerForScreenNotifications(){
          
        NotificationCenter.default.addObserver(self, selector: #selector(self.setupScreen), name: UIScreen.didConnectNotification, object: nil)
          
        NotificationCenter.default.addObserver(self, selector: #selector(self.screenDisconnected), name: UIScreen.didDisconnectNotification, object: nil)
          
      }

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self

        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        // self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView!.register(UINib(nibName: "DrgsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
        
    }
    
    
    var processItem: ProcessModel? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    
       func configureView() {
           // Set up font
           let font = UIFont.systemFont(ofSize: 24)
           UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font : font]
           let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.darkGray]
           UINavigationBar.appearance().titleTextAttributes = textAttributes

           // This is a WOC Batch level comment - i.e. all associated with the hdrIX
           if let detail = processItem {
               DispatchQueue.main.async {
                self.title = "View Drawings for Batch: \(detail.BatchNumber)"
               }
            self.GetDrawingsByProcessAPI(detail.RouteCardHdrIndex, detail.UniqueStepID)
           }
           
       }
    

    
    // MARK: - Navigation


    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       let vc = segue.destination as! DrgDetailViewController
        vc.drawing = sender as? DrawingModel
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
    
    // MARK: - WEB API Calls
    
    func GetDrawingsByProcessAPI(_ hdrIX: Int, _ prcIX: Int) {
      // http://localhost:6002/api/routecardservice/GetDrawingsByProcess/1870/50223
      // GetDrawingsByProcess/{hdrIX}/{prcIX}
      WebRequest<DrawingModel>.get(self, service: "GetDrawingsByProcess", params:[String(hdrIX), String(prcIX)], success: { result in

                    DispatchQueue.main.async {
                        // Flat list of data captures ...
                        self.drawings = result
                        self.collectionView.reloadData()
                    }
                }
            )
      }
    
    func getPDFPost(_ path: String) -> Data {
           // This passes path for PDF
           let semaphore = DispatchSemaphore (value: 0)
           var octetData = Data()
           let fullPath = path
           let parameters = ["Param1": fullPath]
           //create the url with URL
           guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/GetPDF") else {
                 print("Invalid URL!")
                 return Data()
           }
           //now create the URLRequest object using the url object
           var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 30)
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           request.addValue("application/json", forHTTPHeaderField: "Accept")
           request.httpMethod = "POST"
           do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
           } catch let error {
               print(error.localizedDescription)
           }
            URLSession.shared.dataTask(with: request) { data, response, error in
                    if let error = error {
                        print("Something went wrong: \(error)")
                    }
                    if let data = data {
                        octetData = data
                        semaphore.signal()
                    }
               }.resume()
          semaphore.wait()
          return octetData
       }
    

    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return self.drawings.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! DrgsCollectionViewCell
        
        let drg: DrawingModel = self.drawings[indexPath.row]
        let pdfView = PDFView(frame: cell.viewHolder.bounds)
        var pdfData = Data()
    
        
        
        DispatchQueue.global(qos: .userInitiated).async {
            pdfData = self.getPDFPost(drg.DA_File_Path)
            let castGesture = NorcottTapGestureRecognizer(target: self, action: #selector(self.castScreen(data:)))
            castGesture.name = "Cast"
            castGesture.myCustomValue = pdfData
            castGesture.indexPath = indexPath
            
            let detailGesture = NorcottTapGestureRecognizer(target: self, action: #selector(self.detailScreen(data:)))
            detailGesture.name = "Detail"
            detailGesture.myCustomValue = drg
            detailGesture.indexPath = indexPath
               
                DispatchQueue.main.async {
                    
                    cell.castBtn.addGestureRecognizer(castGesture)
                    cell.detailsBtn.addGestureRecognizer(detailGesture)
                    
                    cell.titleLabel.text = drg.DA_Doc_Type
                    cell.titleLabel.adjustsFontSizeToFitWidth = true
                    cell.titleLabel.minimumScaleFactor = 0.5
                    cell.titleLabel.sizeToFit()
                    
                    pdfView.translatesAutoresizingMaskIntoConstraints = false
                   
                    pdfView.autoresizesSubviews = true
                    pdfView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleTopMargin, .flexibleLeftMargin]
                    pdfView.displayDirection = .vertical
                    
                    pdfView.displayMode = .singlePageContinuous
                    pdfView.displaysPageBreaks = true
                    pdfView.document = PDFDocument(data: pdfData)
                    pdfView.maxScaleFactor = 4.0
                    pdfView.minScaleFactor = pdfView.scaleFactorForSizeToFit
                    pdfView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                    pdfView.displayMode = .singlePageContinuous

                    cell.viewHolder.addSubview(pdfView)
                    pdfView.leadingAnchor.constraint(equalTo: cell.viewHolder.safeAreaLayoutGuide.leadingAnchor).isActive = true
                    pdfView.trailingAnchor.constraint(equalTo: cell.viewHolder.safeAreaLayoutGuide.trailingAnchor).isActive = true
                    pdfView.topAnchor.constraint(equalTo: cell.viewHolder.safeAreaLayoutGuide.topAnchor).isActive = true
                    pdfView.bottomAnchor.constraint(equalTo: cell.viewHolder.safeAreaLayoutGuide.bottomAnchor).isActive = true
                    pdfView.autoScales = true
                    pdfView.isUserInteractionEnabled = true
            }
                
        }
        
    
        // Configure the cell
    
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var drawing: DrawingModel
        drawing = self.drawings[indexPath.row]
        self.performSegue(withIdentifier: "ShowDrawingDetail", sender: drawing)
    }

    // MARK: UICollectionViewDelegate

    
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }

    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
 
        return true
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return true
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
 */
    
    /*
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == UICollectionView.elementKindSectionFooter) {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CartFooterCollectionReusableView", for: indexPath)
            // Customize footerView here
            return footerView
        } else if (kind == UICollectionView.elementKindSectionHeader) {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CartHeaderCollectionReusableView", for: indexPath)
            // Customize headerView here
            return headerView
        }
        fatalError()
    }
 */
 
    
    

}
