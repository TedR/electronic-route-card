//
//  EditInstController.swift
//  DRC
//
//  Created by Edward Reilly on 06/07/2021.
//  Copyright © 2021 Norcott. All rights reserved.
//

import UIKit


class EditInstController: UIViewController {
    
    @IBOutlet weak var worksInstText: UITextView!
    @IBOutlet weak var editPadlock: UIButton!
    @IBOutlet weak var emailFESlider: UISwitch!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var updateButton: UIButton!
    
    var editEnabled: Bool = false
    
    var process: ProcessModel? {
        didSet {
            
            // Update the view.
            configureView()
        }
    }
    
    
    
    //MARK: - Life Cycle
    
    
    @objc func configureView() {
        if let detail = process {
            DispatchQueue.main.async {
                self.navigationItem.title = "\(detail.ProcessName!) - \(detail.StepText!)"
               
            }
            // self.GetMeasurementsAPI(process!.UniqueStepID)
        }
        
    }
    
    // Use NSCoder to support dependency injection of objects in to this view
    required init?(coder: NSCoder, process: ProcessModel) {
        super.init(coder: coder)
        self.process = process
     }
    
    required init(coder: NSCoder) {
      fatalError("You must provide a WOC and/or ProcessModel and userRoles object to this view controller")
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.worksInstText.text = self.process?.Instruction
        self.worksInstText.isEditable = false
        self.updateButton.isEnabled = false
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        let userInfo = ["sendEmail": self.emailFESlider.isOn]
        // Post notification passing value of new text, and if email is required
        NotificationCenter.default.post(name: Notification.Name("refreshInstruction"), object: self.worksInstText.text, userInfo: userInfo)
    }
    
    //MARK: - Edit Functions
    
    @IBAction func dismissView(_ sender: UIButton) {
        // As cancel don't invoke email
        self.emailFESlider.isOn = false
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updateInstruction(_ sender: UIButton) {
        self.PostInstructionAPI(self.process!.RouteCardHdrIndex , self.process!.UniqueStepID, worksInstText.text)
    }
    
    @IBAction func editToggle(sender: UIButton) {
        let button: UIButton = sender as UIButton
    
        if self.editEnabled {
                button.setImage(UIImage(systemName: "lock.fill"), for: .normal)
                button.tintColor = .systemOrange
            } else {
                button.setImage(UIImage(systemName: "lock.open.fill"), for: .normal)
                button.tintColor = .systemRed
        }
        self.editEnabled = !self.editEnabled
        self.worksInstText.isEditable = !self.worksInstText.isEditable
        self.updateButton.isEnabled = !self.editEnabled
    }
    
    //MARK: - Web API
    
    
    func PostInstructionAPI(_ hdrIX: Int, _ stepIX: Int, _ instruction: String) {
        let params = ["RCI_RCH_IX": hdrIX,
                      "RCI_PRC_IX": stepIX,
                      "RCI_Instructions": instruction] as [String : Any]
           //create the url with URL
           guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/UpdateInstruction") else {
                 print("Invalid URL!")
                 return
           }
           //now create the URLRequest object using the url object
           var request = URLRequest(url: url)
          
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           request.addValue("application/json", forHTTPHeaderField: "Accept")
           request.httpMethod = "POST"
           request.timeoutInterval = 20
           
           do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
           } catch let error {
               print(error.localizedDescription)

           }

            URLSession.shared.dataTask(with: request) { data, response, error in
                    if let error = error {
                        print("Something went wrong: \(error)")
                    }
                    if let data = data {
                        if let decodedResponse = try? JSONDecoder().decode(POSTStatus.self, from: data) {
                            DispatchQueue.main.async {
                                if (decodedResponse.StatusCode == 1) {
                                    print("Success: \(decodedResponse.StatusMessage)")
                                   self.dismiss(animated: true, completion: nil)
                                }
                            }
                        }
                    }
                    
               }.resume()
       }
    
    
    
    // MARK: - Alert Dialog
    
    func showAlert(_ title: String, _ body: String) {
        let alertController = UIAlertController(title: title, message: body, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: {
            
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
