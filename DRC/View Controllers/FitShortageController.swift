//
//  FitShortageController.swift
//  DRC
//
//  Created by Edward Reilly on 19/03/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

extension FitShortageController: UIContextMenuInteractionDelegate {

    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {

        return UIContextMenuConfiguration(identifier: nil, previewProvider: nil, actionProvider: { suggestedActions in

            return self.makeContextMenu()
        })
    }
    
    func makeContextMenu() -> UIMenu {

        // Create a UIAction for sharing
        let share = UIAction(title: "GNCs", image: UIImage(systemName: "00.circle")) { action in
            // Show system share sheet
        }

        // Create and return a UIMenu with the share action
        return UIMenu(title: "GNC Menu", children: [share])
    }
    
}

class FitShortageController: UIViewController, UIAlertViewDelegate  {
    
    var processItem: ProcessModel? {
        didSet {
            // Update the view.
            
        }
    }
    
    var shortageItem: ShortageModel? {
        didSet {
            // Update the view.
        }
    }

    @IBOutlet var pinNumberLbl: UILabel!
    @IBOutlet var designatorLbl: UILabel!
    @IBOutlet var userLbl: UILabel!
    @IBOutlet var updateBtn: UIBarButtonItem!
    @IBOutlet var cancelBtn: UIBarButtonItem!
    @IBOutlet var detailView: UITextView!
    
    var editEnabled: Bool = false
    //@IBOutlet var lockImg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.detailView.isEditable = false
        
        self.configureView()
        
        
        // let interaction = UIContextMenuInteraction(delegate: self)
        // self.lockImg.addInteraction(interaction)

    }
    
    func configureView() {
        // Set up font
        let font = UIFont.systemFont(ofSize: 24)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font : font]
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.darkGray]
        UINavigationBar.appearance().titleTextAttributes = textAttributes
        // Update the user interface for the detail item.
        // This is a process step level comment
        if let detail = processItem {
            self.title = "Report Shortage for Step: \(detail.StepText!)"
        }
        // This is a Shortage Item
        if let detail = shortageItem {
            self.detailView.text = detail.CSD_Detail
            self.pinNumberLbl.text = detail.CSD_Pin
            self.designatorLbl.text = detail.CSD_Location
            self.userLbl.text = AppDelegate.UserData.userInitials
        }
        
    }
    
    @IBAction func updateButton(_ sender: UIButton) {
        (sender as UIButton).isEnabled = false
        let shortage = ShortageModel(CSD_IX: shortageItem!.CSD_IX, CSD_RCH_IX: processItem!.RouteCardHdrIndex, CSD_Pin: processItem!.PinNumber!, CSD_Detail: self.detailView.text ?? "", CSD_Qty: shortageItem!.CSD_Qty, CSD_Location: shortageItem?.CSD_Location ?? "", CSD_Confirmed_Fitted: AppDelegate.UserData.userInitials, CSD_PRS_IX: processItem!.ProcessID, CSD_Parent_PRS_IX: processItem!.ParentIndex, CSD_PRC_IX: processItem!.UniqueStepID, CSD_UN_IX_Reported: 0, CSD_UN_Initials_Reported: nil, CSD_UN_IX_Fitted_By: AppDelegate.UserData.userID, CSD_Fitted: nil, CSD_Process: nil)
        self.PostUpdateShortageAPI(shortage)
    }
    
    @IBAction func dismissView(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func editToggle(sender: UIButton) {
        let button: UIButton = sender as UIButton
    
        if self.editEnabled {
                button.setImage(UIImage(systemName: "lock.fill"), for: .normal)
                button.tintColor = .systemOrange
            } else {
                button.setImage(UIImage(systemName: "lock.open.fill"), for: .normal)
                button.tintColor = .systemRed
        }
        self.editEnabled = !self.editEnabled
        self.detailView.isEditable = !self.detailView.isEditable
    }
    
     @IBAction func displayActionSheet(_ sender: Any) {
         
        let alert = UIAlertController(title: "This is an alert!", message: "Using UIAlertController", preferredStyle: UIAlertController.Style.alert)
        let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (okSelected) -> Void in
            print("Ok Selected")
        }
        let okButton2 = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (okSelected) -> Void in
            print("Ok Selected")
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (cancelSelected) -> Void in
            print("Cancel Selected")
        }
        alert.addAction(okButton)
        alert.addAction(okButton2)
        alert.addAction(cancelButton)

        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Web API Calls
    
    func PostUpdateShortageAPI(_ shortage: ShortageModel) {
        // http://192.168.33.9:6002/api/routecardservice/UpdateShortages
        let semaphore = DispatchSemaphore(value: 1)
        let params = ["CSD_IX": shortage.CSD_IX,
                      "CSD_RCH_IX": shortage.CSD_RCH_IX,
                      "CSD_PRS_IX": shortage.CSD_PRS_IX,
                      "CSD_Parent_PRS_IX": shortage.CSD_Parent_PRS_IX,
                      "CSD_PRC_IX": shortage.CSD_PRC_IX,
                      "CSD_UN_IX_Fitted_By": shortage.CSD_UN_IX_Fitted_By,
                      "CSD_Confirmed_Fitted": shortage.CSD_Confirmed_Fitted ?? "",
                      "CSD_Detail": shortage.CSD_Detail ?? ""
                     ] as [String : Any]
              //create the url with URL
              guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/UpdateShortages") else {
                    print("Invalid URL!")
                    self.updateBtn.isEnabled = true
                    return
              }
              //now create the URLRequest object using the url object
              var request = URLRequest(url: url)
             
              request.addValue("application/json", forHTTPHeaderField: "Content-Type")
              request.addValue("application/json", forHTTPHeaderField: "Accept")
              request.httpMethod = "POST"
              request.timeoutInterval = 20
              
              do {
               request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
              } catch let error {
                  print(error.localizedDescription)
                  self.updateBtn.isEnabled = true
              }

               URLSession.shared.dataTask(with: request) { data, response, error in
                       if let error = error {
                           print("Something went wrong: \(error)")
                           self.updateBtn.isEnabled = true
                       }
                guard let data = data else {
                    print(String(describing: error))
                    return
                }
                semaphore.signal()
                   if let decodedResponse = try? JSONDecoder().decode(POSTStatus.self, from: data) {
                       DispatchQueue.main.async {
                           if (decodedResponse.StatusCode == 1) {
                               print("Success: \(decodedResponse.StatusMessage)")
                            NotificationCenter.default.post(name: Notification.Name("refreshShortages"), object: nil)
                               self.updateBtn.isEnabled = true
                               self.dismiss(animated: true, completion: nil)
                           }
                       }
                   }
                }
                .resume()
                semaphore.wait()
          }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.post(name: Notification.Name("refreshShortages"), object: false)
    }

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
