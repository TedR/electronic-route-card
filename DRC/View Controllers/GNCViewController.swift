//
//  GNCViewController.swift
//  DRC
//
//  Created by Edward Reilly on 28/02/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

class GNCViewController: UITableViewController {
    
    var gncs = [GNCModel]()
  
    //MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        
        let exit = UIBarButtonItem(title: "Exit", style: .done, target: self, action: #selector(dismissAlertView))

        exit.tintColor = .red
        self.navigationItem.rightBarButtonItems = [exit]
        
        
        //self.tableView.backgroundColor = UIColor.clear
        
        // Register my custom cell
        self.tableView.register(UINib(nibName: "GNCViewCell", bundle: nil), forCellReuseIdentifier: "GNCCell")
        
        let headerNib = UINib.init(nibName: "GNCHeaderCell", bundle: Bundle.main)
        self.tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "GNCHeader")
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = UITableView.automaticDimension
        
        //self.tableView.rowHeight = UITableView.automaticDimension
        //self.tableView.estimatedRowHeight = 220.0
        /*
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        self.tableView.backgroundView = blurEffectView
        self.tableView.separatorEffect = UIVibrancyEffect(blurEffect: blurEffect)
 */
        
    }
    
    @objc func dismissAlertView() {
        self.dismiss(animated: true, completion: nil)
    }

    

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return gncs.count
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 220
    }
 


    
    @IBAction func doRefresh(_ sender: UIRefreshControl) {
        let font = UIFont.systemFont(ofSize: 16)
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: UIColor.red,
        ]
        let title = NSAttributedString(string: "Refreshing GNCs ..", attributes: attributes)
        sender.attributedTitle = title
        self.configureView()

        self.tableView.reloadData()
        sender.endRefreshing()
        
    }
    
    var wocItem: WOCModel? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    
    var stepIX: Int? {
        didSet {
            // Update the view.
        }
    }
    
    func configureView() {
        // Set up font
        let font = UIFont.systemFont(ofSize: 24)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font : font]
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.darkGray]
        UINavigationBar.appearance().titleTextAttributes = textAttributes
        // Update the user interface for the detail item.

        // This is a GNC
        if let detail = wocItem {
            self.title = "Viewing Active GNCs for Batch: \(detail.BatchNumber)"
            self.GetGNCAPI(wocItem?.PinNumber ?? "", 12, AppDelegate.UserData.userID)
        }
        
    }
    
    //MARK: - Table Views
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "GNCHeader")
         
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let additionalSeparatorThickness = CGFloat(1)
        let additionalSeparator = UIView(frame: CGRect(x: 0,
                                                       y: cell.frame.size.height - additionalSeparatorThickness,
                                                       width: cell.frame.size.width,
                                                       height: additionalSeparatorThickness))
        additionalSeparator.backgroundColor = UIColor.systemBlue
        cell.addSubview(additionalSeparator)
    }
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GNCCell", for: indexPath) as! GNCViewCell
        var gnc: GNCModel
        gnc = gncs[indexPath.row]

        // Configure the cell...
        cell.pinLbl.text = gnc.PinNumber
        cell.gncNumberLbl.text = "GNC-" + String(format: "%03d", gnc.GNCNumber)
        cell.dateRaisedLbl.text = ("\(AppDelegate.dateFormatterNoTime.string(from: gnc.Raised))")
        cell.descTextView.text = gnc.Description
        cell.descTextView.isScrollEnabled = false
        cell.descTextView.sizeToFit()
        cell.capaTextView.text = gnc.PreventiveAction
        cell.capaTextView.isScrollEnabled = false
        cell.capaTextView.sizeToFit()
        cell.statusImg.image = (gnc.Status == "Closed") ? UIImage(systemName: "checkmark.circle.fill")?.withTintColor(UIColor.systemGreen, renderingMode: .alwaysOriginal) : UIImage(systemName: "exclamationmark.circle")!.withTintColor(UIColor.systemRed, renderingMode: .alwaysOriginal)
        cell.acceptedImg.image = (gnc.Accepted == true) ? UIImage(systemName: "hand.thumbsup.fill")?.withTintColor(UIColor.systemGreen, renderingMode: .alwaysOriginal) : UIImage(systemName: "hand.thumbsdown")!.withTintColor(UIColor.systemRed, renderingMode: .alwaysOriginal)
        
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath, animated: true)
           let cell = tableView.cellForRow(at: indexPath)
           let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.init(red: 0.0, green: 1.0, blue: 0.0, alpha: 0.2)
           cell?.selectedBackgroundView = backgroundView
    }

    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let count = self.gncs.filter { $0.Accepted == false }.count
        if count == 0 { return 0 }
        return 50
    }
 
    
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return "Swipe Cell(s) to Confirm Acceptance"
    }
    
    override func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        let footerView = view as! UITableViewHeaderFooterView
        footerView.textLabel?.font = UIFont(name: "Futura", size: 18)!
        footerView.contentView.backgroundColor = .red
        footerView.textLabel?.textColor = .white
        footerView.textLabel?.textAlignment = .center
        
    }

    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        // Get current state from data source
        let gnc = gncs[indexPath.row]
           // This will hold array of available actions once determined
        var actions = [UIContextualAction]()
           // Add actions based on scan type
        switch gnc.Status {
           case "Open":
            print("Type: \(gnc.Status ?? "N/A")")
               default:
                   print("Not Valid Case!")
           }
           
           let actionAccept = UIContextualAction(style: .normal, title: String("Accept"),
             handler: { (actionReminder, view, completionHandler) in
              // Update data source when user taps action
              // Store in UAC Table for next time
              let woc: WOCModel = AppDelegate.AppData.currentWOC
                let uac = UACModel(PAC_IX: -1, PAC_RCH_IX: woc.RouteCardHdrIndex, PAC_PRC_IX: self.stepIX!, PAC_UN_IX: AppDelegate.UserData.userID, PAC_Time: Date(), PAC_Comment: gnc.Description ?? "Not Set", PAC_Ref_Number: gnc.GNCNumber)
              self.PostUACAPI(uac)
              completionHandler(true)
           })
           actionAccept.image = UIImage(systemName: "hand.thumbsup.fill")
           actionAccept.backgroundColor = .systemGreen

           gnc.Accepted == false ? actions.append(actionAccept) : nil
           
           let configuration = UISwipeActionsConfiguration(actions: actions)
           
           
           return configuration
       }
    

    // MARK: - Table view data source
    
    func GetGNCAPI(_ pin: String, _ period: Int, _ userID: Int) {
        // http://192.168.33.9:6002/api/routecardservice/getGNCs/MB-MIC-00611_001/12/104
        // GetGNCs/{pin}/{period}/{userID}
        let validPin = pin.replacingOccurrences(of: "/", with: "_")
        
        WebRequest<GNCModel>.get(self, service: "GetGNCs", params:[validPin, String(period), String(userID)], success: { result in
                    self.gncs = result
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
         )
     }
    
    
    func PostUACAPI(_ uac: UACModel) {
        // http://192.168.33.9:6002/api/routecardservice/AddUAConfirmation
        let params = [
                      "PAC_RCH_IX": uac.PAC_RCH_IX,
                      "PAC_PRC_IX": uac.PAC_PRC_IX,
                      "PAC_UN_IX": uac.PAC_UN_IX,
                      "PAC_Comment": uac.PAC_Comment,
                      "PAC_Ref_Number": uac.PAC_Ref_Number
                     ] as [String : Any]
           //create the url with URL
           guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/AddUAConfirmation") else {
                 print("Invalid URL!")
                 return
           }
           //now create the URLRequest object using the url object
           var request = URLRequest(url: url)
          
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           request.addValue("application/json", forHTTPHeaderField: "Accept")
           request.httpMethod = "POST"
           request.timeoutInterval = 20
           
           do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
           } catch let error {
               print(error.localizedDescription)

           }

            URLSession.shared.dataTask(with: request) { data, response, error in
                    if let error = error {
                        print("Something went wrong: \(error)")
                    }
                    if let data = data {
                        if let decodedResponse = try? JSONDecoder().decode(POSTStatus.self, from: data) {
                            DispatchQueue.main.async {
                                if (decodedResponse.StatusCode == 1) {
                                    print("Success: \(decodedResponse.StatusMessage)")
                                    self.doRefresh(self.refreshControl!)
                                }
                            }
                        } else {
                            print("Something went wrong decoding ...")
                        }
                    }
                    
               }.resume()
       }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
