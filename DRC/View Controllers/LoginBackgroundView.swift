//
//  LoginBackgroundView.swift
//  DRC
//
//  Created by Edward Reilly on 26/04/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

class LoginBackgroundView: UIViewController {

     var window =  UIWindow()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "background")
        backgroundImage.contentMode = UIView.ContentMode.scaleToFill
        backgroundImage.alpha = 0.2
        self.view.insertSubview(backgroundImage, at: 0)
        
       
        
        // self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background")!)
    }
    
    override func viewDidAppear(_ animated: Bool) {

        performSegue(withIdentifier: "showLoginSegue",  sender: self)
               
    }
    
    override func viewDidDisappear(_ animated: Bool) {

    }
    
    
    @IBAction func unwind( _ segue: UIStoryboardSegue) {
        // Resets link to split controller
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let splitViewController: UISplitViewController = mainStoryboard.instantiateInitialViewController() as! UISplitViewController
        self.window.rootViewController = splitViewController;
        let navigationController: UINavigationController = splitViewController.viewControllers.last as! UINavigationController
        splitViewController.delegate = navigationController.topViewController as? UISplitViewControllerDelegate
       self.window.makeKeyAndVisible()
        
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}
