//
//  MasterViewController.swift
//  DRC
//
//  Created by Edward Reilly on 16/09/2019.
//  Copyright © 2019 Norcott. All rights reserved.
//

import UIKit
import Combine
import Foundation
import Swift

class MasterViewController: UITableViewController, UISearchBarDelegate, UNUserNotificationCenterDelegate  {

    
    @IBOutlet var searchBar: UISearchBar!
 
    var filtered: Bool = false
    var showAllocated: Bool = true
    var showMyProcesses: Bool = false
    var detailViewController: ERCTabController? = nil
    var wocs = [WOCModel]()
    var filteredWocs = [WOCModel]()
    var processes = [ProcessModel]()
    var filteredProcesses = [ProcessModel]()
    var feedBack = [FeedbackModel]()
    var timeStates = [TimeStateModel]()
    var timeReasons = [TimeReasonModel]()
    
    // This flag indicates if the LHS screen is displaying WOCs or Processes
    // In elevated mode this will either be Batches or Processes
    // WOC Primary = False
    // Process = True
    // In User mode this will always be by Batch/Process - there is no overloaded view
    // Always True
    var primary: Bool = false

    @IBOutlet var backToTopButton: UIBarButtonItem!
    
    @objc func reOrder(_ sender: UIBarButtonItem) {
        self.tableView.isEditing = !self.tableView.isEditing
        sender.tintColor = self.tableView.isEditing ? .systemRed : nil
    }
    
    @objc func loadRequests(_ sender: UIBarButtonItem) {
        
        if (self.wocs.count > 0) {
            
            let vc = self.newSignOffViewController(identifier: "SignOffController", wocItem: self.wocs.first!, notification: nil)
            
            // Attach to NAV for presentation
            let nav : UINavigationController = UINavigationController(rootViewController: vc)
            nav.definesPresentationContext = true
            nav.modalPresentationStyle = .formSheet
            self.navigationController?.present(nav, animated: true, completion: nil)
        }
        
    }
    
    
    // MARK: - Life Cycle
    
    
    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.isEditing = false
    
        
        // Do any additional setup after loading the view.
        // navigationItem.leftBarButtonItem = editButtonItem

        let reOrderButton = UIBarButtonItem(image: UIImage(systemName: "list.number"), style: .plain, target: self, action: #selector(reOrder))
        navigationItem.rightBarButtonItem = reOrderButton
        
        let requestsButton = UIBarButtonItem(image: UIImage(systemName: "bell"), style: .plain, target: self, action: #selector(loadRequests))

        
        navigationItem.leftBarButtonItem = requestsButton
       
        navigationItem.rightBarButtonItem = reOrderButton
        
        
        self.title = AppDelegate.UserData.elevatedMode == true ? "Works Order Queue" : "My Job Queue"
        
        self.clearsSelectionOnViewWillAppear = false
        
        
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? ERCTabController
         
        }
        
        // Hide re-order button if not in elevated mode
        navigationItem.rightBarButtonItem?.isEnabled = AppDelegate.UserData.elevatedMode
        // Set active tab to processes and disable WOC tab item
        if (AppDelegate.UserData.elevatedMode != true) {
            detailViewController?.selectedIndex = 1
            detailViewController?.tabBar.items?[0].isEnabled = false
            self.primary = true
            self.showMyProcesses = true
            self.GetFilteredProcessesAPI(0, AppDelegate.UserData.userID, self.showMyProcesses)
        // Show WOC View
        } else {
            detailViewController?.selectedIndex = 0
            detailViewController?.tabBar.items?[0].isEnabled = true
            self.primary = false
            self.showMyProcesses = false
        }

        
        searchBar.delegate = self
        
        self.tableView.register(UINib(nibName: "WOCTableCell", bundle: nil), forCellReuseIdentifier: "WOCCell")
        self.tableView.register(UINib(nibName: "TLPTableCell", bundle: nil), forCellReuseIdentifier: "TLPCell")
        // This is the user-centric design
        self.tableView.register(UINib(nibName: "TLPUserTableCell", bundle: nil), forCellReuseIdentifier: "TLPUserCell")
        
        self.showAllocated = AppDelegate.AppData.currentWOC.StockAllocated
        

        self.GetLiveBatchesAPI(true, true, AppDelegate.AppData.currentWOC.StockAllocated, 48, 0)

        
        self.GetTimeMonitorStatesAPI(true)
        self.GetTimeMonitorReasonsAPI(true)
        // Get legacy mappings
        self.GetLegacyMappingsAPI()
        

        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("updateWOCS"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("updateProcesses"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("refreshUserCentricProcess"), object: nil)
        
        
        UNUserNotificationCenter.current().delegate = self
        
        let content = UNMutableNotificationContent()
        content.title = "Electronic Route Card Notification"
        content.body = "\nLast batch processed: \(String(AppDelegate.AppData.currentWOC.BatchNumber))"
        content.badge = 1
        content.sound = UNNotificationSound.default
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.5, repeats: false)
        let request = UNNotificationRequest(identifier: "ercIdentifier", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
        self.addKeyBoardListener()
        
       
    }
    
    func addKeyBoardListener() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification , object:nil)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification , object:nil)
    }

    @objc func keyboardWillShow(notification: NSNotification) {

    }

    @objc func keyboardWillHide(notification: NSNotification) {
       tableView.beginUpdates()
       tableView.endUpdates()
    }
    
    
    //MARK: - Notifications
    
    deinit {
        // Release all resources
        // perform the deinitialization
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "keyboardWillShow"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "keyboardWillHide"), object: nil)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if let aps = notification.request.content.userInfo["aps"] as? NSDictionary {
            
            // let alert = aps["alert"] as? NSDictionary
            // let locKey = aps["loc-key"] as? String
            // let locArgs = aps["loc-args"] as? String
            let category = aps["category"] as? String
            
            print(aps .description)
            // Only display list if woc present and the correct category
            if (self.wocs.count > 0) && (category == "User_Signoff_Request") {
                let vc = self.newSignOffViewController(identifier: "SignOffController", wocItem: self.wocs.first!, notification: notification) as! SignOffViewController
                // Attach to NAV for presentation
                let nav : UINavigationController = UINavigationController(rootViewController: vc)
                nav.definesPresentationContext = true
                nav.modalPresentationStyle = .formSheet
                self.navigationController?.present(nav, animated: true, completion: nil)
            }
        }
        
        
        completionHandler([.badge, .alert, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if (response.notification.request.identifier == "ercIdentifier") {
           print("Handled Notification in MasterView \(response.notification.request.identifier)")
            self.searchBar.text = String(AppDelegate.AppData.currentWOC.BatchNumber)
            self.searchBarSearchButtonClicked(self.searchBar)
        }
        completionHandler()
    }
    
    func splitViewController(_ splitViewController: UISplitViewController,
        collapseSecondary secondaryViewController:UIViewController, onto primaryViewController:UIViewController) -> Bool {
        guard let secondaryAsNavController = secondaryViewController as? UINavigationController else { return false }
        guard let topAsDetailController = secondaryAsNavController.topViewController as? SetupViewController else { return false }
        if topAsDetailController.detailItem == nil {
            // Return true to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
            return true
        }
        return false
    }
    
    
    
    // This is receiver object for message sent from TAB Controller
    // Choose whether to display list of WOCS or Processess
    @objc func methodOfReceivedNotification(notification: Notification) {
        // Important!  Maintains state of user filtered processes...
        print("Message Received: \(self.primary) : \(notification.name)")
        
        // This is posted from ProcessViewController when state changes ..
        if (notification.name.rawValue == "refreshUserCentricProcess") {
            self.splitViewController?.preferredDisplayMode = .primaryOverlay
            
        } else if (notification.name.rawValue == "updateProcesses") {
            // Posted from ERCTabController, sets primary as true
            self.primary = (notification.object! as! Bool)
        } else if (notification.name.rawValue == "updateWOCS") {
            // Posted from ERCTabController, sets primary as true
            self.primary = (notification.object! as! Bool)
        }
        
        
        // False so load WOCs
        if (self.primary == false) {
            self.GetLiveBatchesAPI(true, true, AppDelegate.AppData.currentWOC.StockAllocated, 48, 0)
            if (AppDelegate.UserData.elevatedMode == true) {
                self.searchBar.isHidden = false
                self.searchBar.frame.size.height = 56.0 }
            self.tableView.reloadData()
            // Reset process index path
            AppDelegate.AppData.SLPIndexPath = []
            self.title = AppDelegate.UserData.elevatedMode == true ? "Works Order Queue" : "My Job Queue"
            // Ensure only active in elevated mode
            navigationItem.rightBarButtonItem?.isEnabled = AppDelegate.UserData.elevatedMode
            
        } else  {
            // Primary == True so Load Processes
            if (AppDelegate.UserData.elevatedMode == true) {
                self.searchBar.frame.size.height = 0.0
                self.searchBar.isHidden = true     
        }
            
            // User processes if not in elevated mode
            if (AppDelegate.UserData.elevatedMode != true) {
                 self.title = "User Processes"
                // User View
                self.GetFilteredProcessesAPI(0, AppDelegate.UserData.userID, self.showMyProcesses)
                
            } else {
                 self.title = "All Processes"
                // Elevated View so load norma screen
                self.GetProcessesAPI(AppDelegate.AppData.currentWOC.BatchNumber, 1, AppDelegate.UserData.userID, self.showMyProcesses)
            }
            
            self.tableView.reloadData()
            // Always disable re-order button on processes tab
            navigationItem.rightBarButtonItem?.isEnabled = false
            
     }
  }


    // MARK: - Table View
    
    override func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
    
            return UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { suggestedActions in
            // Not Primary processes so load items for WOC
            if (self.primary == false) {
                if (self.filtered) {
                    AppDelegate.AppData.currentWOC = self.filteredWocs[indexPath.row]
                    } else {
                    AppDelegate.AppData.currentWOC = self.wocs[indexPath.row]
                    }

                let woc: WOCModel = AppDelegate.AppData.currentWOC
                // Create an action for comments
                let comment = UIAction(title: "Comments", image: UIImage(systemName: "list.dash")) { action in
                    print("WOC: \(woc.BatchNumber)")
                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "CommentsViewController") as! CommentsViewController
                    vc.wocItem = woc
                    let nav : UINavigationController = UINavigationController(rootViewController: vc)
                    self.navigationController?.present(nav, animated: true, completion: nil)
                    
                }
                
                // Create an action for loading PCB Images
                let pcbImage = UIAction(title: "PCB Image", image: UIImage(systemName: "photo")?.withTintColor(UIColor.systemGreen, renderingMode: .alwaysOriginal)) { action in
                    print("WOC: \(woc.BatchNumber)")

                    // Send Params over to invoke PCB Image Load ViewController identified in prepare for segue function ...
                    // self.performSegue(withIdentifier: "showDetail", sender: woc)
                    
                    // Get Access to Tab Controller
            
                    let controller = self.detailViewController!
                    controller.selectedIndex = 0
                    // Get Access to Tab Views
                    let customizableViewControllers = controller.customizableViewControllers! as NSArray
                    
                    // Target the specific Tab View from array
                    // 4 = Tab ... Shortages
                    let vcs: ShortagesViewController = customizableViewControllers[4] as! ShortagesViewController
                    vcs.wocItem = woc
                    vcs.canEdit = false
                    
                    
                    // 0 = First Tab ... Setup
                    let vc: SetupViewController = customizableViewControllers[0] as! SetupViewController
                    // Call Configure Method for selected Tab View
                    // Overide the data type so that receiving controller can respond in different ways
                    // E.G. To stop the image loading pass string dictionary of pin & pmr ..
                    
                    // ----------------------------------------------
                    vc.detailItem = AppDelegate.AppData.currentWOC
                    
                    // ----------------------------------------------
                    
                    
                }
                
                // Create an action for loading Batch Shortages
                let plannerNodes = UIAction(title: "Shortages", image: UIImage(systemName: "minus.rectangle")?.withTintColor(UIColor.red, renderingMode: .alwaysOriginal)) { action in
                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "Shortages") as! ShortagesViewController
                    // load for BATCH
                    vc.wocItem = woc
                    vc.canEdit = false
                    let nav : UINavigationController = UINavigationController(rootViewController: vc)
                    self.navigationController?.present(nav, animated: true, completion: nil)
                }
                
                // Create an action for displaying Alert if present
                let alerter = UIAction(title: "QA Alert (" + String(AppDelegate.AppData.currentWOC.QAAlerts) + ")", image: UIImage(systemName: "exclamationmark.triangle.fill")?.withTintColor(UIColor.red, renderingMode: .alwaysOriginal)) { action in
                    print("WOC: \(woc.BatchNumber)")
                    
                    let woc: WOCModel = AppDelegate.AppData.currentWOC
                    let vc = self.newQAAlertController(identifier: "QAAlertController", wocItem: woc)
                    
                    // Attach to NAV for presentation
                    let nav : UINavigationController = UINavigationController(rootViewController: vc)
                    nav.definesPresentationContext = true
                    nav.modalPresentationStyle = .formSheet
                    self.navigationController?.present(nav, animated: true, completion: nil)

                }
                 
                var items = [UIMenuElement]()
                
                AppDelegate.AppData.currentWOC.QAAlerts > 0 ? items.append(alerter) : nil
                
                
                
                // Create an action for displaying GNC(s) if present
                let gncs = UIAction(title: "GNC", image: UIImage(systemName: String(format: "%02d", AppDelegate.AppData.currentWOC.GNCS) + ".circle.fill")?.withTintColor(UIColor.red, renderingMode: .alwaysOriginal)  ) { action in
                    print("WOC: \(AppDelegate.AppData.currentWOC.BatchNumber)")
                    self.showGNCAlert("GNC Alert", "GNCs: \(AppDelegate.AppData.currentWOC.GNCS)", 0)
               }
                    
                if (AppDelegate.AppData.currentWOC.GNCS > 0) {
                    items.append(gncs)
                }
                
 
                // Create an action for comments
                let feedback = UIAction(title: "Feedback", image: UIImage(systemName: "speaker.3")) { action in
                    print("WOC: \(AppDelegate.AppData.currentWOC.BatchNumber)")
                    self.GetFeedbackAPI(AppDelegate.AppData.currentWOC.RouteCardHdrIndex)
                        
                    }
                
                
                items.append(comment)
                items.append(pcbImage)
                items.append(plannerNodes)
                items.append(feedback)
          
                return UIMenu(title: "WOC Options: \(AppDelegate.AppData.currentWOC.BatchNumber)", children: items)
            }
            else if (AppDelegate.UserData.elevatedMode == true)
            // It is Primary Processes and we're in elevated mode so
            {
                var items = [UIMenuElement]()
                let item: ProcessModel = self.processes[indexPath.row]
                
                // Create an action for displaying GNC(s) if present
                let gncs = UIAction(title: "GNC", image: UIImage(systemName: String(format: "%02d", AppDelegate.AppData.currentWOC.GNCS) + ".circle.fill")?.withTintColor(UIColor.red, renderingMode: .alwaysOriginal)  ) { action in
                    self.showGNCAlert("GNC Alert", "GNCs: \(AppDelegate.AppData.currentWOC.GNCS)", item.UniqueStepID)
                }
                AppDelegate.AppData.currentWOC.GNCS > 0 ? items.append(gncs) : nil

                // Create an action for displaying Alert if present
                let alerter = UIAction(title: "QA Alert (" + String(AppDelegate.AppData.currentWOC.QAAlerts) + ")", image: UIImage(systemName: "exclamationmark.triangle.fill")?.withTintColor(UIColor.red, renderingMode: .alwaysOriginal)) { action in
                    
                    let woc: WOCModel = AppDelegate.AppData.currentWOC
                    let vc = self.newQAAlertController(identifier: "QAAlertController", wocItem: woc)
                    
                    // Attach to NAV for presentation
                    let nav : UINavigationController = UINavigationController(rootViewController: vc)
                    nav.definesPresentationContext = true
                    nav.modalPresentationStyle = .formSheet
                    self.navigationController?.present(nav, animated: true, completion: nil)

                }
                AppDelegate.AppData.currentWOC.QAAlerts > 0 ? items.append(alerter) : nil
                
                
                return UIMenu(title: "Process Options: \(item.ProcessName ?? "N/A")", children: items)
            }
            // In user mode so give option of detailed view
            else {
                var items = [UIMenuElement]()
                let item: ProcessModel
                if (self.filtered) {
                      item = self.filteredProcesses[indexPath.row]
                    } else {
                      item = self.processes[indexPath.row]
                }

                // Get WOC Header for this process
                
                if (self.wocs.count <= 0) { return nil}
          
                let currentWOC: WOCModel = self.wocs.filter {
                    return ( $0.RouteCardHdrIndex == item.RouteCardHdrIndex )
                }.first!
                     
                AppDelegate.AppData.currentWOC = currentWOC


                print(currentWOC.BatchNumber .description)
                
                
                // Create an action for displaying Alert if present
                let alerter = UIAction(title: "QA Alert (" + String(AppDelegate.AppData.currentWOC.QAAlerts) + ")", image: UIImage(systemName: "exclamationmark.triangle.fill")?.withTintColor(UIColor.red, renderingMode: .alwaysOriginal)) { action in
                    
                    let woc: WOCModel = AppDelegate.AppData.currentWOC
                    let vc = self.newQAAlertController(identifier: "QAAlertController", wocItem: woc)
                    
                    // Attach to NAV for presentation
                    let nav : UINavigationController = UINavigationController(rootViewController: vc)
                    nav.definesPresentationContext = true
                    nav.modalPresentationStyle = .formSheet
                    self.navigationController?.present(nav, animated: true, completion: nil)

                }
                AppDelegate.AppData.currentWOC.QAAlerts > 0 ? items.append(alerter) : nil
                
                // Create an action for displaying GNC(s) if present
                let gncs = UIAction(title: "GNC", image: UIImage(systemName: String(format: "%02d", AppDelegate.AppData.currentWOC.GNCS) + ".circle.fill")?.withTintColor(UIColor.red, renderingMode: .alwaysOriginal)  ) { action in
                    print("WOC: \(currentWOC.BatchNumber)")
                    self.showGNCAlert("GNC Alert", "GNCs: \(AppDelegate.AppData.currentWOC.GNCS)", 0)
               }
                    
                if (AppDelegate.AppData.currentWOC.GNCS > 0) {
                    items.append(gncs)
                }
                
                    
                    // Create an action for displaying DRG(s) if present
                    let drgs = UIAction(title: "Switch to Detailed View", image: UIImage(systemName: "doc.text.magnifyingglass")?.withTintColor(UIColor.systemBlue, renderingMode: .alwaysOriginal)) { action in
                        // This loads the detailed user view as page turn
                        // Uses DI to pass data
                        let vc  = self.storyboard!.instantiateViewController(identifier: "UserPageView", creator: { coder in
                            UserDetailedBook(coder: coder, wocItem: currentWOC, processItem: item)
                         })

                        // Instantiate controller
                        let nav : UINavigationController = UINavigationController(rootViewController: vc)
                        nav.definesPresentationContext = true
                        // Load in full screen mode over top
                        nav.modalPresentationStyle = .fullScreen
                        // Finally .. present ..
                        self.navigationController?.present(nav, animated: false, completion: nil)
           
                    }
                    items.append(drgs)
                
                    return UIMenu(title: "My Process Options: \(item.ProcessName ?? "N/A")", children: items)
                 
                }
        }
        
    }
    
    // Add Feedback for Frontend ...
    func showFeedbackView(_ woc: WOCModel, _ fb: [FeedbackModel]) {
          let alertController = UIAlertController(title: "Add Feedback", message: nil, preferredStyle: .alert)
          alertController.message = fb.first?.RCH_Feedback
          let paraStyle = NSMutableParagraphStyle()
          paraStyle.alignment = .left
          let message = NSAttributedString (
            string: fb.count > 0 ? fb.first!.RCH_Feedback : "None added yet",
            attributes: [NSAttributedString.Key.paragraphStyle: paraStyle,
                         NSAttributedString.Key.foregroundColor: UIColor.secondaryLabel,
                         NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 12.0)!]
          )
          alertController.setValue(message, forKey: "attributedMessage")
          let confirmAction = UIAlertAction(title: "Add", style: .default) { (_) in
              if let txtField = alertController.textFields?.first, let text = txtField.text {
                // operations
                print("Feedback to add: " + text)
                self.PostFeedbackAPI(woc.RouteCardHdrIndex, text, AppDelegate.UserData.userInitials)
              }
          }
          let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
          alertController.addTextField { (textField) in
            textField.placeholder = "Add Feedback"
          }
          alertController.addAction(confirmAction)
          alertController.addAction(cancelAction)
          self.present(alertController, animated: true, completion: nil)
      }
    
    
    func showMessage(_ title: String, _ alert: String) {
        var alertView = UIAlertController()
        alertView = UIAlertController(title: title, message: alert, preferredStyle: .alert)
       
        let addAction = UIAlertAction(title: "Close", style: .cancel, handler: { _ in

            // Do Somethng Here ....
            
            
        })
        addAction.setValue(UIImage(systemName: "info.circle")?.withRenderingMode(UIImage.RenderingMode.automatic), forKey: "image")
        alertView.addAction(addAction)
        present(alertView, animated: true)
    }
    
    func showAlert(_ title: String, _ alert: String, _ confirmed: Bool, _ stepIX: Int) {
        var alertView = UIAlertController()
        if (confirmed) {
            alertView = UIAlertController(title: title, message: alert, preferredStyle: .alert)
            alertView.view.tintColor = .systemGreen
            let addAction = UIAlertAction(title: "OK", style: .cancel, handler: { _ in
                print("No Action Required")
            })
            addAction.setValue(UIImage(systemName: "hand.thumbsup.fill")?.withRenderingMode(UIImage.RenderingMode.automatic), forKey: "image")
            alertView.addAction(addAction)
        } else {
            alertView = UIAlertController(title: title, message: alert, preferredStyle: .alert)
            alertView.view.tintColor = .systemRed
            let addAction = UIAlertAction(title: "Confirm Acceptance", style: .destructive, handler: { _ in
                print("Action Required")
                // Store in UAC Table for next time
                let woc: WOCModel = AppDelegate.AppData.currentWOC
                let uac = UACModel(PAC_IX: -1, PAC_RCH_IX: woc.RouteCardHdrIndex, PAC_PRC_IX: stepIX, PAC_UN_IX: AppDelegate.UserData.userID, PAC_Time: Date(), PAC_Comment: woc.AlertComment ?? "Not Set", PAC_Ref_Number: woc.GPinIndex)
                self.PostUACAPI(uac)
            })
            addAction.setValue(UIImage(systemName: "hand.thumbsup")?.withRenderingMode(UIImage.RenderingMode.automatic), forKey: "image")
            alertView.addAction(addAction)
        }
    
        present(alertView, animated: true)
    }
    
    func showGNCAlert(_ title: String, _ alert: String, _ stepIX: Int) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "GNCViewController") as! GNCViewController
        let woc: WOCModel = AppDelegate.AppData.currentWOC
        vc.wocItem = woc
        vc.stepIX = stepIX
        let nav : UINavigationController = UINavigationController(rootViewController: vc)
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
 
    

    private func newSignOffViewController(identifier: String, wocItem: WOCModel, notification: UNNotification?) -> UIViewController {
       let vc  = self.storyboard!.instantiateViewController(identifier: identifier, creator: { coder in
        SignOffViewController(coder: coder, wocItem: wocItem, notification: notification)
        })
        return vc
    }
    
    private func newQAAlertController(identifier: String, wocItem: WOCModel) -> QAAlertController {
       let vc  = self.storyboard!.instantiateViewController(identifier: identifier, creator: { coder in
           QAAlertController(coder: coder, wocItem: wocItem)
        })
        return vc
    }
    
    
   override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    let backgroundView = UIView()
    backgroundView.backgroundColor = UIColor.init(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.6)
    cell.selectedBackgroundView = backgroundView
    
    if (AppDelegate.UserData.elevatedMode != true) && (self.primary == true) {
            let cell: TLPUserTableCell = cell as! TLPUserTableCell
            let process = processes[indexPath.row]
            
            // Get WOC Headder for this process
            let currentWOC = self.wocs.filter {
                return ( $0.RouteCardHdrIndex == process.RouteCardHdrIndex )
            }.first
            
            
            var cellBorderColor = UIColor()
            switch String(currentWOC?.Finish ?? "") {
                case "Lead Free":
                    cellBorderColor = UIColor.init(red: 52/255, green: 199/255, blue: 89/255, alpha: 1)
                case "Tin Lead":
                    cellBorderColor = UIColor.init(red: 255/255, green: 45/255, blue: 85/255, alpha: 1)
                case "Lead Free Aqueous":
                    cellBorderColor = UIColor.init(red: 255/255, green: 204/255, blue: 0/255, alpha: 1)
                case "Tin Lead Aqueous":
                    cellBorderColor = UIColor.init(red: 255/255, green: 149/255, blue: 0/255, alpha: 1)
                case "Customer Specific":
                    cellBorderColor = UIColor(red:255/255.0, green:204/255.0, blue:102/255.0, alpha: 1);
                default:
                    cellBorderColor = .secondarySystemBackground
            }
            
          cell.layer.borderColor = cellBorderColor.cgColor
        
        
        var cellBackColor = UIColor()
        switch process.ProcStatus {
            case 0: // Not Started - Gray
                cellBackColor = UIColor.init(red: 142/255, green: 142/255, blue: 147/255, alpha: 0.3)
            case 1: // Started - Green
                cellBackColor = UIColor.init(red: 52/255, green: 199/255, blue: 89/255, alpha: 0.3)
            case 2: // Paused - Orange
                cellBackColor = UIColor.init(red: 255/255, green: 149/255, blue: 0/255, alpha: 0.3)
            case 3: // Complete - Red
                cellBackColor = UIColor.init(red: 255/255, green: 59/255, blue: 48/255, alpha: 0.3)
            default:
                cellBackColor = .systemGray6
        }
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = cellBackColor
        cell.backgroundView = backgroundView
        
        
    }
    
    /*
    let imageView = cell.subviews.first(where: { $0.description.contains("Reorder") })?.subviews.first(where: { $0 is UIImageView }) as? UIImageView
    imageView?.image = UIImage(systemName: "arrow.up.arrow.down")?.withTintColor(.black, renderingMode: .alwaysOriginal)
    imageView?.frame.size.width = 18
    imageView?.frame.size.height = 18
    */
    /*
    if (self.primary == false) {
            Animations().addPulsations(object: cell.layer, count: 1)
        }
    else {
        Animations().fadeInAnimation(object: cell.layer)
        Animations().addPulsations(object: cell.layer, count: 1)
      }
    */
    }
 
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let indexPath = tableView.indexPathForSelectedRow {
            // This is Initial BATCH list in Elevated mode
            // Not set to processes so pass WOC Object
            if (self.primary == false) {
                if (filtered) {
                    AppDelegate.AppData.currentWOC = filteredWocs[indexPath.row]
                } else {
                   AppDelegate.AppData.currentWOC = wocs[indexPath.row]
                }
                // Send Entire WOC Model over to ViewController identified in prepare for segue function ...
                //self.performSegue(withIdentifier: "showDetail", sender: AppDelegate.AppData.currentWOC)
                
                // Get Access to Tab Controller
        
                let controller = self.detailViewController!
                controller.selectedIndex = 0
                // Get Access to Tab Views
                let customizableViewControllers = controller.customizableViewControllers! as NSArray
                
                // 2 =  Tab ... BOM
                let bomNav: UINavigationController = customizableViewControllers[2] as! UINavigationController
                let bomController = bomNav.topViewController as! BOMViewController
                bomController.navigationItem.title =  String(stringLiteral: "Route Card BOM")
                bomController.wocItem = AppDelegate.AppData.currentWOC
                
                
                // 3 = Layout Tab
                let dvc: DrawingsViewController = customizableViewControllers[3] as! DrawingsViewController
                dvc.processItem = nil
                dvc.wocItem = AppDelegate.AppData.currentWOC
                
                // Target the specific Tab View from array
                // 4 =  Tab ... Shortages
                let vcs: ShortagesViewController = customizableViewControllers[4] as! ShortagesViewController
                // Load all shortages for batch for overall view
                vcs.wocItem = AppDelegate.AppData.currentWOC
                vcs.canEdit = false
                
                
                // 5 =  Tab ... Layout
                let pnl: PanelViewController = customizableViewControllers[5] as! PanelViewController
                pnl.wocItem = AppDelegate.AppData.currentWOC
 

                
                // 0 = First Tab ... Setup
                let vc: SetupViewController = customizableViewControllers[0] as! SetupViewController
                // Call Configure Method for selected Tab View
                // Overide the data type so that receiving controller can respond in different ways
                // E.G. To stop the image loading pass string dictionary of pin & pmr ..
                
                // ----------------------------------------------
                vc.detailItem = AppDelegate.AppData.currentWOC
                
                // ----------------------------------------------
                
                // Target the specific Tab View from array
                // 1 = Second Tab ... Processes
                let pvc: ProcessViewController = customizableViewControllers[1] as! ProcessViewController
                // Dummy invocation to erase current processes so to force refresh on WOC selection
                pvc.processes.removeAll()
                
                AppDelegate.AppData.TLPIndexPath = indexPath
                
                controller.navigationItem.title = String(stringLiteral: "Route Card Processes - \(AppDelegate.AppData.currentWOC.BatchNumber)")

                
            } else
                // Once a BATCH is selected this loads Processes
                // Load primary processess mode
                if (self.primary) && (AppDelegate.UserData.elevatedMode == true) {
                    // Hide LHS View to expose full SLP ....
                    
                    let controller = self.detailViewController!
                    controller.selectedIndex = 1
                    // Get Access to Tab Views
                    let customizableViewControllers = controller.customizableViewControllers! as NSArray
                
                    // 2 =  Tab ... BOM
                    let bomNav: UINavigationController = customizableViewControllers[2] as! UINavigationController
                    let bomController = bomNav.topViewController as! BOMViewController
                    bomController.navigationItem.title =  String(stringLiteral: "Route Card BOM")
                    bomController.processItem = processes[indexPath.row]
                    
                    // 3 = Drawings Tab
                    let dvc: DrawingsViewController = customizableViewControllers[3] as! DrawingsViewController
                    dvc.wocItem = nil
                    dvc.processItem = processes[indexPath.row]
                    
                    // 4 = Tab ... Shortages
                    
                    // 5 =  Tab ... Layout
                    let pnl: PanelViewController = customizableViewControllers[5] as! PanelViewController
                    // TODO: - This object will not be set
                    pnl.wocItem = AppDelegate.AppData.currentWOC
                    
                    
                    // Target the specific Tab View from array
                    // 1 = Second Tab ... Processes
                    let vc: ProcessViewController = customizableViewControllers[1] as! ProcessViewController
                    // Call Configure Method for selected Tab View
                    vc.showMyProcesses = self.showMyProcesses
                    vc.subProcess = processes[indexPath.row]
                    // Shows Expand/Collapse Button for Split Controller
                    var title = "Route Card Processes"
                    let batch = vc.subProcess?.BatchNumber.description
                    let pin = vc.subProcess?.PinNumber
                    let process = (vc.subProcess?.StepText ?? vc.subProcess?.ProcessName ?? "")
                    title = title + " - " + "BN" + (batch ?? "") + " - " + (pin ?? "") + " - " + process
                
                    controller.navigationItem.title = title
                    self.splitViewController?.preferredDisplayMode = .primaryHidden
            
                    AppDelegate.AppData.TLPStepID = processes[indexPath.row].UniqueStepID
                    AppDelegate.AppData.SLPIndexPath = indexPath
                   
  
                }
                // This is USER MODE
                else if (self.primary) && (AppDelegate.UserData.elevatedMode == false) {
                    var process: ProcessModel
                    if (filtered) {
                        process = filteredProcesses[indexPath.row]
                    } else {
                        process = processes[indexPath.row]
                    }
                    
                    // Get Access to Tab Controller
                    let controller = self.detailViewController!
                    controller.selectedIndex = 0
                    // Get Access to Tab Views
                    let customizableViewControllers = controller.customizableViewControllers! as NSArray
                    // Target the specific Tab View from array
                    
                    // 2 =  Tab ... BOM
                    let bomNav: UINavigationController = customizableViewControllers[2] as! UINavigationController
                    let bomController = bomNav.topViewController as! BOMViewController
                    bomController.navigationItem.title =  String(stringLiteral: "Route Card BOM")
                    bomController.processItem = processes[indexPath.row]
                    
                    // 3 = Drawings Tab
                    let dvc: DrawingsViewController = customizableViewControllers[3] as! DrawingsViewController
                    dvc.wocItem = nil
                    dvc.processItem = process
                    
                    // 4 =  Tab ... Shortages
                    let vcs: ShortagesViewController = customizableViewControllers[4] as! ShortagesViewController
                    // Load all shortages for batch for overall view
                    vcs.processItem = process
                    vcs.canEdit = false
                    print (process.ProcessName! .description)
                    
                    // 5 =  Tab ... Layout
                    let pnl: PanelViewController = customizableViewControllers[5] as! PanelViewController
                    // TODO: - This object will not be set
                    pnl.processItem = process
                    
                    
                }
            
            
                // If wanting to load form cell tap then uncomment but remember to add icons for Alerts
                // TODO: - Cell Tap Option
                /*
                else if (self.primary) && (AppDelegate.UserData.elevatedMode == false) {
                    // This loads the detailed user view as page turn
                    // Uses DI to pass data
                    let vc  = self.storyboard!.instantiateViewController(identifier: "UserPageView", creator: { coder in
                        UserDetailedBook(coder: coder, wocItem: AppDelegate.AppData.currentWOC, processItem: self.processes[indexPath.row])
                     })

                    // Instantiate controller
                    let nav : UINavigationController = UINavigationController(rootViewController: vc)
                    nav.definesPresentationContext = true
                    // Load in full screen mode over top
                    nav.modalPresentationStyle = .fullScreen
                    // Finally .. present ..
                    self.navigationController?.present(nav, animated: false, completion: nil)
                }
             */
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (self.primary == false) {
           return 220
        } else {
            if (AppDelegate.UserData.elevatedMode == true) {
                return 120 }
            else {
                return 200
            }
        }
    }
    
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        // Create a toolbar
        let bar = UIToolbar(frame: CGRect(x: 0, y: self.view.frame.size.height - 50, width: self.view.frame.size.width, height: 50))
        // LHS button - action of go to top, scrolls view
        let btnTop = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(goToTop))
        btnTop.setBackgroundImage(UIImage(systemName: "square.and.arrow.up"), for: .normal, barMetrics: .default)
        // Spacer
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        // Add label for switch
        let font: UIFont = UIFont.boldSystemFont(ofSize: 10)
        let lbl = UILabel(frame: CGRect(x: 150, y:150, width:0, height:0))
        if (self.primary == false) {
            lbl.text = (self.showAllocated ? "Reserved: " : "Pipeline: ")
        } else
         {
            lbl.text = (self.showMyProcesses ? "My Processes: " : "All Processes: ")
    
        }
        lbl.font = font
        lbl.shadowColor = .lightGray
        // Reserved Stock Switch
        let reservedSwitch = UISwitch(frame: CGRect(x: 150, y:150, width:0, height:0))
        if (self.primary == false) {
            reservedSwitch.setOn(self.showAllocated, animated: false)
        } else {
            reservedSwitch.setOn(self.showMyProcesses, animated: false)
        }
        reservedSwitch.addTarget(self, action: #selector(switchDidChangeState), for: .valueChanged)
        reservedSwitch.thumbTintColor = .red
        reservedSwitch.tintColor = .lightGray
        reservedSwitch.thumbTintColor = .black
        // Need to add as custom view if adding to UIToolBar ...
        let switchButton: UIBarButtonItem = UIBarButtonItem(customView: reservedSwitch)
        // Need to add as custom view if adding to UIToolBar ...
        let labelButton: UIBarButtonItem = UIBarButtonItem(customView: lbl)
        bar.setItems([btnTop, spacer, labelButton, switchButton], animated: true)
        // Hide button if in user mode and in primary process mode
        if (AppDelegate.UserData.elevatedMode == false) && (self.primary == true) {
            lbl.isHidden = true
            reservedSwitch.isHidden = true
        }

        return bar
    }
    
    @objc func goToTop(sender: UIButton!) {
        self.tableView.setContentOffset(CGPoint(x: 0, y: -self.searchBar.bounds.maxY-20), animated: true)
     }
    
    @objc func switchDidChangeState(sender: UISwitch!) {
        // Filtering WOCS
        if (self.primary == false) {
            //if (!filtered) {
                self.showAllocated = sender.isOn
                self.GetLiveBatchesAPI(true, true, self.showAllocated, 48, 0)
            //}
        } else {
            self.showMyProcesses = sender.isOn
            if (AppDelegate.UserData.elevatedMode != true) {
                // User Mode so show user TLP process View ...
                self.GetFilteredProcessesAPI(0, AppDelegate.UserData.userID, false)
            } else {
                // Currently have to set top level process to false so that child sub-processes exposed ... !!!
                // This shows STD hierarchy of all processes
                self.GetProcessesAPI(AppDelegate.AppData.currentWOC.BatchNumber, 1, AppDelegate.UserData.userID, false)
            }

            NotificationCenter.default.post(name: Notification.Name("refreshMySubProcess"), object: self.showMyProcesses)
        }
    }

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
         if (self.primary == false) {
            return 50
         }
         else {
            return 50
        }
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Are we displaying WOCs - primary = false?
        if (self.primary == false) {
            // Are they Filtered?
            if (filtered) {
                return filteredWocs.count
            } else {
                return wocs.count
            }
        } else
        // primary = true == show processes
        if (AppDelegate.UserData.elevatedMode == true) {
            return processes.count
        } else {
            // Are they Filtered?
            if (filtered) {
                return filteredProcesses.count
            } else {
                return processes.count
            }
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (self.primary == false) {
            
            let cell: WOCTableCell = tableView.dequeueReusableCell(withIdentifier: "WOCCell", for: indexPath) as! WOCTableCell
            var woc: WOCModel
           
            if (filtered) {
                woc = filteredWocs[indexPath.row]
            } else {
               woc = wocs[indexPath.row]
            }
            
            
            var cellBackColor = UIColor()
            switch String((woc.Finish)!) {
                case "Lead Free":
                    cellBackColor = UIColor.init(red: 52/255, green: 199/255, blue: 89/255, alpha: 0.6)
                case "Tin Lead":
                    cellBackColor = UIColor.init(red: 255/255, green: 45/255, blue: 85/255, alpha: 0.6)
                case "Lead Free Aqueous":
                    cellBackColor = UIColor.init(red: 255/255, green: 204/255, blue: 0/255, alpha: 0.6)
                case "Tin Lead Aqueous":
                    cellBackColor = UIColor.init(red: 255/255, green: 149/255, blue: 0/255, alpha: 0.6)
                case "Customer Specific":
                    cellBackColor = UIColor(red:255/255.0, green:204/255.0, blue:102/255.0, alpha: 0.6);
                default:
                    cellBackColor = .secondarySystemBackground
            }
            
            let backgroundView = UIView()
            backgroundView.backgroundColor = cellBackColor
            cell.backgroundView = backgroundView
            
            cell.pinLabel?.text = woc.PinNumber
            cell.batchLabel?.text = String(woc.BatchNumber)
            cell.jobLabel?.text = String(woc.JobNumber)
            cell.qtyLabel?.text = ("QTY: \(String(woc.Quantity))")
            cell.pmrLabel?.text = ("PMR: \(woc.PMR ?? "")")
            cell.buildLabel?.text = ("Build Date: \(AppDelegate.dateFormatterNoTime.string(from: woc.BuildDate))")
            cell.FirstSerial?.text = ("Serial: \(String(woc.FirstSerial ?? "N/A"))")
            cell.ProcessFinish?.text = woc.Finish ?? "Not Specified"
            cell.ClientPart.text = ("Client Part: \(woc.ClientPart ?? "No Client Part")")
            cell.ERCInstructions.text = woc.ERCInstructions ?? "No Special Instructions"
            
            
            
            cell.TLPIndicator.image = (woc.ActiveTLP > 0) ? UIImage(systemName: "sidebar.left")?.withTintColor(UIColor.magenta, renderingMode: .alwaysOriginal) : UIImage(systemName: "sidebar.left")!.withTintColor(UIColor.gray, renderingMode: .alwaysOriginal)
         
            cell.SLPIndicator.image = (woc.ActiveSLP > 0) ? UIImage(systemName: String(format: "%02d", woc.ActiveSLP) + ".square.fill")?.withTintColor(UIColor.magenta, renderingMode: .alwaysOriginal) : UIImage(systemName: "sidebar.right")?.withTintColor(UIColor.gray, renderingMode: .alwaysOriginal)
            
            cell.AlertIndicator.image = (woc.QAAlerts > 0) ? UIImage(systemName: "exclamationmark.triangle.fill")?.withTintColor(UIColor.red, renderingMode: .alwaysOriginal) : UIImage(systemName: "exclamationmark.triangle")!.withTintColor(UIColor.gray, renderingMode: .alwaysOriginal)
            
            cell.GNCAlert.image = (woc.GNCS > 0) ? UIImage(systemName: String(format: "%02d", woc.GNCS) + ".circle.fill")?.withTintColor(UIColor.red, renderingMode: .alwaysOriginal) : UIImage(systemName: "00.circle")?.withTintColor(UIColor.gray, renderingMode: .alwaysOriginal)
            
            cell.AOIImage.image = (woc.AOI == true) ? UIImage(systemName: "photo.fill.on.rectangle.fill")?.withTintColor(UIColor.systemGreen, renderingMode: .alwaysOriginal) : UIImage(systemName: "photo")!.withTintColor(UIColor.gray, renderingMode: .alwaysOriginal)
            
            
                
                 return cell
            
        } else if AppDelegate.UserData.elevatedMode == true {
            let cell: TLPTableCell = tableView.dequeueReusableCell(withIdentifier: "TLPCell", for: indexPath) as! TLPTableCell
            var process: ProcessModel

                process = processes[indexPath.row]
            
            /*
            let percentFormatter = NumberFormatter()
            percentFormatter.usesGroupingSeparator = true
            percentFormatter.minimumFractionDigits = 2
            percentFormatter.numberStyle = .percent
            percentFormatter.locale = Locale.current
            cell.PercentLbl?.text = percentFormatter.string(from: NSNumber(value: Double(truncating: NSDecimalNumber(decimal: process.ProgressPercent / 100.00))))
            cell.PercentLbl.textColor = process.ProgressPercent < 100 ? UIColor.red : UIColor.green
            
            cell.progressPercent.progress = Float(truncating: NSNumber(value: Double(truncating: NSDecimalNumber(decimal: process.ProgressPercent / 100.00))))
            cell.InstructionLbl?.text = process.Instruction
            */
 
            cell.InstructionLbl?.text = process.Instruction
            cell.TLPName?.text = process.ProcessName
            cell.tlpImage.image = (process.ActiveTLP > 0) ? UIImage(systemName: "sidebar.left")?.withTintColor(UIColor.red, renderingMode: .alwaysOriginal) : UIImage(systemName: "sidebar.left")!.withTintColor(UIColor.gray, renderingMode: .alwaysOriginal)
            
            cell.slpImage.image = (process.ActiveSLP > 0) ? UIImage(systemName: String(format: "%02d", process.ActiveSLP) + ".square.fill")?.withTintColor(UIColor.red, renderingMode: .alwaysOriginal) : UIImage(systemName: "sidebar.right")?.withTintColor(UIColor.gray, renderingMode: .alwaysOriginal)
 
            return cell
        } else {
            let cell: TLPUserTableCell = tableView.dequeueReusableCell(withIdentifier: "TLPUserCell", for: indexPath) as! TLPUserTableCell
            var process: ProcessModel
            if (filtered) {
                process = filteredProcesses[indexPath.row]
            } else {
                process = processes[indexPath.row]
            }
            
            // Get WOC Headder for this process
            let currentWOC = self.wocs.filter {
                return ( $0.RouteCardHdrIndex == process.RouteCardHdrIndex )
            }.first
            
            // Get current state for process
            let currentState = self.timeStates.filter {
                return ( $0.PTS_State == process.ProcStatus )
            }.first
            
            switch currentState?.PTS_Description {
            case "Not Started": // Paused
                 cell.stateImg.image = UIImage(systemName: "timelapse")
                 cell.stateImg.tintColor = .systemGray
            case "Active": // Started so option to PAUSE or COMPLETE
                 cell.stateImg.image = UIImage(systemName: "play.circle.fill")
                 cell.stateImg.tintColor = .systemGreen
            case "Paused":
                cell.stateImg.image = UIImage(systemName: "pause.circle.fill")
                cell.stateImg.tintColor = .systemOrange
            case "Complete":
                cell.stateImg.image = UIImage(systemName: "stop.circle.fill")
                cell.stateImg.tintColor = .systemRed
            default: // Error!
                print("Error - Option not Configured")
            }
            
            cell.batchLbl?.text = String(process.BatchNumber)
            cell.TLPLbl?.text = process.ProcessName
            cell.pinLbl?.text = process.PinNumber
            cell.processesImg.image = (process.ActiveSLP > 0) ? UIImage(systemName: String(format: "%02d", process.ActiveSLP) + ".circle.fill")?.withTintColor(UIColor.systemRed, renderingMode: .alwaysOriginal) : UIImage(systemName: "0.circle.fill")?.withTintColor(UIColor.systemGray, renderingMode: .alwaysOriginal)
            cell.descLbl?.text = currentWOC?.GDescription
            cell.buildLbl?.text =  currentWOC?.BuildDate != nil ? AppDelegate.dateFormatterNoTime.string(from: currentWOC!.BuildDate) : "N/A"
            return cell
        }

    }

    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = self.wocs[sourceIndexPath.row]
        wocs.remove(at: sourceIndexPath.row)
        wocs.insert(movedObject, at: destinationIndexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }

    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    // MARK: - Web Services
    
    
    @IBAction func doRefresh(_ sender: UIRefreshControl) {
        let font = UIFont.systemFont(ofSize: 16)
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: UIColor.red,
        ]
        if (self.primary == false) {
            let title = NSAttributedString(string: "Refreshing Works Orders ..", attributes: attributes)
            sender.attributedTitle = title
            self.GetLiveBatchesAPI(true, true, true, 48, 0)
            //self.showAllocated = true
        } else {
            let title = NSAttributedString(string: "Refreshing Process Sequence ..", attributes: attributes)
            sender.attributedTitle = title
            if (AppDelegate.UserData.elevatedMode != true) {
                // User View
                // Should this be by user or not ????
                self.GetFilteredProcessesAPI(0, AppDelegate.UserData.userID, self.showMyProcesses) //self.showMyProcesses)
            } else {
                // Elevated View
                self.GetProcessesAPI(AppDelegate.AppData.currentWOC.BatchNumber, 1, AppDelegate.UserData.userID, false)
            }
        }
        
       // self.showAllocated = true
       // self.showAllProcesses = true
        
        sender.endRefreshing()
        
    }
    

    
    // Get Time Monitor States
    func GetTimeMonitorStatesAPI(_ showAll: Bool) {
       // http://192.168.33.9:6002/api/routecardservice/getTimeState/true
       // GetTimeState/{showAll}
        
        WebRequest<TimeStateModel>.get(self, service: "GetTimeState", params:[String(showAll)], success: { result in
                // Flat list of data captures ...
                self.timeStates = result
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            )
        
      }
    
    // Get Time Monitor States
    func GetTimeMonitorReasonsAPI(_ showAll: Bool) {
        // http://192.168.33.9:6002/api/routecardservice/getTimeReasons/true
        // GetTimeReasons/{showAll}
        
        WebRequest<TimeReasonModel>.get(self, service: "GetTimeReasons", params:[String(showAll)], success: { result in
                    DispatchQueue.main.async {
                         self.timeReasons = result
                    }
                }
            )
      }
    
    // Get Legacy Mapping Details
    func GetLegacyMappingsAPI() {
        // http://192.168.33.9:6002/api/routecardservice/GetLegacyMapping
        WebRequest<LegacyModel>.get(self, service: "GetLegacyMapping", params:[String("")], success: { result in
                    DispatchQueue.main.async {
                        AppDelegate.AppData.legacyMappings = result
                    }
                }
            )
      }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filtered = searchText.count > 0
        if (AppDelegate.UserData.elevatedMode == true) {
            self.filteredWocs = self.wocs.filter {
                return ( String($0.BatchNumber).contains(searchText) || String($0.PinNumber!).uppercased().contains(searchText.uppercased()) || String($0.ERCInstructions ?? "").uppercased().contains(searchText.uppercased())
                )}
            self.tableView.reloadData()
        } else {
            self.filteredProcesses  = self.processes.filter {
                return ( String($0.BatchNumber).contains(searchBar.text!) || String($0.PinNumber!).uppercased().contains(searchBar.text!.uppercased()) || String($0.ProcessName ?? "").uppercased().contains(searchBar.text!.uppercased())
                )}
            self.tableView.reloadData()
        }
      }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.filtered = searchBar.text!.count > 0
        if (AppDelegate.UserData.elevatedMode == true) {
            self.filteredWocs = self.wocs.filter {
                return ( String($0.BatchNumber).contains(searchBar.text!) || String($0.PinNumber!).uppercased().contains(searchBar.text!.uppercased()) || String($0.ERCInstructions ?? "").uppercased().contains(searchBar.text!.uppercased())
                )}
            self.tableView.reloadData()
        } else {
            self.filteredProcesses  = self.processes.filter {
                return ( String($0.BatchNumber).contains(searchBar.text!) || String($0.PinNumber!).uppercased().contains(searchBar.text!.uppercased()) || String($0.Instruction ?? "").uppercased().contains(searchBar.text!.uppercased())
                )}
            self.tableView.reloadData()
        }
      }
    
    
    func GetLiveBatchesAPI(_ rca: Bool, _ vis: Bool, _ alloc: Bool, _ period: Int, _ batch: Int) {
        
        // http://192.168.33.9:6002/api/routecardservice/getLiveBatches/true/false/true/48#
        // GetLiveBatches/{rca}/{bhc}/{alloc}/{period}
        
        WebRequest<WOCModel>.get(self, service: "GetLiveBatches", params:[String(rca), String(vis), String(alloc), String(period), String(batch)], success: { result in
                    self.wocs = result
                    self.filteredWocs = result
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        if !AppDelegate.AppData.TLPIndexPath.isEmpty && self.filtered == false {
                            if (self.wocs.count > 0) {
                                self.tableView.beginUpdates()
                                self.tableView.reloadRows(at: [AppDelegate.AppData.TLPIndexPath], with: .automatic)
                                self.tableView.scrollToRow(at: AppDelegate.AppData.TLPIndexPath, at: .top, animated: true)
                                self.tableView.endUpdates()
                                self.tableView.selectRow(at: AppDelegate.AppData.TLPIndexPath, animated: true, scrollPosition: .top)
                            }
                        }
                    }
                }
         )
        
     }
        
    func GetProcessesAPI(_ batch: Int, _ tlp: Int, _ un_ix: Int, _ myProc: Bool) {

        // http://192.168.33.9:6002/api/routecardservice/getProcesses/10192/1/104/true
        // GetProcesses/{batch}/{tlp}/{un_ix}/{myProc}
        
        WebRequest<ProcessModel>.get(self, service: "GetProcesses", params:[String(batch), String(tlp), String(un_ix), String(myProc)], success: { result in
                    self.processes = result
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        if (self.processes.count > 0) {
                           self.tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
                        }
                    }
                }
         )
        
     }
    
    func GetFilteredProcessesAPI(_ batch: Int, _ un_ix: Int, _ myProc: Bool) {

        // http://192.168.33.9:6002/api/routecardservice/getFilteredProcesses/10192/104/true
        // GetFilteredProcesses/{batch}/{un_ix}/{myProc}
        
        WebRequest<ProcessModel>.get(self, service: "GetFilteredProcesses", params:[String(batch), String(un_ix), String(myProc)], success: { result in
                    self.processes = result
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        if (self.processes.count > 0) {
                           self.tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
                        }
                    }
                }
         )
        
     }
    
    
    
    func GetFeedbackAPI(_ hdrIX: Int) {

        // http://192.168.33.9:6002/api/routecardservice/getFeeback/1870
        // GetProcesses/{batch}/{tlp}/{un_ix}
        
        WebRequest<FeedbackModel>.get(self, service: "GetFeedback", params:[String(hdrIX)], success: { result in
                    self.feedBack = result
                    DispatchQueue.main.async {
                        self.showFeedbackView(AppDelegate.AppData.currentWOC, self.feedBack)
                    }
                }
         )
        
     }
    
    func PostFeedbackAPI(_ hdrIX: Int, _ comment: String, _ user: String) {
        // http://192.168.33.9:6002/api/routecardservice/UpdateFeedback

        let params = ["RCH_IX": hdrIX,
                      "RCH_Feedback": (comment + "\t" + "(" + user + ")"),
                      "RCH_Last_Edit_By": user] as [String : Any]
           //create the url with URL
           guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/UpdateFeedback") else {
                 print("Invalid URL!")
                 return
           }
           //now create the URLRequest object using the url object
           var request = URLRequest(url: url)
          
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           request.addValue("application/json", forHTTPHeaderField: "Accept")
           request.httpMethod = "POST"
           request.timeoutInterval = 20
           
           do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
           } catch let error {
               print(error.localizedDescription)

           }

            URLSession.shared.dataTask(with: request) { data, response, error in
                    if let error = error {
                        print("Something went wrong: \(error)")
                    }
                    if let data = data {
                        if let decodedResponse = try? JSONDecoder().decode(POSTStatus.self, from: data) {
                            DispatchQueue.main.async {
                                if (decodedResponse.StatusCode == 1) {
                                    print("Success: \(decodedResponse.StatusMessage)")
                                    self.doRefresh(self.refreshControl!)
                                }
                            }
                        } else {
                            print("Something went wrong decoding ...")
                        }
                    }
                    
               }.resume()
       }
    
    // Converts raw data to Bool
    func parse(data: Data) -> Bool? {
        return String(data: data, encoding: .utf8).flatMap(Bool.init)
    }
    
    func GetUACAPI(_ hdrIX: Int, _ userID: Int, _ refIX: Int, _ stepIX: Int) {
        // http://192.168.33.9:6002/api/routecardservice/getUAConfirmation/1870/104/1234/0
        // GetUAConfirmation(int hdrIX, int userID, int stepIX = -1)
        let params = "/\(hdrIX)/\(userID)/\(refIX)/\(stepIX)"
        guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/getUAConfirmation" + params) else {
                    print("Invalid URL!")
                return
            }
            let request = URLRequest(url: url)
            URLSession.shared.dataTask(with: request) { data, response, error in
                if let data = data {
                    if let confirmed = self.parse(data: data) {
                        DispatchQueue.main.async {
             
                          self.showAlert("Quality Alert", AppDelegate.AppData.currentWOC.AlertComment ?? "N/A", confirmed, stepIX)
                            
                        }
                    }
                }
            }.resume()
     }
    
    
    func PostUACAPI(_ uac: UACModel) {
        // http://192.168.33.9:6002/api/routecardservice/AddUAConfirmation

        let params = [
                      "PAC_RCH_IX": uac.PAC_RCH_IX,
                      "PAC_PRC_IX": uac.PAC_PRC_IX,
                      "PAC_UN_IX": uac.PAC_UN_IX,
                      "PAC_Comment": uac.PAC_Comment,
                      "PAC_Ref_Number": uac.PAC_Ref_Number
                     ] as [String : Any]
           //create the url with URL
           guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/AddUAConfirmation") else {
                 print("Invalid URL!")
                 return
           }
           //now create the URLRequest object using the url object
           var request = URLRequest(url: url)
          
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           request.addValue("application/json", forHTTPHeaderField: "Accept")
           request.httpMethod = "POST"
           request.timeoutInterval = 20
           
           do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
           } catch let error {
               print(error.localizedDescription)

           }

            URLSession.shared.dataTask(with: request) { data, response, error in
                    if let error = error {
                        print("Something went wrong: \(error)")
                    }
                    if let data = data {
                        if let decodedResponse = try? JSONDecoder().decode(POSTStatus.self, from: data) {
                            DispatchQueue.main.async {
                                if (decodedResponse.StatusCode == 1) {
                                    print("Success: \(decodedResponse.StatusMessage)")
                                    // self.doRefresh(self.refreshControl!)
                                }
                            }
                        } else {
                            print("Something went wrong decoding ...")
                        }
                    }
                    
               }.resume()
       }
    


}

