//
//  PanelViewController.swift
//  DRC
//
//  Created by Edward Reilly on 29/04/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

class PanelViewController: UITableViewController {

    // Flat data array
    var layout = [LayoutModel]()
    // Grouped dictionary of data
    var dataSource = [String : [LayoutModel]]()
    // Keys of grouped data
    var uniqueKeys = NSOrderedSet()
    
    
    var wocItem: WOCModel? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    
    var processItem: ProcessModel? {
        didSet {
           // Update the view.
           configureView()
        }
    }



    //MARK: - Life Cycle
    

    
    @objc func configureView() {
        // Set up font
        let font = UIFont.systemFont(ofSize: 24)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font : font]
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.darkGray]
        UINavigationBar.appearance().titleTextAttributes = textAttributes
        // This is a WOC Batch level comment - i.e. all associated with the hdrIX
        if let detail = wocItem {
            self.GetLayoutAPI(detail.RouteCardHdrIndex)
        }
        if let detail = processItem {
            self.GetLayoutAPI(detail.RouteCardHdrIndex)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
        
        // Register my custom cells
        self.tableView.register(UINib(nibName: "LayoutCellHeader", bundle: nil), forCellReuseIdentifier: "LayoutCellHeader")
        self.tableView.register(UINib(nibName: "PanelSubCell", bundle: nil), forCellReuseIdentifier: "PanelSubCell")
        self.tableView.register(UINib(nibName: "CoordsSubCell", bundle: nil), forCellReuseIdentifier: "CoordsSubCell")
        self.tableView.register(UINib(nibName: "FidsSubCell", bundle: nil), forCellReuseIdentifier: "FidsSubCell")

    }
    
    
    // MARK: - WEB API
    
    func GetLayoutAPI(_ hdrIX: Int) {
        // If requesting all comments against a batch then set stepIX = - 1
        WebRequest<LayoutModel>.get(self, service: "GetPanelLayout", params:[String(hdrIX)], success: { result in
                self.layout.removeAll()
                self.dataSource.removeAll()
                // Flat list of data captures ...
                self.layout = result
                // Create keys on user intials
                self.uniqueKeys = NSMutableOrderedSet(array: (self.layout.map { $0.PCB_Group }))
                // For each key in ordered key set, create a section list
                for key in self.uniqueKeys.array {
                    let group: [LayoutModel] = self.layout.filter {
                        return ($0.PCB_Group) == key as! String
                    }
                    // Add section to datasource
                    self.dataSource.updateValue(group, forKey: key as! String)
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        )
     }



    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        if dataSource.count == 0 {
            tableView.setEmptyView(title: "No Panel Details.",
                                   message: "PCB Panel Details will be shown here.")
        }
        else {
            tableView.restore()
            return self.dataSource.count
        }
        
        return 0
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (self.dataSource.count > 0) {
            // Get the key for this section from ordered set
            let sect = self.uniqueKeys.object(at: section)
            // Get matching rows for section
            var group: [LayoutModel] = self.layout.filter {
                return ($0.PCB_Group) == sect as! String
            }
            // Sort by IX ASC
            group.sort {
                return $0.PCB_IX < $1.PCB_IX
            }
            return group.count
        }
 
        return 0

    }
    


    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        // Configure the cell...
        var layout: LayoutModel
        // Configure the cell...
        let sect = self.uniqueKeys.object(at: indexPath.section)
        var group: [LayoutModel] = self.layout.filter {
            return ($0.PCB_Group) == sect as! String
        }
        // Sort by IX ASC
        group.sort {
            return $0.PCB_IX < $1.PCB_IX
        }
        layout = group[indexPath.row]
        
        if (layout.PIT_Item == "Panel") {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PanelSubCell", for: indexPath) as! PanelSubCell
            cell.SectionTitle.text = layout.PCB_Item_Description
            cell.DataField1.text = "X"
            cell.ValueField1.text = String(layout.PCB_X_Value)
            cell.DataField2.text = "Y"
            cell.ValueField2.text = String(layout.PCB_Y_Value)
            cell.DataField3.text = "Thickness"
            cell.ValueField3.text = String(layout.PCB_Thickness)
            cell.backgroundColor = .systemGray6
            return cell
        }
        else if (layout.PIT_Item == "Coordinates") {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CoordsSubCell", for: indexPath) as! CoordsSubCell
            cell.SectionTitle.text = layout.PCB_Item_Description
            cell.DataField1.text = "X"
            cell.ValueField1.text = String(layout.PCB_X_Value)
            cell.DataField2.text = "Y"
            cell.ValueField2.text = String(layout.PCB_Y_Value)
            cell.backgroundColor = .systemGray6
            return cell
        }
        else if (layout.PIT_Item == "Panel Fiducials from Datum") {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FidsSubCell", for: indexPath) as! FidsSubCell
            cell.SectionTitle.text = layout.PCB_Item_Description
            cell.DataField1.text = "Diameter"
            cell.ValueField1.text = String(layout.PCB_Diameter)
            cell.DataField2.text = "Style";
            var temp = UIImage()
            switch (layout.PCB_Style ) {
                case 0:
                    break
                case 1:
                    temp = UIImage(named: "Circle.png")!
                    break
                case 2:
                    temp = UIImage(named: "Rhombus.png")!
                    break
                default:
                    break
            }
            cell.StyleImage.image = temp
            cell.DataField3.text = "X"
            cell.ValueField3.text = String(layout.PCB_X_Value)
            cell.DataField4.text = "Y"
            cell.ValueField4.text = String(layout.PCB_Y_Value)
            cell.backgroundColor = .secondarySystemGroupedBackground
            return cell
        }
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // Use Layout Header Cell Design
        let headerView: LayoutCellHeader = tableView.dequeueReusableCell(withIdentifier: "LayoutCellHeader") as! LayoutCellHeader
        if (self.uniqueKeys.count > 0) {

            // Declare variable for model
            var layout: LayoutModel
            // Get current section as string
            let sect = self.uniqueKeys.object(at: section)
            // Filter data by this group
            var group: [LayoutModel] = self.layout.filter {
                 return ($0.PCB_Group) == sect as! String
            }
            // Sort by IX ASC
            group.sort {
                return $0.PCB_IX < $1.PCB_IX
            }
            if (group.count >= 1) {
                // Get Items after sorting
                layout = group[0]
                // If we're a Panel ...
                if (layout.PIT_Item == "Panel") {
                       headerView.SectionTitle.text = layout.PCB_Category
                
                }
                //
                else {
                       headerView.SectionTitle.text = String("\(layout.PCB_Category) \(layout.PIT_Item)")
                }
             
                
                headerView.backgroundColor = AppDelegate.NorcottLightOrange
            
            }
        }
        

        return headerView
    }
 
 
    func getCellHeight(itemType: String) -> CGFloat {
        
        if (itemType == "Panel") {
            return 90.0
        } else if (itemType == "Coordinates") {
            return 65.0
        } else if (itemType == "Panel Fiducials from Datum") {
            return 120.0
        }
        return 0.0
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let sect = self.uniqueKeys.object(at: indexPath.section)
        let group: [LayoutModel] = self.layout.filter {
            return ($0.PCB_Group) == sect as! String
        }
        let layout = group[indexPath.row]
        return self.getCellHeight(itemType: layout.PIT_Item)
    }
 
 
 
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34.0
    }
    
    @IBAction func doTableRefresh(_ sender: UIRefreshControl) {
        let font = UIFont.systemFont(ofSize: 16)
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: UIColor.red,
        ]
        let title = NSAttributedString(string: "Refreshing Panel Layout ..", attributes: attributes)
        sender.attributedTitle = title
        self.configureView()
        sender.endRefreshing()
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */



    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}
