//
//  PasswordViewController.swift
//  DRC
//
//  Created by Edward Reilly on 31/10/2019.
//  Copyright © 2019 Norcott. All rights reserved.
//

import UIKit
import AVFoundation
import Combine
import Swift
import UserNotifications




class PasswordViewController: UIViewController  {
    
    
    @IBOutlet var userName: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var activitySpinner: UIActivityIndicatorView!
    @IBOutlet var errorImage: UIImageView!
    @IBOutlet var versionLbl: UILabel!
    @IBOutlet var deviceIDLbl: UILabel!
    @IBOutlet var errorMessage: UILabel!
    
    var models = [LoginCredentialsModel]()
    var window =  UIWindow()
    var soundEffect: AVAudioPlayer?

    
    //MARK: - View Life Cycle Methods
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isModalInPresentation = true
        
        print("WOC Pin: \(String(describing: AppDelegate.AppData.currentWOC.PinNumber))")
        print("WOC Batch: \(String(describing: AppDelegate.AppData.currentWOC.BatchNumber))")
        
        print("TLP: \(String(describing: AppDelegate.AppData.TLPStepID))")
        print("SLP: \(String(describing: AppDelegate.AppData.SLPStepID))")
        
        // Do any additional setup after loading the view.
        
        print("Mode: \(String(describing: AppDelegate.UserData.elevatedMode))")
        
        self.userName.text =  AppDelegate.UserData.userName
        self.userName.reloadInputViews()
        self.password.text = AppDelegate.UserData.password
        self.password.reloadInputViews()
        // This disable the password accessory icon on keyboard as it does not get included in keyboard height!
        self.userName.textContentType = .oneTimeCode
        self.password.textContentType = .oneTimeCode
        AppDelegate.UserData.userInitials = ""
        
        self.versionLbl.text = ("Version: \(UIApplication.appVersion ?? "")")
        self.deviceIDLbl.text = ("Device ID: \(UIApplication.deviceID ?? "")")
        
        // self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "background")
        backgroundImage.contentMode = UIView.ContentMode.scaleAspectFit
        backgroundImage.alpha = 0.2
        self.view.insertSubview(backgroundImage, at: 0)
        
        

    }
    
    override func  viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(textFieldDidBeginEditing),
            name: UITextField.textDidBeginEditingNotification,
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(textFieldDidEndEditing),
            name: UITextField.textDidEndEditingNotification,
            object: nil)
 
        
        /*
        [[NSNotificationCenter defaultCenter]
            addObserver:self selector:@selector(textFieldBegan:)
            name: UITextFieldTextDidBeginEditingNotification object:nil];
         */
        
        self.password.becomeFirstResponder()
        
       
        
        Animations().createPulse(button: loginButton, color: .orange)
        Animations().addPulsation(object: loginButton.layer)
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    
    //MARK: - Text Field Delegate Methods
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    @objc func textFieldDidBeginEditing(textField: UITextField!) {
        print("Begin Edit")

    }
    
    @objc func textFieldDidEndEditing(textField: UITextField) {
        print("End Edit")
    }
    
    @IBAction func Login(sender: Any) {
        self.secureLoginRequest()
    }
    
    
   

    private func secureLoginRequest() {
        self.activitySpinner.startAnimating()
        // This passes credentials of user
        let parameters = ["UserName": self.userName.text , "Password": self.password.text]
        //create the url with URL
        guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/SecureLogin") else {
              print("Invalid URL!")
              return
        }
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
       
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        request.timeoutInterval = 20
        
        // Put request data into JSON object and add to Body
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            self.errorImage.isHidden = false
            Animations().addPulsation(object: self.errorImage.layer)
            self.password.becomeFirstResponder()
        }
        URLSession.shared.dataTask(with: request) { data, response, error in
            // Give me a clue what's wrong?!!!!
            do {
                _ =  try JSONDecoder().decode(LoginCredentialsModel.self, from: data!)
            } catch {
                print("Error Decoding:  \(error)")
            }
            
            if let data = data {

                if let decodedResponse = try? JSONDecoder().decode(LoginCredentialsModel.self, from: data) {
                    DispatchQueue.main.async {
                        if (decodedResponse.UserID > 0) {
                            print("Success: \(decodedResponse.UserInitials)")
                            self.errorImage.isHidden = true
                            //self.dismiss(animated: true, completion: nil)
                            self.window.frame = UIScreen.main.bounds
                            // Share dictionaries
                            // These are device owners credentials
                            print(AppDelegate.UserData.deviceUUID)
                            print(AppDelegate.UserData.deviceName)
                            print(AppDelegate.UserData.deviceToken)
                            AppDelegate.UserData.password = self.password.text ?? ""
                            AppDelegate.UserData.userName = self.userName.text ?? ""
                            AppDelegate.UserData.userInitials = decodedResponse.UserInitials
                            AppDelegate.UserData.userID = decodedResponse.UserID
                            AppDelegate.UserData.userRights = decodedResponse.UserRights
                            // These are new fields to store the device details
                            /*
                            AppDelegate.UserData.deviceIndex = decodedResponse.DeviceIndex
                            AppDelegate.UserData.deviceName = decodedResponse.DeviceName
                            AppDelegate.UserData.deviceUUID = decodedResponse.DeviceUUID
                            AppDelegate.UserData.deviceToken = decodedResponse.DeviceToken
                            */
                            AppDelegate.UserData.lastLogin = decodedResponse.LastLogin
                            
                            // Override the setting based on User rights
                            // 1 = Elevated Mode == ERCMode Field
                            // Updated 26/08/21
                            AppDelegate.UserData.elevatedMode = AppDelegate.UserData.userRights == 1 ? true : false
                            self.password.resignFirstResponder()
                            do {
                                let path = Bundle.main.path(forResource: "finished.wav", ofType:nil)!
                                let url = URL(fileURLWithPath: path)
                                self.soundEffect = try AVAudioPlayer(contentsOf: url)
                                self.soundEffect?.play()
                             } catch {
                                print("couldn't load file")
  
                             }
    
                            
                            self.updateDeviceDetails(decodedResponse)
                            
                            self.RegisterDeviceAPI()
                            
                            self.activitySpinner.stopAnimating()
                            
                            self.performSegue(withIdentifier: "unwindSegue",  sender: self)
                          
                        } else {
                            do {
                                let path = Bundle.main.path(forResource: "warn.wav", ofType:nil)!
                                let url = URL(fileURLWithPath: path)
                                self.soundEffect = try AVAudioPlayer(contentsOf: url)
                                self.soundEffect?.play()
                            } catch {
                                print("couldn't load file")
                            }
                            self.errorImage.isHidden = false
                            Animations().addPulsation(object: self.errorImage.layer)
                            self.errorMessage.text = "Username or Password Incorrect"
                            self.errorMessage.isHidden = false
                            Animations().fadeInAnimation(object: self.errorMessage.layer)
                            self.activitySpinner.stopAnimating()
                        
                      }
                    }
                } else {
                    do {
                        let path = Bundle.main.path(forResource: "warn.wav", ofType:nil)!
                        let url = URL(fileURLWithPath: path)
                        self.soundEffect = try AVAudioPlayer(contentsOf: url)
                        self.soundEffect?.play()
                    } catch {
                        print("couldn't load file")
                    }
                    DispatchQueue.main.async {
                        self.errorImage.isHidden = false
                    
                        Animations().addPulsation(object: self.errorImage.layer)
                        self.errorMessage.text = "Data Decoding Failure"
                       
                        Animations().fadeInAnimation(object: self.errorMessage.layer)
                        self.activitySpinner.stopAnimating()
                    }
                    
                }
            }
            else {
                do {
                    let path = Bundle.main.path(forResource: "warn.wav", ofType:nil)!
                    let url = URL(fileURLWithPath: path)
                    self.soundEffect = try AVAudioPlayer(contentsOf: url)
                    self.soundEffect?.play()
                } catch {
                    print("couldn't load file")
                }
                DispatchQueue.main.async {
                    self.errorImage.isHidden = false
                }
                Animations().addPulsation(object: self.errorImage.layer)
                self.errorMessage.text = "Data Service Connection Failure"
                self.errorMessage.isHidden = false
                Animations().fadeInAnimation(object: self.errorMessage.layer)
                self.activitySpinner.stopAnimating()
            }
        }.resume()

    }
    
    // Register device for push notification service
    func RegisterDeviceAPI() {
        DispatchQueue.main.async {
           self.activitySpinner.startAnimating()
        }
        
        let params = ["SubTitle": "Register Device",
                      "Title": "iERC Notification",
                      "Message": "Do Not Respond to this request",
                      "UserID":  AppDelegate.UserData.userID,
                      "UserName": AppDelegate.UserData.userName,
                      "DeviceToken": AppDelegate.UserData.deviceToken,
                      "DeviceName": AppDelegate.UserData.deviceName,
                      "DeviceUUID": AppDelegate.UserData.deviceUUID
                     ] as [String : Any]
        

          // create the URL
          // https://notificationapi20210427110435.azurewebsites.net/api/APNSService
          guard let url = URL(string: AppDelegate.UserData.apnsServerIP + "/api/APNSService/RegisterERCAzureDevice") else {
                print("Invalid URL!")
                return
          }
          //now create the URLRequest object using the url object
          var request = URLRequest(url: url)
         
          request.addValue("application/json", forHTTPHeaderField: "Content-Type")
          request.addValue("application/json", forHTTPHeaderField: "Accept")
          request.httpMethod = "POST"
          request.timeoutInterval = 20
          
          do {
           request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
          } catch let error {
              print(error.localizedDescription)
          }

        
           URLSession.shared.dataTask(with: request) { data, response, error in
                   if let error = error {
                    print("Something went wrong: \(error)")
                   }
            guard let data = data else {
                print(String(describing: error))
                return
            }
            
 
               if let decodedResponse = try? JSONDecoder().decode(ServiceReturn.self, from: data) {
                   DispatchQueue.main.async {
                      if (decodedResponse.statusCode == 200) {
                       
                        print("Device Registered : \(decodedResponse.statusMessage)")
                        DispatchQueue.main.async {
                           self.activitySpinner.stopAnimating()
                        }
          
                      } else {
                        // Display Error
                        print("Something went wrong!")
                      }
                   }
               }
            }
            .resume()
      }
    
    
    private func updateDeviceDetails(_ loginModel: LoginCredentialsModel) {
        self.activitySpinner.startAnimating()
        // Build JSON object
        let parameters = ["UserID": AppDelegate.UserData.userID ,
                          "UserName": AppDelegate.UserData.userName,
                          "Password": AppDelegate.UserData.password,
                          "DeviceName": AppDelegate.UserData.deviceName,
                          "DeviceToken": AppDelegate.UserData.deviceToken,
                          "DeviceUUID": AppDelegate.UserData.deviceUUID] as Any
        //create the url with URL
        guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/UpdateDevice") else {
              print("Invalid URL!")
              return
        }
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
       
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        request.timeoutInterval = 20
        
        // Put request data into JSON object and add to Body
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        URLSession.shared.dataTask(with: request) { data, response, error in
            // Give me a clue what's wrong?!!!!
            do {
                _ =  try JSONDecoder().decode(LoginCredentialsModel.self, from: data!)
            } catch {
                print("Error Decoding:  \(error)")
            }
            
            if let data = data {
                if let decodedResponse = try? JSONDecoder().decode(LoginCredentialsModel.self, from: data) {
                    print(decodedResponse.DeviceToken .description)
                    DispatchQueue.main.async {
                       self.activitySpinner.stopAnimating()
                    }
 
                }
            }
             else {
                DispatchQueue.main.async {
                   self.activitySpinner.stopAnimating()
                }
              }
        }.resume()
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
    }
    

}
