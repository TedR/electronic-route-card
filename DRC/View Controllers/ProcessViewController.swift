//
//  ProcessViewController.swift
//  DRC
//
//  Created by Edward Reilly on 19/12/2019.
//  Copyright © 2019 Norcott. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import MessageUI


extension ProcessViewController: UIContextMenuInteractionDelegate, MFMailComposeViewControllerDelegate {

    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {

        return UIContextMenuConfiguration(identifier: nil, previewProvider: nil, actionProvider: { suggestedActions in

            return self.makeContextMenu()
        })
    }
    
    func makeContextMenu() -> UIMenu {

        // Create a UIAction for sharing
        let share = UIAction(title: "GNCs", image: UIImage(systemName: "00.circle")) { action in
            // Show system share sheet
        }

        // Create and return a UIMenu with the share action
        return UIMenu(title: "GNC Menu", children: [share])
    }
    
}

class ProcessViewController: UITableViewController {
    
    private let menuView = UIView()
    var processes = [ProcessModel]()
    var currentSLP: ProcessModel?
    var timeStates = [TimeStateModel]()
    var timeReasons = [TimeReasonModel]()
    var userRoles = [UserRole]()
    var showMyProcesses = Bool()

    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureView()
        
        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false

        // Register my custom cell
        self.tableView.register(UINib(nibName: "SLPTableCell", bundle: nil), forCellReuseIdentifier: "SLPCell")
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = UITableView.automaticDimension
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("refreshSubProcess"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("refreshMySubProcess"), object: nil)
        
        // Register to be notified of works instruction update
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("refreshInstruction"), object: nil)
        

        
        self.GetTimeMonitorStatesAPI(true)
        self.GetTimeMonitorReasonsAPI(true)
        
    }
    
    
    // This is receiver object for message sent from FitShortageController
    @objc func methodOfReceivedNotification(notification: Notification) {
        print("Message Received : \(notification.name)")
        
        if (notification.name.rawValue == "refreshInstruction") {
           
            print("Message Received for Instruction Update: \(notification.name)")
            // This message receives updated text and checks if email required
            let sendEmail = notification.userInfo!["sendEmail"] as? Bool
            if (sendEmail == true) {
               self.sendEmail(self.currentSLP!, "Works Instruction Update", notification.object as! String)
            }
        }
        
        if (notification.name.rawValue == "refreshMySubProcess") {
            self.showMyProcesses = notification.object as! Bool
            self.configureView()
        }
        else {
        
        guard case let ip as IndexPath = notification.object else {
            self.configureView()
            return
        }
        self.tableView.beginUpdates()
        self.configureView()
        self.tableView.reloadRows(at: [IndexPath(row: ip.row, section: ip.section)], with: .automatic)
        self.tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: UITableView.ScrollPosition.top)
        self.tableView.endUpdates()
        }
        
     }
    
    // Selected SLP so load processes
    func configureView() {
          print(AppDelegate.UserData.userID)
        // Update the user interface for the detail item.
        if let detail = subProcess {
            print("Batch: \(detail.BatchNumber)")
            if (AppDelegate.UserData.elevatedMode == true) {
                self.GetUserRolesAPI(true)
                self.GetSubProcessesAPI(detail.BatchNumber, 0, subProcess!.ProcessID, subProcess!.UniqueStepID,  AppDelegate.UserData.userID, self.showMyProcesses, subProcess!.ProcStatus) }
            else {
                self.GetSubProcessesAPI(detail.BatchNumber, 0, subProcess!.ParentIndex, subProcess!.ParentPRCIndex,  AppDelegate.UserData.userID, self.showMyProcesses, subProcess!.ProcStatus)
            }
            self.tableView.reloadData()
        }
    }

    // This holds TLP Process model selected
    var subProcess: ProcessModel? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    
    private func newQAAlertProcessController(identifier: String, slp: ProcessModel) -> QAAlertProcessController {
        let vc  = self.storyboard!.instantiateViewController(identifier: identifier, creator: { coder in
           QAAlertProcessController(coder: coder, slp: slp)
        })
        return vc
    }
    
    
    
    // MARK: - Alert Dialog
    
    func showAlert(_ title: String, _ body: String) {
        let alertController = UIAlertController(title: title, message: body, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: {
            
        })
    }
    
    
    // MARK: - Web Services
    
    func PostCommentAPI(_ hdrIX: Int, _ stepIX: Int, _ comment: String) {
          // If requesting all comments against a batch then set stepIX = - 1
          // http://192.168.33.9:6002/api/routecardservice/AddProcessComment

        let params = ["PUC_PCH_IX": hdrIX,
                      "PUC_PRC_IX": stepIX,
                      "PUC_UN_IX": AppDelegate.UserData.userID,
                      "PUC_Comments": comment] as [String : Any]
           //create the url with URL
           guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/AddProcessComment") else {
                 print("Invalid URL!")
                 return
           }
           //now create the URLRequest object using the url object
           var request = URLRequest(url: url)
          
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           request.addValue("application/json", forHTTPHeaderField: "Accept")
           request.httpMethod = "POST"
           request.timeoutInterval = 20
           
           do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
           } catch let error {
               print(error.localizedDescription)

           }

            URLSession.shared.dataTask(with: request) { data, response, error in
                    if let error = error {
                        print("Something went wrong: \(error)")
                    }
                    if let data = data {
                        if let decodedResponse = try? JSONDecoder().decode(POSTStatus.self, from: data) {
                            DispatchQueue.main.async {
                                if (decodedResponse.StatusCode == 1) {
                                    print("Success: \(decodedResponse.StatusMessage)")
                                    self.doRefresh(self.refreshControl!)
                                }
                            }
                        }
                    }
                    
               }.resume()
       }
    
    // Get User Roles
    func GetUserRolesAPI(_ elevated: Bool) {
       // http://192.168.33.9:6002/api/routecardservice/GetUserRoles/true
        
        WebRequest<UserRole>.get(self, service: "GetUserRoles", params:[String(elevated)], success: { result in
            
            self.userRoles = result
            
            for role in self.userRoles {
                print(role.DeviceToken? .description ?? "Not Set")
            }

          })
        
      }
    
    // Get Process Steps
    func GetSubProcessesAPI(_ batch: Int, _ tlp: Int, _ prsIX: Int, _ prcIX: Int, _ un_ix: Int, _ myProc: Bool, _ procState:Int) {
       // http://192.168.33.9:6002/api/routecardservice/getProcesses/10192/1/104/true
       // GetProcesses/{batch}/{tlp}/{un_ix}/{myProc}
        
        WebRequest<ProcessModel>.get(self, service: "GetProcesses", params:[String(batch), String(tlp), String(un_ix), String(myProc)], success: { result in
                    self.processes = result
                    DispatchQueue.main.async {
                         self.processes = self.processes.filter {
                            if (AppDelegate.UserData.elevatedMode == true) {
                                return ( $0.ParentIndex == prsIX && $0.ParentPRCIndex == prcIX) }
                                    else {
                                return ( $0.ParentIndex == prsIX && $0.ParentPRCIndex == prcIX && $0.ProcStatus == procState)
                                }
                         }
                         self.tableView.reloadData()
                        // TODO: - If only a TLP, the subProcess is cloned.  This causes
                        // an issue with the refresh when editing an instruction as the self.subProcess
                        // is not refreshed.
                         if (self.processes.count <= 0) && (myProc == false) {
                             self.processes.append(self.subProcess!)
                             self.tableView.reloadData()
                          }
                        }
                    }
          )
        
     }
    
    // Get Time Monitor States
    func GetTimeMonitorStatesAPI(_ showAll: Bool) {
       // http://192.168.33.9:6002/api/routecardservice/getTimeState/true
       // GetTimeState/{showAll}
        
        WebRequest<TimeStateModel>.get(self, service: "GetTimeState", params:[String(showAll)], success: { result in
                // Flat list of data captures ...
                self.timeStates = result
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            )
        
      }
    
    // Get Time Monitor States
    func GetTimeMonitorReasonsAPI(_ showAll: Bool) {
        // http://192.168.33.9:6002/api/routecardservice/getTimeReasons/true
        // GetTimeReasons/{showAll}
        
        WebRequest<TimeReasonModel>.get(self, service: "GetTimeReasons", params:[String(showAll)], success: { result in
                    DispatchQueue.main.async {
                         self.timeReasons = result
                    }
                }
            )
        
        
      }
    
    
    func PostTimeMonitorAPI(_ timeMonitor: TimeMonitorModel, _ indexPath: IndexPath) {
    // http://192.168.33.9:6002/api/routecardservice/AddTimeMonitor
    let semaphore = DispatchSemaphore(value: 1)
        let params = ["PTM_RCH_IX": timeMonitor.PTM_RCH_IX,
                      "PTM_PRC_IX": timeMonitor.PTM_PRC_IX,
                      "PTM_RCI_IX": timeMonitor.PTM_RCI_IX,
                      "PTM_UN_IX": timeMonitor.PTM_UN_IX,
                      "PTM_PTS_IX": timeMonitor.PTM_PTS_IX ?? 0,
                      "PTM_PTR_IX": timeMonitor.PTM_PTR_IX ?? 0
                 ] as [String : Any]
          //create the url with URL
          guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/AddTimeMonitor") else {
                print("Invalid URL!")
                return
          }
          //now create the URLRequest object using the url object
          var request = URLRequest(url: url)
         
          request.addValue("application/json", forHTTPHeaderField: "Content-Type")
          request.addValue("application/json", forHTTPHeaderField: "Accept")
          request.httpMethod = "POST"
          request.timeoutInterval = 20
          
          do {
           request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
          } catch let error {
              print(error.localizedDescription)
          }

           URLSession.shared.dataTask(with: request) { data, response, error in
                   if let error = error {
                       print("Something went wrong: \(error)")

                   }
            guard let data = data else {
                print(String(describing: error))
                return
            }
            semaphore.signal()
               if let decodedResponse = try? JSONDecoder().decode(POSTStatus.self, from: data) {
                   DispatchQueue.main.async {
                      if (decodedResponse.StatusCode == 1) {
                           print("Success: \(decodedResponse.StatusMessage)")
                           // Reload data and refresh the changed row ...
                           NotificationCenter.default.post(name: Notification.Name("refreshSubProcess"), object: indexPath)
                           
                           // If not in Elvated Mode Force a reload of list on the Master Page ..
                        if (AppDelegate.UserData.elevatedMode == false) {
                            NotificationCenter.default.post(name: Notification.Name("refreshUserCentricProcess"), object: nil)
                        }
                      }
                   }
               }
            }
            .resume()
            semaphore.wait()
      }
    
    
    

    
    func displayActionSheet(_ slp: ProcessModel, _ indexPath: IndexPath) {
        
        
        // Get new status required to make Paused
        let newState = self.timeStates.filter {
            return ( $0.PTS_Description == "Paused" )
        }.first
        
        let alert = UIAlertController(title: "Process Monitor", message: "\nSelect Reason if Extended Delay", preferredStyle: UIAlertController.Style.alert)
    
        alert.view.tintColor = .systemRed
        
        for reason in self.timeReasons {
            // Add a button for each reason
            let btn = UIAlertAction(title: reason.PTR_Reason, style: UIAlertAction.Style.default) { (action) -> Void in
                let timeMonitor = TimeMonitorModel(PTM_IX: 0, PTM_RCH_IX: slp.RouteCardHdrIndex, PTM_PRC_IX: slp.UniqueStepID, PTM_RCI_IX:  slp.InstIndex, PTM_UN_IX: AppDelegate.UserData.userID, PTM_Event_Time: nil, PTM_PTS_IX: newState?.PTS_State, PTM_PTR_IX: reason.PTR_IX)
                self.PostTimeMonitorAPI(timeMonitor, indexPath)
                
          }
          btn.setValue(UIImage(systemName: "wrench.fill")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), forKey: "image")
          alert.addAction(btn)
        
        }
        // Default action with nil reason code set
        let cancelButton = UIAlertAction(title: "Just Pause", style: UIAlertAction.Style.default) { (cancelSelected) -> Void in
            let timeMonitor = TimeMonitorModel(PTM_IX: 0, PTM_RCH_IX: slp.RouteCardHdrIndex, PTM_PRC_IX: slp.UniqueStepID, PTM_RCI_IX:  slp.InstIndex, PTM_UN_IX: AppDelegate.UserData.userID, PTM_Event_Time: nil, PTM_PTS_IX: newState?.PTS_State, PTM_PTR_IX: nil)
            self.PostTimeMonitorAPI(timeMonitor, indexPath)
        }
        cancelButton.setValue(UIColor.systemOrange, forKey: "titleTextColor")
        alert.addAction(cancelButton)
        self.present(alert, animated: true, completion: nil)
    }
    

    // MARK: -  Table Functions

    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        // Get current sub-process ..
        let slp = processes[indexPath.row]
        // Get current state for process
        let currentState = self.timeStates.filter {
            return ( $0.PTS_State == slp.ProcStatus )
        }.first
        print(String("Current State: \(currentState?.PTS_Description)"))
        // This will hold array of available actions once determined
        var actions = [UIContextualAction]()
        // Add actions based on scan type
        switch slp.ScanTypeIndex {
            case AppDelegate.ScanMode.NoScan.rawValue:
                print("Type: \(slp.ScanTypeText ?? "N/A")")
            case AppDelegate.ScanMode.Process.rawValue:
                print("Type: \(slp.ScanTypeText ?? "N/A")")
            case AppDelegate.ScanMode.Inspection.rawValue:
                print("Type: \(slp.ScanTypeText ?? "N/A")")
            case AppDelegate.ScanMode.Batch.rawValue:
                print("Type: \(slp.ScanTypeText ?? "N/A")")
            case AppDelegate.ScanMode.Confirmation.rawValue:
                print("Type: \(slp.ScanTypeText ?? "N/A")")
            // Added Sample
            case AppDelegate.ScanMode.Sample.rawValue:
                print("Type: \(slp.ScanTypeText ?? "N/A")")
            // Added Panel
            case AppDelegate.ScanMode.Panel.rawValue:
                print("Type: \(slp.ScanTypeText ?? "N/A")")
            default:
                print("Not Valid Case!")
        }
        
        let actionStartProcess = UIContextualAction(style: .normal, title: String("Resume Process"),
          handler: { (actionStartProcess, view, completionHandler) in
            
            // Get new status required to make Active
            let newState = self.timeStates.filter {
                return ( $0.PTS_Description == "Active" )
            }.first
            print(String("New State: \(newState?.PTS_Description)"))
            
            // Perform additional validation??
        
            let timeMonitor = TimeMonitorModel(PTM_IX: 0, PTM_RCH_IX: slp.RouteCardHdrIndex, PTM_PRC_IX: slp.UniqueStepID, PTM_RCI_IX:  slp.InstIndex, PTM_UN_IX: AppDelegate.UserData.userID, PTM_Event_Time: nil, PTM_PTS_IX: newState?.PTS_State, PTM_PTR_IX: nil)
            self.PostTimeMonitorAPI(timeMonitor, indexPath)
            
            // Signal we have completed action ..
            completionHandler(true)
        })
        actionStartProcess.image = UIImage(systemName: "play.circle.fill")
        actionStartProcess.backgroundColor = .systemGreen
        
        let actionPauseProcess = UIContextualAction(style: .normal, title: String("Pause Process"),
          handler: { (actionPauseProcess, view, completionHandler) in
          
          // Get new status required to make Paused
          let newState = self.timeStates.filter {
              return ( $0.PTS_Description == "Paused" )
          }.first
          print(String("New State: \(newState?.PTS_Description)"))
          
          // Perform additional validation??
          self.displayActionSheet(slp, indexPath)
            
          // Signal we have completed action ..
         
            completionHandler(true)
                
            
        })
        actionPauseProcess.image = UIImage(systemName: "pause.circle.fill")
        actionPauseProcess.backgroundColor = .systemOrange
        
        let actionCompleteProcess = UIContextualAction(style: .normal, title: String("Complete Process"),
          handler: { (actionCompleteProcess, view, completionHandler) in
            
            // Get new status required to make Complete
            let newState = self.timeStates.filter {
                return ( $0.PTS_Description == "Complete" )
            }.first
            print(String("New State: \(newState?.PTS_Description)"))
            
            // Perform additional validation??
              
            let timeMonitor = TimeMonitorModel(PTM_IX: 0, PTM_RCH_IX: slp.RouteCardHdrIndex, PTM_PRC_IX: slp.UniqueStepID, PTM_RCI_IX:  slp.InstIndex, PTM_UN_IX: AppDelegate.UserData.userID, PTM_Event_Time: nil, PTM_PTS_IX: newState?.PTS_State, PTM_PTR_IX: nil)
            self.PostTimeMonitorAPI(timeMonitor, indexPath)
          
            // Signal we have completed action ..
          completionHandler(true)
        })
        actionCompleteProcess.image = UIImage(systemName: "stop.circle.fill")
        actionCompleteProcess.backgroundColor = .systemRed
        
        
        // Think this should just show an alert as quick way of adding a comment?
        // This loads new view controller before alert ...
        // View full list of comments for this process
        let actionComment = UIContextualAction(style: .normal, title: String("Comment"),
          handler: { (actionComment, view, completionHandler) in
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "CommentsViewController") as! CommentsViewController
            vc.processItem = slp
            let nav : UINavigationController = UINavigationController(rootViewController: vc)
            self.navigationController?.present(nav, animated: true, completion: nil)
            completionHandler(true)
        })
        actionComment.image = UIImage(systemName: "list.dash")
        actionComment.backgroundColor = .systemBlue

        
        let actionConsumable = UIContextualAction(style: .normal, title: String("Consumable"),
          handler: { (actionConsumable, view, completionHandler) in
          // Update data source when user taps action
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "Consumables") as! ConsumablesViewController
            vc.processItem = slp
            let nav : UINavigationController = UINavigationController(rootViewController: vc)
            nav.definesPresentationContext = true
            nav.modalPresentationStyle = .fullScreen
            self.navigationController?.present(nav, animated: false, completion: nil)
            
          completionHandler(true)
        })
        actionConsumable.image = UIImage(systemName: "leaf.arrow.circlepath")
        actionConsumable.backgroundColor = .systemIndigo
        /*
        actionConsumable.image = UIGraphicsImageRenderer(size: CGSize(width: 64, height: 64)).image {
            _ in UIImage(systemName: "leaf.arrow.circlepath")!.draw(in: CGRect(x: 0, y: 0, width: 64, height: 64))
        }
        */
        
        // Shortage
        let actionShortage = UIContextualAction(style: .normal, title: String("Shortages"),
          handler: { (actionShortage, view, completionHandler) in
              // Update data source when user taps action
              let vc = self.storyboard!.instantiateViewController(withIdentifier: "Shortages") as! ShortagesViewController
              // load for individual process
              vc.processItem = slp
              vc.canEdit = true
              let nav : UINavigationController = UINavigationController(rootViewController: vc)
            self.navigationController?.present(nav, animated: true, completion: nil)
          completionHandler(true)
        })
        actionShortage.image = UIImage(systemName: "minus.rectangle")
        actionShortage.backgroundColor = .systemTeal
        
        // Measurement
        let actionMeasurement = UIContextualAction(style: .normal, title: String("Measurement"),
          handler: { (actionMeasurement, view, completionHandler) in
              // Update data source when user taps action
            
            let vc  = self.storyboard!.instantiateViewController(identifier: "MeasurementController", creator: { coder in
                MeasurementTabController(coder: coder, wocItem: AppDelegate.AppData.currentWOC, processItem: slp)
             })
              let nav : UINavigationController = UINavigationController(rootViewController: vc)
                  self.navigationController?.present(nav, animated: true, completion: nil)
          completionHandler(true)
        })
        actionMeasurement.image = UIImage(systemName: "plus.circle")
        actionMeasurement.backgroundColor = .systemOrange
        
        // Request Assistance 1st-off
        let actionRequestFirstOff = UIContextualAction(style: .normal, title: String("User 1st-off"),
          handler: { (actionRequestFirstOff, view, completionHandler) in
              // Update data source when user taps action
        
            
            self.displayRequestList(slp)

            
            completionHandler(true)
        })
        actionRequestFirstOff.image = UIImage(systemName: "bell")
        actionRequestFirstOff.backgroundColor = .systemRed
        
        
        
            
        switch currentState?.PTS_Description {
        case "Not Started": // Paused
            actions.append(actionStartProcess);
            actionStartProcess.title = "Start Process"
        case "Active": // Started so option to PAUSE or COMPLETE
            actions.append(actionPauseProcess);
            actions.append(actionCompleteProcess);
        case "Paused":
            actions.append(actionStartProcess);
            actions.append(actionCompleteProcess);
        case "Complete":
            print("Complete!")
        default: // Error!
            print("Error - Option not Configured")
            let alert = UIAlertController(title: "Error Adding Process Time", message: "Time Monitor Option Not Configured", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { _ in
                      //Cancel Action
                      print("Cancel")
                   }))
            present(alert, animated: true)
        }
    

        actions.append(actionComment)
        actions.append(actionConsumable)
        actions.append(actionShortage)
        actions.append(actionMeasurement)
        actions.append(actionRequestFirstOff)
        
        let configuration = UISwipeActionsConfiguration(actions: actions)
        
        return configuration
    }
    
    // Displays a list of User Request Notifications
    func displayRequestList(_ slp: ProcessModel) {
        // let requestReason: [String] =  [ "Inspection Sign Off", "Query (Frontend)", "Problem (Process)" ]
        var image: String = ""
       
        let alert = UIAlertController(title: "User Assistance", message: "\nSelect Personnel for Request", preferredStyle: UIAlertController.Style.alert)
        alert.view.tintColor = .systemGreen
        // Get roles where device is registered
        let roles = self.userRoles.filter {
            // return (  ($0.DeviceToken?.isEmpty) != nil && ($0.UserRoleIndex == 4 || $0.UserRoleIndex == 13))
            // && ($0.TTSIndex == slp.TTSIndex_1)
            // Now permitting any process step to initiate a First Off Request IF Inspector or QA
            return (  ($0.DeviceToken?.isEmpty) != nil && ($0.TTSIndex == 4 || $0.TTSIndex == 13 || $0.TTSIndex == 23 || $0.TTSIndex == 24) )
        }
        // $0.TTSIndex == slp.TTSIndex_1 &&
        
        // Remove duplicate users as may have multiple roles
        let uniqueRoles = removeDuplicateElements(roles: roles)
        
        // Loop thru each role
        for role in uniqueRoles {
            // Add a button for each reason
            let btn = UIAlertAction(title: "\(role.UserInitials) : \(role.UserTitle)", style: UIAlertAction.Style.default) { (action) -> Void in
                self.PushMessageAPI(slp, "\(role.UserInitials) : \(role.UserTitle)",  "User_Signoff_Request", role)
            }
            switch role.TTSIndex {
                case 4, 13:
                    // Send to QA
                    image = "checkmark.circle"
                default:
                image = "asterisk.circle"

            }
            btn.setValue(UIImage(systemName: image)?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), forKey: "image")
            alert.addAction(btn)
        }
        
        // Default action with nil reason code set
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (cancelSelected) -> Void in

        }
        cancelButton.setValue(UIColor.systemRed, forKey: "titleTextColor")
        alert.addAction(cancelButton)
        
        // Broadcast message to all QA
        let allButton = UIAlertAction(title: "Broadcast", style: UIAlertAction.Style.default) { (allSelected) -> Void in
                self.BroadcastMessageAPI(slp, roles)
        }
        allButton.setValue(UIColor.systemBlue, forKey: "titleTextColor")
        alert.addAction(allButton)
        
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func removeDuplicateElements(roles: [UserRole]) -> [UserRole] {
        var uniqueRoles = [UserRole]()
        for role in roles {
            if !uniqueRoles.contains(where: {$0.UserID == role.UserID}) {
                uniqueRoles.append(role)
            }
        }
        return uniqueRoles
    }
    
    // Send Request for Sign of Using Push Service
    // This needs to be passed the correct device from userRoles table lookup
    func PushMessageAPI(_ process: ProcessModel, _ request: String, _ category: String, _ userRole: UserRole) {
    // http://192.168.33.9:6002/api/routecardservice/PushMessage
        // self.waitSpinner.isHidden = false
        // self.waitSpinner.startAnimating()
        let msgReq = ["SubTitle": request,
                      "Title": "iERC Notification",
                      "Message": "Please respond to this request",
                      "Category": category,
                      "UserID":  userRole.UserID,
                      "UserName": userRole.UserName,
                      "DeviceToken": userRole.DeviceToken!,
                      "DeviceName": userRole.DeviceName!,
                      "DeviceUUID": userRole.DeviceUUID!
                     ] as [String : Any]
        
        let params = ["BatchNumber": process.BatchNumber,
                      "JobNumber": process.JobNumber,
                      "ParentIndex": process.ParentIndex,
                      "ParentPRCIndex": process.ParentPRCIndex,
                      "UniqueStepID": process.UniqueStepID,
                      "ProcessID": process.ProcessID,
                      "ProcessName": process.ProcessName!,
                      "RouteCarHdrIndex": process.RouteCardHdrIndex,
                      "TTSIndex_1": process.TTSIndex_1,
                      "TTSIndex_2": process.TTSIndex_2,
                      "Message": msgReq
                     ] as [String : Any]
          // create the URL
          // https://notificationapi20210427110435.azurewebsites.net/api/APNSService
          guard let url = URL(string: AppDelegate.UserData.apnsServerIP + "/api/APNSService/PushERCAzureNotification") else {
                print("Invalid URL!")
                return
          }
          //now create the URLRequest object using the url object
          var request = URLRequest(url: url)
         
          request.addValue("application/json", forHTTPHeaderField: "Content-Type")
          request.addValue("application/json", forHTTPHeaderField: "Accept")
          request.httpMethod = "POST"
          request.timeoutInterval = 20
          
          do {
           request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
          } catch let error {
              print(error.localizedDescription)
                DispatchQueue.main.async {
                self.showAlert("Error", "Error sending message")
               }
          }

        
           URLSession.shared.dataTask(with: request) { data, response, error in
                   if let error = error {
                    print("Something went wrong: \(error)")
                    DispatchQueue.main.async {
                       self.showAlert("Error", "Error sending message")
                    }
                   }
            guard let data = data else {
                print(String(describing: error))
                DispatchQueue.main.async {
                    self.showAlert("Error", "Error sending message")
                }
                return
            }
            
 
               if let decodedResponse = try? JSONDecoder().decode(ServiceReturn.self, from: data) {
                   DispatchQueue.main.async {
                      if (decodedResponse.statusCode == 200) {
                        let alertController = UIAlertController(title: "Success",
                                                                message: "Message sent successfully\nTracking ID: " + decodedResponse.statusMessage, preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: "OK", style: .default)
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion: { [self] in
                            print("Message Sent Completion")
                            print("Success: \(decodedResponse.statusMessage)")
                            // Now write the TrackingID to SQL
                            let request = RequestNotification(RN_IX: -1, RN_RCH_IX: process.RouteCardHdrIndex, RN_PRC_IX: process.UniqueStepID, RN_RCI_IX: process.InstIndex, RN_RCI_TTS_IX: process.TTSIndex_1, RN_UN_IX: userRole.UserID, RN_PDR_IX: userRole.DeviceIndex, RN_Tracking_ID: decodedResponse.statusMessage, RN_Date_Added: Date(), RN_Status: "Open", RN_User_Name: userRole.UserName)
                            
                            // PSO_PRS_IX: process.UniqueStepID,
                            let signoff = SignOff(BH_Number: process.BatchNumber, BH_PIN: AppDelegate.AppData.currentWOC.PinNumber ?? "", PSO_IX: -1, PSO_RN_Tracking_ID: decodedResponse.statusMessage, PSO_PRC_IX: process.UniqueStepID, PSO_PCH_IX: process.RouteCardHdrIndex, PSO_RCI_IX: process.InstIndex, PSO_PRS_Step_IX: process.ProcessID, PSO_User: AppDelegate.UserData.userID, UN_Full_Name: AppDelegate.UserData.userName, PSO_Approved_By: -1, PSO_PSN_IX: -1, PSO_PSN_Serial: "", PSO_Quantity: -1, PSO_Completed: nil, PSO_Comments: "", RN_UN_IX: -1, UN_Initials: "", RN_Status: "Open", RCI_Step: process.StepText, RCI_Instruction: process.Instruction, PSO_Status: "Pending")
                            
                            
                            // print(request)
                            
                            self.SaveMessageAPI(request)
                            
                            // print(signoff)
                            
                            self.SaveSignOffAPI(signoff)
                            
                            
                            // self.waitSpinner.stopAnimating()
                            
                        })
                           
                      } else {
                        // Display Error

                        self.showAlert("Error Sending Message\n", decodedResponse.statusMessage)
                      }
                   }
               }
            }
            .resume()
      }
    
    // Send Request for Sign of Using Push Service
    // This needs to be passed the correct device from userRoles table lookup
    // Broadcasts to all inspectors
    func BroadcastMessageAPI(_ process: ProcessModel, _ userRoles: [UserRole]) {
    // http://192.168.33.9:6002/api/routecardservice/PushMessage
        // self.waitSpinner.isHidden = false
        // self.waitSpinner.startAnimating()
        for userRole in userRoles {
            let category = "User_Signoff_Request"
            let msgReq = ["SubTitle": "\(userRole.UserInitials) : \(userRole.UserTitle)",
                      "Title": "iERC Notification",
                      "Message": "Please respond to this request",
                      "Category": category,
                      "UserID":  userRole.UserID,
                      "UserName": userRole.UserName,
                      "DeviceToken": userRole.DeviceToken!,
                      "DeviceName": userRole.DeviceName!,
                      "DeviceUUID": userRole.DeviceUUID!
                     ] as [String : Any]
        
        let params = ["BatchNumber": process.BatchNumber,
                      "JobNumber": process.JobNumber,
                      "ParentIndex": process.ParentIndex,
                      "ParentPRCIndex": process.ParentPRCIndex,
                      "UniqueStepID": process.UniqueStepID,
                      "ProcessID": process.ProcessID,
                      "ProcessName": process.ProcessName!,
                      "RouteCarHdrIndex": process.RouteCardHdrIndex,
                      "TTSIndex_1": process.TTSIndex_1,
                      "TTSIndex_2": process.TTSIndex_2,
                      "Message": msgReq
                     ] as [String : Any]
          // create the URL
          // https://notificationapi20210427110435.azurewebsites.net/api/APNSService
          guard let url = URL(string: AppDelegate.UserData.apnsServerIP + "/api/APNSService/PushERCAzureNotification") else {
                print("Invalid URL!")
                return
          }
          //now create the URLRequest object using the url object
          var request = URLRequest(url: url)
         
          request.addValue("application/json", forHTTPHeaderField: "Content-Type")
          request.addValue("application/json", forHTTPHeaderField: "Accept")
          request.httpMethod = "POST"
          request.timeoutInterval = 20
          
          do {
           request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
          } catch let error {
              print(error.localizedDescription)
                DispatchQueue.main.async {
                self.showAlert("Error", "Error sending message")
               }
          }

        
           URLSession.shared.dataTask(with: request) { data, response, error in
                   if let error = error {
                    print("Something went wrong: \(error)")
                    DispatchQueue.main.async {
                       self.showAlert("Error", "Error sending message")
                    }
                   }
            guard let data = data else {
                print(String(describing: error))
                DispatchQueue.main.async {
                    self.showAlert("Error", "Error sending message")
                }
                return
            }
            
 
               if let decodedResponse = try? JSONDecoder().decode(ServiceReturn.self, from: data) {
                   DispatchQueue.main.async {
                      if (decodedResponse.statusCode == 200) {
                            print("Message Sent Completion")
                            print("Success: \(decodedResponse.statusMessage)")
                            // Now write the TrackingID to SQL
                            let request = RequestNotification(RN_IX: -1, RN_RCH_IX: process.RouteCardHdrIndex, RN_PRC_IX: process.UniqueStepID, RN_RCI_IX: process.InstIndex, RN_RCI_TTS_IX: process.TTSIndex_1, RN_UN_IX: userRole.UserID, RN_PDR_IX: userRole.DeviceIndex, RN_Tracking_ID: decodedResponse.statusMessage, RN_Date_Added: Date(), RN_Status: "Open", RN_User_Name: userRole.UserName)
                            
                        // PSO_PRS_IX: process.UniqueStepID, 
                          let signoff = SignOff(BH_Number: process.BatchNumber, BH_PIN: AppDelegate.AppData.currentWOC.PinNumber ?? "", PSO_IX: -1, PSO_RN_Tracking_ID: decodedResponse.statusMessage, PSO_PRC_IX: process.UniqueStepID, PSO_PCH_IX: process.RouteCardHdrIndex, PSO_RCI_IX: process.InstIndex, PSO_PRS_Step_IX: process.ProcessID, PSO_User: AppDelegate.UserData.userID, UN_Full_Name: AppDelegate.UserData.userName, PSO_Approved_By: -1, PSO_PSN_IX: -1, PSO_PSN_Serial: "", PSO_Quantity: -1, PSO_Completed: nil, PSO_Comments: "", RN_UN_IX: -1, UN_Initials: "", RN_Status: "Open", RCI_Step: process.StepText, RCI_Instruction: process.Instruction, PSO_Status: "Pending")
                            
                            
                            // print(request)
                            
                            self.SaveMessageAPI(request)
                            
                            // print(signoff)
                            
                            self.SaveSignOffAPI(signoff)
                            
                            
                            // self.waitSpinner.stopAnimating()
                          
                          
                           let alert = UIAlertController(title: "Broadcast Status", message: "\nMessages sent succesffully", preferredStyle: UIAlertController.Style.alert)
                           alert.view.tintColor = .systemGreen
                           // Default action
                           let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (OKSelected) -> Void in
                          }
                          okButton.setValue(UIColor.systemGreen, forKey: "titleTextColor")
                          alert.addAction(okButton)
                          self.present(alert, animated: true, completion: nil)
                             
                      } else {
                        // Display Error

                        self.showAlert("Error Sending Message\n", decodedResponse.statusMessage)
                      }
                   }
               }
            }
            .resume()
        }
      }
    
    // Once a Tracking ID is received back, save to SQL
    func SaveMessageAPI(_ request: RequestNotification) {
        let params = ["RN_RCH_IX": request.RN_RCH_IX,
                      "RN_PRC_IX": request.RN_PRC_IX,
                      "RN_RCI_IX": request.RN_RCI_IX,
                      "RN_RCI_TTS_IX": request.RN_RCI_TTS_IX,
                      "RN_UN_IX": request.RN_UN_IX,
                      "RN_PDR_IX": request.RN_PDR_IX,
                      "RN_Tracking_ID": request.RN_Tracking_ID,
                      "RN_Status": request.RN_Status,
                      "RN_User_Name": request.RN_User_Name
                     ] as [String : Any]
        
        // create the url with URL
        guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/AddRequestNotification") else {
              print("Invalid URL!")
              return
        }
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        
        do {
         request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
              DispatchQueue.main.async {
                self.showAlert("Error", "Error Encoding message")
             }
        }
        //now create the URLRequest object using the url object
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        request.timeoutInterval = 20
        
        do {
         request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
              DispatchQueue.main.async {
              self.showAlert("Error", "Error Encoding message")
             }
        }

      
        URLSession.shared.dataTask(with: request) { data, response, error in
        if let error = error {
                  print("Something went wrong: \(error)")
                  DispatchQueue.main.async {
                     self.showAlert("Error", "Error saving message")
                  }
        }
        guard let data = data else {
              print(String(describing: error))
              DispatchQueue.main.async {
                 self.showAlert("Error", "Error daving message")
              }
              return
          }
          

             if let decodedResponse = try? JSONDecoder().decode(ServiceReturn.self, from: data) {
                 DispatchQueue.main.async {
                    if (decodedResponse.statusCode == 1) {
                          print("Message Saved Completion")
                          print("Success: \(decodedResponse.statusMessage)")
                    } else {
                      // Display Error
                        print("Message Save Error!")
                        print("Error: \(decodedResponse.statusMessage)")
                    }
                 }
             }
          }
          .resume()

        
    }
    
    
    // Once a Tracking ID is received back, save to SQL
    func SaveSignOffAPI(_ signoff: SignOff) {
        let params = ["PSO_RN_Tracking_ID": signoff.PSO_RN_Tracking_ID!,
                      "PSO_PRC_IX": signoff.PSO_PRC_IX,
                      "PSO_PCH_IX": signoff.PSO_PCH_IX,
                      "PSO_RCI_IX": signoff.PSO_RCI_IX,
                      "PSO_PRS_Step_IX": signoff.PSO_PRS_Step_IX,
                      "PSO_User": signoff.PSO_User,
                      "PSO_PSN_Serial": "",
                      "PSO_Comments": ""
                     ] as [String : Any]
        
        // create the url with URL
        guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/AddUserSignoff") else {
              print("Invalid URL!")
              return
        }
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        
        do {
         request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
              DispatchQueue.main.async {
                self.showAlert("Error", "Error Encoding message")
             }
        }
        //now create the URLRequest object using the url object
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        request.timeoutInterval = 20
        
        do {
         request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
              DispatchQueue.main.async {
              self.showAlert("Error", "Error Encoding Request")
             }
        }

      
        URLSession.shared.dataTask(with: request) { data, response, error in
        if let error = error {
                  print("Something went wrong: \(error)")
                  DispatchQueue.main.async {
                     self.showAlert("Error", "Error saving Request")
                  }
        }
        guard let data = data else {
              print(String(describing: error))
              DispatchQueue.main.async {
                 self.showAlert("Error", "Error saving Request")
              }
              return
          }
          

         if let decodedResponse = try? JSONDecoder().decode(ServiceReturn.self, from: data) {
             DispatchQueue.main.async {
                if (decodedResponse.statusCode == 1) {
                      print("Request Saved Completion")
                      print("Success: \(decodedResponse.statusMessage)")
                } else {
                  // Display Error
                    print("Request Save Error!")
                    print("Error: \(decodedResponse.statusMessage)")
                }
             }
         }
      }
      .resume()

        
    }
    
    @IBAction func doRefresh(_ sender: UIRefreshControl) {
        if (subProcess != nil) {
            let font = UIFont.systemFont(ofSize: 16)
            let attributes: [NSAttributedString.Key: Any] = [
                .font: font,
                .foregroundColor: UIColor.systemGreen,
            ]
            let title = NSAttributedString(string: "Refreshing Sequence for Batch: \(subProcess!.BatchNumber)", attributes: attributes)
            sender.attributedTitle = title
            if (AppDelegate.UserData.elevatedMode == true) {
                self.GetSubProcessesAPI(subProcess!.BatchNumber, 0, subProcess!.ProcessID, subProcess!.UniqueStepID,  AppDelegate.UserData.userID, self.showMyProcesses, subProcess!.ProcStatus) }
            else {
                self.GetSubProcessesAPI(subProcess!.BatchNumber, 0, subProcess!.ParentIndex, subProcess!.ParentPRCIndex,  AppDelegate.UserData.userID, self.showMyProcesses, subProcess!.ProcStatus)
            }
        }
        
        sender.endRefreshing()
        
    }
    
    @objc func showDrawingsTapped(_ sender: NorcottTapGestureRecognizer) {
        // Get current state from data source
        let slp: ProcessModel = sender.myCustomValue as! ProcessModel
        self.performSegue(withIdentifier: "DrawingsCollection", sender: slp)
        /*
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "DrawingsViewController") as! DrawingsViewController
        vc.processItem = slp
        self.navigationController?.present(vc, animated: true, completion: nil)
        */
    }
    
    @objc func buttonTapped(_ sender: UIButton) {
        // Get current state from data source
        print(sender.tag)
        let slp = processes[sender.tag]
        // Add actions based on scan type
        switch slp.ScanTypeIndex {
            // Handles NO SCAN
            case AppDelegate.ScanMode.NoScan.rawValue:
                print("Type: \(slp.ScanTypeText ?? "N/A")")
            
            // Handle a production SCAN
            case AppDelegate.ScanMode.Process.rawValue:
                print("Type: \(slp.ScanTypeText ?? "N/A")")
                // Modified to use batch scan screen
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "BatchScanCapture") as! BatchScanViewController
                vc.processItem = slp
                vc.indexPath = Int(sender.tag)
                self.navigationController?.present(vc, animated: true, completion: nil)
            
            // Handle an inspection SCAN
            // Added Sample SCAN
            // Added Panel SCAN
            case AppDelegate.ScanMode.Inspection.rawValue, AppDelegate.ScanMode.Sample.rawValue, AppDelegate.ScanMode.Panel.rawValue:
                print("Type: \(slp.ScanTypeText ?? "N/A")")
                if (AppDelegate.UserData.batchQAMode == true) {
                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "BatchScanCapture") as! BatchScanViewController
                    vc.processItem = slp
                    vc.indexPath = Int(sender.tag)
                    self.navigationController?.present(vc, animated: true, completion: nil)
                } else {
                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "ScanCapture") as! ScanCaptureViewController
                    vc.processItem = slp
                    vc.indexPath = Int(sender.tag)
                    self.navigationController?.present(vc, animated: true, completion: nil)
                }
            
            // Handle an interactive User Input Amount
            case AppDelegate.ScanMode.Batch.rawValue:
                print("Type: \(slp.ScanTypeText ?? "N/A")")
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "UserDataCapture") as! DataCaptureViewController
                vc.processItem = slp
                vc.indexPath = Int(sender.tag)
                //self.navigationController?.present(vc, animated: true, completion: nil)
                // Attach to NAV for presentation
                let nav : UINavigationController = UINavigationController(rootViewController: vc)
                nav.definesPresentationContext = true
                nav.modalPresentationStyle = .formSheet
                self.navigationController?.present(nav, animated: true, completion: nil)
            
            // Handle a Confirmatory Signature
            case AppDelegate.ScanMode.Confirmation.rawValue:
                print("Type: \(slp.ScanTypeText ?? "N/A")")
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "UserConfirmation") as! UserConfirmViewController
                vc.processItem = slp
                vc.indexPath = Int(sender.tag)
                //self.navigationController?.present(vc, animated: true, completion: nil)
                // Attach to NAV for presentation
                let nav : UINavigationController = UINavigationController(rootViewController: vc)
                nav.definesPresentationContext = true
                nav.modalPresentationStyle = .formSheet
                self.navigationController?.present(nav, animated: true, completion: nil)
            
            // It's not been thought of yet!
            default:
                print("Not Valid Case!")
        }
    }

    
    override func numberOfSections(in tableView: UITableView) -> Int {
       return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // tableView.isHidden = processes.count < 1
        if processes.count == 0 {
            tableView.setEmptyView(title: "No authorised sub-processes selected.",
                                   message: "Authorised processes will be shown here.")
        }
        else {
            tableView.restore()
        }

        
        
       return processes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: SLPTableCell = tableView.dequeueReusableCell(withIdentifier: "SLPCell", for: indexPath) as! SLPTableCell
        let slp: ProcessModel = processes[indexPath.row]
        cell.WorkInstsView.isScrollEnabled = false
        cell.WorkInstsView.sizeToFit()
        // Configure the cell...
        cell.ProcessNameLbl?.text = slp.ProcessName
        cell.ProcessStepLbl?.text = slp.StepText
        cell.TTS1Lbl?.text = String("Sign 1: \(slp.TTS_TEAM_1 ?? "")")
        cell.TTS2Lbl?.text = String("Sign 2: \(slp.TTS_TEAM_2 ?? "")")
        cell.TT1SIG?.text = String("Initials: \(slp.TT1SIG ?? "")")
        cell.TT2SIG?.text = String("Initials: \(slp.TT2SIG ?? "")")
        
        cell.WorkInstsView?.text = slp.Instruction
        
        cell.ProcModeLbl?.text = slp.ScanTypeText
        

        switch String((AppDelegate.AppData.currentWOC.Finish)!) {
            case "Lead Free":
                cell.worksInstLbl.text =  String(stringLiteral: "Works Instructions/Special Comments - \(AppDelegate.AppData.currentWOC.Finish!)")
                cell.worksInstLbl.backgroundColor = UIColor.init(red: 52/255, green: 199/255, blue: 89/255, alpha: 0.6)
            case "Tin Lead":
                cell.worksInstLbl.text =  String(stringLiteral: "Works Instructions/Special Comments - \(AppDelegate.AppData.currentWOC.Finish!)")
                cell.worksInstLbl.backgroundColor = UIColor.init(red: 255/255, green: 45/255, blue: 85/255, alpha: 0.6)
            case "Lead Free Aqueous":
                cell.worksInstLbl.text =  String(stringLiteral: "Works Instructions/Special Comments - \(AppDelegate.AppData.currentWOC.Finish!)")
                cell.worksInstLbl.backgroundColor = UIColor.init(red: 255/255, green: 204/255, blue: 0/255, alpha: 0.6)
            case "Tin Lead Aqueous":
                cell.worksInstLbl.text =  String(stringLiteral: "Works Instructions/Special Comments - \(AppDelegate.AppData.currentWOC.Finish!)")
                cell.worksInstLbl.backgroundColor = UIColor.init(red: 255/255, green: 149/255, blue: 0/255, alpha: 0.6)
            case "Customer Specific":
                cell.worksInstLbl.text =  String(stringLiteral: "Works Instructions/Special Comments - \(AppDelegate.AppData.currentWOC.Finish!)")
                cell.worksInstLbl.backgroundColor = UIColor(red:255/255.0, green:204/255.0, blue:102/255.0, alpha: 0.6);
            default:
                cell.worksInstLbl.text =  String(stringLiteral: "Works Instructions/Special Comments - \(AppDelegate.AppData.currentWOC.Finish!)")
                cell.worksInstLbl.backgroundColor = .secondarySystemBackground
        }
        
        if (slp.QAAlerts > 0) {
            cell.worksInstLbl.backgroundColor = UIColor.systemRed
            cell.worksInstLbl.text! += " - See QA Alert!"
        }
        
        cell.ProcModeImg.tag = indexPath.row
        cell.ProcModeImg.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        
        // Get current state for process
        let currentState = self.timeStates.filter {
            return ( $0.PTS_State == slp.ProcStatus )
        }.first
        
        switch currentState?.PTS_Description {
        case "Not Started": // Paused
             cell.procStateImg.image = UIImage(systemName: "timelapse")
             cell.procStateImg.tintColor = .systemGray
        case "Active": // Started so option to PAUSE or COMPLETE
             cell.procStateImg.image = UIImage(systemName: "play.circle.fill")
             cell.procStateImg.tintColor = .systemGreen
        case "Paused":
            cell.procStateImg.image = UIImage(systemName: "pause.circle.fill")
            cell.procStateImg.tintColor = .systemOrange
        case "Complete":
            cell.procStateImg.image = UIImage(systemName: "stop.circle.fill")
            cell.procStateImg.tintColor = .systemRed
        default: // Error!
            print("Error - Option not Configured")
        }
        
        
        switch slp.ScanTypeIndex {
            case AppDelegate.ScanMode.Process.rawValue :
                cell.ProcModeImg?.setBackgroundImage(UIImage(systemName: "barcode.viewfinder"), for: .normal)
                cell.ProcModeImg.isEnabled = (slp.Authorised_1 == 1) ? true : false
                cell.ProcModeImg.tintColor = (slp.Authorised_1 == 1) ? .systemBlue : .systemRed
            case AppDelegate.ScanMode.Inspection.rawValue :
                cell.ProcModeImg?.setBackgroundImage(UIImage(systemName: "checkmark.square"), for: .normal)
                cell.ProcModeImg.isEnabled = (slp.Authorised_1 == 1) ? true : false
                cell.ProcModeImg.tintColor = (slp.Authorised_1 == 1) ? .systemBlue : .systemRed
            case AppDelegate.ScanMode.Confirmation.rawValue :
                cell.ProcModeImg?.setBackgroundImage(UIImage(systemName: "signature"), for: .normal)
            cell.ProcModeImg.isEnabled = (slp.Authorised_1 == 1 || slp.Authorised_2 == 1) ? true : false
                cell.ProcModeImg.tintColor = (slp.Authorised_1 == 1 || slp.Authorised_2 == 1) ? .systemBlue : .systemRed
            case AppDelegate.ScanMode.Batch.rawValue :
                cell.ProcModeImg?.setBackgroundImage(UIImage(systemName: "list.number"), for: .normal)
                cell.ProcModeImg.isEnabled = (slp.Authorised_1 == 1) ? true : false
                cell.ProcModeImg.tintColor = (slp.Authorised_1 == 1) ? .systemBlue : .systemRed
            // Added Sample
            case AppDelegate.ScanMode.Sample.rawValue :
                cell.ProcModeImg?.setBackgroundImage(UIImage(systemName: "qrcode.viewfinder"), for: .normal)
                cell.ProcModeImg.isEnabled = (slp.Authorised_1 == 1) ? true : false
                cell.ProcModeImg.tintColor = (slp.Authorised_1 == 1) ? .systemBlue : .systemRed
            // Added Panel
            case AppDelegate.ScanMode.Panel.rawValue :
                cell.ProcModeImg?.setBackgroundImage(UIImage(systemName: "square.3.layers.3d.down.right"), for: .normal)
                cell.ProcModeImg.isEnabled = (slp.Authorised_1 == 1) ? true : false
                cell.ProcModeImg.tintColor = (slp.Authorised_1 == 1) ? .systemBlue : .systemRed
            default:
                cell.ProcModeImg?.setBackgroundImage(UIImage(systemName: "questionmark.square"), for: .normal)
                cell.ProcModeImg.isEnabled = (slp.Authorised_1 == 1) ? true : false
                cell.ProcModeImg.tintColor = (slp.Authorised_1 == 1) ? .systemBlue : .systemRed
        }
        
        // Override button enabled state if QA alert ..
        cell.ProcModeImg.isEnabled = ((slp.QAAlerts <= 0) && (cell.ProcModeImg.isEnabled))
        
        cell.drgImg.image = (slp.Drawings > 0) ? UIImage(systemName: "hand.draw.fill")?.withTintColor(.systemBlue, renderingMode: .alwaysOriginal) : UIImage(systemName: "hand.draw")!.withTintColor(UIColor.lightGray, renderingMode: .alwaysOriginal)
        
        cell.tts1StatusImg.image = (slp.TS1 > 1) ? UIImage(systemName: "checkmark.circle.fill")?.withTintColor(UIColor.systemGreen, renderingMode: .alwaysOriginal) : UIImage(systemName: "xmark.circle.fill")!.withTintColor(UIColor.red, renderingMode: .alwaysOriginal)
        
        cell.tts2StatusImg.image = (slp.TS2 > 1) ? UIImage(systemName: "checkmark.circle.fill")?.withTintColor(UIColor.systemGreen, renderingMode: .alwaysOriginal) : UIImage(systemName: "xmark.circle.fill")!.withTintColor(UIColor.red, renderingMode: .alwaysOriginal)
        
        cell.auth1Img.image = (slp.Authorised_1 == 1) ? UIImage(systemName: "person.circle")?.withTintColor(UIColor.systemGreen, renderingMode: .alwaysOriginal) : UIImage(systemName: "person.crop.circle.badge.xmark")!.withTintColor(UIColor.red, renderingMode: .alwaysOriginal)
        
        cell.auth2Img.image = (slp.Authorised_2 == 1) ? UIImage(systemName: "person.circle")?.withTintColor(UIColor.systemGreen, renderingMode: .alwaysOriginal) : UIImage(systemName: "person.crop.circle.badge.xmark")!.withTintColor(UIColor.red, renderingMode: .alwaysOriginal)
        
        cell.expectedLbl.text = String(slp.ExpectedScans)
        //cell.actualLbl.text = String((slp.ScanTypeIndex == 1 || slp.ScanTypeIndex == 2) ? slp.ScanCount : slp.Signatures)
        
        switch slp.ScanTypeIndex {
        case 0: cell.actualLbl.text = "-"
            // Added Sample = 5
            // Added Panel = 6
        case 1, 2, 5, 6:
            cell.actualLbl.text = String(slp.ScanCount)
        case 3:
            cell.actualLbl.text = String(slp.EnteredQuantity)
        case 4:
            cell.actualLbl.text = String(slp.Signatures)
        default:
            cell.actualLbl.text = "Error"
        }
        
        cell.actualLbl.textColor = (slp.ProgressPercent == 100.00 ? .systemGreen : .systemRed)
        UIView.animate(withDuration: 2.0) {
            cell.progressBar.setProgress(Float(truncating: NSNumber(value: Double(truncating: NSDecimalNumber(decimal: slp.ProgressPercent / 100.00)))), animated: true)
        }
        
        
        var tapGesture = UITapGestureRecognizer(target: self, action: #selector(slpImageTapped))
        tapGesture.name = "Comments"
        cell.viewCommentsImg.addGestureRecognizer(tapGesture)
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(slpImageTapped))
        tapGesture.name = "Message"
        cell.messageImg.addGestureRecognizer(tapGesture)
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(slpImageTapped))
        tapGesture.name = "Reminder"
        cell.reminderImg.addGestureRecognizer(tapGesture)
        
        /*
        if (slp.Drawings > 0) {
            cell.ShowDrawings.isHidden = false
            cell.ShowDrawings.setTitle(String(" - View Drawings(\(slp.Drawings))"), for: .normal)
            let norcottGesture = NorcottTapGestureRecognizer(target: self, action: #selector(showDrawingsTapped))
            norcottGesture.name = "Drawings"
            norcottGesture.myCustomValue = slp
            cell.ShowDrawings.addGestureRecognizer(norcottGesture)
        } else {
            cell.ShowDrawings.isHidden = true
        }
 */
        
        if (slp.Drawings > 0) {
            cell.drgImg.isUserInteractionEnabled = true
            tapGesture = UITapGestureRecognizer(target: self, action: #selector(slpImageTapped))
            tapGesture.name = "Drawings"
            cell.drgImg.addGestureRecognizer(tapGesture)
        } else {
            cell.drgImg.isUserInteractionEnabled = false
        }



        
        /*
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(slpImageTapped))
        tapGesture.name = "DataCapture"
        cell.ProcModeImg.addGestureRecognizer(tapGesture)
        */
   
        // let interaction = UIContextMenuInteraction(delegate: self)
        // cell.ProcModeImg.addInteraction(interaction)
         
        
        // cell.contentView.backgroundColor = slp.Authorised > 0 ? AppDelegate.NorcottPaleBlue : UIColor(red:255/255.0, green:0/255.0, blue:0/255.0, alpha:0.8)
       
        
        return cell
    }

    override func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        let item: ProcessModel = processes[indexPath.row]

        return UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { suggestedActions in

            // Create an action for comments
            let comment = UIAction(title: "Comments", image: UIImage(systemName: "list.dash")) { action in
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "CommentsViewController") as! CommentsViewController
                vc.processItem = item
                let nav : UINavigationController = UINavigationController(rootViewController: vc)
                self.navigationController?.present(nav, animated: true, completion: nil)
            }
            
            // Create an action for sending message
            let message = UIAction(title: "Message", image: UIImage(systemName: "message")) { action in
                if let slp: ProcessModel = self.currentSLP {
                    let alertController = UIAlertController(title: "Send Message to Frontend", message: nil, preferredStyle: .alert)
                    
                    let confirmAction = UIAlertAction(title: "Send", style: .default) { (_) in
                        if let txtField = alertController.textFields?.first, let text = txtField.text {
                          // operations
                          print("Message Added: " + text)
                         self.sendEmail(slp, "ERC Message: ", "<p>" + text + "</p>")
                        }
                    }
                    
                    let confirmSaveAction = UIAlertAction(title: "Send & Save", style: .default) { (_) in
                        if let txtField = alertController.textFields?.first, let text = txtField.text {
                          // operations
                          print("Message Added & Saved: " + text)
                         self.sendSaveEmail(slp , "ERC Message: ", text)
                        }
                    }
                    
                    
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
                    alertController.addTextField { (textField) in
                        textField.placeholder = "Message Body"
                    }

                    alertController.addAction(confirmAction)
                    alertController.addAction(confirmSaveAction)
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                }
            }
            
            // Create an action for setting reminder
            let reminder = UIAction(title: "Reminder", image: UIImage(systemName: "calendar.badge.plus")) { action in
                self.setReminder()
            }

            // Create an action for shortage
            let shortage = UIAction(title: "Shortages", image: UIImage(systemName: "minus.rectangle")) { action in
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "Shortages") as! ShortagesViewController
                // load for individual process
                vc.processItem = item
                vc.canEdit = true
                let nav : UINavigationController = UINavigationController(rootViewController: vc)
                self.navigationController?.present(nav, animated: true, completion: nil)
            }
            
            // Create an action for editing a works instruction
            let editInstruction = UIAction(title: "Edit Instruction", image: UIImage(systemName: "pencil")) { action in
                // let vc = self.storyboard!.instantiateViewController(withIdentifier: "editInstruction") as! EditInstController
                // load for individual process
                // vc.processItem = item
                // vc.canEdit = true
                
                let vc  = self.storyboard!.instantiateViewController(identifier: "editInstruction", creator: { coder in
                    EditInstController(coder: coder, process: item)
                 })

                
                let nav : UINavigationController = UINavigationController(rootViewController: vc)
                nav.modalPresentationStyle = UIModalPresentationStyle.formSheet
                self.navigationController?.present(nav, animated: true, completion: nil)
            }
            
            
            var items = [UIMenuElement]()
            if let _: ProcessModel = self.currentSLP {
                items.append(shortage)
                items.append(comment)
                items.append(message)
                items.append(reminder)
                // If the current user is authorised and TTS = Process Eng then add menu option
                if (item.Authorised_1 == 1 && item.TTS_TEAM_1 == "PROCESS ENG") {
                    items.append(editInstruction)
                }
            }
        
        
            return UIMenu(title: "WOC Options: \(AppDelegate.AppData.currentWOC.BatchNumber)", children: items)
        }
    }
    
    
    @objc func slpImageTapped(recognizer: UITapGestureRecognizer)
    {
        print("Tapped on Image \(recognizer.name!)")
        // Full list of comments for this process
        switch recognizer.name {
        
        case "Drawings":
            
            // Get current state from data source
            if let slp: ProcessModel = self.currentSLP {
                self.performSegue(withIdentifier: "DrawingsCollection", sender: slp)
            }
            
        
        
        case "Comments":
            let vc = storyboard!.instantiateViewController(withIdentifier: "CommentsViewController") as! CommentsViewController
            if let slp: ProcessModel = self.currentSLP {
                vc.processItem = slp
                let nav : UINavigationController = UINavigationController(rootViewController: vc)
                    self.navigationController?.present(nav, animated: true, completion: nil)
            }

        case "Message":
            if let slp: ProcessModel = self.currentSLP {
                let alertController = UIAlertController(title: "Send Message to Frontend", message: nil, preferredStyle: .alert)
                
                let confirmAction = UIAlertAction(title: "Send", style: .default) { (_) in
                    if let txtField = alertController.textFields?.first, let text = txtField.text {
                      // operations
                      print("Message Added: " + text)
                     self.sendEmail(slp, "ERC Message: ", "<p>" + text + "</p>")
                    }
                }
                
                let confirmSaveAction = UIAlertAction(title: "Send & Save", style: .default) { (_) in
                    if let txtField = alertController.textFields?.first, let text = txtField.text {
                      // operations
                      print("Message Added & Saved: " + text)
                     self.sendSaveEmail(slp , "ERC Message: ", text)
                    }
                }
                
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
                alertController.addTextField { (textField) in
                    textField.placeholder = "Message Body"
                }

                alertController.addAction(confirmAction)
                alertController.addAction(confirmSaveAction)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
                
            }
            
        case "Reminder":
            self.setReminder()
                
        default: break
            
        }
        
    }
    
    
    func showGNCAlert(_ title: String, _ alert: String) {
        let alert = UIAlertController(title: title, message: alert, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Acceptance of GNC(s)", style: .default, handler: { _ in
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "GNCViewController") as! GNCViewController
            let woc: WOCModel = AppDelegate.AppData.currentWOC
            vc.wocItem = woc
            let nav : UINavigationController = UINavigationController(rootViewController: vc)
            self.navigationController?.present(nav, animated: true, completion: nil)
           }))
        present(alert, animated: true)
    }
    
    
    @objc func setReminder() {
          let alertController = UIAlertController(title: "Add New Reminder", message: "Reminder will be set for 8:30am tomorrow", preferredStyle: .alert)
          let confirmAction = UIAlertAction(title: "Add", style: .default) { (_) in
              if let txtField = alertController.textFields?.first, let text = txtField.text {
                // operations
                print("Reminder Added: " + text)
                self.scheduleLocal("Reminder", text, (self.currentSLP?.StepText)!)
              }
          }
          let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
          alertController.addTextField { (textField) in
              textField.placeholder = "Reminder"
          }
          confirmAction.setValue(UIImage(systemName:  "calendar.badge.plus")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
          cancelAction.setValue(UIImage(systemName: "escape")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
          alertController.addAction(confirmAction)
          alertController.addAction(cancelAction)
          self.present(alertController, animated: true, completion: nil)
      }
    
    
    @objc func scheduleLocal(_ title: String, _ body: String, _ category: String) {
        let center = UNUserNotificationCenter.current()

        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.categoryIdentifier = category
        content.userInfo = ["customData": AppDelegate.UserData.userID]
        content.sound = UNNotificationSound.default
        
        var dateComponents = DateComponents()
        dateComponents.hour = 15
        dateComponents.minute = 35
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        // let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)

        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        center.add(request)
        
       // center.removeAllPendingNotificationRequests()
    }
    
    func sendEmail(_ proc: ProcessModel, _ subject: String, _ body: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            // Live = front.end@norcott.local
            mail.setToRecipients(["front.end@norcott.local"])
            mail.setSubject(subject + "BN: " + String(proc.BatchNumber) + "/" + "JN: " + String(proc.JobNumber) + " - " + " Pin: " + String(proc.PinNumber!) + " - " + " Process: " + String(proc.StepText!))
            mail.setMessageBody(body + "<p><b>" + proc.ProcessName! + " - " + proc.StepText! + "</b></p>", isHTML: true)
            present(mail, animated: true)
        } else {
            self.showAlert("Send Message Error", "Check device is configured to send email")
        }
    }
    
    func sendSaveEmail(_ proc: ProcessModel, _ subject: String, _ body: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            // Live = front.end@norcott.local
            mail.setToRecipients(["front.end@norcott.local"])
            mail.setSubject(subject + "BN: " + String(proc.BatchNumber) + "/" + "JN: " + String(proc.JobNumber) + " - " + " Pin: " + String(proc.PinNumber!) + " - " + " Process: " + String(proc.StepText!))
            mail.setMessageBody("<p>" + body + "</p>" + "<p><b>" + proc.ProcessName! + " - " + proc.StepText! + "</b></p>", isHTML: true)
            present(mail, animated: true)
            self.PostCommentAPI(proc.RouteCardHdrIndex, proc.UniqueStepID, body)
            
        } else {
            self.showAlert("Send Message Error", "Check device is configured to send email")
        }
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let indexPath = tableView.indexPathForSelectedRow {
           let slp: ProcessModel = processes[indexPath.row]
           currentSLP = processes[indexPath.row]
           AppDelegate.AppData.SLPStepID = slp.UniqueStepID
        }
        let cell: SLPTableCell = tableView.dequeueReusableCell(withIdentifier: "SLPCell", for: indexPath) as! SLPTableCell
        //let cell = tableView.cellForRow(at: indexPath)
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.init(red: 0.5, green: 0.6, blue: 0.4, alpha: 0.2)
        cell.selectedBackgroundView = backgroundView
        
        if (currentSLP!.QAAlerts > 0) {
            let vc = self.newQAAlertProcessController(identifier: "QAAlertProcessController", slp: currentSLP!)
            // Attach to NAV for presentation
            let nav : UINavigationController = UINavigationController(rootViewController: vc)
            nav.definesPresentationContext = true
            nav.modalPresentationStyle = .formSheet
            self.navigationController?.present(nav, animated: true, completion: nil)
        }
        
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        if (tableView .cellForRow(at: indexPath)?.reuseIdentifier == "SLPCell") {
            return true
        }
        return false
    }
    

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        let vc = segue.destination as! DrgsCollectionViewController
        vc.title = "Drawings Collection"
        vc.processItem = sender as? ProcessModel

    }


}
