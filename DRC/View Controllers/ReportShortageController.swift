//
//  ReportShortageController.swift
//  DRC
//
//  Created by Edward Reilly on 23/03/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

class ReportShortageController: UIViewController {
    @IBOutlet var pinNumberTxt: UITextField!
    @IBOutlet var designatorsTxt: UITextField!
    @IBOutlet var processTxt: UITextField!
    @IBOutlet var detailsTxt: UITextView!
    @IBOutlet var qtyTxt: UITextField!
    @IBOutlet var qtyStepper: UIStepper!
    @IBOutlet var addBtn: UIBarButtonItem!
    @IBOutlet var cancelBtn: UIBarButtonItem!
    
    var processItem: ProcessModel? {
        didSet {
            // Update the view.
        }
    }
    

    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.configureView()
    }
    
    @IBAction func stepperValueChanged(_ sender: UIStepper) {
        qtyTxt.text = Int(sender.value).description
    }
    
    @IBAction func dismissView(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func reportButton(_ sender: UIButton) {
        (sender as UIButton).isEnabled = false
        let shortage = ShortageModel(CSD_IX: 0, CSD_RCH_IX: processItem!.RouteCardHdrIndex, CSD_Pin: pinNumberTxt.text, CSD_Detail: self.detailsTxt.text ?? "", CSD_Qty: Int(self.qtyStepper!.value), CSD_Location: designatorsTxt.text ?? "", CSD_Confirmed_Fitted: "", CSD_PRS_IX: processItem!.ProcessID, CSD_Parent_PRS_IX: processItem!.ParentIndex, CSD_PRC_IX: processItem!.UniqueStepID, CSD_UN_IX_Reported: AppDelegate.UserData.userID, CSD_UN_Initials_Reported: nil, CSD_UN_IX_Fitted_By: 0, CSD_Fitted: nil, CSD_Process: nil)
        self.PostReportShortageAPI(shortage)
    }
    
    func configureView() {
        // Set up font
        let font = UIFont.systemFont(ofSize: 24)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font : font]
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.darkGray]
        UINavigationBar.appearance().titleTextAttributes = textAttributes
        // Update the user interface for the detail item.
        // This is a process step level comment
        if let detail = processItem {
            self.title = "Report Shortage for Step: \(detail.StepText!)"
            self.processTxt.text = detail.StepText
            
        }
    }
    
    // MARK: - Web API Calls
    
    func PostReportShortageAPI(_ shortage: ShortageModel) {
        // http://192.168.33.9:6002/api/routecardservice/ReportShortages
        let semaphore = DispatchSemaphore(value: 1)
        let params = ["CSD_RCH_IX": shortage.CSD_RCH_IX,
                      "CSD_Pin": shortage.CSD_Pin ?? "",
                      "CSD_Detail": shortage.CSD_Detail ?? "",
                      "CSD_Qty": shortage.CSD_Qty,
                      "CSD_Location": shortage.CSD_Location ?? "",
                      "CSD_PRS_IX": shortage.CSD_PRS_IX,
                      "CSD_Parent_PRS_IX": shortage.CSD_Parent_PRS_IX,
                      "CSD_PRC_IX": shortage.CSD_PRC_IX,
                      "CSD_UN_IX_Reported": shortage.CSD_UN_IX_Reported
                     ] as [String : Any]
              //create the url with URL
              guard let url = URL(string: "http://" + AppDelegate.UserData.serverIP + ":" + AppDelegate.UserData.portNumber + "/api/routecardservice/ReportShortages") else {
                    print("Invalid URL!")
                    self.addBtn.isEnabled = true
                    return
              }
              //now create the URLRequest object using the url object
              var request = URLRequest(url: url)
             
              request.addValue("application/json", forHTTPHeaderField: "Content-Type")
              request.addValue("application/json", forHTTPHeaderField: "Accept")
              request.httpMethod = "POST"
              request.timeoutInterval = 20
              
              do {
               request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .withoutEscapingSlashes) // pass dictionary to nsdata object and set it as request body
              } catch let error {
                  print(error.localizedDescription)
                  self.addBtn.isEnabled = true
              }

               URLSession.shared.dataTask(with: request) { data, response, error in
                       if let error = error {
                           print("Something went wrong: \(error)")
                           self.addBtn.isEnabled = true
                       }
                guard let data = data else {
                    print(String(describing: error))
                    return
                }
                semaphore.signal()
                   if let decodedResponse = try? JSONDecoder().decode(POSTStatus.self, from: data) {
                       DispatchQueue.main.async {
                           if (decodedResponse.StatusCode == 1) {
                               print("Success: \(decodedResponse.StatusMessage)")
                               self.addBtn.isEnabled = true
                               self.dismiss(animated: true, completion: nil)
                           }
                       }
                   }
                }
                .resume()
                semaphore.wait()
          }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.post(name: Notification.Name("refreshShortages"), object: false)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
