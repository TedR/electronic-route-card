//
//  ShortagesViewController.swift
//  DRC
//
//  Created by Edward Reilly on 17/03/2020.
//  Copyright © 2020 Norcott. All rights reserved.
//

import UIKit

class ShortagesViewController: UITableViewController, UNUserNotificationCenterDelegate {
     
    // Flat data array
    var shortages = [ShortageModel]()
    // Grouped dictionary of data
    var dataSource = [String : [ShortageModel]]()
    // Keys of grouped data
    var uniqueKeys = NSOrderedSet()

    // MARK: - Lifecycle
    
    @objc func configureView() {
        // Update the user interface for the detail item.
        // This is a WOC Batch level comment - i.e. all associated with the hdrIX
        if let detail = wocItem {
            DispatchQueue.main.async {
                self.navigationItem.title = "Shortages for batch: \(detail.BatchNumber)"
                let count = self.shortages.filter { $0.CSD_UN_IX_Fitted_By == 0 }.count
                self.tabBarItem.badgeValue = String(count * -1)
            }
            self.GetShortagesAPI(detail.RouteCardHdrIndex, -1)
        }
        if let detail = processItem {
            DispatchQueue.main.async {
                self.navigationItem.title = "Shortages for process: \(detail.ProcessName!) - \(detail.StepText ?? "N/A")"
                let count = self.shortages.filter { $0.CSD_UN_IX_Fitted_By == 0 }.count
                self.tabBarItem.badgeValue = String(count * -1)
            }
            self.GetShortagesAPI(detail.RouteCardHdrIndex, detail.UniqueStepID)
        }
        
    }
    
    var wocItem: WOCModel? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    
    var processItem: ProcessModel? {
        didSet {
           // Update the view.
           configureView()
        }
    }
    
    var canEdit: Bool? {
        didSet {
        }
    }
    
   // This is receiver object for message sent from FitShortageController
   @objc func methodOfReceivedNotification(notification: Notification) {
      print("Message Received : \(notification.name)")
      self.configureView()
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        // Remove the observer
        NotificationCenter.default.removeObserver(self)
        print("Notification Removed")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Add the observer
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("refreshShortages"), object: nil)
        
        let font = UIFont.systemFont(ofSize: 24)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font : font]
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.darkGray]
        UINavigationBar.appearance().titleTextAttributes = textAttributes
       

        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
        let exit = UIBarButtonItem(title: "Exit", style: .done, target: self, action: #selector(dismissAlertView))

        exit.tintColor = .red
        self.navigationItem.rightBarButtonItems = [exit]
        
        if (canEdit == true) {
            let add = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(reportShortage))
            add.tintColor = .systemGreen
            self.navigationItem.leftBarButtonItems = [add]
        }
        
        //self.tableView.backgroundColor = UIColor.clear
        
        // Register my custom cell
        self.tableView.register(UINib(nibName: "ShortageTableCell", bundle: nil), forCellReuseIdentifier: "ShortageCell")
        
        let headerNib = UINib.init(nibName: "ShortageHeader", bundle: Bundle.main)
        self.tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "ShortageHeader")
        
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = UITableView.automaticDimension

        /*
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        self.tableView.backgroundView = blurEffectView
        self.tableView.separatorEffect = UIVibrancyEffect(blurEffect: blurEffect)
        */
        
    }
    
    @objc func dismissAlertView() {
        self.dismiss(animated: true, completion: nil)
    }
    

    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {


        // This will hold array of available actions once determined
        var actions = [UIContextualAction]()
        if (canEdit ?? true) {
        // Get current state from data source
        // TODO: - This should be group!
            
            var shortage: ShortageModel
        
            let sect = self.uniqueKeys.object(at: indexPath.section)
            let group: [ShortageModel] = self.shortages.filter {
                return ($0.CSD_Process ?? "") == sect as! String
            }
            shortage = group[indexPath.row]
            
            
            // let shortage = shortages[indexPath.row]
            print(shortage.CSD_Pin ?? "N/A")
            let actionUpdate = UIContextualAction(style: .normal, title: String("Fit"),
                 handler: { (actionUpdate, view, completionHandler) in
                   // Update data source when user taps action
                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "UpdateShortage") as! FitShortageController
                    vc.processItem = self.processItem
                    vc.shortageItem = shortage
                    vc.definesPresentationContext = true
                    vc.modalPresentationStyle = .formSheet
                    // Attach to NAV for presentation
                    let nav : UINavigationController = UINavigationController(rootViewController: vc)
                    nav.definesPresentationContext = true
                    nav.modalPresentationStyle = .formSheet
                    self.navigationController?.present(nav, animated: true, completion: nil)

                    completionHandler(true)
               })
               actionUpdate.image = UIImage(systemName: "wrench")
               actionUpdate.backgroundColor = .systemGreen


            
            let actionAdd = UIContextualAction(style: .normal, title: String("Report"),
                  handler: { (actionAdd, view, completionHandler) in
                  // Update data source when user taps action
                  self.reportShortage()
                  completionHandler(true)
                })
                actionAdd.image = UIImage(systemName: "plus.circle")
                actionAdd.backgroundColor = .systemRed
            
            actions.append(actionUpdate)
            actions.append(actionAdd)
           
        }
            
        let configuration = UISwipeActionsConfiguration(actions: actions)
        
           
           
        return configuration
    }
    
    @objc func reportShortage() {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "ReportShortage") as! ReportShortageController
        vc.processItem = self.processItem
        vc.definesPresentationContext = true
        vc.modalPresentationStyle = .formSheet
        // Attach to NAV for presentation
        let nav : UINavigationController = UINavigationController(rootViewController: vc)
        nav.definesPresentationContext = true
        nav.modalPresentationStyle = .formSheet
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    

    // MARK: - Table WEB API Calls
    
    func GetShortagesAPI(_ hdrIX: Int, _ prcIX: Int) {
        // http://192.168.33.9:6002/api/routecardservice/getShortages/1870
        // GetShortages/{hdrIDX}/{prcIX}
        
        WebRequest<ShortageModel>.get(self, service: "GetShortages", params:[String(hdrIX), String(prcIX)], success: { result in
                DispatchQueue.main.async {
                     self.dataSource.removeAll()
                     // Flat list of data
                      self.shortages = result
                      // Create keys
                      self.uniqueKeys = NSMutableOrderedSet(array: (self.shortages.map { $0.CSD_Process ?? "" }))
                      // For each key in ordered key set, create a section list
                      for key in self.uniqueKeys.array {
                          let group: [ShortageModel] = self.shortages.filter {
                              return ($0.CSD_Process ?? "") == key as! String
                          }
                          print(group.count)
                          // Add section to datasource
                          self.dataSource.updateValue(group, forKey: key as! String)
                      }
                     if self.uniqueKeys.count > 0 {
                         let count = self.shortages.filter { $0.CSD_UN_IX_Fitted_By == 0 }.count
                         self.tabBarItem.badgeValue = String(count * -1)
                       }
                     else {
                         self.tabBarItem.badgeValue = nil
                     }
                     self.tableView.reloadData()
                     }
                }
            )
        
     }
    
    
    
    // MARK: - Table view data source
    
    @IBAction func doRefresh(_ sender: UIRefreshControl) {
        let font = UIFont.systemFont(ofSize: 16)
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: UIColor.red,
        ]
        let title = NSAttributedString(string: "Refreshing Shortages ..", attributes: attributes)
        sender.attributedTitle = title
        
        self.configureView()

        sender.endRefreshing()
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont.systemFont(ofSize: 18.0, weight: .ultraLight)
        header.textLabel?.sizeToFit()
        header.textLabel?.numberOfLines = 0
    }
    
    
       override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
           let footerView = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:40))
           footerView.backgroundColor = UIColor.init(red: 52/255, green: 199/255, blue: 89/255, alpha: 0.4)
           let sect = self.uniqueKeys.object(at: section)
           let group: [ShortageModel] = self.shortages.filter {
               return ($0.CSD_Process ?? "") == sect as! String
           }
           let total = group.count
           // Add label for total
           let font: UIFont = UIFont.boldSystemFont(ofSize: 14)
           let lbl = UILabel(frame: CGRect(x: 20, y:0, width: 400, height: 40))
            lbl.text = String("Process: \(sect as! String) =  \(String(total))")
           lbl.font = font
           lbl.shadowColor = .lightGray
           footerView.addSubview(lbl)
           return footerView
       }
    
 
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ShortageHeader")
         
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.uniqueKeys.object(at: section) as? String
    }


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if (self.dataSource.count > 0) {
            // Get the key for this section from ordered set
            let sect = self.uniqueKeys.object(at: section)
            // Get matching rows for section
                let group: [ShortageModel] = self.shortages.filter {
                    return ($0.CSD_Process ?? "") == sect as! String
            }
            return group.count
        }
        return 0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        if dataSource.count == 0 {
            tableView.setEmptyView(title: "No Shortage Details.",
                                   message: "Shortage Details will be shown here.")
        }
        else {
            tableView.restore()
            return self.dataSource.count
        }
        
        return 0
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let shrt = cell as! ShortageTableCell
        if (shrt.fittedByLbl.text == nil) {
            Animations().addPulsations(object: cell.layer, count: 1)
            cell.backgroundColor = .init(displayP3Red: 1.0, green: 0.2, blue: 0.2, alpha: 0.2)
        }
        else {
            cell.backgroundColor = UIColor.systemGray6 // .init(displayP3Red: 0.2, green: 1.0, blue: 0.2, alpha: 0.2)
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "ShortageCell", for: indexPath) as! ShortageTableCell
        var shortage: ShortageModel
    
        let sect = self.uniqueKeys.object(at: indexPath.section)
        let group: [ShortageModel] = self.shortages.filter {
            return ($0.CSD_Process ?? "") == sect as! String
        }
        shortage = group[indexPath.row]
        
        cell.pinNumberLbl.text = shortage.CSD_Pin
        cell.descriptionLbl.text = shortage.CSD_Detail
        cell.qtyLbl.text = String(shortage.CSD_Qty)
        cell.designatorLbl.text = shortage.CSD_Location
        cell.fittedByLbl.text = shortage.CSD_Confirmed_Fitted
        cell.reportedByLbl.text = shortage.CSD_UN_Initials_Reported
        cell.dateFittedLbl.text =  shortage.CSD_Fitted == nil ? "Not Fitted" : ("\(AppDelegate.dateFormatter.string(from: shortage.CSD_Fitted!))")
    
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }



}
